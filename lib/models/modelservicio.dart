
class ServiciosF {
  String id;
  String categoria;
  String ciudad;
  String descripcion;
  bool estado;
  String genero;
  String idusuario;
  String marca;
  String precio;
  String sectores;
  String subcategoria;
  String cancelacion;
  String diasdisponible;
  String horaFinal;
  String horaInicio;
  ServiciosF({
    this.id,
    this.categoria,
    this.ciudad,
    this.descripcion,
    this.estado,
    this.genero,
    this.idusuario,
    this.marca,
    this.precio,
    this.sectores,
    this.cancelacion,
    this.diasdisponible,
    this.horaFinal,
    this.horaInicio,
    this.subcategoria, 
    });
}