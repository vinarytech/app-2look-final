import 'dart:io';

class Producto {
  String ubicacion;
  File imagen;
  String genero;
  String categoria;
  String ciudad;
  String marca;
  String sector;
  String cancelacion;

  Producto(
      {this.cancelacion,
      this.sector,
      this.marca,
      this.ubicacion,
      this.imagen,
      this.genero,
      this.categoria,
      this.ciudad});
}
