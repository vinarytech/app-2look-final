import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/providernotification.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Intro/registro.dart';
import 'package:tl_app/src/Clientes/Perfil/notificaciones.dart';
import 'package:tl_app/widgets/buttongray.dart';
// import 'package:tl_app/src/Notificaciones/notificaciones.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/src/Clientes/Intro/splash.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/size_config.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  initializeDateFormatting('es');
  await Firebase.initializeApp();
  final pref = new PreferenciasUsuario();
  final usuarioProvider = new UsuarioServices();
  await pref.initPrefs();
  if (pref.id != null) {
    usuarioProvider.obtenerUsuarioId(pref.id);
  }
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  runApp(MyApp());
  position = await Geolocator()
      .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
}

Position position;

class MyApp extends StatefulWidget {
  const MyApp({
    Key key,
  }) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  ServiceApi apiData = new ServiceApi();

  @override
  void initState() {
    super.initState();
    if (prefs.id != null) {
      FirebaseFirestore.instance
          .collection("Paquetes")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
        if (value.docs.first.data()['prueba'].toString() != '') {
          if (DateFormat('dd/MM/yyyy')
              .parse(value.docs.first.data()['prueba'].toString())
              .isBefore(DateTime.now())) {
            FirebaseFirestore.instance
                .collection("Paquetes")
                .doc(value.docs.first.id)
                .update({"prueba": "", "estadoPublicar": false});
          }
          print('aqui0');
          setState(() {});
        }
      });

      FirebaseFirestore.instance
          .collection("Paquetes")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
        if (DateFormat('dd/MM/yyyy')
            .parse(value.docs.first.data()['paquete1'].toString())
            .isBefore(DateTime.now())) {
          if (value.docs.first.data()['suscripcion'].toString() == "true") {
            pagosRecurrentes(value.docs.first.data()['token'],
                value.docs.first.data()['total'].toString(), "paquete1");
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": true});
              });
            });
          } else {
            FirebaseFirestore.instance
                .collection("Paquetes")
                .doc(value.docs.first.id)
                .update({"paquete1": "", "estadoPublicar": false});
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": false});
              });
            });
          }
        }
        print('aqui1');
        setState(() {});
      });

      FirebaseFirestore.instance
          .collection("Paquetes")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
        if (DateFormat('dd/MM/yyyy')
            .parse(value.docs.first.data()['paquete2'].toString())
            .isBefore(DateTime.now())) {
          if (value.docs.first.data()['suscripcion'].toString() == "true") {
            pagosRecurrentes(value.docs.first.data()['token'],
                value.docs.first.data()['total'].toString(), "paquete2");
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": true});
              });
            });
          } else {
            FirebaseFirestore.instance
                .collection("Paquetes")
                .doc(value.docs.first.id)
                .update({"paquete2": "", "estadoPublicar": false});
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": false});
              });
            });
          }
        }
        print('aqui2');
        setState(() {});
      });

      FirebaseFirestore.instance
          .collection("Paquetes")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
        if (DateFormat('dd/MM/yyyy')
            .parse(value.docs.first.data()['paquete3'].toString())
            .isBefore(DateTime.now())) {
          if (value.docs.first.data()['suscripcion'].toString() == "true") {
            pagosRecurrentes(value.docs.first.data()['token'],
                value.docs.first.data()['total'].toString(), "paquete3");
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": true});
              });
            });
          } else {
            FirebaseFirestore.instance
                .collection("Paquetes")
                .doc(value.docs.first.id)
                .update({"paquete3": "", "estadoPublicar": false});
            FirebaseFirestore.instance
                .collection("Servicios")
                .where("id_usuario", isEqualTo: prefs.id)
                .get()
                .then((value) {
              value.docs.forEach((element) {
                FirebaseFirestore.instance
                    .collection("Servicios")
                    .doc(element.id)
                    .update({"estado": false});
              });
            });
          }
        }
        print('aqui3');
        setState(() {});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final pushProvider = new PushNotificationsProvider();
    pushProvider.initNotificaciones(context);
    // setState(() {
    pushProvider.mensajes.listen((argumento) {
      print("argumento" + argumento.toString());
      notificacion = true;
      notificacionPerfil = true;
      Navigator.push(
          context,
          PageTransition(
              child: Notificaciones(), type: PageTransitionType.fade));
      // print('argumento del push' + notificacion.toString());
    });

    // print('argumento del push');
    // print(argumento);
    // navigatorKey.currentState.pushNamed('notificacion', arguments: argumento);
    // _showBigTextNotification();

    // });
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: darkColor,
          fontFamily: "Poppins",
          textTheme: TextTheme(
            bodyText1: TextStyle(color: textColorClaro),
            bodyText2: TextStyle(color: textColorMedio),
          ),
          visualDensity: VisualDensity.adaptivePlatformDensity),
      initialRoute: 'splash',
      routes: {
        'splash': (_) => Splash(),
        // 'mail': (_) => HomeMail1(),
      },
    );
  }

  void pagosRecurrentes(token, total, paquete) {
    if (prefs.usuario['tipoLogin'] == 'icloud') {
      if (prefs.usuario['correo'].toString().split('@')[1].toString()[0] ==
          'p') {
        _alertaDatosCorreo(
            context,
            'Por favor ingresa un email al que te llegara la confirmación del pago.',
            token,
            double.parse(total),
            paquete);
      } else {
        apiData
            .debitTokenPaquetes(
                token,
                double.parse(total),
                'Pago a 2Look por la compra de paquete numero de ' +
                    total +
                    'dolares')
            .then((value) {
          var decodeData = jsonDecode(value.body);
          if (decodeData[0]['transaction']['status'] == 'success') {
            Navigator.pop(context);
            Navigator.pop(context);
            FirebaseFirestore.instance
                .collection('Paymentez')
                .add(decodeData[0])
                .then((value) => metodopagartarjeta(
                    value.id, token, double.parse(total), paquete));
          } else if (decodeData[0]['transaction']['status'] == 'pending') {
            print(decodeData[0].toString());
            Navigator.pop(context);
            Navigator.pop(context);
            _alertaDatosOpt(
                context,
                'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                decodeData[0]['card']['transaction_reference'],
                decodeData[0]['card']['token'],
                total,
                paquete,
                prefs.usuario['correo']);
          } else if (decodeData[0]['transaction']['status'] == 'failure') {
            Navigator.pop(context);
            Navigator.pop(context);
            _alertaFinal(context, decodeData[0]['transaction']['status']);
          }
        }).then((value) {
          // Navigator.pop(context);
          // Navigator.pop(context);
        });
      }
    } else {
      apiData
          .debitTokenPaquetes(
              token,
              double.parse(total),
              'Pago a 2Look por la compra de paquete numero de ' +
                  total +
                  'dolares')
          .then((value) {
        var decodeData = jsonDecode(value.body);
        if (decodeData[0]['transaction']['status'] == 'success') {
          FirebaseFirestore.instance
              .collection('Paymentez')
              .add(decodeData[0])
              .then((value) => metodopagartarjeta(
                  value.id, token, double.parse(total), paquete));
        } else if (decodeData[0]['transaction']['status'] == 'pending') {
          print(decodeData[0].toString());
          Navigator.pop(context);
          Navigator.pop(context);
          _alertaDatosOpt(
              context,
              'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
              decodeData[0]['card']['transaction_reference'],
              decodeData[0]['card']['token'],
              total,
              paquete,
              prefs.usuario['correo']);
        } else if (decodeData[0]['transaction']['status'] == 'failure') {
          _alertaFinal(context, decodeData[0]['transaction']['status']);
        }
      }).then((value) {
        // Navigator.pop(context);
        // Navigator.pop(context);
      });
    }

    // print(snapshot.token);
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaDatosOpt(
      BuildContext context, String text, transaction, token, total, paquete, email) {
    final optCtlr = TextEditingController();
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => WillPopScope(
              onWillPop: () async => false,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                height: getProportionateScreenHeight(30),
                                width: getProportionateScreenHeight(370),
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: TextField(
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    controller: optCtlr,
                                    textAlignVertical: TextAlignVertical.center,
                                    autocorrect: true,
                                    style: DecorationsOnInputs.textoInput(),
                                    decoration: InputDecoration(
                                      hintText: "INGRESE EL CODIGO OTP",
                                      errorStyle: TextStyle(
                                        color: textColorClaro,
                                      ),
                                      focusedErrorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      labelStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white70,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "Poppins",
                                      ),
                                      hintStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white38,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: "Poppins",
                                      ),
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                    ),
                                  ),
                                )),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {
                                  Future(() => loaderDialogNormal(context))
                                      .then((v) {
                                    apiData
                                        .verifyTokenPay(
                                            transaction, optCtlr.text,email)
                                        .then((value) {
                                      Navigator.pop(context);
                                      var value2 = json.decode(value.body)[0];
                                      print("estado" + value2.toString());
                                      if (value2['transaction']['status'] ==
                                          'success') {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        FirebaseFirestore.instance
                                            .collection('Paymentez')
                                            .add(value2)
                                            .then((value) => metodopagartarjeta(
                                                value.id,
                                                token,
                                                double.parse(total),
                                                paquete));
                                      } else if (value2['transaction']
                                              ['status'] ==
                                          'failure') {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        _alertaFinal(context,
                                            value2['transaction']['status']);
                                      }
                                    }).catchError((onError) {
                                      Navigator.pop(context);
                                      _alertaFinal(context, onError);
                                    });
                                  });
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alertaDatosCorreo(
      BuildContext context, String text, token, total, paquete) {
    final emailCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  controller: emailCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                apiData
                                    .debitTokenPaquetesEmail(
                                        token,
                                        double.parse(total),
                                        'Pago a 2Look por la compra de paquete numero de ' +
                                            total +
                                            'dolares',
                                        emailCtlr.text)
                                    .then((value) {
                                  var decodeData = jsonDecode(value.body);
                                  if (decodeData[0]['transaction']['status'] ==
                                      'success') {
                                    FirebaseFirestore.instance
                                        .collection('Paymentez')
                                        .add(decodeData[0])
                                        .then((value) => metodopagartarjeta(
                                            value.id,
                                            token,
                                            double.parse(total),
                                            paquete));
                                  } else if (decodeData[0]['transaction']
                                          ['status'] ==
                                      'pending') {
                                    print(decodeData[0].toString());
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    _alertaFinal(context,
                                        'Por favor intenta con otro tipo de tarjeta');
                                    _alertaDatosOpt(
                                        context,
                                        'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                        decodeData[0]['card']
                                            ['transaction_reference'],
                                        decodeData[0]['card']['token'],
                                        total,
                                        paquete,
                                        emailCtlr.text);
                                  } else if (decodeData[0]['transaction']
                                          ['status'] ==
                                      'failure') {
                                    _alertaFinal(context,
                                        decodeData[0]['transaction']['status']);
                                  }
                                }).then((value) {
                                  // Navigator.pop(context);
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  metodopagartarjeta(idPaimentez, token, total, paquete) {
    FirebaseFirestore.instance
        .collection('Paquetes')
        .where('id_usuario', isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (paquete == "paquete1") {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete1': Jiffy(DateTime.now()).add(days: 7).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 7).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 7).year.toString(),
          'estado': true,
          'suscripcion': true,
          'estadoPublicar': true,
          'token': token,
          'total': total
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_Paymentez': idPaimentez,
          'id_transferencia': '',
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 5,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': false,
        });
      } else if (paquete == "paquete2") {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete2': Jiffy(DateTime.now()).add(days: 30).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).year.toString(),
          'estado': true,
          'suscripcion': true,
          'estadoPublicar': true,
          'token': token,
          'total': total
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_Paymentez': idPaimentez,
          'id_transferencia': '',
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 15,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': false,
        });
      } else if (paquete == "paquete3") {
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'h1ieGiNgm6EfCroysjKH',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': '',
          'id_Paymentez': idPaimentez,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 25,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update(
                {'estado': false, 'estadosoli': false, 'suscripcion': true});
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete3': Jiffy(DateTime.now()).add(days: 30).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).year.toString(),
          'estado': true,
          'suscripcion': true,
          'estadoPublicar': true,
          'token': token,
          'total': total
        });
      }
    });
    // .then((value) => Navigator.pop(context));
  }
}
