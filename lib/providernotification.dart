import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNotificationsProvider{
  final FirebaseMessaging _fcm = FirebaseMessaging();

    final  _mensajeStreamController= StreamController<Map<String, dynamic>>.broadcast();
    Stream<Map<String, dynamic>> get mensajes => _mensajeStreamController.stream;
  StreamSubscription iosSubscription;

initNotificaciones(BuildContext contexto){

   if (Platform.isIOS) {
            iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
                // save the token  OR subscribe to a topic here
            });

            _fcm.requestNotificationPermissions(IosNotificationSettings());
        }
      
        _fcm.configure(
          onMessage: (Map<String, dynamic> message) async {
           // print("onMessage: $message");
            _mensajeStreamController.sink.add(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
           // print("onLaunch: $message");
             _mensajeStreamController.sink.add(message);
           
        },
        onResume: (Map<String, dynamic> message) async {
           // print("onResume: $message");
          _mensajeStreamController.sink.add(message);
        },
      );
}
  dispose(){
    _mensajeStreamController?.close();
  }


}