import 'dart:math';

import 'package:flutter/material.dart';

class RotationAnimation extends StatefulWidget {
  final Widget widgetRotation;

  RotationAnimation({this.widgetRotation});

  @override
  _RotationAnimationState createState() => _RotationAnimationState();
}

class _RotationAnimationState extends State<RotationAnimation>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );

    _curve = CurvedAnimation(parent: _controller, curve: Curves.bounceIn);

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_curve);

    super.initState();

    _controller.forward();

    _animation.addStatusListener((status) async {
      if (status == AnimationStatus.completed) {
        _controller.stop();
        await Future.delayed(const Duration(seconds: 1), () {});
      } else if (status == AnimationStatus.dismissed) _controller.forward();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final animation = Tween(begin: -0.3 * pi, end: 0.0).animate(_controller);
    return AnimatedBuilder(
        animation: animation,
        child: widget.widgetRotation,
        builder: (BuildContext context, Widget child) {
          return Transform.rotate(
            angle: animation.value,
            child: child,
          );
        });
  }
}
