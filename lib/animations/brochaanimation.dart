import 'package:flutter/material.dart';

class BrochaAnimation extends StatefulWidget {
  final Widget widgetBrocha;

  BrochaAnimation({this.widgetBrocha});
  @override
  _BrochaAnimationState createState() => _BrochaAnimationState();
}

class _BrochaAnimationState extends State<BrochaAnimation>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;
  CurvedAnimation _curve;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        duration: Duration(milliseconds: 1900),vsync:  this);
    _controller.forward();
    _curve = CurvedAnimation(parent: _controller, curve: Curves.easeInExpo);
    _animation = Tween(begin: 0.0, end: 1.5).animate(_curve);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: SizeTransition(
      sizeFactor: _animation,
      axis: Axis.vertical,
      axisAlignment: -0.99,
      child: widget.widgetBrocha,
    ));
  }
}
