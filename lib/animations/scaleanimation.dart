import 'package:flutter/material.dart';

class ScaleAnimation extends StatefulWidget {
  final Widget widgetScale;

  ScaleAnimation({this.widgetScale});

  @override
  _ScaleAnimationState createState() => _ScaleAnimationState();
}

class _ScaleAnimationState extends State<ScaleAnimation>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation _animation;
  CurvedAnimation _curve;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
    );

    _curve = CurvedAnimation(parent: _controller, curve: Curves.bounceIn);

    _animation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(_curve);

    super.initState();

    _controller.forward();

    _animation.addStatusListener((status) async {
      if (status == AnimationStatus.completed) {
        _controller.stop();
        await Future.delayed(const Duration(seconds: 1), () {});
      } else if (status == AnimationStatus.dismissed) _controller.forward();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final animation = Tween(begin: 0.0, end: 0.6).animate(_controller);
    return AnimatedBuilder(
        animation: animation,
        child: widget.widgetScale,
        builder: (BuildContext context, Widget child) {
          return Transform.scale(
            scale: animation.value * 2.0,
            child: child,
          );
        });
  }
}
