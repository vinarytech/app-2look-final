import 'package:flutter/material.dart';
import 'dart:ui' as ui;

ui.Image image;

class LineAnimation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LineAnimationState();
}

class _LineAnimationState extends State<LineAnimation> with SingleTickerProviderStateMixin {
  double _progress = 0.0;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    var controller = AnimationController(
        duration: Duration(milliseconds: 1000), vsync: this);

    animation = Tween(begin: 0.0, end: 1.0).animate(controller)
      ..addListener(() {
        setState(() {
          _progress = animation.value;
        });
      });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return CustomPaint(painter: LinePainter(_progress));
  }
}

class LinePainter extends CustomPainter {
  Paint _paint;
  double _progress;

  LinePainter(this._progress) {
    _paint = Paint()
      ..color = Colors.white10
      ..strokeWidth = 40.0;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawLine(
        Offset(size.height + 38, size.width + 38),
        Offset(size.width + 38 - size.width * _progress / 1.5,
            size.height + 38 + size.height * _progress / 1.5),
        _paint);
  }

  @override
  bool shouldRepaint(LinePainter oldDelegate) {
    return oldDelegate._progress != _progress;
  }
}
