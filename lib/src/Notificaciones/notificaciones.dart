import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class Notificaciones extends StatefulWidget {
  @override
  _NotificacionesState createState() => _NotificacionesState();
}

class _NotificacionesState extends State<Notificaciones> {
  String title;
String body;
String id;
  @override
  Widget build(BuildContext context) {
   
     Map arg = ModalRoute.of(context).settings.arguments;
    
    if(arg!=null){
   title = arg['data']['title'];
    body = arg['data']['body'];
    //id = arg['data']['id'];
    }

    return Scaffold(
       appBar: AppBar( 
         title: Text('Notificaciones', ),
          flexibleSpace: Container(
           
          ),
       ),
       backgroundColor: Colors.white,
       body:
        ListView(
         children: <Widget>[

         (title!=null|| body!=null || id!=null)? Padding(
           padding: const EdgeInsets.all(15.0),
           child:  Container(
            margin: EdgeInsets.only(top:2.0),
           //width: double.infinity,
            //height: double.infinity,
            decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
                boxShadow: [
                BoxShadow(
                color: Colors.black12,blurRadius: 20
                              
                )
                ]
            ),
            child: Column(children: <Widget>[
              ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(Icons.notifications_active, color: Colors.amber,)),
                      title:  Text( title,  ),
                        
                      subtitle: Text(body, ) ,
                  ),
            /*  FlatButton(
                color: Colors.green,
                child: Text('Ver Pedidos'),
                onPressed: (){
                    Navigator.pushNamed(context, 'pedidosrecibidos');
                },
                
                )  */

            ],
            )

        )
  

         ):
         Padding(
           padding: EdgeInsets.all(8.0),
           child: Center(child: Text('Nada',)),
         ) 
       
         ],
       )
      
    );
  }
}