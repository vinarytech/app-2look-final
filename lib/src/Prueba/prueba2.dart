import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/filtros.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/importante.dart';
import 'package:tl_app/src/Clientes/Reservas/enProceso.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/cardVertical.dart';
import 'package:tl_app/widgets/carouselhorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';

import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';

import 'package:tl_app/widgets/iconobutton.dart';
import 'package:tl_app/widgets/mensajeCompleto.dart';
import 'package:tl_app/widgets/mensajeResumen.dart';

import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/rangesliderCustomed.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/slidervertical.dart';
import 'package:tl_app/widgets/textlineButton.dart';
import 'package:tl_app/widgets/validation.dart';
import 'package:tl_app/widgets/ventana.dart';
import 'package:tl_app/widgets/carddividida.dart';

class Prueba2 extends StatefulWidget {
  @override
  _Prueba2State createState() => new _Prueba2State();
}

class _Prueba2State extends State<Prueba2> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        //backgroundColor: primaryColor,
        body: SingleChildScrollView(
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width,
                  minHeight: MediaQuery.of(context).size.height,
                ),
                child: IntrinsicHeight(
                    child: SafeArea(
                        child: Stack(children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Mensajes()
                ]))))));
  }
}
