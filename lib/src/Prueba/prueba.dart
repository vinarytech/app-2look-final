import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/importante.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/cardVertical.dart';
import 'package:tl_app/widgets/carouselhorizontal.dart';
import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/iconobutton.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:tl_app/widgets/pago/tarjetasInfo.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/pago/tipopago.dart';
import 'package:tl_app/widgets/pago/total.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/slidervertical.dart';
import 'package:tl_app/widgets/validation.dart';

class Prueba extends StatefulWidget {
  @override
  _PruebaState createState() => new _PruebaState();
}

class _PruebaState extends State<Prueba> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          // Navigator.pushReplacement(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade, child: AlertaCustom()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [Container(), Container(), Container()],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
}
