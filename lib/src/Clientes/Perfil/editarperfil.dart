import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Intro/crearcuenta.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/size_config.dart';

class EditarPerfil extends StatefulWidget {
  EditarPerfil({Key key}) : super(key: key);

  @override
  EeditarPerfilState createState() => EeditarPerfilState();
}

class EeditarPerfilState extends State<EditarPerfil> {
  final correoCtrl = TextEditingController();
  final nombreCtrl = TextEditingController();
  final telefonoCtrl = TextEditingController();

  File _image;
  File _image2;
  final usuarioS = new UsuarioServices();
  final prefs = new PreferenciasUsuario();
  @override
  void initState() {
    super.initState();
    setState(() {
      usuarioS.obtenerUsuarioId(prefs.id);
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SafeArea(
        child: Scaffold(
            body: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 95.0),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                        SizedBox(
                          height: 40,
                        ),
                        (prefs.usuario['tipoLogin'] == 'mail')
                            ? GestureDetector(
                                onTap: () {},
                                child: Container(
                                  height: 100,
                                  width: 100,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(200))),
                                  child: GestureDetector(
                                    onTap: () {
                                      _showPickOptions(context);
                                    },
                                    child: (prefs
                                                        .usuario[
                                                    'fotografia_perfil'] ==
                                                null ||
                                            prefs
                                                        .usuario[
                                                    'fotografia_perfil'] ==
                                                '')
                                        ? ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(200),
                                            child: (_image ==
                                                    null)
                                                ? Image.asset(
                                                    'lib/assets/images/perfil/perfil.png')
                                                : Image.file(_image))
                                        : ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(200),
                                            child: Image.network(prefs
                                                .usuario['fotografia_perfil'])),
                                  ),
                                ))
                            : Container(
                                height: 100,
                                width: 100,
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(200))),
                                child: Image.network(
                                    prefs.usuario['fotografia_perfil'])),
                        SizedBox(
                          height: 20,
                        ),
                        InputIconCustomTextField(
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 10.0,
                          ),
                          isPassword: false,
                          keyboardType: TextInputType.text,
                          label: prefs.usuario['nombre'],
                          placeholder: 'Nombre',
                          textController: nombreCtrl,
                        ),
                        InputIconCustomTextField(
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 10.0,
                          ),
                          isPassword: false,
                          keyboardType: TextInputType.phone,
                          label: (prefs.usuario['celular'] == '')
                              ? 'Teléfono'
                              : prefs.usuario['celular'],
                          placeholder: "Teléfono",
                          textController: telefonoCtrl,
                        ),
                        InputIconCustomTextFieldDisable(
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          isPassword: false,
                          keyboardType: TextInputType.text,
                          label: prefs.usuario['correo'],
                          placeholder: prefs.usuario['correo'],
                          textController: correoCtrl,
                        ),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              // Material(
                              //     type: MaterialType.transparency,
                              //     child: Text("CÉDULA",
                              //         style: TextStyle(
                              //           color: textColorClaro,
                              //           fontSize:
                              //               getProportionateScreenWidth(11),
                              //           fontWeight: FontWeight.normal,
                              //         ))),
                              // SizedBox(
                              //   height: getProportionateScreenHeight(30),
                              // ),
                              // Container(
                              //   child: GestureDetector(
                              //       onTap: () {
                              //         if (prefs.usuario['verif_cedula']
                              //                 .toString() ==
                              //             'true') {
                              //         } else {
                              //           _showPickOptions2(context);
                              //         }
                              //       },
                              //       child: (_image2 == null)
                              //           ? Column(
                              //               children: [
                              //                 (prefs.usuario['cedula']
                              //                             .toString() ==
                              //                         '')
                              //                     ? Camara()
                              //                     : Container(
                              //                         height: 200,
                              //                         width:
                              //                             MediaQuery.of(context)
                              //                                 .size
                              //                                 .width,
                              //                         decoration: BoxDecoration(
                              //                             borderRadius:
                              //                                 BorderRadius.all(
                              //                                     Radius
                              //                                         .circular(
                              //                                             200))),
                              //                         child: Image.network(
                              //                           prefs.usuario['cedula'],
                              //                           fit: BoxFit.fitHeight,
                              //                         )),
                              //                 SizedBox(
                              //                   height: 20,
                              //                 ),
                              //               ],
                              //             )
                              //           : Image.file(_image2)),
                              // ),
                            ],
                          ),
                        ),
                        Divider(color: Colors.white),
                        SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Verificaciones',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: getProportionateScreenHeight(16)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: getProportionateScreenWidth(80),
                              right: getProportionateScreenWidth(80)),
                          child: Column(
                            children: [
                              SizedBox(
                                height: 20,
                              ),
                              // Row(
                              //   mainAxisAlignment:
                              //       MainAxisAlignment.spaceBetween,
                              //   children: [
                              //     Text(
                              //       'Cédula',
                              //       style:
                              //           TextStyle(fontWeight: FontWeight.bold),
                              //     ),
                              //     (prefs.usuario['verif_cedula'] == true)
                              //         ? Icon(
                              //             Icons.check,
                              //             color: Colors.green,
                              //           )
                              //         : Icon(
                              //             Icons.close,
                              //             color: Colors.red,
                              //           )
                              //   ],
                              // ),
                              SizedBox(
                                height: 20,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Correo',
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  (prefs.usuario['verif_correo'] == true)
                                      ? Icon(
                                          Icons.check,
                                          color: Colors.green,
                                        )
                                      : Icon(
                                          Icons.close,
                                          color: Colors.red,
                                        )
                                ],
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ButtonGray(
                          textoBoton: "GUARDAR PERFIL",
                          bFuncion: () {
                            _guardarPerfil();
                          },
                          anchoBoton: getProportionateScreenHeight(350),
                          largoBoton: getProportionateScreenWidth(27),
                          colorTextoBoton: textColorClaro,
                          opacityBoton: 1,
                          sizeTextBoton: getProportionateScreenWidth(11),
                          weightBoton: FontWeight.w400,
                          color1Boton: primaryColor,
                          color2Boton: primaryColor,
                        ),
                      ],
                    )),
                    BotonAtras(),
                  ],
                )),
          ],
        )),
      ),
    );
  }

  void subirfotoportada() async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image2);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    new Future(() => loaderDialogNormal(context))
        .then((v) => FirebaseFirestore.instance
            .collection('Usuarios')
            .doc(prefs.id)
            .update({'cedula': url})
            .then((v) => Navigator.pop(context))
            // .then((v) => Navigator.pop(context))
            .then((v) {
              Navigator.pop(context, usuarioS.obtenerUsuarioId(prefs.id));
              Navigator.pop(context);
              usuarioS.obtenerUsuarioId(prefs.id);
              _alertaProducto(context,
                  "Por favor espera a que la cédula sea verificada, te informaremos cuando seas verificado.");
            }));
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _showPickOptions2(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker2(ImageSource.gallery);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker2(ImageSource.camera);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();
    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          print(_image);
          _cropImage(_image);
        } else {}
      });
    }
  }

  _loadPicker2(ImageSource source) async {
    final picker = ImagePicker();
    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image2 = File(pickedFile.path);
          print(_image);
          _cropImage2(_image2);
        } else {}
      });
    }
  }

  void _cropImage2(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 9.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image2 = cropped;
      });
    } else {
      setState(() {
        print('aqui');
        _image2 = null;
      });
    }
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        print('aqui');
        _image = null;
      });
    }
  }

  void _guardarPerfil() {
    if (_image != null) {
      uploadStatusImage(_image);
    }
    if (_image2 != null) {
      subirfotoportada();
    }
    if (nombreCtrl.text != '' || telefonoCtrl.text != '') {
      savetoDatabase().then((value) => Navigator.pop(context)).then((value) {
        usuarioS.obtenerUsuarioId(prefs.id);
        Navigator.pop(context, usuarioS.obtenerUsuarioId(prefs.id));
      });
    }
    if (_image == null &&
        _image2 == null &&
        nombreCtrl.text == '' &&
        telefonoCtrl.text == '') {
      _alertaProducto(
          context, "Por favor ingresa la información para editar tu perfil");
    }
  }

  void uploadStatusImage(imagen) async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Usuarios');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(imagen);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();

    new Future(() => loaderDialogNormal(context))
        .then((v) => FirebaseFirestore.instance
            .collection('Usuarios')
            .doc(prefs.id)
            .update({'fotografia_perfil': url})
            .then((v) => Navigator.pop(context))
            // .then((v) => Navigator.pop(context))
            .then((v) {
              Navigator.pop(context, usuarioS.obtenerUsuarioId(prefs.id));
              usuarioS.obtenerUsuarioId(prefs.id);
            }));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  // void savetoDatabase() async {
  //   await new Future(() => loaderDialogNormal(context))
  //       .then((v) => FirebaseFirestore.instance
  //           .collection('Usuarios')
  //           .doc(prefs.id)
  //           .update({'nombre': nombreCtrl.text, 'celular': telefonoCtrl.text})
  //           .then((v) => Navigator.pop(context))
  //           // .then((v) => Navigator.pop(context))
  //           .then((v) {
  //             Navigator.pop(context, usuarioS.obtenerUsuarioId(prefs.id));
  //             usuarioS.obtenerUsuarioId(prefs.id);
  //           }));
  // }

  savetoDatabase() async {
    await new Future(() => loaderDialogNormal(context)).then((v) {
      if (nombreCtrl.text != '') {
        FirebaseFirestore.instance
            .collection('Usuarios')
            .doc(prefs.id)
            .update({'nombre': nombreCtrl.text});
      }
      if (telefonoCtrl.text != '') {
        FirebaseFirestore.instance
            .collection('Usuarios')
            .doc(prefs.id)
            .update({'celular': telefonoCtrl.text});
      }
    });
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
