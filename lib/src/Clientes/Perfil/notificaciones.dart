import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/notificacion.dart';
import 'package:tl_app/widgets/size_config.dart';

class Notificaciones extends StatefulWidget {
  Notificaciones({Key key}) : super(key: key);

  @override
  _NotificacionesState createState() => _NotificacionesState();
}

class _NotificacionesState extends State<Notificaciones> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
    @override
    void initState() { 
      super.initState();
      notificacionPerfil = false;
      print('paseaqui');
    }
  @override
  Widget build(BuildContext context) {

    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            height: 50.0,
            backgroundColor: Color(0xfff191B1F),
            color: Color(0xfff191B1F),
            buttonBackgroundColor: Color(0xfffA91D74),
            items: <Widget>[
              TijerasIcon(),
              GuardadoIcon(),
              BuscarIcon(),
              MensajeIcon(),
              PerfilIcon()
            ],
            animationDuration: Duration(milliseconds: 200),
            animationCurve: Curves.bounceInOut,
            index: page,
            onTap: (index) {
              //Handle button tap
              setState(() {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade, child: Home()));
                page = index;
              });
            },
          ),
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("NOTIFICACIONES",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                             StreamBuilder<QuerySnapshot>(
                              stream: FirebaseFirestore.instance
                                  .collection("Notificaciones")
                                  .where("idreceptor", isEqualTo: prefs.id).orderBy('date', descending: true)
                                  .snapshots(),
                              builder:
                                  (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                if (snapshot.hasData) {
                                  return Column(children: getExpenseItemsMio(snapshot));
                                  // return Text(snapshot.data.docs[0].data()['cancelacion'].toString());
                                } else if (snapshot.hasError) {
                                  return Center(child: Text("Ha ocurrido un error"));
                                } else {
                                  return Center(child: CircularProgressIndicator());
                                }
                              }),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          BotonAtras()
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
  getExpenseItemsMio(AsyncSnapshot<QuerySnapshot> snapshot) {

    /*var snap= snapshot.data.docs.sort((a,b) => a.data()['date'].compareTo(b.data()['date']));
    print('snap ${snap}');*/
    return snapshot.data.docs.map((doc) {
      //  print('mi latitud ${doc["latitud"]}');
      
      
      return Notificacion( documentSnapshot: doc,);
    }).toList();
  }
}
