import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:http/http.dart' as http;

import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/pago/tipopago.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';
import 'package:tl_app/widgets/radiometodopago.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/tarjetasMetodo.dart';
import 'package:tl_app/widgets/tarjetasMetodoPorPagar.dart';

class MetodoDePago extends StatefulWidget {
  final String servicio;
  final String precio;
  final String cantidad;
  final String total;
  final String iddoc;
  final String idEncargado;
  final String token;
  MetodoDePago(
      {Key key,
      this.servicio,
      this.precio,
      this.cantidad,
      this.total,
      this.iddoc,
      this.idEncargado,
      this.token})
      : super(key: key);

  @override
  _MetodoDePagoState createState() => _MetodoDePagoState();
}

class _MetodoDePagoState extends State<MetodoDePago> {
  bool showtarjetas = false;
  bool seleccionado;
  String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
  final searchCtrl = TextEditingController();
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    List<Widget> _children = [
      metodo(),
      Guardado(),
      Busqueda(),
      Mensajes(),
      Perfil(),
    ];
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          // Navigator.pushReplacement(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade, child: AlertaCustom()));
          page = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    // color: Colors.amber,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  _children[page]
                  //:_children[widget.index],
                ],
              )
              /**/
              )),
    );
  }

  Widget metodo() {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("lib/assets/images/splash/fondo.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 38.0),
          child: Container(
            height: getProportionateScreenHeight(45),
            child: Align(
              alignment: Alignment.topCenter,
              child: Image.asset(
                'lib/assets/images/busqueda/2look.png',
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 95.0),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  detalle(),
                  Container(
                    alignment: Alignment.center,
                    child: Material(
                        type: MaterialType.transparency,
                        child: Text("MÉTODO DE PAGO",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(18),
                              fontWeight: FontWeight.bold,
                            ))),
                  ),
                  MetodoPagoRadio(
                    titulo: 'Seleccione:',
                    tipo1: 'Efectivo',
                    tipo2: 'Tarjeta',
                    retorno1: (value1) {
                      if (value1) {
                        seleccionado = true;
                        showtarjetas = false;
                        setState(() {});
                      } else {
                        seleccionado = false;
                        setState(() {});
                      }
                    },
                    retorno2: (value2) {
                      if (value2) {
                        seleccionado = false;
                        showtarjetas = true;
                        setState(() {});
                      } else {
                        showtarjetas = false;
                        setState(() {});
                      }
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  showtarjetas == true
                      ? TarjetasMetodoPorPagar(total: widget.total.split("\$")[0], snapshot: widget.iddoc, idEncargado: widget.idEncargado, token: widget.token)
                      : SizedBox(),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: (seleccionado == true)
                        ? ButtonGray(
                            textoBoton: "PAGAR",
                            bFuncion: () {
                              print(seleccionado);
                              if (seleccionado == false ||
                                  seleccionado == null) {
                                _alertapago(
                                    context, 'Selecciona el metodo de pago');
                              } else {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(widget.iddoc)
                                      .update({
                                    'estado': 'EN TRANSCURSO',
                                    'metodoPago': 'Efectivo',
                                    'estadoMetodoPago': true
                                  });
                                  String date = DateFormat("yyyy-MM-dd hh:mm")
                                      .format(DateTime.now());
                                  String notificacion =
                                      'Se ha realizado el pago del servicio, ahora se encuentra en transcurso';

                                  FirebaseFirestore.instance
                                      .collection('Notificaciones')
                                      .add({
                                    'idemisor': prefs.id,
                                    'idreceptor': widget.idEncargado,
                                    'date': date,
                                    'notificacion': notificacion,
                                    // 'tokensolicitante': prefs.token,
                                    //  'remitente':'usuario',
                                    'accion': 'chat'
                                  }).then((value) {
                                    sendNotification(
                                        notificacion, '2Look', widget.token);
                                    sendMessage(
                                      prefs.id,
                                      widget.idEncargado,
                                    );
                                  }).then((value) => Navigator.pop(context));
                                });
                                _alerta(context,
                                    'Tu servicio se encuentra en transcurso, enviaremos un mensaje al profesional para que se contacte contigo');
                              }
                            },
                            anchoBoton: getProportionateScreenHeight(330),
                            largoBoton: getProportionateScreenWidth(27),
                            colorTextoBoton: textColorClaro,
                            opacityBoton: 1,
                            sizeTextBoton: getProportionateScreenWidth(15),
                            weightBoton: FontWeight.bold,
                            color1Boton: primaryColor,
                            color2Boton: primaryColor,
                          )
                        : SizedBox(),
                  ),

                  //Container()
                ],
              ),
            ),
          ),
        ),
        Center(
            child: Column(
          children: <Widget>[
            Container(
                child: Stack(
              children: [
                Container(
                  width: getProportionateScreenWidth(450),
                  height: getProportionateScreenHeight(70),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image:
                          AssetImage("lib/assets/images/busqueda/cabecera.png"),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ],
            )),
          ],
        )),
        BotonAtras(),
        //
      ],
    );
  }

  void _alerta(
    BuildContext context,
    String text,
  ) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                // Navigator.of(context).pop();
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    PageTransition(
                                        child: Home(index: 3),
                                        type: PageTransitionType.fade),
                                    (route) => false);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertapago(
    BuildContext context,
    String text,
  ) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  detalle() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text(widget.servicio,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text(
                      // "SERVICIO/PELUQERIA",
                      'Cantidad'.toUpperCase(),
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text("TOTAL:",
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text(widget.precio,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text(widget.cantidad,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
              Container(
                child: Material(
                  type: MaterialType.transparency,
                  child: Text(widget.total,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  sendMessage(idEmisor, idReceptor) {
    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(widget.idEncargado)
        .get()
        .then((value) {
      var mensaje = 'Hola ' +
          value.data()['nombre'].toString() +
          ", la entrega de mi producto o servicio esta solicitada lista en la hora acordada te llamare cuando me encuentre cerca." +
          value.data()['celular'].toString();
      FirebaseFirestore.instance.collection('Mensajes').add({
        'fecha': DateTime.now(),
        'idemisor': idEmisor,
        'idreceptor': idReceptor,
        'idsolicitud': widget.iddoc,
        'mensaje': mensaje,
        'estado': true,
      }).then((value) =>
          FirebaseFirestore.instance.collection('MensajesDetalle').add({
            'fecha': DateTime.now(),
            'idemisor': idEmisor,
            'idsolicitud': widget.iddoc,
            'idMensaje': value.id,
            'mensaje': mensaje,
            'estado': true,
          }));
    });
  }

  Future<void> sendNotification(subject, title, token) async {
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {"body": subject, "title": title},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": token
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }
}
