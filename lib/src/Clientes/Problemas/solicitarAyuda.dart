import 'dart:ui';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/size_config.dart';

class SolicitarAyuda extends StatefulWidget {
  SolicitarAyuda({Key key}) : super(key: key);

  @override
  _SolicitarAyudaState createState() => _SolicitarAyudaState();
}

class _SolicitarAyudaState extends State<SolicitarAyuda> {
  final descripcionCtrl = TextEditingController();
  final dataApi = ServiceApi();
  final prefs = PreferenciasUsuario();
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          // Navigator.pushReplacement(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade, child: AlertaCustom()));
          page = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("SOLICITAR AYUDA",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20.0, right: 50, left: 50),
                              child: Text(
                                "Envianos un mensaje detallado en que necesitas ayuda y un operado resolvera tus inquietudes lo antes posible",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  color: textColorClaro,
                                  fontSize: getProportionateScreenWidth(11),
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: lightColor,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  width: getProportionateScreenHeight(370),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextField(
                                      controller: descripcionCtrl,
                                      keyboardType: TextInputType.multiline,
                                      maxLines: 5,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      autocorrect: true,
                                      style: DecorationsOnInputs.textoInput(),
                                      decoration: InputDecoration(
                                        enabledBorder: const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(27)),
                                          borderSide: const BorderSide(
                                              color: lightColor, width: 5.0),
                                        ),
                                        errorStyle: TextStyle(
                                          color: textColorClaro,
                                        ),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        labelText: "Ingrese su mensaje aqui...",
                                        labelStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Poppins",
                                        ),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 0.0,
                                                horizontal: 10.0),
                                        hintStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white38,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: "Poppins",
                                        ),
                                        focusedBorder: InputBorder.none,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  )),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              child: ButtonGray(
                                textoBoton: "ENVIAR",
                                bFuncion: () {
                                  _enviar();
                                },
                                anchoBoton: getProportionateScreenHeight(356),
                                largoBoton: getProportionateScreenWidth(28),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }

  _enviar() {
    Future(() => loaderDialogNormal(context)).then((value) {
      var informacion = descripcionCtrl.text;
      // dataApi.sedEmail(informacion, nombre, email,).then((value) {
      dataApi
          .sedEmail(informacion, prefs.usuario['nombre'],
              prefs.usuario['correo'], prefs.usuario['celular'])
          .then((value) {
        print("valor" + value.body.toString());
        Navigator.pop(context);

        _alertaProducto(context,
            'Tu solicitud fue enviada con exito, muy pronto un administrador de 2LOOK se pondra en contacto contigo');
      });
    });
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop();

                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
}
