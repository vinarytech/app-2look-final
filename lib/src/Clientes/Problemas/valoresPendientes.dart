import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/crearcuenta.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:tl_app/widgets/importante/camara.dart';

import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:ui';

import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/widgets/radiometodopago.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tl_app/widgets/tarjetasMetodoPagarMultas.dart';

class ValoresPendientes extends StatefulWidget {
  ValoresPendientes({Key key}) : super(key: key);

  @override
  _ValoresPendientesState createState() => _ValoresPendientesState();
}

class _ValoresPendientesState extends State<ValoresPendientes> {
  final searchCtrl = TextEditingController();
  final convert = new NumberFormat("#,##0.00", "en_US");
  final transferenciaCtlr = TextEditingController();
  File _image;
  String tipo = '';
  String codigo = '';
  String url;

  bool transferencia = false;
  bool tarjeta = false;
  final prefs = PreferenciasUsuario();
  double total = 0.0;
  double acumulada = 0;

  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // var curvedNavigationBar = CurvedNavigationBar(
    //   height: 50.0,
    //   backgroundColor: Color(0xfff191B1F),
    //   color: Color(0xfff191B1F),
    //   buttonBackgroundColor: Color(0xfffA91D74),
    //   items: <Widget>[
    //     TijerasIcon(),
    //     GuardadoIcon(),
    //     BuscarIcon(),
    //     MensajeIcon(),
    //     PerfilIcon()
    //   ],
    //   animationDuration: Duration(milliseconds: 200),
    //   animationCurve: Curves.bounceInOut,
    //   index: page,
    //   onTap: (index) {
    //     //Handle button tap
    //     setState(() {
    //       Navigator.pushReplacement(context,
    //           PageTransition(type: PageTransitionType.fade, child: Home()));
    //       page = index;
    //     });
    //   },
    // );
    return SafeArea(
      child: Scaffold(
          // bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("VALORES PENDIENTES",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("MULTAS",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                                child: StreamBuilder<QuerySnapshot>(
                                    stream: FirebaseFirestore.instance
                                        .collection("Pago_multas")
                                        .where("id_usuario",
                                            isEqualTo: prefs.id)
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.hasData) {
                                        acumulada = 0;
                                        snapshot.data.docs.forEach((element) {
                                          if (element.data()['pago'] !=
                                              'Verificado') {
                                            acumulada = acumulada +
                                                double.parse(
                                                    element.data()['total']);
                                            // print(acumulada);
                                            // total = acumulada;
                                          }
                                        });
                                        return Column(children: [
                                          Container(
                                            alignment: Alignment.center,
                                            child: Material(
                                                type: MaterialType.transparency,
                                                child: Text(
                                                    r'$ ' +
                                                        acumulada
                                                            .toStringAsFixed(2),
                                                    style: TextStyle(
                                                      color: textColorClaro,
                                                      fontSize:
                                                          getProportionateScreenWidth(
                                                              38),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ))),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Column(
                                            children: getExpenseItems(snapshot),
                                          )
                                        ]);
                                      } else if (snapshot.hasError) {
                                        return Center(
                                            child:
                                                Text("Ha ocurrido un error"));
                                      } else {
                                        return Center(
                                            child: CircularProgressIndicator());
                                      }
                                    })),
                            SizedBox(
                              height: 10,
                            ),
                            metodo()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      return Column(
        children: [
          ListTile(
            title: Text('Multa por: ' + doc.data()['motivo'],
                style: TextStyle(
                  color: textColorClaro,
                  fontSize: getProportionateScreenWidth(12),
                  fontWeight: FontWeight.normal,
                )),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Fecha de cancelación',
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(12),
                      fontWeight: FontWeight.normal,
                    )),
                Text(doc.data()['fecha'],
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(12),
                      fontWeight: FontWeight.normal,
                    )),
                (doc.data()['pago'] == 'Pendiente')
                    ? Text('En Revisión',
                        style: TextStyle(
                          color: textColorMedio,
                          fontSize: getProportionateScreenWidth(12),
                          fontWeight: FontWeight.normal,
                        ))
                    : (doc.data()['pago'] == 'Verificado')
                        ? Text('Pagado',
                            style: TextStyle(
                              color: textColorMedio,
                              fontSize: getProportionateScreenWidth(12),
                              fontWeight: FontWeight.normal,
                            ))
                        : SizedBox()
              ],
            ),
            trailing: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text('Total',
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(12),
                      fontWeight: FontWeight.normal,
                    )),
                Text(r'$ ' + convert.format(double.parse(doc.data()['total'])),
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(12),
                      fontWeight: FontWeight.normal,
                    ))
              ],
            ),
          ),
          Divider()
        ],
      );
    }).toList();
  }

  Widget metodo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
          child: Container(
            child: Column(
              children: [
                Container(
                  child: Material(
                    type: MaterialType.transparency,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20),
                          ),
                          child: Text('MÉTODO DE PAGO',
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(18),
                                fontWeight: FontWeight.bold,
                              )),
                        )),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     Material(
                      //       type: MaterialType.transparency,
                      //       child: Align(
                      //           alignment: Alignment.topLeft,
                      //           child: Padding(
                      //             padding: EdgeInsets.only(
                      //               left: getProportionateScreenWidth(20),
                      //             ),
                      //             child: Text('Transferencia o depósito',
                      //                 style: TextStyle(
                      //                   color: textColorClaro,
                      //                   fontSize:
                      //                       getProportionateScreenWidth(14),
                      //                   fontWeight: FontWeight.normal,
                      //                 )),
                      //           )),
                      //     ),
                      //     Padding(
                      //       padding: const EdgeInsets.only(right: 20.0),
                      //       child: Container(
                      //         height: 20,
                      //         width: 20,
                      //         child: CircularCheckBox(
                      //             value: transferencia,
                      //             checkColor: textColorMedio,
                      //             activeColor: primaryColor,
                      //             inactiveColor: textColorMedio,
                      //             disabledColor: Colors.grey,
                      //             onChanged: (val) => this.setState(() {
                      //                   if (acumulada > 0) {
                      //                     transferencia = !transferencia;
                      //                     if (transferencia) {
                      //                       tarjeta = false;
                      //                     }
                      //                   } else {
                      //                     _alertaProducto(context, 'No tienes valores pendientes de pago');
                      //                   }
                      //                 })),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: 10,
                      // ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Material(
                            type: MaterialType.transparency,
                            child: Align(
                                alignment: Alignment.topLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(
                                    left: getProportionateScreenWidth(20),
                                  ),
                                  child: Text('Tarjeta',
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(14),
                                        fontWeight: FontWeight.normal,
                                      )),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 20.0),
                            child: Container(
                              height: 20,
                              width: 20,
                              child: CircularCheckBox(
                                  value: tarjeta,
                                  checkColor: textColorMedio,
                                  activeColor: primaryColor,
                                  inactiveColor: textColorMedio,
                                  disabledColor: Colors.grey,
                                  onChanged: (val) => this.setState(() {
                                      if (acumulada > 0) {
                                        tarjeta = !tarjeta;
                                        if (tarjeta) {
                                          transferencia = false;
                                        }
                                        } 
                                      })),
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: getProportionateScreenHeight(10),
        ),
        (transferencia == false && tarjeta == false)
            ? Container()
            : (transferencia == true)
                ? GestureDetector(
                    onTap: () {},
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: getProportionateScreenWidth(30)),
                          child: Container(
                            child: Column(
                              children: [
                                Text("Datos de la transferencia: ",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(19),
                                      fontWeight: FontWeight.bold,
                                    )),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text("Nombre del titular: ",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Text("Norma Carrera Padilla",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text("Cuenta corriente: ",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Text("420005225",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text("Banco: ",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Text("Banco Internacional",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(14),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ],
                                ),
                              ],
                            ),
                            padding: EdgeInsets.only(
                                top: 16, bottom: 16, left: 16, right: 16),
                            decoration: BoxDecoration(
                                color: lightColor,
                                borderRadius: BorderRadius.circular(20),
                                // shape: BoxShape.rectangle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      blurRadius: 10,
                                      offset: Offset(0, 10))
                                ]),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        GestureDetector(
                          onTap: () {
                            _showPickOptions(context);
                          },
                          child: Container(
                            child: (_image == null)
                                ? Camara()
                                : Image.file(_image),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 18.0),
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text("Foto de transferencia o deposito",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(11),
                                      fontWeight: FontWeight.normal,
                                    ))),
                          ),
                        ),
                        InputIconCustomTextField(
                          icon: Icon(
                            Icons.credit_card,
                            color: Colors.white,
                            size: 20.0,
                          ),
                          isPassword: false,
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                          label: "Número de comprobante",
                          placeholder: "...",
                          textController: transferenciaCtlr,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: ButtonGray(
                            textoBoton: "PAGAR",
                            bFuncion: () {
                              if (transferencia) {
                                _alertaProducto(
                                    context,
                                    '¿Estás seguro de querer realizar el pago con el total de ' +
                                        total.toString() +
                                        '?');
                              }
                            },
                            anchoBoton: getProportionateScreenHeight(330),
                            largoBoton: getProportionateScreenWidth(27),
                            colorTextoBoton: textColorClaro,
                            opacityBoton: 1,
                            sizeTextBoton: getProportionateScreenWidth(15),
                            weightBoton: FontWeight.w400,
                            color1Boton: primaryColor,
                            color2Boton: primaryColor,
                          ),
                        ),
                      ],
                    ),
                  )
                : TarjetasMetodoPagarMultas(total: acumulada.toString(),),
        SizedBox(
          height: getProportionateScreenHeight(20),
        ),
        Container()
      ],
    );
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);
        } else {}
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 16.0),
        // aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 6.0),

        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  FirebaseFirestore.instance
                                      .collection("Pago_multas")
                                      .where("id_usuario", isEqualTo: prefs.id)
                                      .get()
                                      .then((value) =>
                                          value.docs.forEach((element) {
                                            if (element.data()['pago'] == '') {
                                              FirebaseFirestore.instance
                                                  .collection('Pago_multas')
                                                  .doc(element.id)
                                                  .update({
                                                'forma_pago': 'Tranferencia',
                                                'pago': 'Pendiente'
                                              });
                                              subirTransferencia(element.id);
                                            }
                                          }))
                                      .then((value) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Alerta(context, 'Exito',
                                        'Te informaremos apenas el pago por tu transferencia sea realizada ');
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));



            
    void _alertaProducto(BuildContext context, String text, String estado) {
      showDialog(
          context: context,
          builder: (context) => BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {},
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ));
    }
  }




    void _alertaProductoFinal(BuildContext context, String text) {
      showDialog(
          context: context,
          builder: (context) => BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {
                                  Navigator.pop(context);
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ));
    }



  void subirTransferencia(id) async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Pago_multas')
        .doc(id)
        .update({'fotografia_transferencia': url});
  }
}
