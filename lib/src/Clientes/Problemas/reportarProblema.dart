import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:intl/intl.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/src/Clientes/Alertas/alertaCalificacion.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:ui';

import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/informacionfinal.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';

class ReportarProblema extends StatefulWidget {
     final InformacionFinal snap;

  ReportarProblema({Key key, this.snap}) : super(key: key,);

  @override
  _ReportarProblemaState createState() => _ReportarProblemaState();
}

class _ReportarProblemaState extends State<ReportarProblema> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  String mensaje;
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          // Navigator.pushReplacement(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade, child: AlertaCustom()));
          page = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonGray(
                              textoBoton: "El profesional no llego",
                              bFuncion: () {
                                mensaje="El profesional no llego";
                                reportarProblema(mensaje);

                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "No completaron mi servicio",
                              bFuncion: () {
                                 mensaje="No completaron mi servicio";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "El producto esta defectuoso",
                              bFuncion: () {
                                 mensaje="El producto esta defectuoso";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "Me cobraron otro precio",
                              bFuncion: () {
                                mensaje="Me cobraron otro precio";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "Otros",
                              bFuncion: () {
                                mensaje="Otros";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
  loaderDialogNormal(BuildContext context) {
    showDialog(

        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
  reportarProblema(String mensaje) {
    var promedio;
    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
     Future(() => loaderDialogNormal(context)).then((v) {


            FirebaseFirestore.instance.collection('Problemas').add({
              'id_usuario_recibe':widget.snap.docservicio.data()["id_usuario"],
              'id_usuario_envia':prefs.id,
              'idservicio': widget.snap.docservicio.id,
              'idsolicitud': widget.snap.documento.id,
              'created_at': date,
              'token_recibe': widget.snap.doc.data()["token"],
              'token_envia': prefs.usuario['token'],
              'nombre_recibe': widget.snap.doc.data()["nombre"],
              'nombre_envia':prefs.usuario['nombre'],
              'telefono_envia':prefs.usuario['celular'],
              'telefono_recibe':widget.snap.doc.data()["celular"],
              'correo_envia':prefs.usuario['correo'],
              'correo_recibe':widget.snap.doc.data()["correo"],
              'problema':mensaje,
              'status':true                                  
             }).then((value) {
            //  sendNotification(notificacion, '2Look',widget.token);
              FirebaseFirestore.instance
            .collection('Solicitudes')
            .doc(widget.snap.documento.id)
            .update({'estado': 'FINALIZADO'});
                                     
             }).then((value) =>  Navigator.pop(context)).then((value) {
               _alerta(context, 'Tu problema ha sido enviado, pronto nos comunicaremos contigo');                  
             });
                                  
                                    
        });
  }
  _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                               Navigator.of(context).pop();
                                 Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                     Home(index: 0, )), (Route<dynamic> route) => false);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
           
  }
}