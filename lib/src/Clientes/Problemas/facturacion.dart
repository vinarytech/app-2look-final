import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/crearcuenta.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/importante/camara.dart';

import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:ui';

import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/widgets/radiometodopago.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:tl_app/widgets/tarjetasMetodoPagarMultas.dart';

class Facturacion extends StatefulWidget {
  Facturacion({Key key}) : super(key: key);

  @override
  _FacturacionState createState() => _FacturacionState();
}

class _FacturacionState extends State<Facturacion> {
  final searchCtrl = TextEditingController();
  final convert = new NumberFormat("#,##0.00", "en_US");
  final transferenciaCtlr = TextEditingController();
  final nombreCtrl = TextEditingController();
  final cedulaCtrl = TextEditingController();
  final correoCtrl = TextEditingController();
  final direccionCtrl = TextEditingController();
  final telefonoCtrl = TextEditingController();

  File _image;
  String tipo = '';
  String codigo = '';
  String url;

  bool transferencia = false;
  bool tarjeta = false;
  final prefs = PreferenciasUsuario();
  double total = 0.0;
  double acumulada = 0;

  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 95.0, left: 20, right: 20),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.center,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("DATOS DE LA FACTURACIÓN",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("Nombres y apellidos",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                width: getProportionateScreenHeight(370),
                                child: TextField(
                                  controller: nombreCtrl,
                                  keyboardType: TextInputType.text,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 0.0, horizontal: 10.0),
                                    hintStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("Cédula/RUC o Pasaporte",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                width: getProportionateScreenHeight(370),
                                child: TextField(
                                  controller: cedulaCtrl,
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 0.0, horizontal: 10.0),
                                    hintStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("Correo",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                width: getProportionateScreenHeight(370),
                                child: TextField(
                                  controller: correoCtrl,
                                  keyboardType: TextInputType.emailAddress,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 0.0, horizontal: 10.0),
                                    hintStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("Direccion",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                width: getProportionateScreenHeight(370),
                                child: TextField(
                                  controller: direccionCtrl,
                                  keyboardType: TextInputType.text,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 0.0, horizontal: 10.0),
                                    hintStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("Teléfono",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                width: getProportionateScreenHeight(370),
                                child: TextField(
                                  controller: telefonoCtrl,
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    contentPadding: new EdgeInsets.symmetric(
                                        vertical: 0.0, horizontal: 10.0),
                                    hintStyle: TextStyle(
                                      fontSize:
                                          getProportionateScreenHeight(30),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                )),
                            SizedBox(
                              height: 30,
                            ),
                            ButtonGray(
                              textoBoton: "Guardar",
                              bFuncion: () {
                              },
                              anchoBoton: MediaQuery.of(context).size.width,
                              largoBoton: getProportionateScreenHeight(50),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.w400,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);
        } else {}
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 16.0),
        // aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 6.0),

        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  FirebaseFirestore.instance
                                      .collection("Pago_multas")
                                      .where("id_usuario", isEqualTo: prefs.id)
                                      .get()
                                      .then((value) =>
                                          value.docs.forEach((element) {
                                            if (element.data()['pago'] == '') {
                                              FirebaseFirestore.instance
                                                  .collection('Pago_multas')
                                                  .doc(element.id)
                                                  .update({
                                                'forma_pago': 'Tranferencia',
                                                'pago': 'Pendiente'
                                              });
                                              subirTransferencia(element.id);
                                            }
                                          }))
                                      .then((value) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Alerta(context, 'Exito',
                                        'Te informaremos apenas el pago por tu transferencia sea realizada ');
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));

    void _alertaProducto(BuildContext context, String text, String estado) {
      showDialog(
          context: context,
          builder: (context) => BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {},
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ));
    }
  }

  void _alertaProductoFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void subirTransferencia(id) async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Pago_multas')
        .doc(id)
        .update({'fotografia_transferencia': url});
  }
}
