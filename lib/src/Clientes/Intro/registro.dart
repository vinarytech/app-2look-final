import 'dart:convert';
import 'dart:io';
import 'dart:ui';

// import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:intl/intl.dart';
import 'package:tl_app/src/Clientes/Intro/importante.dart';
import 'package:tl_app/src/Clientes/Intro/importantePrivate.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';

import 'package:tl_app/src/Clientes/Intro/importantePublic.dart';
import 'package:tl_app/widgets/constants.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Intro/crearcuenta.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/registro/icono.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';

class Registro extends StatefulWidget {
  final regreso;

  const Registro({Key key, this.regreso}) : super(key: key);
  @override
  _RegistroState createState() => new _RegistroState();
}

class _RegistroState extends State<Registro> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  String user;
  String password;
  final userCtrl = TextEditingController();
  final usuarioS = new UsuarioServices();
  final passCtrl = TextEditingController();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();
  var firebaseUser;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: new SafeArea(
          child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("lib/assets/images/splash/fondo.png"),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                (widget.regreso == true)
                    ? Padding(
                        padding: const EdgeInsets.only(left: 10, top: 10),
                        child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Stack(
                            children: [
                              Icon(
                                Icons.circle,
                                size: 50,
                                color: Color(0Xff707070),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 18, top: 13),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  color: Colors.white,
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
                Padding(
                  padding:
                      EdgeInsets.only(top: getProportionateScreenHeight(40)),
                  child: Container(
                    height: MediaQuery.of(context).size.height * .965,
                    child: Column(
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(),
                        Container(
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: IconoRegistro(),
                              ),
                              SizedBox(height: 20),
                              Material(
                                  type: MaterialType.transparency,
                                  child: Text("INICIA SESIÓN",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(17),
                                        fontWeight: FontWeight.normal,
                                      ))),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Container(
                          child: Material(
                            type: MaterialType.transparency,
                            child: Container(
                                child: Column(
                              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                InputCustomTextField(
                                  isPassword: false,
                                  keyboardType: TextInputType.text,
                                  placeholder: "Ingrese su nombre de usuario",
                                  textController: userCtrl,
                                  label: "  CORREO ELECTRONICO",
                                ),
                                InputCustomTextField(
                                  isPassword: true,
                                  keyboardType: TextInputType.text,
                                  placeholder: "Ingrese su contraseña",
                                  textController: passCtrl,
                                  label: "  CONTRASEÑA",
                                ),
                              ],
                            )),
                          ),
                        ),
                        ButtonGray(
                          textoBoton: "INICIAR SESIÓN",
                          bFuncion: () async {
                            final token = await _fcm.getToken();

                            if (userCtrl.text == null || userCtrl.text == '') {
                              _alertaFinal(context,
                                  "Por favor ingresa el usuario para continuar.");
                            } else if (passCtrl.text == null ||
                                passCtrl.text == '') {
                              _alertaFinal(context,
                                  "Por favor ingresa una contraseña para continuar.");
                            } else {
                              new Future(
                                  () =>
                                      loaderDialogNormal(context)).then((v) =>
                                  usuarioS
                                      .login(userCtrl.text, passCtrl.text)
                                      .then((v) {
                                    print(v);
                                    if (v['ok'] == false) {
                                      if (v['mensaje'] == 'EMAIL_NOT_FOUND') {
                                        Navigator.pop(context);
                                        _alertaFinal(context,
                                            'Este email no existe, por favor verifica y vuelvelo a intentar.');
                                      } else if (v['mensaje'] ==
                                          'INVALID_PASSWORD') {
                                        Navigator.pop(context);
                                        _alertaFinal(context,
                                            'Contraseña incorrecta, por favor verifica y vuelvelo a intentar.');
                                      } else if (v['mensaje'] ==
                                          'TOO_MANY_ATTEMPTS_TRY_LATER : Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later.') {
                                        Navigator.pop(context);
                                        _alertaFinal(context,
                                            'El acceso a esta cuenta ha sido deshabilitado temporalmente, porfavor intentalo mas tarde.');
                                      }
                                      // Navigator.pop(context);
                                      // Alerta(context, 'Alerta',
                                      //     'El usuario con este email ya existe por favor inicia sesión.');
                                    } else {
                                      actualizarContrasenia().then((value) {
                                        new Future(() =>
                                                loaderDialogNormal(context))
                                            .then((v) {
                                          usuarioS
                                              .obtenerUsuario(
                                                  userCtrl.text, passCtrl.text)
                                              .then((v) {
                                                print('hola');
                                                Navigator.pop(context);
                                              })
                                              .then((value) => FirebaseFirestore
                                                  .instance
                                                  .collection('Usuarios')
                                                  .doc(prefs.id)
                                                  .update({'token': token}))
                                              // .then((v) => Navigator.pop(context))
                                              .then((v) {
                                                Navigator.pushAndRemoveUntil(
                                                    context,
                                                    PageTransition(
                                                        child: Home(
                                                          index: 2,
                                                        ),
                                                        type: PageTransitionType
                                                            .fade),
                                                    (route) => false);

                                                // Navigator.pushReplacement(
                                                //     context,
                                                //     PageTransition(
                                                //         child: Home(
                                                //           index: 2,
                                                //         ),
                                                //         type:
                                                //             PageTransitionType.fade));
                                                // Alerta(context, 'Éxito',
                                                //     'BIENVENIDO.');
                                              });
                                        });
                                      });
                                    }
                                  }));
                            }
                          },
                          anchoBoton: getProportionateScreenHeight(330),
                          largoBoton: getProportionateScreenWidth(50),
                          colorTextoBoton: textColorClaro,
                          opacityBoton: 1,
                          sizeTextBoton: getProportionateScreenWidth(15),
                          weightBoton: FontWeight.bold,
                          color1Boton: primaryColor,
                          color2Boton: primaryColor,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Material(
                                type: MaterialType.transparency,
                                child: Text("Crear una cuenta ",
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(11),
                                      fontWeight: FontWeight.w300,
                                      fontStyle: FontStyle.normal,
                                    ))),
                            GestureDetector(
                              onTap: () => Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.fade,
                                      child: CrearCuenta())),
                              child: Container(
                                height: 20,
                                //color: Colors.white,
                                child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.center,
                                        child: Container(
                                          //color: Colors.white,
                                          child: Center(
                                            child: RichText(
                                                text: new TextSpan(children: [
                                              new TextSpan(
                                                  text: "AQUI",
                                                  style: TextStyle(
                                                    color: primaryColor,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            12),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            ])),
                                          ),
                                        ))),
                              ),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () async {
                            _alertaDatos(context,
                                "Por favor ingresa el correo electronico");
                          },
                          child: Container(
                            // height: 10,
                            //color: Colors.white,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Align(
                                    alignment: Alignment.center,
                                    child: Container(
                                      //color: Colors.white,
                                      child: Center(
                                        child: RichText(
                                            text: new TextSpan(children: [
                                          new TextSpan(
                                              text: "Recuperar mi contraseña!",
                                              style: TextStyle(
                                                color: primaryColor,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        12),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ])),
                                      ),
                                    ))),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Text('O ingresa con:',
                            style: TextStyle(
                              fontFamily: 'Poppins',
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(11),
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        ButtonGrayRedes(
                          textoBoton: "Inicia sesión con Gmail",
                          bFuncion: () {
                            signInWithGoogle();
                          },
                          imagen: "lib/assets/images/redes/gma.png",
                          anchoBoton: getProportionateScreenHeight(330),
                          largoBoton: getProportionateScreenWidth(50),
                          colorTextoBoton: textColorMedio,
                          opacityBoton: 0.10,
                          sizeTextBoton: getProportionateScreenWidth(11),
                          weightBoton: FontWeight.w300,
                          color1Boton: lightColor,
                          color2Boton: lightColor,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ButtonGrayRedes(
                          textoBoton: "Inicia sesión con Facebook",
                          bFuncion: () {
                            _loginFacebook(context);
                          },
                          imagen: "lib/assets/images/redes/fa.png",
                          anchoBoton: getProportionateScreenHeight(330),
                          largoBoton: getProportionateScreenWidth(50),
                          colorTextoBoton: textColorMedio,
                          opacityBoton: 0.10,
                          sizeTextBoton: getProportionateScreenWidth(11),
                          weightBoton: FontWeight.w300,
                          color1Boton: lightColor,
                          color2Boton: lightColor,
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        (Platform.isIOS)
                            ? ButtonGrayRedes(
                                imagen: "lib/assets/images/redes/iclod.png",
                                textoBoton: "Inicia sesión con Apple",
                                bFuncion: () async {
                                  String date = DateFormat("yyyy-MM-dd hh:mm")
                                      .format(DateTime.now());
                                  final token = await _fcm.getToken();
                                  // print('TokenTelefono'+token);
                                  final usuarioS = new UsuarioServices();
                                  final authResult = await _loginIcloud();
                                  firebaseUser = authResult.user.toString();
                                  //print(firebaseUser);

                                  var email = firebaseUser
                                      .split('email: ')[1]
                                      .toString()
                                      .split(',')[0];

                                  FirebaseFirestore.instance
                                      .collection('Usuarios')
                                      .where('correo2', isEqualTo: email)
                                      .snapshots()
                                      .forEach((element) async {
                                    if (element.docs.length > 0) {
                                      if (element.docs[0].data()['tipoLogin'] ==
                                          'icloud') {
                                        var data = await FirebaseFirestore
                                            .instance
                                            .collection("Usuarios")
                                            .where('correo2', isEqualTo: email)
                                            .get();

                                        print(data.docs[0].data().toString());
                                        if (data == null) return null;
                                        print(data.docs[0]
                                            .data()['contrasenia']
                                            .toString());
                                        data.docs.forEach((element) {
                                          prefs.id = element.id.toString();
                                          prefs.usuario =
                                              json.encode(element.data());
                                          return element.data();
                                        });
                                        page = 2;
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            PageTransition(
                                                type: PageTransitionType.fade,
                                                child: Home()),
                                            (route) => false);

                                        // Navigator.pop(context);
                                        // Navigator.pushReplacement(context,
                                        //     PageTransition(type: PageTransitionType.fade, child: Home()));
                                      } else {
                                        Navigator.pop(context);

                                        _alertaFinal(context,
                                            'El correo ya tiene un usuario porfavor verifica que estes logeado con  Gmail o un correo personal');
                                      }
                                    } else {
                                      // Navigator.push(
                                      //     context,
                                      //     PageTransition(
                                      //         child: ImportantePrivate(
                                      //             foto: null,
                                      //             usuario: null,
                                      //             correo: email,
                                      //             tipo: 'icloud'),
                                      //         type: PageTransitionType.fade));

                                      usuarioS
                                          .nuevoUsuario(email, passCtrl.text,
                                              email.split('@')[0].toString())
                                          .then((v) {
                                        if (v['mensaje'] == 'EMAIL_EXISTS') {
                                          _alertaFinal(context,
                                              'El usuario con este email ya existe por favor inicia sesión.');
                                        } else {
                                          FirebaseFirestore.instance
                                              .collection('Usuarios')
                                              .add({
                                            'nombre': email.split('@')[0],
                                            'correo': email,
                                            'correo2': email,
                                            'contrasenia': '',
                                            'celular': '',
                                            'cedula': '',
                                            'token': token,
                                            'created_at': date,
                                            'fotografia_perfil': '',
                                            'tipo': 'usuario',
                                            'estrellas': 3,
                                            'estado': true,
                                            'bloqueado': false,
                                            'tipoLogin': 'icloud',
                                            'verif_celular': false,
                                            'verif_cedula': false,
                                            'verif_correo': true,
                                            'status': true,
                                          }).then((value) {
                                            // subirfotoportada(value.id);
                                            FirebaseFirestore.instance
                                                .collection('Paquetes')
                                                .add({
                                              'token': token,
                                              'id_usuario': value.id,
                                              'estadosoli': false,
                                              'estado': false,
                                              'estadoPrueba': false,
                                              'suscripcion': false,
                                              'estadoPublicar': true,
                                              'prueba': '',
                                              'paquete1': '',
                                              'paquete2': '',
                                              'paquete3': '',
                                            });
                                          }).then((value) async {
                                            if ('icloud' == 'icloud') {
                                              await usuarioS.obtenerUsuario(
                                                  email.toString(),
                                                  passCtrl.text);
                                            } else {
                                              var data = await FirebaseFirestore
                                                  .instance
                                                  .collection("Usuarios")
                                                  .where('correo',
                                                      isEqualTo:
                                                          email.toString())
                                                  .get();

                                              print(data.docs[0]
                                                  .data()
                                                  .toString());
                                              if (data == null) return null;
                                              print(data.docs[0]
                                                  .data()['contrasenia']
                                                  .toString());
                                              data.docs.forEach((element) {
                                                prefs.id =
                                                    element.id.toString();
                                                prefs.usuario =
                                                    json.encode(element.data());
                                                return element.data();
                                              });
                                            }
                                            page = 2;
                                            Navigator.pushAndRemoveUntil(
                                                context,
                                                PageTransition(
                                                    type:
                                                        PageTransitionType.fade,
                                                    child: Home()),
                                                (route) => false);
                                            _alertaFinal(context, 'BIENVENIDO');
                                          });
                                        }
                                      });
                                    }
                                  });
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(50),
                                colorTextoBoton: textColorMedio,
                                opacityBoton: 0.10,
                                sizeTextBoton: getProportionateScreenWidth(11),
                                weightBoton: FontWeight.w300,
                                color1Boton: lightColor,
                                color2Boton: lightColor,
                              )
                            : SizedBox(),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      )),
    );
  }

  Future<void> _loginFacebook(BuildContext context) async {
    final AccessToken accessTokenLogi = await FacebookAuth.instance.isLogged;
    try {
      final AccessToken accessToken = await FacebookAuth.instance.login();
      print(accessToken.toJson().toString());
      if (accessTokenLogi != null) {
        final userData = await FacebookAuth.instance.getUserData();
        await singInFacebook(userData);
      } else {
        try {
          final AccessToken accessToken = await FacebookAuth.instance.login();
          print(accessToken.toJson().toString());
        } catch (e) {
          final userData = await FacebookAuth.instance.getUserData();
          await singInFacebook(userData);
        }
      }
    } catch (e) {}
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Alerta(context, titulo, mensaje) {
    showDialog(
      context: context,
      child: AlertDialog(
        title: Text(titulo),
        content: Text(mensaje),
        actions: <Widget>[
          OutlineButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("Ok"),
          ),
        ],
      ),
    );
  }

  void _alertaDatos(BuildContext context, String text) {
    final emailCtlrRecover = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.emailAddress,
                                  controller: emailCtlrRecover,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText:
                                        "CORREO ELECTRONICO QUE DESEA RECUPERAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () async {
                                print(emailCtlrRecover.text);
                                // await _auth.sendPasswordResetEmail(
                                //     email: emailCtlrRecover.text);
                                await _auth
                                    .sendPasswordResetEmail(
                                        email: emailCtlrRecover.text)
                                    .then((value) {
                                  Navigator.pop(context);
                                  _alertaFinal(context,
                                      "Se te ha enviado un mensaje un correo electrónico donde puedes cambiar la contraseña");
                                }).catchError((error) {
                                  _alertaFinal(context,
                                      "El email no es una cuenta valida");
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  singInFacebook(usuario) async {
    Future(() => loaderDialogNormal(context)).then((v) {
      FirebaseFirestore.instance
          .collection('Usuarios')
          .where('correo', isEqualTo: usuario['email'])
          .snapshots()
          .forEach((element) async {
        if (element.docs.length > 0) {
          if (element.docs[0].data()['tipoLogin'] == 'facebook') {
            var data = await FirebaseFirestore.instance
                .collection("Usuarios")
                .where('correo', isEqualTo: usuario['email'])
                .get();

            print(data.docs[0].data().toString());
            if (data == null) return null;
            print(data.docs[0].data()['contrasenia'].toString());
            data.docs.forEach((element) {
              prefs.id = element.id.toString();
              prefs.usuario = json.encode(element.data());
              return element.data();
            });
            page = 2;
            Navigator.pushAndRemoveUntil(
                context,
                PageTransition(type: PageTransitionType.fade, child: Home()),
                (route) => false);

            // Navigator.pop(context);
            // Navigator.pushReplacement(context,
            //     PageTransition(type: PageTransitionType.fade, child: Home()));
          } else {
            Navigator.pop(context);
            _alertaFinal(context,
                'El correo ya tiene un usuario porfavor verifica que estes logeado con  Gmail o un correo personal');
          }
        } else {
          String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
          final token = await _fcm.getToken();
          // print('TokenTelefono'+token);
          final usuarioS = new UsuarioServices();
          Navigator.pop(context);
          // Navigator.push(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade,
          //         child: Importante(
          //             foto: usuario['picture']['data']['url'].toString(),
          //             usuario: usuario['name'].toString(),
          //             correo: usuario['email'].toString(),
          //             tipo: 'facebook')));

          usuarioS
              .nuevoUsuario(usuario['email'].toString(), passCtrl.text,
                  usuario['name'].toString())
              .then((v) {
            if (v['mensaje'] == 'EMAIL_EXISTS') {
              _alertaFinal(context,
                  'El usuario con este email ya existe por favor inicia sesión.');
            } else {
              FirebaseFirestore.instance.collection('Usuarios').add({
                'nombre': usuario['name'].toString(),
                'correo': usuario['email'].toString(),
                'correo2': usuario['email'].toString(),
                'contrasenia': '',
                'celular': '',
                'cedula': '',
                'token': token,
                'created_at': date,
                'fotografia_perfil':
                    usuario['picture']['data']['url'].toString(),
                'tipo': 'usuario',
                'estrellas': 3,
                'estado': true,
                'bloqueado': false,
                'tipoLogin': 'facebook',
                'verif_celular': false,
                'verif_cedula': false,
                'verif_correo': true,
                'status': true,
              }).then((value) {
                // subirfotoportada(value.id);
                FirebaseFirestore.instance.collection('Paquetes').add({
                  'token': token,
                  'id_usuario': value.id,
                  'estadosoli': false,
                  'estado': false,
                  'estadoPrueba': false,
                  'suscripcion': false,
                  'prueba': '',
                  'estadoPublicar': true,
                  'paquete1': '',
                  'paquete2': '',
                  'paquete3': '',
                });
              }).then((value) async {
                if ('facebook' == 'facebook') {
                  await usuarioS.obtenerUsuario(
                      usuario['email'].toString(), passCtrl.text);
                } else {
                  var data = await FirebaseFirestore.instance
                      .collection("Usuarios")
                      .where('correo', isEqualTo: usuario['email'].toString())
                      .get();

                  print(data.docs[0].data().toString());
                  if (data == null) return null;
                  print(data.docs[0].data()['contrasenia'].toString());
                  data.docs.forEach((element) {
                    prefs.id = element.id.toString();
                    prefs.usuario = json.encode(element.data());
                    return element.data();
                  });
                }
                page = 2;
                Navigator.pushAndRemoveUntil(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade, child: Home()),
                    (route) => false);
                _alertaFinal(context, 'BIENVENIDO');
              });
            }
          });
        }
      }).then((value) => Navigator.pop(context));
    });
  }

  Future<String> signInWithGoogle() async {
    await Firebase.initializeApp();
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;
    Future(() => loaderDialogNormal(context)).then((v) => FirebaseFirestore
            .instance
            .collection('Usuarios')
            .where('correo', isEqualTo: googleSignInAccount.email)
            .snapshots()
            .forEach((element) async {
          if (element.docs.length > 0) {
            if (element.docs[0].data()['tipoLogin'] == 'google') {
              var data = await FirebaseFirestore.instance
                  .collection("Usuarios")
                  .where('correo', isEqualTo: googleSignInAccount.email)
                  .get();

              print(data.docs[0].data().toString());
              if (data == null) return null;
              print(data.docs[0].data()['contrasenia'].toString());
              data.docs.forEach((element) {
                prefs.id = element.id.toString();
                prefs.usuario = json.encode(element.data());
                return element.data();
              });
              page = 2;
              Navigator.pushAndRemoveUntil(
                  context,
                  PageTransition(type: PageTransitionType.fade, child: Home()),
                  (route) => false);
              // Navigator.pop(context);
              // Navigator.pushReplacement(context,
              //     PageTransition(type: PageTransitionType.fade, child: Home()));
            } else {
              Navigator.pop(context);
              _alertaFinal(context,
                  'El correo ya tiene un usuario porfavor verifica que estes logeado con Facebook o un correo personal.');
            }
          } else {
            String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
            final token = await _fcm.getToken();
            // print('TokenTelefono'+token);
            final usuarioS = new UsuarioServices();
            final AuthCredential credential = GoogleAuthProvider.credential(
              accessToken: googleSignInAuthentication.accessToken,
              idToken: googleSignInAuthentication.idToken,
            );

            final UserCredential authResult =
                await _auth.signInWithCredential(credential);
            final User user = authResult.user;

            if (user != null) {
              assert(!user.isAnonymous);
              assert(await user.getIdToken() != null);

              final User currentUser = _auth.currentUser;
              assert(user.uid == currentUser.uid);
              Navigator.pop(context);
              usuarioS
                  .nuevoUsuario(user.email, passCtrl.text, user.displayName)
                  .then((v) {
                if (v['mensaje'] == 'EMAIL_EXISTS') {
                  _alertaFinal(context,
                      'El usuario con este email ya existe por favor inicia sesión.');
                } else {
                  FirebaseFirestore.instance.collection('Usuarios').add({
                    'nombre': user.displayName,
                    'correo': user.email,
                    'correo2': user.email,
                    'contrasenia': '',
                    'celular': '',
                    'cedula': '',
                    'token': token,
                    'created_at': date,
                    'fotografia_perfil': user.photoURL,
                    'tipo': 'usuario',
                    'estrellas': 3,
                    'estado': true,
                    'bloqueado': false,
                    'tipoLogin': 'google',
                    'verif_celular': false,
                    'verif_cedula': false,
                    'verif_correo': true,
                    'status': true,
                  }).then((value) {
                    // subirfotoportada(value.id);
                    FirebaseFirestore.instance.collection('Paquetes').add({
                      'token': token,
                      'id_usuario': value.id,
                      'estadosoli': false,
                      'estado': false,
                      'estadoPrueba': false,
                      'suscripcion': false,
                      'prueba': '',
                      'estadoPublicar': true,
                      'paquete1': '',
                      'paquete2': '',
                      'paquete3': '',
                    });
                  }).then((value) async {
                    if ('google' == 'google') {
                      await usuarioS.obtenerUsuario(user.email, passCtrl.text);
                    } else {
                      var data = await FirebaseFirestore.instance
                          .collection("Usuarios")
                          .where('correo', isEqualTo: user.email)
                          .get();

                      print(data.docs[0].data().toString());
                      if (data == null) return null;
                      print(data.docs[0].data()['contrasenia'].toString());
                      data.docs.forEach((element) {
                        prefs.id = element.id.toString();
                        prefs.usuario = json.encode(element.data());
                        return element.data();
                      });
                    }
                    page = 2;
                    Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade, child: Home()),
                        (route) => false);
                    _alertaFinal(context, 'BIENVENIDO');
                  });
                }
              });
            }

            // return null;

          }
        }).then((value) => Navigator.pop(context)));
  }

  Future actualizarContrasenia() async {
    await FirebaseFirestore.instance
        .collection("Usuarios")
        .where('correo', isEqualTo: userCtrl.text)
        .get()
        .then((value) {
      FirebaseFirestore.instance
          .collection('Usuarios')
          .doc(value.docs.first.id)
          .update({'contrasenia': passCtrl.text});
      setState(() {});
    });
  }

  Future _loginIcloud() async {
    var _res;
    final AuthorizationResult result = await AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
    ]);
    print('RESULT');
    print(result);
    switch (result.status) {
      case AuthorizationStatus.authorized:
        try {
          Future(() => loaderDialogNormal(context)).then((v) {});
          print("successfull sign in");
          final AppleIdCredential appleIdCredential = result.credential;
          OAuthProvider oAuthProvider = new OAuthProvider("apple.com");
          final AuthCredential credential = oAuthProvider.credential(
            idToken: String.fromCharCodes(appleIdCredential.identityToken),
            accessToken:
                String.fromCharCodes(appleIdCredential.authorizationCode),
          );
          _res = await FirebaseAuth.instance.signInWithCredential(credential);
          Navigator.pop(context);
        } catch (e) {
          print("error");
        }
        break;
      case AuthorizationStatus.error:
        print('User auth error');
        break;
      case AuthorizationStatus.cancelled:
        print('User cancelled');
        break;
    }
    return _res;
  }
}
