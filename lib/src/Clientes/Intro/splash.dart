import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/animations/brochaanimation.dart';
import 'package:tl_app/animations/splashanimation.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/splash/byvinary.dart';
import 'package:tl_app/widgets/splash/esquina.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State<Splash> {
  var firstStateEnabled = true;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: new Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("lib/assets/images/splash/fondo.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
                child: Center(
                    child: FlareActor("lib/assets/animations/2lookfinal.flr",
                        alignment: Alignment.center,
                        fit: BoxFit.contain,
                        animation: "2lookaparece"))),
            BrochaAnimation(
              widgetBrocha: Center(
                child: Image.asset(
                  'lib/assets/images/splash/brocha.png',
                  width: getProportionateScreenWidth(150.0),
                  height: getProportionateScreenHeight(150.0),
                ),
              ),
            ),
            Container(
              child: Positioned(
                top: getProportionateScreenHeight(510),
                left: getProportionateScreenHeight(160),
                child: BrochaAnimation(
                  widgetBrocha: Esquina(),
                ),
              ),
            ),
            SplashAnimation(
              widgetSplash: Align(
                alignment: Alignment(0, .9),
                child: ByVinary(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
