import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/src/Clientes/Intro/importante.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/registro/icono.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/validation.dart';

class CrearCuenta extends StatefulWidget {
  @override
  _CrearCuentaState createState() => new _CrearCuentaState();
}

class _CrearCuentaState extends State<CrearCuenta> {
  final userCtrl = TextEditingController();
  final passCtrl = TextEditingController();
  final correoCtrl = TextEditingController();
  final passConfirmCtrl = TextEditingController();
  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Stack(children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("lib/assets/images/splash/fondo.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            BotonAtras(),
            Container(
              height: MediaQuery.of(context).size.height * .965,
              child: Column(children: <Widget>[
                Container(
                    child: Column(children: <Widget>[
                  SizedBox(
                    height: 150,
                  ),
                  IconoRegistro(),
                  SizedBox(
                    height: 20,
                  ),
                  Material(
                      type: MaterialType.transparency,
                      child: Text("CREAR CUENTA",
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(17),
                            fontWeight: FontWeight.normal,
                          )))
                ])),
                SizedBox(
                  height: 20,
                ),
                Container(
                  child: Material(
                    type: MaterialType.transparency,
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InputCustomTextField(
                          isPassword: false,
                          keyboardType: TextInputType.text,
                          placeholder: "Ingrese su nombre de usuario",
                          textController: userCtrl,
                          label: "NOMBRE DE USUARIO",
                        ),
                        InputCustomTextField(
                          isPassword: false,
                          keyboardType: TextInputType.emailAddress,
                          placeholder: "Ingrese su correo",
                          textController: correoCtrl,
                          label: "CORREO",
                        ),
                        InputCustomTextField(
                          isPassword: true,
                          keyboardType: TextInputType.text,
                          placeholder: "Ingrese su contraseña",
                          textController: passCtrl,
                          label: "CONTRASEÑA",
                        ),
                        InputCustomTextField(
                          isPassword: true,
                          keyboardType: TextInputType.text,
                          placeholder: "Ingrese su contraseña",
                          textController: passConfirmCtrl,
                          label: "CONFIRMAR CONTRASEÑA",
                        ),
                      ],
                    )),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                ButtonGray(
                  textoBoton: "SIGUIENTE",
                  bFuncion: () {
                    _funcionSiguien();
                  },
                  anchoBoton: getProportionateScreenHeight(300),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.w400,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
                Container(),
              ]),
            ),
          ]),
        )),
      ),
    );
  }

  void _funcionSiguien() async {
    final token = await _fcm.getToken();
    final usuarioS = new UsuarioServices();
    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());

    if (userCtrl.text == null || userCtrl.text == '') {
      Alerta(context, '!Alerta',
          'Por favor ingresa el nombre de usuario para continuar');
    } else if (correoCtrl.text == null || correoCtrl.text == '') {
      Alerta(context, '!Alerta',
          'Por favor ingresa el correo de usuario para continuar');
    } else if (passCtrl.text == null || passCtrl.text == '') {
      Alerta(context, '!Alerta',
          'Por favor ingresa el contraseña de usuario para continuar');
    } else if (passConfirmCtrl.text == null || passConfirmCtrl.text == '') {
      Alerta(context, '!Alerta',
          'Por favor ingresa la confirmacion de la contraseña de usuario para continuar');
    } else if (passCtrl.text == null || passConfirmCtrl.text == '') {
      Alerta(context, '!Alerta',
          'Las contraseñas no son compatibles, por favor verificalas');
    } else {
      // Navigator.pushReplacement(
      //     context,
      //     PageTransition(
      //         type: PageTransitionType.fade,
      //         child: Importante(
      //             usuario: userCtrl.text,
      //             correo: correoCtrl.text,
      //             contrasenia: passCtrl.text,
      //             tipo:'mail')));

      new Future(() => loaderDialogNormal(context)).then((value) => usuarioS
              .nuevoUsuario(correoCtrl.text, passCtrl.text, userCtrl.text)
              .then((v) {
            if (v['mensaje'] == 'EMAIL_EXISTS') {
              _alertaFinal(context,
                  'El usuario con este email ya existe por favor inicia sesión.');
            } else {
              FirebaseFirestore.instance.collection('Usuarios').add({
                'nombre': userCtrl.text,
                'correo': correoCtrl.text,
                'correo2': correoCtrl.text,
                'contrasenia': passCtrl.text,
                'createda_at': DateTime.now().year.toString() +
                    "-" +
                    DateTime.now().month.toString() +
                    "" +
                    DateTime.now().day.toString(),
                'celular': '',
                'cedula': '',
                'token': token,
                'created_at': date,
                'fotografia_perfil': '',
                'tipo': 'usuario',
                'estrellas': 3,
                'estado': true,
                'bloqueado': false,
                'tipoLogin': 'mail',
                'verif_celular': false,
                'verif_cedula': false,
                'verif_correo': true,
                'status': true,
              }).then((value) {
                // subirfotoportada(value.id);
                FirebaseFirestore.instance.collection('Paquetes').add({
                  'token': token,
                  'id_usuario': value.id,
                  'estadosoli': false,
                  'estado': false,
                  'estadoPrueba': false,
                  'suscripcion': false,
                  'estadoPublicar': true,
                  'prueba': '',
                  'paquete1': '',
                  'paquete2': '',
                  'paquete3': '',
                });
              }).then((value) async {
                if ('mail' == 'mail') {
                  await usuarioS.obtenerUsuario(correoCtrl.text, passCtrl.text);
                } else {
                  var data = await FirebaseFirestore.instance
                      .collection("Usuarios")
                      .where('correo', isEqualTo: correoCtrl.text)
                      .get();

                  print(data.docs[0].data().toString());
                  if (data == null) return null;
                  print(data.docs[0].data()['contrasenia'].toString());
                  data.docs.forEach((element) {
                    prefs.id = element.id.toString();
                    prefs.usuario = json.encode(element.data());
                    return element.data();
                  });
                }
                page = 2;
                Navigator.pushAndRemoveUntil(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade, child: Home()),
                    (route) => false);
                _alertaFinal(context, 'BIENVENIDO');
              });
            }
          }));
    }
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
}

Alerta(context, titulo, mensaje) {
  showDialog(
    context: context,
    child: AlertDialog(
      title: Text(titulo),
      content: Text(mensaje),
      actions: <Widget>[
        OutlineButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text("Ok"),
        ),
      ],
    ),
  );
}
