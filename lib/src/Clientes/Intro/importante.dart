import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/checkbox.dart';

import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/registro/icono.dart';
import 'package:tl_app/widgets/size_config.dart';

class Importante extends StatefulWidget {
  final foto;
  final usuario;
  final correo;
  final tipo;
  final contrasenia;

  const Importante(
      {Key key,
      this.tipo,
      this.usuario,
      this.correo,
      this.contrasenia,
      this.foto})
      : super(key: key);
  @override
  _ImportanteState createState() => new _ImportanteState();
}

class _ImportanteState extends State<Importante> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  File _image;
  bool aceptado = false;

  @override
  void initState() {
    super.initState();
    print(widget.usuario);
    print(widget.correo);
    print(widget.contrasenia);
  }

  final celCtrl = TextEditingController();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("lib/assets/images/splash/fondo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: BotonAtras()),
              Container(
                height: MediaQuery.of(context).size.height * 1.01,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(),
                    Container(
                      child: Material(
                        type: MaterialType.transparency,
                        child: Text("IMPORTANTE",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            )),
                      ),
                    ),
                    Container(
                      child: Material(
                        type: MaterialType.transparency,
                        child: Container(

                            //margin: EdgeInsets.only(bottom: 7.0, top: 7),
                            child: InputCustomTextField(
                          label: "Celular",
                          placeholder: "Ingresa tu número celular",
                          textController: celCtrl,
                          isPassword: false,
                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                        )),
                      ),
                    ),
                    Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Material(
                              type: MaterialType.transparency,
                              child: Text("CÉDULA",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(11),
                                    fontWeight: FontWeight.normal,
                                  ))),
                          SizedBox(
                            height: getProportionateScreenHeight(30),
                          ),
                          Container(
                            child: GestureDetector(
                                onTap: () {
                                  _showPickOptions(context);
                                },
                                child: (_image == null)
                                    ? Camara()
                                    : Image.file(_image)),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        child: Text(
                          'He leído y acepta todo lo detallado en los términos y condiciones de la aplicación',
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: (13),
                            fontWeight: FontWeight.normal,
                          ),
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: CheckBoxWidget(
                        onpresscheck: (value) {
                          print(value);
                          aceptado = value;
                        },
                      ),
                    ),
                    Container(
                      child: ButtonGray(
                        textoBoton: "SIGUIENTE",
                        bFuncion: () {
                          _botonSiguiente();
                        },
                        anchoBoton: getProportionateScreenHeight(330),
                        largoBoton: getProportionateScreenWidth(27),
                        colorTextoBoton: textColorClaro,
                        opacityBoton: 1,
                        sizeTextBoton: getProportionateScreenWidth(15),
                        weightBoton: FontWeight.w400,
                        color1Boton: primaryColor,
                        color2Boton: primaryColor,
                      ),
                    ),
                    Container(),
                  ],
                ),
              )
            ],
          ),
        )),
      ),
    );
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);
        } else {}
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 9.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  void _botonSiguiente() async {
    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
    final token = await _fcm.getToken();
    // print('TokenTelefono'+token);
    final usuarioS = new UsuarioServices();
    if (celCtrl.text == null || celCtrl.text == '') {
      _alertaFinal(context,
          'Por favor ingresa el numero de celular de usuario para continuar');
    } else if (_image == null) {
      _alertaFinal(
          context, 'Por favor ingresa la foto de la cédula para la validacion');
    } else if (aceptado == null || aceptado == false) {
      _alertaFinal(context, 'Por favor acepte los términos y condiciones');
    } else {
      new Future(() => loaderDialogNormal(context)).then((value) => usuarioS
              .nuevoUsuario(widget.correo, widget.contrasenia, widget.usuario)
              .then((v) {
            if (v['mensaje'] == 'EMAIL_EXISTS') {
              _alertaFinal(context,
                  'El usuario con este email ya existe por favor inicia sesión.');
            } else {
              FirebaseFirestore.instance.collection('Usuarios').add({
                'nombre': widget.usuario,
                'correo': widget.correo,
                'correo2': widget.correo,
                'contrasenia': widget.contrasenia,
                'celular': celCtrl.text,
                'token': token,
                'created_at': date,
                'fotografia_perfil': widget.foto,
                'tipo': 'usuario',
                'estrellas': 3,
                'estado': true,
                'bloqueado': false,
                'tipoLogin': widget.tipo,
                'verif_celular': false,
                'verif_cedula': false,
                'verif_correo': true,
                'status': true,
              }).then((value) {
                subirfotoportada(value.id);
                FirebaseFirestore.instance.collection('Paquetes').add({
                  'token': token,
                  'id_usuario': value.id,
                  'estadosoli': false,
                  'estado': false,
                  'estadoPrueba': false,
                  'suscripcion':false,
                  'prueba': '',
                  'paquete1': '',
                  'paquete2': '',
                  'paquete3': '',
                });
              }).then((value) async {
                if (widget.tipo == 'mail') {
                  await usuarioS.obtenerUsuario(
                      widget.correo, widget.contrasenia);
                } else {
                  var data = await FirebaseFirestore.instance
                      .collection("Usuarios")
                      .where('correo', isEqualTo: widget.correo)
                      .get();

                  print(data.docs[0].data().toString());
                  if (data == null) return null;
                  print(data.docs[0].data()['contrasenia'].toString());
                  data.docs.forEach((element) {
                    prefs.id = element.id.toString();
                    prefs.usuario = json.encode(element.data());
                    return element.data();
                  });
                }
                page = 2;
                Navigator.pushAndRemoveUntil(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade, child: Home()),
                    (route) => false);
                _alertaFinal(context, 'BIENVENIDO');
              });
            }
          }));

      //
    }
  }

  void subirfotoportada(id) async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(id)
        .update({'cedula': url});
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
