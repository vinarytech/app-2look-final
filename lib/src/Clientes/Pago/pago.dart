import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarProblema.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/Pago/tarjetaspago.dart';
import 'package:tl_app/widgets/Pago/tipopago.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:tl_app/widgets/pago/tarjetasInfo.dart';
import 'package:tl_app/widgets/pago/total.dart';
import 'package:tl_app/widgets/size_config.dart';

class Pago extends StatefulWidget {
  @override
  _PagoState createState() => new _PagoState();
}

class _PagoState extends State<Pago> {
  final searchCtrl = TextEditingController();
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: Home()));
          page = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  _children[page],
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TarjetasInfo(
                              titulo: "SERVICIO / PELUQUERÍA",
                              descripcion:
                                  "Corte de cabello para hombre y mujeres",
                              valor: "\$20.00",
                            ),
                            TarjetasInfo(
                              titulo: "PRODUCTO / PELUQUERÍA",
                              descripcion:
                                  "Producto 100% original, a excelente precio, \nse entrega en todas las zonas de Quito",
                              valor: "\$3.50",
                            ),
                            Total(),
                            TipoPago(),
                            TarjetasPago(),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 12.0, right: 20, left: 20),
                              child: Container(
                                width: getProportionateScreenWidth(370),
                                child: InputIconCustomTextField(
                                  icon: Icon(
                                    Icons.credit_card,
                                    color: Colors.white,
                                    size: 20.0,
                                  ),
                                  isPassword: false,
                                  keyboardType: TextInputType.text,
                                  label: "Número de Tarjeta",
                                  placeholder: "...",
                                  textController: searchCtrl,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12.0, left: 20),
                                  child: Container(
                                    width: getProportionateScreenWidth(150),
                                    child: InputIconCustomTextField(
                                      icon: Icon(
                                        Icons.credit_card,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                      isPassword: false,
                                      keyboardType: TextInputType.text,
                                      label: "Vencimiento",
                                      placeholder: "...",
                                      textController: searchCtrl,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12.0, right: 20),
                                  child: Container(
                                    width: getProportionateScreenWidth(190),
                                    child: InputIconCustomTextField(
                                      icon: Icon(
                                        Icons.credit_card,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                      isPassword: false,
                                      keyboardType: TextInputType.text,
                                      label: "Codigo de Seguridad",
                                      placeholder: "...",
                                      textController: searchCtrl,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 28.0),
                              child: Row(
                                children: <Widget>[
                                  IconoSeguridad(),
                                  Text(
                                    "  Tu información de pago es guardada de forma segura",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(11),
                                      fontWeight: FontWeight.normal,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: ButtonGray(
                                textoBoton: "PAGAR",
                                bFuncion: () {
                                  Navigator.pushReplacement(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.fade,
                                          child: FinalizarProblema()));
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.w400,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                          BotonAtras()
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
}
