import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarReserva.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/size_config.dart';

class FinalizarProblema extends StatefulWidget {
  FinalizarProblema({Key key}) : super(key: key);

  @override
  _FinalizarProblemaState createState() => _FinalizarProblemaState();
}

class _FinalizarProblemaState extends State<FinalizarProblema> {
  @override
  Widget build(BuildContext context) {
    final userCtrl = TextEditingController();
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
          onPanDown: (_) {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 95.0, bottom: 10),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment
                                        .center, //Center Column contents vertically,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center, //Center Column contents horizontally,
                                    children: <Widget>[
                                      Hero(
                                        tag: "Hero",
                                        child: CardHorizontal(
                                          nameImage:
                                              "lib/assets/images/card/I1.png",
                                          bFunction: () {
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType.fade,
                                                    child: DentroProducto()));
                                          },
                                        ),
                                      ),
                                      Hero(
                                        tag: "Hero1",
                                        child: CardHorizontal(
                                          nameImage:
                                              "lib/assets/images/card/I2.png",
                                          bFunction: () {
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                    type: PageTransitionType.fade,
                                                    child: DentroServicio()));
                                          },
                                        ),
                                      ),
                                    ],
                                  )),
                              TipoUbicacion(),
                              SizedBox(
                                height: 20,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Container(
                                    child: Column(
                                  children: [
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left: getProportionateScreenWidth(
                                                    20),
                                              ),
                                              child: Text("FECHA DE ENTREGA",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    DropDownCustomed(text: "20/07/2020")
                                  ],
                                )),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      DateTimePickerCustom(),
                                      Text("a",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.normal,
                                          )),
                                      DateTimePickerCustom(),
                                      Icon(
                                        Icons.add_circle,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left: getProportionateScreenWidth(8),
                                      ),
                                      child: Text(
                                        "DIRECCIÓN",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(17),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: getProportionateScreenWidth(370),
                                      child: InputCustomTextField(
                                        isPassword: false,
                                        keyboardType: TextInputType.text,
                                        label: "Direccón",
                                        placeholder: "...",
                                        textController: userCtrl,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                decoration: BoxDecoration(boxShadow: [
                                  BoxShadow(
                                      color: Colors.black26,
                                      offset: Offset(0, 2.0),
                                      blurRadius: 6.0)
                                ]),
                                child: Image(
                                  image: AssetImage(
                                      "lib/assets/images/finalizar/mapa.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: ButtonGray(
                                  textoBoton: "FINALIZADO",
                                  bFuncion: () {
                                    Navigator.pushReplacement(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: FinalizarReserva()));
                                  },
                                  anchoBoton: getProportionateScreenHeight(350),
                                  largoBoton: getProportionateScreenWidth(28),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton: getProportionateScreenWidth(11),
                                  weightBoton: FontWeight.w400,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Text(
                                  "REPORTAR PROBLEMA",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(17),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                  ],
                ))),
      ),
    );
  }
}
