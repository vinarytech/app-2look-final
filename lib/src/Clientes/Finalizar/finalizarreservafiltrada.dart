import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import  'package:flutter/src/widgets/basic.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Alertas/alertaCalificacion.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/src/mapa_page.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:loading/loading.dart';
import 'package:url_launcher/url_launcher.dart';


class FinalizarReservaFiltrada extends StatefulWidget {
   final InformacionF snapshotf;
   final int cantidad;
  FinalizarReservaFiltrada({Key key, this.snapshotf, this.cantidad}) : super(key: key);
  
  @override
  _FinalizarReservaFiltradaState createState() => _FinalizarReservaFiltradaState();
}

class _FinalizarReservaFiltradaState extends State<FinalizarReservaFiltrada> {
   final prefs = PreferenciasUsuario();
   final convert = new NumberFormat("#,##0.00", "en_US");
  String fechaescogida;
  String horainicioescogida;
  String horafinescogida;
  DateTime horainiciodt;
  List<Horario> horarios=[];
  bool selected1 = false;
  String ubicacion;
  bool valor2=false;
  Ubicacion direcciondomicilio;
  var valor;
  Future<List> futuro;
  GoogleMapController _controller;

  @override
  void initState()  { 
    super.initState();
    futuro=_obtener(widget.snapshotf.documento.id);
   
  }
  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

   Future<List> _obtener(String id) async {
   List resultado=[]; 
    await FirebaseFirestore.instance
        .collection("Ubicaciones")
        .where('id_servicio', isEqualTo: id)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
       
       if(result.data()["establecimiento"]==true &&result.data()["domicilio"]==false){
       resultado=["establecimiento",result.data()["establecimiento_lt"],result.data()["establecimiento_lng"],result.data()["establecimiento_dir"]];
       }
       if(result.data()["domicilio"]==true &&result.data()["establecimiento"]==false){
       resultado=["domicilio"];
       }
       if(result.data()["domicilio"]==true &&result.data()["establecimiento"]==true){
       resultado=["union",result.data()["establecimiento_lt"],result.data()["establecimiento_lng"],result.data()["establecimiento_dir"]];
       }


       print('resultado $resultado');
      });
    });
    return resultado;
  }
  @override
  Widget build(BuildContext context) {
    final userCtrl = TextEditingController();
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
              body: FutureBuilder(
          future: futuro,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            //  print('snapshot pasar $snapshotpasar');
            if (snapshot.hasData) {
              return SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 95.0, bottom: 10),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment
                                        .center, //Center Column contents vertically,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center, //Center Column contents horizontally,
                                    children: <Widget>[
                                     carddetalle()
                                      
                                    ],
                                  )),
                            /* 
                            TipoUbicacion(
                                onpress: (value){
                                  print('mi valorcito $value');
                                },
                                lista: snapshot.data,),*/
                         snapshot.data[0]=="domicilio"?  Container(
                             child:Column(
                               children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: getProportionateScreenWidth(20),
                                      ),
                                      child: Text("UBICACIÓN",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize: getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.bold,
                                          )),
                                    )),
                                  Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left: getProportionateScreenWidth(20),
                                          ),
                                          child: Text("Domicilio",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize: getProportionateScreenWidth(14),
                                                fontWeight: FontWeight.normal,
                                              )),
                                        )),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 35.0),
                                    child:  CircularCheckBox(
                                    value: this.selected1,
                                    checkColor: textColorMedio,
                                    activeColor: primaryColor,
                                    inactiveColor: textColorMedio,
                                    disabledColor: Colors.grey,
                                    onChanged: (val) async { 
                                     
                                          this.selected1 = !this.selected1;
                                      if(this.selected1){
                                     direcciondomicilio = await Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: MapPage()));
                                      if(direcciondomicilio==null){
                                        this.selected1=false;
                                         ubicacion=null;
                                      }else{
                                        this.selected1 =true;
                                        setState(() {
                                          ubicacion = 'domicilio';
                                        });
                                      }
                                   }else{
                                      
                                     setState(() {
                                       this.selected1 = false;
                                      ubicacion=null;
                                     });
                                   }
                                    } 
                                    )   
                                  )
                                ],
                              ),
                               ],
                             )
                            ):SizedBox(),
                           snapshot.data[0]=="establecimiento"?Container(
                             child:Column(
                               children: [
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Padding(
                                      padding: EdgeInsets.only(
                                        left: getProportionateScreenWidth(20),
                                      ),
                                      child: Text("UBICACIÓN",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize: getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.bold,
                                          )),
                                    )),
                                  Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left: getProportionateScreenWidth(20),
                                          ),
                                          child: Text("En establecimiento",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize: getProportionateScreenWidth(14),
                                                fontWeight: FontWeight.normal,
                                              )),
                                        )),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 35.0),
                                    child:  CircularCheckBox(
                                    value: this.selected1,
                                    checkColor: textColorMedio,
                                    activeColor: primaryColor,
                                    inactiveColor: textColorMedio,
                                    disabledColor: Colors.grey,
                                    onChanged: (val) {
                                      this.selected1 = !this.selected1;
                                      if (this.selected1) {
                                      setState(() {
                                         ubicacion='establecimiento';
                                       print(ubicacion);
                                      });
                                      }
                                       else{ setState(() {
                                       this.selected1 = false;
                                      ubicacion=null;
                                     });
                                   }
                                    } 
                                          
                                    )
                                  )
                                ],
                              ),
                               ],
                             )
                            ):SizedBox(),
                           snapshot.data[0]=="union"?     TipoUbicacionRadio(
                                  titulo: 'UBICACION',
                                   tipo1: 'En establecimiento',
                                    tipo2: 'Domicilio',
                                    seleccion: valor2 ,
                                   retorno1: (value1) {
                                     print(value1);
                                    setState(() {
                                      if (value1) {
                                        ubicacion = 'establecimiento';
                                        print(ubicacion);
                                        valor2=false;
                                      }else{
                                        ubicacion=null;
                                      }
                                    });
                                  },
                                  retorno2: (value2) async {
                                   print(value2);
                                   if(value2){
                                     direcciondomicilio = await Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: MapPage()));
                                      if(direcciondomicilio==null){
                                        valor2=false;
                                        ubicacion=null;
                                        setState(() {
                                          
                                        });
                                      }else{
                                        valor2=value2;
                                        setState(() {
                                          ubicacion = 'domicilio';
                                        });
                                      }
                                   }else{
                                      valor2=value2;
                                      ubicacion=null;
                                     setState(() {
                                     });
                                   }
                                  },
                                ):SizedBox(),
                              SizedBox(
                                height: 20,
                              ),
                         ubicacion=='establecimiento'? Container(
                                height: 200,
                                width: double.infinity,
                                child: GoogleMap(
                                    onTap: (la) {
                                            print('asqui' + la.toString());
                                            _launchMapsUrl(
                                                double.parse(snapshot.data[1]),
                                                double.parse(snapshot.data[2]));
                                          },
                                markers: Set<Marker>.of([
                                  Marker(
                                      markerId: MarkerId('SomeId'),
                                      position: LatLng(double.parse(snapshot.data[1]),double.parse(snapshot.data[2])),
                                      infoWindow: InfoWindow(
                                        
                                        //anchor: Offset(0.5,1),
                                      title: widget.snapshotf.doc.data()["nombre"]
                                      )
                                  )
                                ]),
                                mapType: MapType.normal,
                                myLocationEnabled: true,
                                onMapCreated: _onMapCreated,
                                initialCameraPosition:
                                    CameraPosition(target: LatLng(double.parse(snapshot.data[1]),double.parse(snapshot.data[2])), zoom: 15)),
                              ):SizedBox(),
                              ubicacion!=null? Padding(
                                padding: const EdgeInsets.only(top: 1.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left: getProportionateScreenWidth(8),
                                      ),
                                      child: Text(
                                        "DIRECCIÓN",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(17),
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(
                                        left: getProportionateScreenWidth(10),
                                        top:10,
                                        bottom: 10),
                                      child: Container(
                                        width: getProportionateScreenWidth(370),
                                        child: 
                                      ubicacion=='establecimiento'?
                                     /* Text(
                                          snapshot.data[3],style: TextStyle( color: textColorClaro,fontSize: getProportionateScreenWidth(12),
                                          ),
                                        )*/
                                        Container(
                                         
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: textColorClaro
                                            ),
                                            borderRadius: BorderRadius.circular(50),
                                          // color: Colors.transparent
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(6.0),
                                            child: Text(
                                            snapshot.data[3],style: TextStyle( color: textColorClaro,fontSize: getProportionateScreenWidth(12),
                                            ),
                                        ),
                                          )
                                        )
                                        :
                                        Text(
                                          direcciondomicilio.direccion,style: 
                                          TextStyle( color: textColorClaro,fontSize: getProportionateScreenWidth(12),
                                          ),
                                        )
                                      ),
                                    ),
                                  ],
                                ),
                              ):SizedBox(),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Container(
                                    child: Column(
                                  children: [
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left: getProportionateScreenWidth(
                                                    20),
                                              ),
                                              child: Text("FECHA DE ENTREGA",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left:15.0, right: 15.0),
                                      child: Container(
                                        width: double.infinity,
                                        child: FlatButton(
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(18.0),
                                               ),
                                              color: Color(0xff707070),
                                           
                                              padding: EdgeInsets.all(8.0),
                                              onPressed: () {
                                                 fecha();
                                                 },
                                              child: 
                                              fechaescogida==null?Text(
                                                "Ej. 20/07/2020", 
                                                style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(18),
                                              fontWeight: FontWeight.normal,
                                            )
                                              ):Text(
                                                fechaescogida, 
                                                style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(18),
                                              fontWeight: FontWeight.normal,
                                            )
                                              ),
                                            ),
                                      ),
                                    ),
                                    
                                  ],
                                )),
                              ),
                            //listahoraswidget(),
                             
                             /*horarios.length!=0? Column(
                               children: List.generate(horarios.length, (index) {
                                  return 
                                  listahoraswidget(horarios[index]);                        
                               })
                             ):hora()
                             ,*/
                            horarios.length==0? hora()
                            :Column(
                              children: List.generate(horarios.length, (index) 
                              {
                               return listahoraswidget(horarios[index], index);
                              }
                              ),
                            ),
                            (horarios.length>0 && horarios.length<2)? hora():SizedBox(),
                             
                             
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: ButtonGray(
                                  textoBoton: "SOLICITAR",
                                  bFuncion: () {

                                    //Aqui
                                      _publicar(snapshot.data);

                                 /*   showDialog(
                                        context: context,
                                        builder: (context) => AlertaCalificacionCustom(
                                            titulo: "Califica tu experiencia con el profesional",
                                              buttonText: "ENVIAR",
                                              descripcion:
                                                  "Esta es la descripcion",
                                            ));*/
                                  },
                                  anchoBoton: getProportionateScreenHeight(350),
                                  largoBoton: getProportionateScreenWidth(28),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton: getProportionateScreenWidth(11),
                                  weightBoton: FontWeight.w400,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                  ],
                ));
            }
            return Center(child: CircularProgressIndicator(),);
          }
        ),
      ),
    );
           
  }

horainicio(){

    DatePicker.showTime12hPicker(
                                                
      context, 
      showTitleActions: true, 
      locale: LocaleType.es,
    // currentTime: ,
      //minTime: fechatimehoy.add(Duration(days: 1)) ,

      onChanged: (date) {
      print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
    }, onConfirm: (date) {
      print('confirm $date');
    String fechartorno = DateFormat('h:mm a').format(date);
    // String horaretorno = DateFormat('hh:mm a').format(date);
                                                
      setState(() {
        horainicioescogida=fechartorno;
        horainiciodt=date;
      });
    }, );
  }
  horaFin(){

    DatePicker.showTime12hPicker(
                                                
      context, 
      showTitleActions: true, 
      locale: LocaleType.es,
      
    // currentTime: ,
      //minTime: fechatimehoy.add(Duration(days: 1)) ,

      onChanged: (date) {
      print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
    }, onConfirm: (date) {
      print('confirm $date');
    String fechartorno = DateFormat('h:mm a').format(date);
    print(fechartorno);
    // String horaretorno = DateFormat('hh:mm a').format(date);
      print('horainiciodt $horainiciodt');      
      if(horainiciodt==null){
        showAlertDialogHora(context);
      }
      
      final differencehoras = date.difference(horainiciodt).inHours;  
      print('difference $differencehoras');  

      if(differencehoras>=1){
        setState(() {
         horafinescogida=fechartorno;
         

      });
      }else{
        showAlertDialog(context);
      }

      
    }, );
  }
  fecha(){
   DateTime now = DateTime.now();
      String fechahoy = DateFormat('yyyy-MM-dd').format(now);  
      print(fechahoy);
      var fechatimehoy = DateTime.parse(fechahoy);
      print(fechatimehoy);
      DatePicker.showDatePicker(
                                                
        context, 
         showTitleActions: true, 
        locale: LocaleType.es,
      // currentTime: ,
        minTime: fechatimehoy.add(Duration(days: 1)) ,
         onChanged: (date) {
        print('change $date in time zone ' + date.timeZoneOffset.inHours.toString());
      }, onConfirm: (date) {
         print('confirm $date');
      String fechartorno = DateFormat('dd/MM/yyyy').format(date);
      // String horaretorno = DateFormat('hh:mm a').format(date);
                                                
        setState(() {
          fechaescogida=fechartorno;
         });
      }, );
  }

  void _launchMapsUrl(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  
  showAlertDialog(BuildContext context) {

  // set up the button
  Widget okButton = FlatButton(
    child: Text("Entendido"),
    onPressed: () { 
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Recordatorio"),
    content: Text("Recuerda seleccionar con 1 hora de adelanto"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

showAlertDialogHora(BuildContext context) {

  // set up the button
  Widget okButton = FlatButton(
    child: Text("Entendido"),
    onPressed: () { 
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Recordatorio"),
    content: Text("Recuerda seleccionar la hora de inicio y luego la hora de finalización"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
Widget listahoraswidget(Horario lista, int index){
 return  Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Container(
                                child: Row(
                                  
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                      width: getProportionateScreenWidth(140),
                                      height: getProportionateScreenWidth(30),
                                      child: FlatButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18.0),
                                           ),
                                          color: Color(0xff707070),
                                       
                                          padding: EdgeInsets.all(2.0),
                                          onPressed: null,
                                          child: 
                                         Text(
                                            lista.horaInicio, 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ),
                                        ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left:3.0, right: 3.0),
                                      child: Text("a",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ),
                                    Container(
                                      width: getProportionateScreenWidth(140),
                                      height: getProportionateScreenWidth(30),
                                      child: FlatButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18.0),
                                           ),
                                          color: Color(0xff707070),
                                       
                                          padding: EdgeInsets.all(2.0),
                                          onPressed: null,
                                          child: 
                                         Text(
                                            lista.horaFin, 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ),
                                        ),
                                    ),
                                    IconButton(icon: Icon(
                                      Icons.delete,
                                      color: Colors.white,
                                    ),
                                    onPressed: (){
                                      horarios.removeAt(index);
                                      setState(() {
                                        
                                      });
                                    },
                                    )
                                    
                                  ],
                                ),
                              ),
                            );
}
Widget hora(){
 return  Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Container(
                                child: Row(
                                  
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Container(
                                      width: getProportionateScreenWidth(140),
                                      height: getProportionateScreenWidth(30),
                                      child: FlatButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18.0),
                                           ),
                                          color: Color(0xff707070),
                                       
                                          padding: EdgeInsets.all(2.0),
                                          onPressed: () {
                                             horainicio();
                                             },
                                          child: 
                                          horainicioescogida==null?Text(
                                            "Ej. 10:00 AM", 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ):Text(
                                            horainicioescogida, 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ),
                                        ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left:3.0, right: 3.0),
                                      child: Text("a",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ),
                                    Container(
                                      width: getProportionateScreenWidth(140),
                                      height: getProportionateScreenWidth(30),
                                      child: FlatButton(
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(18.0),
                                           ),
                                          color: Color(0xff707070),
                                       
                                          padding: EdgeInsets.all(2.0),
                                          onPressed: () {
                                             horaFin();
                                             },
                                          child: 
                                          horafinescogida==null?Text(
                                            "Ej. 10:00 AM", 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ):Text(
                                            horafinescogida, 
                                            style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(13),
                                          fontWeight: FontWeight.normal,
                                        )
                                          ),
                                        ),
                                    ),
                                 (horafinescogida!=null)?IconButton(icon: Icon(
                                      Icons.check_circle,
                                      color: Colors.white,
                                    ), onPressed: (){
                                     setState(() {
                                       horarios.add(Horario(
                                        horaInicio: horainicioescogida,
                                        horaFin: horafinescogida
                                      ));
                                      horainicioescogida=null;
                                      horafinescogida=null;
                                      print(horarios.length);
                                     });
                                    }
                                  ) 
                                  //horarios.length>=2? Icon(Icons.zoom_in, color: Colors.amber,)
                                  :SizedBox()
                           
                                  ],
                                ),
                              ),
                            );
}

Widget carddetalle(){
  return Padding(
                  padding: EdgeInsets.all(getProportionateScreenHeight(16)),
                  child: Container(
                    child: Material(
                        color: Colors.transparent,
                        child: Stack(
                          children: [
                            Container(
                              child: Container(
                                height: getProportionateScreenHeight(150),
                                width: getProportionateScreenWidth(350),
                                decoration: BoxDecoration(
                                  color: lightColor,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 2,
                                  child: Stack(
                                    children: [
                                      Container(
                                        height:
                                            getProportionateScreenHeight(150),
                                        decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                  widget.snapshotf.fotos[0]),
                                              fit: BoxFit.fill,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(20.0)),
                                      ),
                                     
                                    ],
                                  ),
                                ),
                                Expanded(
                                    flex: 3,
                                    child: Stack(
                                      children: [
                                        Container(
                                          height:
                                              getProportionateScreenHeight(150),
                                        ),
                                        Positioned(
                                            top: 10,
                                            left: 15,
                                            child: Column(
                                              children: [
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  width: 150,
                                                  child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text(
                                                        "${widget.snapshotf.documento.subcategoria.toUpperCase()} / " +
                                                            "${widget.snapshotf.documento.categoria.toUpperCase()}",
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  10),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )),
                                                  ),
                                                ),
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  width: 150,
                                                  child: Row(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        alignment:
                                                            Alignment.topLeft,
                                                        height: 30,
                                                        //width: 150,
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "${widget.snapshotf.doc.data()["nombre"]} / ${widget.snapshotf.doc.data()["estrellas"].toString()}",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Container(
                                                        alignment:
                                                            Alignment.topCenter,
                                                        child: Icon(
                                                          Icons.star,
                                                          size: 14,
                                                          color: primaryColor,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  height: 40,
                                                  width: 150,
                                                  child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text(
                                                        widget.snapshotf.documento.descripcion,
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  11),
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        )),
                                                  ),
                                                ),
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  width: 150,
                                                  child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text(
                                                        '${widget.snapshotf.documento.precio}'
                                                        r'$',
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  18),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )),
                                                  ),
                                                ),
                                              ],
                                            ))
                                      ],
                                    )),
                              ],
                            ),
                          ],
                        )),
                  ));
}

 void _alertaProducto(BuildContext context, String text, String estado) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                              
                              if(estado=='PENDIENTE'){
                             //  sendNotification('Pedido', '2Look');
                              Navigator.of(context).pop();
                              Navigator.pushReplacement(context,
                              PageTransition(type: PageTransitionType.fade, child: Home(index: 0,finalizado: false,)));
                              }else{
                             Navigator.of(context).pop();
                              }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _publicar(List lista) async {
   /* if (images.length == 0) {
      _alertaProducto(context, 'Agregue al menos una imagen');
    } else if (categoria == null || categoria == '') {
      _alertaProducto(context, 'No ha seleccionado una categoria');
    } else if ((generoM == null || generoM == '') &&
        (generoF == null || generoF == '')) {
      _alertaProducto(context, 'Seleccione al menos un genero');
    } else if (ciudad == null || ciudad == '') {
      _alertaProducto(context, 'No ha seleccionado una ciudad');
    } else if (sector == null || sector == '') {
      _alertaProducto(context, 'No ha seleccionado un sector');
    } else if (diasDisponibles == null || diasDisponibles == '') {
      _alertaProducto(context, 'No ha seleccionado los dias disponibles');
    } else if (domicilio == null || establecimiento == null) {
      _alertaProducto(context, 'Seleccione la ubicación');
    } else if (cancelacion == null || cancelacion == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else if (precioCtlr.text == null || precioCtlr.text == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else if (descripconCtlr.text == null || descripconCtlr.text == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else {
      new Future(() => loaderDialogNormal(context)).then((v) {
        FirebaseFirestore.instance.collection('Servicios').add({
          'categoria': categoria.toUpperCase(),
          'genero': genero,
          'ciudad': ciudad,
          'sectores': sector,
          'diasdisponible': diasDisponibles,
          'subcategoria': 'servicio',
          'horaInicio': horarioInicio,
          'horaFinal': horarioFinal,
          'ubicacion': ubicacion.direccion,
          'cancelacion': cancelacion,
          'precio': precioCtlr.text,
          'descripcion': descripconCtlr.text,
          'id_usuario': prefs.id
        }).then((v) {
          FirebaseFirestore.instance.collection('Ubicaciones').add({
            'id_servicio': v.id,
            'id_usuario': prefs.id,
            'domicilio': domicilio,
            'domicilio_dir': '',
            'domicilio_lng': '',
            'establecimiento': establecimiento,
            'establecimiento_dir': ubicacion.direccion,
            'establecimiento_lng': ubicacion.longitud,
            'establecimiento_lt': ubicacion.latitud,
          });
          subirImagenes(images, v.id);
          Navigator.pop(context);
        })
            .then((v) => Navigator.pop(context))
            .then((v) {
          _alertaProducto(context, 'Producto Publicado Exitosamente');
        });
      });
    }*/
   // print(direcciondomicilio.direccion);
       String direccion, latitud, longitud, tipo;

    if(widget.snapshotf.documento.subcategoria=='servicio'){
    /*  print('${widget.snapshot.documento.data()["subcategoria"]}');
    print(fechaescogida);
    print(horarios);*/
     if(ubicacion=='domicilio'){
         tipo='domicilio';
         direccion=direcciondomicilio.direccion;
         latitud=direcciondomicilio.latitud;
         longitud=direcciondomicilio.longitud;
       }
       if(ubicacion=='establecimiento'){
         tipo='establecimiento';
         direccion=lista[3];
         latitud=lista[1];
         longitud=lista[2];
       }
   

    if (direccion==null || direccion=='') {
      _alertaProducto(context, 'Seleccione una ubicación','');
    } else if (fechaescogida == null || fechaescogida == '') {
      _alertaProducto(context, 'Seleccione la fecha de entrega','');
    }else if (horarios == null || horarios.length==0) {
      _alertaProducto(context, 'Seleccione los horarios para la entrega','');

    }else{

      
    print(direccion);
    print(latitud);
    print(longitud);
    print(fechaescogida);
    print(horarios);
    int cantidad=1;
    double total= cantidad * double.parse(widget.snapshotf.documento.precio) ;
    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
    String notificacion='Se ha realizado una nueva reserva';
    print(total);
    Future(() => loaderDialogNormal(context)).then((v) {
        FirebaseFirestore.instance.collection('Solicitudes').add({
          'subcategoria': widget.snapshotf.documento.subcategoria,
          'tipo':tipo,
          'cantidad': cantidad,
          'total': total.toStringAsFixed(2),
          'idencargado':widget.snapshotf.documento.idusuario,
          'idservicio': widget.snapshotf.documento.id,
          'idsolicitante': prefs.id,
          'fecha':fechaescogida,
          'direccion': direccion,
          'latitud': latitud,
          'longitud': longitud,
          'estado': 'PENDIENTE',
          'estadosolicitud': true
        }).then((v) {
          for (var item in horarios) {
          FirebaseFirestore.instance.collection('Horarios').add({
            'id_solicitud': v.id,
            'hora_inicio':item.horaInicio,
            'hora_fin':item.horaFin
          });
          }
          
           
        }).then((value) => FirebaseFirestore.instance.collection('Notificaciones').add({
          'idemisor': prefs.id,
          'idreceptor':widget.snapshotf.documento.idusuario,
          'date': date,
          'notificacion': notificacion,
         // 'tokensolicitante': prefs.token,
         // 'remitente':'usuario',
          'accion':'solicitud'
          
        })).then((v) => Navigator.pop(context))
            .then((v) {
              sendNotification(notificacion, '2Look',widget.snapshotf.doc.data()["token"]);
          _alertaProducto(context, 'Listo, tu solicitud esta siendo atendida, te notificaremos cuando el profesional apruebe tu pedido','PENDIENTE');
        });
      });
      }
    }else{
      if(ubicacion=='domicilio'){
        tipo='domicilio';
         direccion=direcciondomicilio.direccion;
         latitud=direcciondomicilio.latitud;
         longitud=direcciondomicilio.longitud;
       }
       if(ubicacion=='establecimiento'){
         tipo='establecimiento';
         direccion=lista[3];
         latitud=lista[1];
         longitud=lista[2];
       }
   

    if (direccion==null || direccion=='') {
      _alertaProducto(context, 'Seleccione una ubicación','');
    } else if (fechaescogida == null || fechaescogida == '') {
      _alertaProducto(context, 'Seleccione la fecha de entrega','');
    }else if (horarios == null || horarios.length==0) {
      _alertaProducto(context, 'Seleccione los horarios para la entrega','');

    }else{

      
    print(direccion);
    print(latitud);
    print(longitud);
    print(fechaescogida);
    print(horarios);
    int cantidad=widget.cantidad;
    double total= cantidad * double.parse(widget.snapshotf.documento.precio) ;
      String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
    String notificacion='Se ha realizado una nueva reserva';
    print(total);
    Future(() => loaderDialogNormal(context)).then((v) {
        FirebaseFirestore.instance.collection('Solicitudes').add({
          'subcategoria': widget.snapshotf.documento.subcategoria,
          'tipo': tipo,
          'cantidad': cantidad,
          'total': total.toStringAsFixed(2),
          'idencargado':widget.snapshotf.documento.idusuario,
          'idservicio': widget.snapshotf.documento.id,
          'idsolicitante': prefs.id,
          'fecha':fechaescogida,
          'direccion': direccion,
          'latitud': latitud,
          'longitud': longitud,
          'estado': 'PENDIENTE',
          'estadosolicitud': true
        }).then((v) {
          for (var item in horarios) {
          FirebaseFirestore.instance.collection('Horarios').add({
            'id_solicitud': v.id,
            'hora_inicio':item.horaInicio,
            'hora_fin':item.horaFin
          });
            
          }
           
        }).then((value) => FirebaseFirestore.instance.collection('Notificaciones').add({
          'idemisor': prefs.id,
          'idreceptor':widget.snapshotf.documento.idusuario,
          'date': date,
          'notificacion': notificacion,
         // 'tokensolicitante': prefs.token,
         // 'remitente':'usuario',
          'accion':'solicitud'
        })).then((v) => Navigator.pop(context))
            .then((v) {
              sendNotification(notificacion, '2Look',widget.snapshotf.doc.data()["token"]);
          _alertaProducto(context, 'Listo, tu solicitud esta siendo atendida, te notificaremos cuando el profesional apruebe tu pedido','PENDIENTE');
        });
      });
      }

    
    }
  }
   loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
        Future<void> sendNotification(subject,title,token) async{
            final postUrl = 'https://fcm.googleapis.com/fcm/send';
            final data = {
            "notification": {"body":subject, "title":title},
            "priority": "high",
            "data": {
            "click_action": "FLUTTER_NOTIFICATION_CLICK",
            "title": title,
            "body": subject,
            },
            "to": token};

            final headers = {
            'content-type': 'application/json',
            'Authorization': 'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
            };

                final response = await http.post(postUrl,
                body: json.encode(data),
                encoding: Encoding.getByName('utf-8'),
                headers: headers);
                if (response.statusCode == 200) {
                print("true");
                } else {
                print("false");
                }
        }
}
class Horario{
String horaInicio;
String horaFin;
Horario({
this.horaInicio,
this.horaFin
});

}