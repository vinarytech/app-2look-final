import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/models/modelservicio.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/busqueda/filtrosIcon.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';

import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/yabusqueda/flechaabajo.dart';

import '../Home/busqueda.dart';
import '../Home/guardado.dart';
import '../Home/home.dart';
import '../Home/mensajes.dart';

class YaBusqueda extends StatefulWidget {
final String nombre;
final String valor;
final String recomendado;
final List<ServiciosF> listafiltrada;

  YaBusqueda({Key key, this.nombre, this.valor, this.listafiltrada, this.recomendado })
      : super(key: key);
  @override
  _YaBusquedaState createState() => new _YaBusquedaState();
}

class _YaBusquedaState extends State<YaBusqueda> {
  final searchCtrl = TextEditingController();

  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: Home()));
          page = index;
         
        });
      },
    );
    return SafeArea(
      child:  GestureDetector(
        onTap: () {
          controller.text = '';
          FocusScope.of(context).requestFocus(new FocusNode());
        },
              child: Scaffold(
            bottomNavigationBar: curvedNavigationBar,
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    _children[page],
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 45.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            height: 35,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 110.0),
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: getProportionateScreenWidth(310),
                              child: TextField(
                                            /*  onChanged: (value){
                                              valor=value;
                                            },*/
                                            onSubmitted: (value) {
                                              valor = value;
                                              if (valor == '') {
                                                valor = null;
                                              }
                                              Navigator.push(
                                                  context,
                                                  PageTransition(
                                                      type:
                                                          PageTransitionType.fade,
                                                      child: YaBusqueda(
                                                          valor: valor
                                                              .toLowerCase())));
                                            },
                                            textAlignVertical:
                                                TextAlignVertical.center,
                                            controller: controller,
                                            autocorrect: true,
                                            style:
                                                DecorationsOnInputs.textoInput(),
                                            decoration: InputDecoration(
                                                suffixIcon: Padding(
                                                  padding: EdgeInsets.only(
                                                      top:
                                                          0), // add padding to adjust icon
                                                  child: Icon(Icons.search,
                                                      color: Colors.white),
                                                ),
                                                enabledBorder:
                                                    const OutlineInputBorder(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(27)),
                                                  borderSide: const BorderSide(
                                                      color: lightColor,
                                                      width: 5.0),
                                                ),
                                                errorStyle: TextStyle(
                                                  color: textColorClaro,
                                                ),
                                                focusedErrorBorder:
                                                    UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.white,
                                                      width: 2.0),
                                                ),
                                                errorBorder: UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: Colors.white,
                                                      width: 2.0),
                                                ),
                                                labelText: 'Buscar',
                                                labelStyle: TextStyle(
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          11),
                                                  color: Colors.white70,
                                                  fontWeight: FontWeight.w600,
                                                  fontFamily: "Poppins",
                                                ),
                                                contentPadding:
                                                    new EdgeInsets.only(
                                                        left: 14.0),
                                                hintStyle: TextStyle(
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          11),
                                                  color: Colors.white38,
                                                  fontWeight: FontWeight.normal,
                                                  fontFamily: "Poppins",
                                                ),
                                                focusedBorder: InputBorder.none,
                                                border: InputBorder.none,
                                                hintText: 'Buscar'),
                                          )
                            ),
                            SizedBox(
                              width: 14,
                            ),
                            FiltrosIcono(),
                          ],
                        ),
                      ),
                    ),
                    
                    Padding(
                      padding: const EdgeInsets.only(top: 170.0, bottom: 30),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 18.0),
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                        child: Column(
                                          
                                      children: <Widget>[
                                        Hero(
                                          tag: "Hero",
                                          child: ( widget.valor==null && widget.listafiltrada==null && widget.recomendado==null)? CardHorizontal(
                                          
                                            nombre:widget.nombre ,
                                            nameImage:
                                                "lib/assets/images/card/I1.png",
                                          
                                          )
                                          :(widget.listafiltrada!=null||widget.listafiltrada==[])?CardHorizontal(
                                            listafiltrada:widget.listafiltrada,
                                            nombre:widget.nombre ,
                                            nameImage:
                                                "lib/assets/images/card/I1.png",
                                          )
                                         :CardHorizontal(
                                           recomendado: widget.recomendado,
                                            valor:widget.valor,
                                            nombre:widget.nombre ,
                                            nameImage:
                                                "lib/assets/images/card/I1.png",
                                          
                                          ),
                                        
                                        ),
                                      ],
                                    )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Container(child: BotonAtras())
                          ],
                        )),
                      ],
                    )),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          child: Stack(
                            children: <Widget>[Container(child: FlechaAbajo())],
                          ),
                        ),
                      ),
                    ),
                  ],
                ))),
      ),
    );
  }
}
