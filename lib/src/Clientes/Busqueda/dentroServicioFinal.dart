import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';

import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Alertas/alertaCalificacion.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Busqueda/reportarProblemaP.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/src/Clientes/Problemas/reportarProblema.dart';
import 'package:tl_app/src/mapa_page.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';
import 'package:tl_app/widgets/guardados/esquina.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:loading/loading.dart';
import 'package:url_launcher/url_launcher.dart';

class DentroServicioFinal extends StatefulWidget {
  final snapshot;
  final String tipo;
  DentroServicioFinal({
    Key key,
    this.snapshot,
    this.tipo
  }) : super(key: key);

  @override
  _DentroServicioFinalState createState() => _DentroServicioFinalState();
}

class _DentroServicioFinalState extends State<DentroServicioFinal> {
  GoogleMapController _controller;

  @override
  void initState() {
    super.initState();
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 95.0, bottom: 10),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 18.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment
                                        .center, //Center Column contents vertically,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center, //Center Column contents horizontally,
                                    children: <Widget>[
                                      carddetalle(),
                                    ],
                                  )),
                              Container(
                                  child: Column(
                                children: [
                                  Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("UBICACIÓN",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      )),
                                ],
                              )),
                              Container(
                                height: 200,
                                width: double.infinity,
                                child: GoogleMap(
                                    onTap: (la) {
                                      print('asqui' + la.toString());
                                      _launchMapsUrl(
                                          double.parse(widget.snapshot.documento
                                              .data()['latitud']),
                                          double.parse(widget.snapshot.documento
                                              .data()['longitud']));
                                    },
                                    markers: Set<Marker>.of([
                                      Marker(
                                          markerId: MarkerId('SomeId'),
                                          position: LatLng(
                                              double.parse(widget
                                                  .snapshot.documento
                                                  .data()['latitud']),
                                              double.parse(widget
                                                  .snapshot.documento
                                                  .data()['longitud'])),
                                          infoWindow: InfoWindow(

                                              //anchor: Offset(0.5,1),
                                              title: widget.snapshot.doc
                                                  .data()["nombre"]))
                                    ]),
                                    mapType: MapType.normal,
                                    myLocationEnabled: true,
                                    onMapCreated: _onMapCreated,
                                    initialCameraPosition: CameraPosition(
                                        target: LatLng(
                                            double.parse(widget
                                                .snapshot.documento
                                                .data()['latitud']),
                                            double.parse(widget
                                                .snapshot.documento
                                                .data()['longitud'])),
                                        zoom: 15)),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: Container(
                                    child: Column(
                                  children: [
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("DIRECCIÓN",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("METODO DE PAGO",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                widget.snapshot.documento
                                                    .data()['metodoPago'],
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                ),
                                              ),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                widget.snapshot.documento
                                                    .data()['direccion'],
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                ),
                                              ),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("FECHA DE INICIO",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                widget.snapshot.documento
                                                    .data()['fecha'],
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                ),
                                              ),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("CANTIDAD",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                widget.snapshot.documento
                                                    .data()['cantidad']
                                                    .toString(),
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                ),
                                              ),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("TOTAL",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                widget.snapshot.documento
                                                    .data()['total']
                                                    .toString(),
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                ),
                                              ),
                                            )),
                                      ),
                                    ),
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("HORARIO ACORDADO",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    hora()
                                  ],
                                )),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: ButtonGray(
                                  textoBoton: "FINALIZAR",
                                  bFuncion: () {
                                    print('finalizar');
                                    showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (context) =>
                                            AlertaCalificacionCustom(
                                              metodo:widget.snapshot.documento
                                                    .data()['metodoPago'],
                                              id: widget.snapshot.documento.id,
                                              idEncargado: widget
                                                  .snapshot.documento
                                                  .data()['idencargado'],
                                                  total: widget
                                                  .snapshot.documento
                                                  .data()['total'],
                                              nombre: widget.snapshot.doc
                                                  .data()['nombre'],
                                              token: widget.snapshot.doc
                                                  .data()['token'],
                                              estrellas: widget.snapshot.doc
                                                  .data()['estrellas']
                                                  .toString(),
                                              titulo:
                                                  "Califica tu experiencia con",
                                              buttonText: "ENVIAR",
                                              descripcion:
                                                  "Esta es la descripcion",
                                            ));
                                  },
                                  anchoBoton: getProportionateScreenHeight(350),
                                  largoBoton: getProportionateScreenWidth(28),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton:
                                      getProportionateScreenWidth(11),
                                  weightBoton: FontWeight.w400,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              ),
                              GestureDetector(

                                onTap: (){
                                  if(widget.tipo=='profesional'){
                                      Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: ReportarProblemaP(snap:widget.snapshot)));
                                  
                                  }else{
                                    Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: ReportarProblema(snap:widget.snapshot)));
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: Text(
                                    "REPORTAR PROBLEMA",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(17),
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: BotonAtras())
                  ],
                ))));
  }

  void _launchMapsUrl(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Widget carddetalle() {
    return Padding(
        padding: EdgeInsets.all(getProportionateScreenHeight(16)),
        child: Container(
          child: Material(
              color: Colors.transparent,
              child: Stack(
                children: [
                  Container(
                    child: Container(
                      height: getProportionateScreenHeight(150),
                      width: getProportionateScreenWidth(350),
                      decoration: BoxDecoration(
                        color: lightColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: Stack(
                          children: [
                            Container(
                              height: getProportionateScreenHeight(150),
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image:
                                        NetworkImage(widget.snapshot.fotos[0]),
                                    fit: BoxFit.fill,
                                  ),
                                  borderRadius: BorderRadius.circular(20.0)),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                          flex: 3,
                          child: Stack(
                            children: [
                              Container(
                                height: getProportionateScreenHeight(150),
                              ),
                              Positioned(
                                  top: 10,
                                  left: 15,
                                  child: Column(
                                    children: [
                                      Container(
                                        alignment: Alignment.topLeft,
                                        width: 150,
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Text(
                                              "${widget.snapshot.docservicio.data()["subcategoria"].toString().toUpperCase()} / " +
                                                  "${widget.snapshot.docservicio.data()["categoria"].toString().toUpperCase()}",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        10),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        width: 150,
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              alignment: Alignment.topLeft,
                                              height: 30,
                                              //width: 150,
                                              child: Material(
                                                type: MaterialType.transparency,
                                                child: Text(
                                                    "${widget.snapshot.doc.data()["nombre"]} / 4.6",
                                                    style: TextStyle(
                                                      color: textColorClaro,
                                                      fontSize:
                                                          getProportionateScreenWidth(
                                                              11),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    )),
                                              ),
                                            ),
                                            SizedBox(
                                              width: 5,
                                            ),
                                            Container(
                                              alignment: Alignment.topCenter,
                                              child: Icon(
                                                Icons.star,
                                                size: 14,
                                                color: primaryColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        height: 40,
                                        width: 150,
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Text(
                                              widget.snapshot.docservicio
                                                  .data()["descripcion"],
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        11),
                                                fontWeight: FontWeight.normal,
                                              )),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        width: 150,
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Text(
                                              '${widget.snapshot.docservicio.data()["precio"]}'
                                              r'$',
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ))
                            ],
                          )),
                    ],
                  ),
                ],
              )),
        ));
  }

  Widget hora() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: getProportionateScreenWidth(140),
              height: getProportionateScreenWidth(30),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                color: Color(0xff707070),
                padding: EdgeInsets.all(2.0),
                onPressed: null,
                child: Text(widget.snapshot.documento.data()['horainicio'],
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(13),
                      fontWeight: FontWeight.normal,
                    )),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 3.0, right: 3.0),
              child: Text("a",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(18),
                    fontWeight: FontWeight.normal,
                  )),
            ),
            Container(
              width: getProportionateScreenWidth(140),
              height: getProportionateScreenWidth(30),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                color: Color(0xff707070),
                padding: EdgeInsets.all(2.0),
                onPressed: null,
                child: Text(widget.snapshot.documento.data()['horafin'],
                    style: TextStyle(
                      color: textColorClaro,
                      fontSize: getProportionateScreenWidth(13),
                      fontWeight: FontWeight.normal,
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
