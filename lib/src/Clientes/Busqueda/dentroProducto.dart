import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarReserva.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarreservafiltrada.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/registro.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/src/Clientes/Perfil/editarperfil.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/cardVertical.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/guardados/esquina.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../Home/busqueda.dart';
import '../Home/guardado.dart';
import '../Home/home.dart';
import '../Home/mensajes.dart';

class DentroProducto extends StatefulWidget {
  final InformacionF snapshotf;
  final Informacion snapshot;
  final bool mostrarmas;

  DentroProducto({Key key, this.snapshot, this.mostrarmas, this.snapshotf})
      : super(key: key);
  @override
  _DentroProductoState createState() => new _DentroProductoState();
}

class _DentroProductoState extends State<DentroProducto> {
  int cantidad;
  bool isSwitched = false;
  bool favorito = false;
  List fotos = [];
  String nombre,
      estrellas,
      subcategoria,
      categoria,
      genero,
      diasdisponibles,
      horainicio,
      horafinal,
      ciudad,
      sectores,
      cancelacion,
      precio,
      descripcion,
      idusuario,
      iddocumento,
      marca;
  var bloqueado;
  var cedula;
  var celular;

  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    Perfil(),
  ];
  @override
  void initState() {
    cantidad = 0;
    super.initState();
    if (widget.snapshotf == null) {
      fotos = widget.snapshot.fotos;
      nombre = widget.snapshot.doc.data()["nombre"];
      estrellas = widget.snapshot.doc.data()["estrellas"].toString();
      subcategoria = widget.snapshot.documento.data()["subcategoria"];
      categoria = widget.snapshot.documento.data()["categoria"];
      genero = widget.snapshot.documento.data()["genero"];
      ciudad = widget.snapshot.documento.data()["ciudad"];
      sectores = widget.snapshot.documento.data()["sectores"];
      cancelacion = widget.snapshot.documento.data()["cancelacion"];
      precio = widget.snapshot.documento.data()["precio"];
      descripcion = widget.snapshot.documento.data()["descripcion"];
      idusuario = widget.snapshot.doc.id;
      iddocumento = widget.snapshot.documento.id;
      marca = widget.snapshot.documento.data()["marca"];
    } else {
      fotos = widget.snapshotf.fotos;
      nombre = widget.snapshotf.doc.data()["nombre"];
      estrellas = widget.snapshotf.doc.data()["estrellas"].toString();
      subcategoria = widget.snapshotf.documento.subcategoria;
      categoria = widget.snapshotf.documento.categoria;
      genero = widget.snapshotf.documento.genero;
      ciudad = widget.snapshotf.documento.ciudad;
      sectores = widget.snapshotf.documento.sectores;
      cancelacion = widget.snapshotf.documento.cancelacion;
      precio = widget.snapshotf.documento.precio;
      descripcion = widget.snapshotf.documento.descripcion;
      idusuario = widget.snapshotf.doc.id;
      iddocumento = widget.snapshotf.documento.id;
      marca = widget.snapshotf.documento.marca;
    }

    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(prefs.id)
        .get()
        .then((value) {
      setState(() {
        bloqueado = value.data()['bloqueado'];
        cedula = value.data()['cedula'];
        celular = value.data()['celular'];
      });
    });

    FirebaseFirestore.instance
        .collection('Favoritos')
        .where('idUsuario', isEqualTo: prefs.id)
        .get()
        .then((value) => value.docs.forEach((element) {
              if (element.data()['idServicio'] == iddocumento) {
                setState(() {
                  favorito = true;
                });
              }
            }));
    //widget.snapshot.fotos.remove(widget.snapshot.fotos[0]);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    //widget.snapshot.fotos.removeAt(0);
    //print(widget.snapshot.fotos.length);
    //print('imprimir ${widget.snapshot.fotos[0]}');

    //print('imprimir ${widget.snapshot.fotos.length}');
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        TijerasIcon(),
        GuardadoIcon(),
        BuscarIcon(),
        MensajeIcon(),
        PerfilIcon()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: page,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: Home()));
          page = index;
        });
      },
    );

    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              child: Stack(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(boxShadow: [
                                      BoxShadow(
                                          color: Colors.black26,
                                          offset: Offset(0, 2.0),
                                          blurRadius: 6.0)
                                    ]),
                                    child: Hero(
                                        tag: "Hero",
                                        child: CarouselSlider(
                                          options: CarouselOptions(
                                            height: 300.0,
                                            aspectRatio: 16 / 9,
                                            viewportFraction: 0.8,
                                            initialPage: 0,
                                            enableInfiniteScroll: true,
                                            reverse: false,
                                            autoPlay: true,
                                            autoPlayInterval:
                                                Duration(seconds: 3),
                                            autoPlayAnimationDuration:
                                                Duration(milliseconds: 800),
                                            autoPlayCurve: Curves.fastOutSlowIn,
                                            enlargeCenterPage: true,
                                            scrollDirection: Axis.horizontal,
                                          ),
                                          items: fotos.map((i) {
                                            return Builder(
                                              builder: (BuildContext context) {
                                                return Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  margin: EdgeInsets.symmetric(
                                                      horizontal: 5.0),
                                                  decoration: BoxDecoration(
                                                      color: Colors.grey),
                                                  child: Image(
                                                    image: NetworkImage(i),
                                                    fit: BoxFit.fill,
                                                  ),
                                                );
                                              },
                                            );
                                          }).toList(),
                                        )),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    // child: BotonAtras(),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        BotonAtras(),
                                        Esquina(
                                          idServicio: iddocumento,
                                          valor: favorito,
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.topCenter,
                                    child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("$nombre / $estrellas",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(13),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Container(
                                    alignment: Alignment.topCenter,
                                    child: Icon(
                                      Icons.star,
                                      size: 17,
                                      color: primaryColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                                padding: EdgeInsets.only(
                                    left: getProportionateScreenHeight(16),
                                    right: getProportionateScreenHeight(16)),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: lightColor,
                                      borderRadius: BorderRadius.circular(
                                          getProportionateScreenHeight(20))),
                                  child: Material(
                                      color: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: [
                                              Container(
                                                alignment: Alignment.topCenter,
                                                child: Material(
                                                  type:
                                                      MaterialType.transparency,
                                                  child: Text(
                                                      subcategoria
                                                          .toUpperCase(),
                                                      style: TextStyle(
                                                        color: textColorClaro,
                                                        fontSize:
                                                            getProportionateScreenWidth(
                                                                14),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      )),
                                                ),
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "CATEGORÍA: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "GENERO: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text("MARCA: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "CIUDAD: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "SECTORES: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "CANCELACIÓN: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              "PRECIO: ",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              categoria
                                                                  .toString(),
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(genero,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(marca,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(ciudad,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(sectores,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              cancelacion,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              '$precio '
                                                              r'$',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ],
                                      )),
                                )),
                            Padding(
                                padding: EdgeInsets.all(
                                    getProportionateScreenHeight(16)),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: lightColor,
                                      borderRadius: BorderRadius.circular(
                                          getProportionateScreenHeight(20))),
                                  child: Material(
                                      color: Colors.transparent,
                                      child: Stack(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text("DESCRIPCIÓN",
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  14),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )),
                                                  ),
                                                ),
                                                Container(
                                                  alignment: Alignment.topLeft,
                                                  child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text(descripcion,
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  13),
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        )),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      )),
                                )),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, right: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("Cantidad",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(14),
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Material(
                                        type: MaterialType.transparency,
                                        child: Row(
                                          children: [
                                            IconButton(
                                                icon: Icon(
                                                  Icons.remove_circle,
                                                  color: Colors.white,
                                                ),
                                                onPressed: () {
                                                  if (cantidad > 0) {
                                                    cantidad--;
                                                    setState(() {});
                                                    print(cantidad);
                                                  }
                                                }),
                                            Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                    border: Border.all(
                                                        color: Colors.white)),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 2.0,
                                                          bottom: 2.0,
                                                          left: 12.0,
                                                          right: 12.0),
                                                  child: Text(
                                                      cantidad.toString(),
                                                      style: TextStyle(
                                                        color: textColorClaro,
                                                        fontSize:
                                                            getProportionateScreenWidth(
                                                                14),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      )),
                                                )),
                                            IconButton(
                                                icon: Icon(
                                                  Icons.add_circle,
                                                  color: Colors.white,
                                                ),
                                                onPressed: () {
                                                  cantidad++;
                                                  setState(() {});
                                                  print(cantidad);
                                                }),
                                          ],
                                        )),
                                  ),
                                ],
                              ),
                            ),
                            /*
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 20.0, right: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("Seleccionar",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(14),
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ),
                                  ),
                                  Switch(
                                    inactiveTrackColor: Colors.grey,
                                    value: isSwitched,
                                    onChanged: (value) {
                                      setState(() {
                                        isSwitched = value;
                                        print(isSwitched);
                                      });
                                    },
                                    activeTrackColor: Color(0xfffA91D74),
                                    activeColor: Colors.white,
                                  ),
                                ],
                              ),
                            ),*/
                            widget.mostrarmas == null
                                ? StreamBuilder<QuerySnapshot>(
                                    stream: FirebaseFirestore.instance
                                        .collection("Servicios")
                                        .where("id_usuario",
                                            isEqualTo: idusuario)
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.hasData) {
                                        //  print('revisa ${snapshot.data.size}');

                                        return snapshot.data.size > 1
                                            ? Column(
                                                children: [
                                                  Container(
                                                    alignment: Alignment.center,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: Text(
                                                          "MAS PRODUCTOS Y SERVICIOS DE ESTE USUARIO",
                                                          style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal)),
                                                    ),
                                                  ),
                                                  Container(
                                                    //  color: Colors.red,
                                                    height: 250,
                                                    child: ListView(
                                                        scrollDirection:
                                                            Axis.horizontal,
                                                        children:
                                                            getExpenseItems(
                                                                snapshot)),
                                                  )
                                                ],
                                              )
                                            : SizedBox();
                                      } else if (snapshot.hasError) {
                                        return Center(
                                            child:
                                                Text("Ha ocurrido un error"));
                                      } else {
                                        return Center(
                                            child: CircularProgressIndicator());
                                      }
                                    })
                                : SizedBox(),
                            (prefs.id != idusuario)
                                ? ButtonGray(
                                    textoBoton: "SOLICITAR",
                                    bFuncion: () {
                                      if (cantidad == 0) {
                                        _alertaProducto(context,
                                            'La cantidad no puede ser 0');
                                      } else {
                                        if (prefs.id == null) {
                                          Navigator.push(
                                              context,
                                              PageTransition(
                                                  child: Registro(regreso: true),
                                                  type:
                                                      PageTransitionType.fade));
                                          _alertaProducto(context,
                                              'Inicia sesión para poer continuar');
                                        } else if (bloqueado == "true") {
                                          _alertaProducto(context,
                                              'Tu usuario esta bloqueado por favor dirigete a perfil y solicitar ayuda para saber cual es el problema');
                                        } else if (celular == null ||
                                            celular == '') {
                                           Navigator.push(
                                              context,
                                              PageTransition(
                                                  child: EditarPerfil(),
                                                  type:
                                                      PageTransitionType.fade));
                                          _alertaProducto(context,
                                              'Por favor ingresa tu teléfono para que los usuarios puedan contactarse contigo ');
                                        // } else if (cedula == null ||
                                        //     cedula == '') {
                                        //   Navigator.push(
                                        //       context,
                                        //       PageTransition(
                                        //           child: EditarPerfil(),
                                        //           type:
                                        //               PageTransitionType.fade));

                                        //   _alertaProducto(context,
                                        //       'Por favor ingresa la foto de la cédula para poder continuar y validar tus datos');
                                        } else {
                                          if (widget.snapshotf == null) {
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                  type: PageTransitionType.fade,
                                                  child: FinalizarReserva(
                                                    snapshot: widget.snapshot,
                                                    cantidad: cantidad,
                                                  ),
                                                ));
                                          } else {
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                  type: PageTransitionType.fade,
                                                  child: (prefs.id == null)
                                                      ? Registro(regreso: true)
                                                      : FinalizarReservaFiltrada(
                                                          snapshotf:
                                                              widget.snapshotf,
                                                          cantidad: cantidad,
                                                        ),
                                                ));
                                          }
                                        }
                                      }
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(350),
                                    largoBoton: getProportionateScreenWidth(28),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(11),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )
                                : Container(),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      //  print('mi latitud ${doc["latitud"]}');
      return iddocumento != doc.id
          ? CardVertical(
              documentSnapshot: doc,
            )
          : SizedBox();
    }).toList();
  }

  loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
