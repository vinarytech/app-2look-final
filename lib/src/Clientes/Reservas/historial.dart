import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/carddividida2.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class Historial extends StatefulWidget {
  Historial({Key key}) : super(key: key);

  @override
  _HistorialState createState() => _HistorialState();
}

class _HistorialState extends State<Historial> {
  final prefs = PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance
                .collection("Solicitudes")
                .where("idsolicitante", isEqualTo: prefs.id)
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.docs.length <= 0) {
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                        child: Text(
                      "No tienes reservas en tu Historial",
                      textAlign: TextAlign.center,
                    )),
                  );
                } else {
                  return Column(children: getExpenseItems(snapshot));
                }
              } else if (snapshot.hasError) {
                return Center(child: Text("Ha ocurrido un error"));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      return doc.data()['estado'] == 'FINALIZADO' &&
              doc.data()['cancelado'] != 'SI'
          ? CardDividida2(
              document: doc,
            )
          : SizedBox();
    }).toList();
  }
}
