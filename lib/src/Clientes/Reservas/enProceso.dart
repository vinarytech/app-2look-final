import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/cardDivididaProf.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class EnProceso extends StatefulWidget {
  EnProceso({Key key}) : super(key: key);

  @override
  _EnProcesoState createState() => _EnProcesoState();
}

class _EnProcesoState extends State<EnProceso> {
  final prefs = PreferenciasUsuario();

  @override
  void initState() {
    print("Preferencias" + prefs.id.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return (prefs.id != null)
        ? Container(
            child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection("Solicitudes")
                    .where("idsolicitante", isEqualTo: prefs.id)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.docs.length <= 0) {
                      return Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                            child: Text(
                          "No tienes reservas en proceso, realizar una reserva para continuar.",
                          textAlign: TextAlign.center,
                        )),
                      );
                    } else {
                      return Column(children: getExpenseItems(snapshot));
                    }
                  } else if (snapshot.hasError) {
                    return Center(child: Text("Ha ocurrido un error"));
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                }))
        : Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(30),
                vertical: getProportionateScreenHeight(20)),
            child: Column(
              children: [
                Text(
                  "Por favor inicia sesión para poder visualizar las reservas pendientes que tienes",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(17),
                    fontWeight: FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                ButtonGray(
                  textoBoton: "Iniciar Sesión",
                  bFuncion: () {
                    setState(() {
                      page = 4;
                    });
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            child: Home(), type: PageTransitionType.fade));
                  },
                  anchoBoton: getProportionateScreenHeight(330),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.bold,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
              ],
            ),
          );
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      return doc.data()['estado'] != 'FINALIZADO' &&
              doc.data()['estadosolicitud'] == true
          ? CardDividida(
              document: doc,
            )
          : SizedBox();
    }).toList();
  }
}

Widget _alertaProducto(BuildContext context, String text) {
  return Dialog(
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(getProportionateScreenHeight(20))),
    elevation: 0,
    backgroundColor: Colors.transparent,
    child: Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 40, bottom: 16, left: 16, right: 16),
          margin: EdgeInsets.only(top: 16),
          decoration: BoxDecoration(
              color: textColorClaro,
              borderRadius: BorderRadius.circular(20),
              shape: BoxShape.rectangle,
              boxShadow: [
                BoxShadow(
                    color: Colors.black26,
                    blurRadius: 10,
                    offset: Offset(0, 10))
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                text,
                style: TextStyle(
                  color: textColorOscuro,
                  fontSize: getProportionateScreenWidth(17),
                  fontWeight: FontWeight.normal,
                ),
              ),
              SizedBox(
                height: 24,
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: ButtonGray(
                  textoBoton: "Entendido",
                  bFuncion: () {
                    Navigator.pop(context);
                  },
                  anchoBoton: getProportionateScreenHeight(330),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.bold,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}

class EnProcesoProf extends StatefulWidget {
  EnProcesoProf({Key key}) : super(key: key);

  @override
  _EnProcesoProfState createState() => _EnProcesoProfState();
}

class _EnProcesoProfState extends State<EnProcesoProf> {
  final prefs = PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        child: StreamBuilder<QuerySnapshot>(
            stream: FirebaseFirestore.instance
                .collection("Solicitudes")
                .where("idencargado", isEqualTo: prefs.id)
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                return Column(children: getExpenseItems(snapshot));
              } else if (snapshot.hasError) {
                return Center(child: Text("Ha ocurrido un error"));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    // return ListView.builder(itemBuilder: (context, index){
    // return CardDivididaProf(document: doc,);
    // },itemCount: snapshot.data.docs.length);
    return snapshot.data.docs.map((doc) {
      // return Text(doc.id);
      return doc.data()['estadosolicitud'] == true &&
              doc.data()['estado'] != 'FINALIZADO'
          ? CardDivididaProf(document: doc, idSolicitud: doc.id)
          : SizedBox();
    }).toList();
  }
}
