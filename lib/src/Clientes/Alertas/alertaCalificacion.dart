import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:http/http.dart' as http;
import 'package:tl_app/src/Clientes/Home/home.dart';

class AlertaCalificacionCustom extends StatefulWidget {
  final String titulo,
      descripcion,
      buttonText,
      metodo,
      nombre,
      estrellas,
      idEncargado,
      id,
      token,
      total;
  AlertaCalificacionCustom(
      {Key key,
      this.descripcion,
      this.buttonText,
      this.titulo,
      this.nombre,
      this.estrellas,
      this.idEncargado,
      this.id,
      this.token,
      this.total,
      this.metodo})
      : super(key: key);

  @override
  _AlertaCalificacionCustomState createState() =>
      _AlertaCalificacionCustomState();
}

class _AlertaCalificacionCustomState extends State<AlertaCalificacionCustom> {
  var rating = 3.0;
  List<Estrellas> listaestrellas = [];

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
      child: Dialog(
        shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(getProportionateScreenHeight(20))),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(top: 40, bottom: 16, left: 16, right: 16),
              margin: EdgeInsets.only(top: 16),
              decoration: BoxDecoration(
                  color: textColorClaro,
                  borderRadius: BorderRadius.circular(20),
                  shape: BoxShape.rectangle,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    widget.titulo,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: textColorOscuro,
                      fontSize: getProportionateScreenWidth(17),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: getProportionateScreenHeight(12.0)),
                    child: Container(
                      height: getProportionateScreenHeight(84),
                      width: getProportionateScreenWidth(150),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            child: Material(
                              type: MaterialType.transparency,
                              child: Text(
                                  "${widget.nombre} / ${widget.estrellas}",
                                  style: TextStyle(
                                    color: textColorOscuro,
                                    fontSize: getProportionateScreenWidth(13),
                                    fontWeight: FontWeight.normal,
                                  )),
                            ),
                          ),
                          SizedBox(
                            width: getProportionateScreenHeight(5),
                          ),
                          Container(
                            alignment: Alignment.center,
                            child: Icon(
                              Icons.star,
                              size: getProportionateScreenHeight(17),
                              color: primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Center(
                    child: SmoothStarRating(
                      allowHalfRating: false,
                      rating: rating,
                      isReadOnly: false,
                      size: 40,
                      filledIconData: Icons.star,
                      halfFilledIconData: Icons.star,
                      defaultIconData: Icons.star_border,
                      starCount: 5,
                      //allowHalfRating: true,
                      spacing: 2.0,
                      color: primaryColor,
                      borderColor: textColorMedio,
                      onRated: (value) {
                        print("rating value -> $value");
                        // print("rating value dd -> ${value.truncate()}");
                        rating = value;
                      },
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: ButtonGray(
                      textoBoton: "ENVIAR",
                      bFuncion: () {
                        String date = DateFormat("yyyy-MM-dd hh:mm")
                            .format(DateTime.now());
                        String notificacion =
                            'El usuario ha finalizado el servicio';

                        var promedio;
                        Future(() => loaderDialogNormal(context)).then((v) {
                          FirebaseFirestore.instance
                              .collection('Calificaciones')
                              .add({
                            'idencargado': widget.idEncargado,
                            'date': date,
                            'estrellas': rating.toString()
                          }).then((value) {
                            sendNotification(
                                notificacion, '2Look', widget.token);
                            FirebaseFirestore.instance
                                .collection('Solicitudes')
                                .doc(widget.id)
                                .update({'estado': 'FINALIZADO'});
                            if (widget.metodo == 'Tarjeta') {
                              FirebaseFirestore.instance
                                  .collection('Billetera')
                                  .add({
                                'id_usuario': widget.idEncargado,
                                'monto': widget.total,
                                'descripcion':
                                    'Acreditación por pago del servicio',
                                'fecha': DateTime.now(),
                                'tipo': 'Acreditación',
                                'estado': true
                              });
                            }
                          }).then((value) async {
                            var cantidadtotal;
                            var acumulado = 0.0;

                            listaestrellas =
                                await _listacalificaciones(widget.idEncargado);
                            print('lista estrellas ${listaestrellas.length}');
                            listaestrellas.forEach((element) {
                              cantidadtotal = double.parse(element.cantidad);
                              acumulado = cantidadtotal + acumulado;
                            });

                            print('acumulado $acumulado');
                            promedio = acumulado / listaestrellas.length;
                            print('promedio $promedio');
                          }).then((value) {
                            FirebaseFirestore.instance
                                .collection('Usuarios')
                                .doc(widget.idEncargado)
                                .update(
                                    {'estrellas': promedio.toStringAsFixed(2)});
                          }).then((value) {
                            /*      
                                       Navigator.pushNamedAndRemoveUntil(context,
                                       PageTransition(type: PageTransitionType.fade, child: Home(index: 2,)));
                             */
                            Navigator.pop(context);
                            Navigator.pop(context);
                            Navigator.pop(context);

                            _alerta(context,
                                'Tu calificación ha sido enviada, gracias por mejorar la comunidad 2Look');
                          });
                        });
                        // _alerta(context, 'Tu servicio se encuentra en transcurso, enviaremos un mensaje al profesional para que se contacte contigo');
                      },
                      anchoBoton: getProportionateScreenHeight(330),
                      largoBoton: getProportionateScreenWidth(27),
                      colorTextoBoton: textColorClaro,
                      opacityBoton: 1,
                      sizeTextBoton: getProportionateScreenWidth(15),
                      weightBoton: FontWeight.bold,
                      color1Boton: primaryColor,
                      color2Boton: primaryColor,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  Future<List<Estrellas>> _listacalificaciones(String idusuario) async {
    QuerySnapshot qShot = await FirebaseFirestore.instance
        .collection('Calificaciones')
        .where('idencargado', isEqualTo: idusuario)
        .get();

    return qShot.docs
        .map((doc) => Estrellas(
              cantidad: doc.data()['estrellas'],
            ))
        .toList();
  }

  Future<void> sendNotification(subject, title, token) async {
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {
        "body": subject,
        "title": title,
      },
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": token
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }

  _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}

class Estrellas {
  String cantidad;
  String idEncargado;
  String date;
  Estrellas({this.cantidad, this.idEncargado, this.date});
}
