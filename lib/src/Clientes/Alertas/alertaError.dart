import 'dart:ui';

import 'package:flutter/material.dart';

import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class AlertaError extends StatefulWidget {
  final String descripcion, buttonText;
  AlertaError({Key key, this.descripcion, this.buttonText}) : super(key: key);

  @override
  _AlertaErrorState createState() => _AlertaErrorState();
}

class _AlertaErrorState extends State<AlertaError> {
  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
      child: Dialog(
        shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(getProportionateScreenHeight(20))),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(top: 40, bottom: 16, left: 16, right: 16),
              margin: EdgeInsets.only(top: 16),
              decoration: BoxDecoration(
                  color: textColorClaro,
                  borderRadius: BorderRadius.circular(20),
                  shape: BoxShape.rectangle,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black26,
                        blurRadius: 10,
                        offset: Offset(0, 10))
                  ]),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    widget.descripcion,
                    style: TextStyle(
                      color: textColorOscuro,
                      fontSize: getProportionateScreenWidth(17),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: ButtonGray(
                      textoBoton: "Entendido",
                      bFuncion: () {
                        Navigator.of(context).pop();
                      },
                      anchoBoton: getProportionateScreenHeight(330),
                      largoBoton: getProportionateScreenWidth(27),
                      colorTextoBoton: textColorClaro,
                      opacityBoton: 1,
                      sizeTextBoton: getProportionateScreenWidth(15),
                      weightBoton: FontWeight.bold,
                      color1Boton: primaryColor,
                      color2Boton: primaryColor,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
