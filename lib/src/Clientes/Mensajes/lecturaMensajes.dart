import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/mensajeCompleto.dart';
import 'package:tl_app/widgets/mensajeResumen.dart';
import 'package:tl_app/widgets/size_config.dart';

class LecturaMensajes extends StatefulWidget {
  final idMensaje;
  final idSolicitud;
  final estrellas;
  final nombre;

  LecturaMensajes({Key key, this.idMensaje, this.idSolicitud, this.estrellas, this.nombre})
      : super(key: key);

  @override
  _LecturaMensajesState createState() => _LecturaMensajesState();
}

class _LecturaMensajesState extends State<LecturaMensajes> {
  final _textController = TextEditingController();
  final prefs = PreferenciasUsuario();
  // String date = DateFormat("yyyy-MM-dd hh:mm:ss").format(DateTime.now());

  // List<Widget> listaEmisor;
  // List<Widget> listaReceptor;
  List<MensajeDetalle> listaMensajeDetalle = [];

  // GlobalKey _bottomNavigationKey = GlobalKey();
  // final List<Widget> _children = [
  //   Reservas(),
  //   Guardado(),
  //   Busqueda(),
  //   Mensajes(),
  //   Perfil(),
  // ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    // var curvedNavigationBar = CurvedNavigationBar(
    //   height: 50.0,
    //   backgroundColor: Color(0xfff191B1F),
    //   color: Color(0xfff191B1F),
    //   buttonBackgroundColor: Color(0xfffA91D74),
    //   items: <Widget>[
    //     TijerasIcon(),
    //     GuardadoIcon(),
    //     BuscarIcon(),
    //     MensajeIcon(),
    //     PerfilIcon()
    //   ],
    //   animationDuration: Duration(milliseconds: 200),
    //   animationCurve: Curves.bounceInOut,
    //   index: page,
    //   onTap: (index) {
    //     //Handle button tap
    //     setState(() {
    //       Navigator.pushReplacement(context,
    //           PageTransition(type: PageTransitionType.fade, child: Home()));
    //       page = index;
    //     });
    //   },
    // );
    return SafeArea(
      child: Scaffold(
          // bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  // _children[page],
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 60.0),
                    child: SingleChildScrollView(
                      reverse: true,
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        height: MediaQuery.of(context).size.height * .750,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18.0),
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical:
                                          getProportionateScreenHeight(40)),
                                  height: getProportionateScreenHeight(570),
                                  child: StreamBuilder(
                                    stream: FirebaseFirestore.instance
                                        .collection('MensajesDetalle')
                                        .where('idMensaje',
                                            isEqualTo: widget.idMensaje)
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      listaMensajeDetalle = [];

                                      if (snapshot.hasData) {
                                        for (var item in snapshot.data.docs) {
                                          print(item.data()['fecha']);

                                          listaMensajeDetalle.add(
                                              MensajeDetalle(
                                                  idMensaje: widget.idMensaje,
                                                  fecha: item.data()['fecha'],
                                                  idEmisor:
                                                      item.data()['idemisor'],
                                                  idSolicitud: item
                                                      .data()['idsolicitud'],
                                                  mensaje:
                                                      item.data()['mensaje']));
                                        }
                                      }

                                      listaMensajeDetalle.sort(
                                          (a, b) => b.fecha.compareTo(a.fecha));

                                      if (snapshot.hasData) {
                                        return ListView(
                                          reverse: true,
                                          children: List.generate(
                                              listaMensajeDetalle.length,
                                              (index) {
                                            if (listaMensajeDetalle[index]
                                                    .idEmisor !=
                                                prefs.id) {
                                              return MensajeCompleto(
                                                  mensaje:
                                                      listaMensajeDetalle[index]
                                                          .mensaje,
                                                          estrellas: widget.estrellas,
                                                          nombre: widget.nombre,);
                                            } else {
                                              return MensajeReseptor(
                                                  mensaje:
                                                      listaMensajeDetalle[index]
                                                          .mensaje);
                                            }
                                          }),
                                        );
                                      } else {
                                        return CircularProgressIndicator();
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                                height: 40,
                                width: MediaQuery.of(context).size.width - 40,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(27)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.white30, blurRadius: 0)
                                  ],
                                  color: lightColor,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: TextField(
                                          controller: _textController,
                                          style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      14),
                                              fontWeight: FontWeight.normal),
                                          decoration: InputDecoration(
                                              hintText: "Envia un mensaje...",
                                              hintStyle: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                  fontWeight:
                                                      FontWeight.normal),
                                              border: InputBorder.none),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          _enviarMensaje();
                                          // print('dada' + _textController.text);
                                        },
                                        child: Container(
                                          height: 40,
                                          width: 60,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(27)),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.white30,
                                                  blurRadius: 0)
                                            ],
                                            color: lightColor,
                                          ),
                                          //padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                              "lib/assets/images/mensajes/flechamensaje.png"),
                                        ),
                                      )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }

  void _enviarMensaje() {
    FirebaseFirestore.instance.collection('MensajesDetalle').add({
      'fecha': DateTime.now(),
      'idemisor': prefs.id,
      'idsolicitud': widget.idSolicitud,
      'idMensaje': widget.idMensaje,
      'mensaje': _textController.text,
      'estado': true
    });
    FirebaseFirestore.instance
        .collection('Mensajes')
        .doc(widget.idMensaje)
        .update({'mensaje': _textController.text, 'fecha': DateTime.now()});
    FocusScope.of(context).unfocus();
    _textController.text = '';
  }
}

class MensajeDetalle {
  Timestamp fecha;
  String idMensaje;
  String idEmisor;
  String idSolicitud;
  String mensaje;

  MensajeDetalle({
    this.fecha,
    this.idMensaje,
    this.idEmisor,
    this.idSolicitud,
    this.mensaje,
  });
}
