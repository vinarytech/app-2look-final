import 'dart:async';
import 'dart:collection';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/models/modelservicio.dart';
import 'package:tl_app/src/Clientes/Busqueda/yabusqueda.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/dropdownCatProf.dart';
import 'package:tl_app/widgets/DropDownCatPArof.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/rangesliderCustomed.dart';
import 'package:tl_app/widgets/size_config.dart';

class Filtros extends StatefulWidget {
  @override
  _FiltrosState createState() => _FiltrosState();
}

String capitalize(String s) => (s != null && s.length > 1)
    ? s[0].toUpperCase() + s.substring(1).toLowerCase()
    : s != null
        ? s.toUpperCase()
        : null;

class _FiltrosState extends State<Filtros> {
  bool selecteds = false;
  bool selectedp = false;
  bool selectedm = false;
  bool selectedf = false;
  bool selected1 = false;
  bool selected2 = false;
  String subcategoria1, subcategoria2, genero1, genero2, ubicacion1, ubicacion2;
  double rango1, rango2;
  String categoria = "Selecciona la categoría";
  String cancelacion = "Selecciona el tipo de cancelación";
  String ciudad = "Selecciona la ciudad";
  List listaSectores = [];
  String horainicioescogida;
  String horafinescogida;
  String sector = "Selecciona el sector";
  DateTime horainiciodt;
  List<ServiciosF> filtrada = [];
  bool selectproser = false;
  bool selectgenero = false;
  bool selectcategoria = false;
  bool selectrango = false;
  bool selectcancelacion = false;
  bool selectciudad = false;
  bool selectsector = false;
  bool selecthorainicio = false;
  bool selecthorafin = false;
  bool selectubicacion = false;

  @override
  void initState() {
    super.initState();
    inicio();
  }

  List<ServiciosF> listaservicios = [];
  List<ServiciosF> unique = [];
  Future<List<ServiciosF>> getUserTaskList() async {
    QuerySnapshot qShot =
        await FirebaseFirestore.instance.collection('Servicios').get();

    return qShot.docs
        .map((doc) => ServiciosF(
              id: doc.id,
              cancelacion: doc.data()['cancelacion'],
              ciudad: doc.data()['ciudad'],
              descripcion: doc.data()['descripcion'],
              diasdisponible: doc.data()['diasdisponible'],
              estado: doc.data()['estado'],
              horaFinal: doc.data()['horaFinal'],
              horaInicio: doc.data()['horaInicio'],
              idusuario: doc.data()['id_usuario'],
              marca: doc.data()['marca'],
              sectores: doc.data()['sectores'],
              subcategoria: doc.data()['subcategoria'],
              genero: doc.data()['genero'],
              categoria: doc.data()['categoria'],
              precio: doc.data()['precio'],
            ))
        .toList();
  }

  inicio() async {
    listaservicios = await getUserTaskList();
  }

  metodobool(bool selectser, bool selectgen, bool selcat, bool ran, bool can,
      bool ciu, bool sec, bool hori, bool horf) {
    filtrada = [];
    filtrada = listaservicios;
    if (selectser) {
      if (subcategoria1 != null ||
          subcategoria2 != null ||
          subcategoria1 != "" ||
          subcategoria2 != "") {
        filtrada = subcategoria(filtrada);
      }
    }
    if (selectgen) {
      if (genero1 != null ||
          genero2 != null ||
          genero1 != "" ||
          genero2 != "") {
        filtrada = genero(filtrada);
      }
    }
    if (selcat) {
      if (categoria != null || categoria != "Selecciona la categoría") {
        filtrada = categoriaelegida(filtrada);
      }
    }
    if (ran) {
      if (rango1 != null || rango2 != null) {
        filtrada = rangoelegido(filtrada);
      }
    }
    if (can) {
      if (cancelacion != 'Selecciona el tipo de cancelación') {
        filtrada = cancelacionelegido(filtrada);
      }
    }
    if (ciu) {
      if (ciudad != 'Selecciona la ciudad') {
        filtrada = ciudadelegido(filtrada);
      }
    }
    if (sec) {
      if (sector != 'Selecciona el sector') {
        filtrada = sectorelegido(filtrada);
      }
    }
    if (hori) {
      if (horainicioescogida != null) {
        filtrada = horainicioelegido(filtrada);
      }
    }
    if (horf) {
      if (horafinescogida != null) {
        filtrada = horafinelegido(filtrada);
      }
    }
  }

  List<ServiciosF> subcategoria(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.subcategoria == subcategoria1 ||
          element.subcategoria == subcategoria2) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> genero(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.genero == genero1 ||
          element.genero == genero2 ||
          element.genero == '$genero1 / $genero2') {
        nueva.add(element);
      }
      if (genero1 == null && genero2 != null) {
        if (element.genero.contains(genero2)) {
          nueva.add(element);
        }
      }
    });
    return nueva;
  }

  List<ServiciosF> categoriaelegida(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.categoria == categoria.toUpperCase()) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> rangoelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (double.parse(element.precio) >= rango1 &&
          double.parse(element.precio) <= rango2) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> cancelacionelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.cancelacion == cancelacion) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> ciudadelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (capitalize(element.ciudad) == ciudad) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> sectorelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (capitalize(element.sectores) == sector) {
        nueva.add(element);
      }
    });
    return nueva;
  }

  List<ServiciosF> horainicioelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.horaInicio != null) {
        if (element.horaInicio.contains(horainicioescogida)) {
          nueva.add(element);
        }
      }
      //else{
      //  print('hola');
      //}
    });
    return nueva;
  }

  List<ServiciosF> horafinelegido(List<ServiciosF> lista) {
    List<ServiciosF> nueva = [];
    lista.forEach((element) {
      if (element.horaFinal != null) {
        if (element.horaFinal.contains(horafinescogida)) {
          nueva.add(element);
        }
      }
    });
    return nueva;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
        child: Scaffold(
            //backgroundColor: primaryColor,
            body: SafeArea(
                child: Stack(children: [
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("lib/assets/images/filtros/FONDO.png"),
            fit: BoxFit.fill,
          ),
        ),
      ),
      ListView(
        children: [
          GestureDetector(
              onTap: () {
                filtrada = null;
              },
              child: Container(child: BotonAtrasF())),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("QUE ESTAS BUSCANDO",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                  type: MaterialType.transparency,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: getProportionateScreenWidth(20),
                        ),
                        child: Text("Servicio",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(14),
                              fontWeight: FontWeight.normal,
                            )),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: CircularCheckBox(
                      value: this.selecteds,
                      checkColor: textColorMedio,
                      activeColor: primaryColor,
                      inactiveColor: textColorMedio,
                      disabledColor: Colors.grey,
                      onChanged: (val) {
                        this.selecteds = !this.selecteds;
                        if (this.selecteds) {
                          setState(() {
                            subcategoria1 = 'servicio';

                            print(subcategoria1);
                            //  print(listaservicios);
                          });
                        } else {
                          setState(() {
                            this.selecteds = false;
                            subcategoria1 = null;
                          });
                        }
                      }),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                  type: MaterialType.transparency,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: getProportionateScreenWidth(20),
                        ),
                        child: Text("Producto",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(14),
                              fontWeight: FontWeight.normal,
                            )),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: CircularCheckBox(
                      value: this.selectedp,
                      checkColor: textColorMedio,
                      activeColor: primaryColor,
                      inactiveColor: textColorMedio,
                      disabledColor: Colors.grey,
                      onChanged: (val) {
                        this.selectedp = !this.selectedp;
                        if (this.selectedp) {
                          setState(() {
                            subcategoria2 = 'producto';
                            print(subcategoria2);
                            // if(listaauxiliar.length==0){
                          });
                        } else {
                          setState(() {
                            this.selectedp = false;
                            subcategoria2 = null;
                          });
                        }
                      }),
                )
              ],
            ),
          ),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("GENERO",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                  type: MaterialType.transparency,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: getProportionateScreenWidth(20),
                        ),
                        child: Text("Masculino",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(14),
                              fontWeight: FontWeight.normal,
                            )),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: CircularCheckBox(
                      value: this.selectedm,
                      checkColor: textColorMedio,
                      activeColor: primaryColor,
                      inactiveColor: textColorMedio,
                      disabledColor: Colors.grey,
                      onChanged: (val) {
                        this.selectedm = !this.selectedm;
                        if (this.selectedm) {
                          setState(() {
                            genero1 = 'Masculino';
                            print(genero1);
                          });
                        } else {
                          setState(() {
                            //     listaauxiliar.removeWhere((element) => element.genero=='Masculino');
                            //   print(listaauxiliar);
                            this.selectedm = false;
                            genero1 = null;
                          });
                        }
                      }),
                )
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Material(
                  type: MaterialType.transparency,
                  child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: getProportionateScreenWidth(20),
                        ),
                        child: Text("Femenino",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(14),
                              fontWeight: FontWeight.normal,
                            )),
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 28.0),
                  child: CircularCheckBox(
                      value: this.selectedf,
                      checkColor: textColorMedio,
                      activeColor: primaryColor,
                      inactiveColor: textColorMedio,
                      disabledColor: Colors.grey,
                      onChanged: (val) {
                        this.selectedf = !this.selectedf;
                        if (this.selectedf) {
                          setState(() {
                            genero2 = 'Femenino';
                            selectgenero = true;
                            print(genero2);
                          });
                        } else {
                          setState(() {
                            this.selectedf = false;
                            genero2 = null;
                            selectgenero = false;
                          });
                        }
                      }),
                )
              ],
            ),
          ),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("CATEGORIA",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          DropDownCatProf1(
              text: categoria,
              lista: [
                'Selecciona la categoría',
                'Peluquería',
                'Diseño de Mirada y Maquillaje',
                'Manicure y Pedicure',
                'Asesoría de Imagen',
                'Cosmetología',
                'Masaje',
                'Bienestar y Cuidado Personal',
                'Peluquerias Caninas'
              ],
              function: (value) {
                setState(() {
                  categoria = value;
                  print(categoria.toUpperCase());
                });
              }),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("RANGO DE PRECIO",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          RangeSliderCustom(
            function: (val1, val2) {
              print('val $val1 $val2');
              rango1 = val1;
              rango2 = val2;
            },
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("CANCELACION",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          DropDownCatProf1(
              text: cancelacion,
              lista: [
                'Selecciona el tipo de cancelación',
                'Flexible (Hasta 8 horas)',
                'Sin Cancelación'
              ],
              function: (value) {
                setState(() {
                  cancelacion = value;
                  print(cancelacion);
                });
              }),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("CIUDAD",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          DropDownCatProf1(
              text: ciudad,
              lista: [
                'QUITO',
                'AMBATO',
                'GUAYAQUIL',
                'CUENCA',
                'IBARRA',
                'RIOBAMBA',
                'MANTA',
                'PORTOVIEJO'
              ],
              function: (value) {
                setState(() {
                  print(listaSectores.length);
                  listaSectores = [];
                  sector = "Selecciona el sector";
                  // selectsector = false;
                  ciudad = value;
                  _cargaLista(value);
                });
              }),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("SECTOR",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          DropDownCatProf3(
              text: sector,
              lista: listaSectores,
              function: (value) {
                sector = capitalize(value);
                print(sector);
                setState(() {});
              }),
          Container(
            child: Material(
              type: MaterialType.transparency,
              child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.only(
                      left: getProportionateScreenWidth(20),
                    ),
                    child: Text("HORARIOS",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(18),
                          fontWeight: FontWeight.bold,
                        )),
                  )),
            ),
          ),
          hora(),
          Padding(
            padding: const EdgeInsets.only(top: 28.0),
            child: ButtonGray(
              textoBoton: "APLICAR",
              bFuncion: () {
                /*  Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.fade,
                                        child: Home()));*/
                print('1 $subcategoria1');
                print('2 $subcategoria2');
                print('3 $genero1');
                print('4 $genero2');
                print('5 $categoria');
                print('6 $rango1');
                print('7 $rango2');
                print('8 $cancelacion');
                print('9 $ciudad');
                print('10 $sector');
                print('11 $horainicioescogida');
                print('12 $horafinescogida');
                print('13 $ubicacion1');
                print('14 $ubicacion2');
                // print(listaservicios);
                if (subcategoria1 != null || subcategoria2 != null) {
                  selectproser = true;
                } else {
                  selectproser = false;
                }
                if (genero1 != null || genero2 != null) {
                  selectgenero = true;
                } else {
                  selectgenero = false;
                }
                if (categoria != 'Selecciona la categoría') {
                  selectcategoria = true;
                } else {
                  selectcategoria = false;
                }
                if (rango1 != null || rango2 != null) {
                  selectrango = true;
                } else {
                  selectrango = false;
                }
                if (cancelacion != 'Selecciona el tipo de cancelación') {
                  selectcancelacion = true;
                } else {
                  selectcancelacion = false;
                }
                if (ciudad != 'Selecciona la ciudad') {
                  selectciudad = true;
                } else {
                  selectciudad = false;
                }
                if (sector != 'Selecciona el sector') {
                  selectsector = true;
                } else {
                  selectsector = false;
                }
                if (horainicioescogida != null) {
                  selecthorainicio = true;
                } else {
                  selecthorainicio = false;
                }
                if (horafinescogida != null) {
                  selecthorafin = true;
                } else {
                  selecthorafin = false;
                }
                metodobool(
                    selectproser,
                    selectgenero,
                    selectcategoria,
                    selectrango,
                    selectcancelacion,
                    selectciudad,
                    selectsector,
                    selecthorainicio,
                    selecthorafin);
                if (selectproser == false &&
                    selectgenero == false &&
                    selectcategoria == false &&
                    selectrango == false &&
                    selectciudad == false &&
                    selectsector == false &&
                    selecthorainicio == false &&
                    selecthorafin == false) {
                  filtrada = [];
                }
                print('*****');
                print(selectproser);
                print(selectgenero);
                print(selectcategoria);
                print(selectrango);
                print(selectcancelacion);
                print(selectciudad);
                print(selectsector);
                print(selecthorainicio);
                print(selecthorafin);
                print(filtrada);
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.fade,
                        child: YaBusqueda(listafiltrada: filtrada)));
              },
              anchoBoton: getProportionateScreenHeight(350),
              largoBoton: getProportionateScreenWidth(27),
              colorTextoBoton: textColorClaro,
              opacityBoton: 1,
              sizeTextBoton: getProportionateScreenWidth(11),
              weightBoton: FontWeight.w400,
              color1Boton: darkColor,
              color2Boton: darkColor,
            ),
          ),
          SizedBox(
            height: 30,
          )
        ],
      )
    ]))));
  }

  void _cargaLista(capital) {
    if (capital == 'QUITO') {
      listaSectores = [
        "Selecciona el sector",
        "BELISARIO QUEVEDO",
        "CARCELÉN",
        "CENTRO HISTÓRICO",
        "COCHAPAMBA",
        "COMITÉ DEL PUEBLO",
        "COTOCOLLAO",
        "CHILIBULO",
        "CHILLOGALLO",
        "CHIMBACALLE",
        "EL CONDADO",
        "GUAMANÍ",
        "IÑAQUITO",
        "ITCHIMBÍA",
        "JIPIJAPA",
        "KENNEDY",
        "LA ARGELIA",
        "LA CONCEPCIÓN",
        "LA ECUATORIANA",
        "LA FERROVIARIA",
        "LA LIBERTAD",
        "LA MAGDALENA",
        "LA MENA",
        "MARISCAL SUCRE",
        "PONCEANO",
        "PUENGASÍ",
        "QUITUMBE",
        "RUMIPAMBA",
        "SAN BARTOLO",
        "SAN ISIDRO DEL INCA",
        "SAN JUAN",
        "SOLANDA",
        "TURUBAMBA",
        "QUITO DISTRITO METROPOLITANO",
        "ALANGASÍ",
        "AMAGUAÑA",
        "ATAHUALPA",
        "CALACALÍ",
        "CALDERÓN",
        "CONOCOTO",
        "CUMBAYÁ",
        "CHAVEZPAMBA",
        "CHECA",
        "EL QUINCHE",
        "GUALEA",
        "GUANGOPOLO",
        "GUAYLLABAMBA",
        "LA MERCED",
        "LLANO CHICO",
        "LLOA",
        "MINDO",
        "NANEGAL",
        "NANEGALITO",
        "NAYÓN",
        "NONO",
        "PACTO",
        "PEDRO VICENTE MALDONADO",
        "PERUCHO",
        "PIFO",
        "PÍNTAG",
        "POMASQUI",
        "PUÉLLARO",
        "PUEMBO",
        "SAN ANTONIO",
        "SAN JOSÉ DE MINAS",
        "SAN MIGUEL DE LOS BANCOS",
        "TABABELA",
        "TUMBACO",
        "YARUQUÍ",
        "ZAMBIZA",
        "PUERTO QUITO"
      ];
    } else if (capital == 'AMBATO') {
      listaSectores = [
        "Selecciona el sector",
        "ATOCHA – FICOA",
        "CELIANO MONGE",
        "HUACHI CHICO",
        "HUACHI LORETO",
        "LA MERCED",
        "LA PENÍNSULA",
        "MATRIZ",
        "PISHILATA",
        "SAN FRANCISCO",
        "AMBATILLO",
        "ATAHUALPA (CHISALATA)",
        "AUGUSTO N. MARTÍNEZ (MUNDUGLEO)",
        "CONSTANTINO FERNÁNDEZ (CAB. EN CULLITAHUA)",
        "HUACHI GRANDE",
        "IZAMBA"
            "JUAN BENIGNO VELA",
        "MONTALVO",
        "NUEVA AMBATO",
        "PASA",
        "PICAIGUA",
        "PILAGÜÍN (PILAHÜÍN)",
        "QUISAPINCHA (QUIZAPINCHA)",
        "SAN BARTOLOMÉ DE PINLLOG",
        "SAN FERNANDO (PASA SAN FERNANDO)",
        "SANTA ROSA",
        "TOTORAS",
        "CUNCHIBAMBA",
        "UNAMUNCHO"
      ];
    } else if (capital == 'GUAYAQUIL') {
      listaSectores = [
        "Selecciona el sector",
        "AYACUCHO",
        "BOLÍVAR (SAGRARIO)",
        "CARBO (CONCEPCIÓN)",
        "FEBRES CORDERO",
        "GARCÍA MORENO",
        "LETAMENDI",
        "NUEVE DE OCTUBRE",
        "OLMEDO (SAN ALEJO)",
        "ROCA",
        "ROCAFUERTE",
        "SUCRE",
        "TARQUI",
        "URDANETA",
        "XIMENA",
        "PASCUALES",
        "GUAYAQUIL",
        "CHONGÓN",
        "JUAN GÓMEZ RENDÓN (PROGRESO)",
        "MORRO",
        "PASCUALES",
        "PLAYAS (GRAL. VILLAMIL)",
        "POSORJA",
        "PUNÁ",
        "TENGUEL"
      ];
    } else if (capital == 'CUENCA') {
      listaSectores = [
        "Selecciona el sector",
        "BELLAVISTA",
        "CAÑARIBAMBA",
        "EL BATÁN",
        "EL SAGRARIO",
        "EL VECINO",
        "GIL RAMÍREZ DÁVALOS",
        "HUAYNACÁPAC",
        "MACHÁNGARA",
        "MONAY",
        "SAN BLAS",
        "SAN SEBASTIÁN",
        "SUCRE",
        "TOTORACOCHA",
        "YANUNCAY",
        "HERMANO MIGUEL",
        "CUENCA",
        "BAÑOS",
        "CUMBE",
        "CHAUCHA",
        "CHECA (JIDCAY)",
        "CHIQUINTAD",
        "LLACAO",
        "MOLLETURO",
        "NULTI",
        "OCTAVIO CORDERO PALACIOS (SANTA ROSA)",
        "PACCHA",
        "QUINGEO",
        "RICAURTE",
        "SAN JOAQUÍN",
        "SANTA ANA",
        "SAYAUSÍ",
        "SIDCAY",
        "SININCAY",
        "TARQUI",
        "TURI",
        "VALLE",
        "VICTORIA DEL PORTETE (IRQUIS)"
      ];
    } else if (capital == 'IBARRA') {
      listaSectores = [
        "Selecciona el sector",
        "CARANQUI",
        "GUAYAQUIL DE ALPACHACA",
        "SAGRARIO",
        "SAN FRANCISCO",
        "LA DOLOROSA DEL PRIORATO",
        "SAN MIGUEL DE IBARRA",
        "AMBUQUÍ",
        "ANGOCHAGUA",
        "CAROLINA",
        "LA ESPERANZA",
        "LITA",
        "SALINAS",
        "SAN ANTONIO"
      ];
    } else if (capital == 'RIOBAMBA') {
      listaSectores = [
        "Selecciona el sector",
        "LIZARZABURU",
        "MALDONADO",
        "VELASCO",
        "VELOZ",
        "YARUQUÍES",
        "RIOBAMBA",
        "CACHA (CAB. EN MACHÁNGARA)",
        "CALPI",
        "CUBIJÍES",
        "FLORES",
        "LICÁN",
        "LICTO",
        "PUNGALÁ",
        "PUNÍN",
        "QUIMIAG",
        "SAN JUAN",
        "SAN LUIS"
      ];
    } else if (capital == 'MANTA') {
      listaSectores = [
        "Selecciona el sector",
        "LOS ESTEROS",
        "MANTA",
        "SAN MATEO",
        "TARQUI",
        "ELOY ALFARO",
        "MANTA",
        "SAN LORENZO",
        "SANTA MARIANITA (BOCA DE PACOCHE)"
      ];
    } else if (capital == 'PORTOVIEJO') {
      listaSectores = [
        "Selecciona el sector",
        "PORTOVIEJO",
        "12 DE MARZO",
        "COLÓN",
        "PICOAZÁ",
        "SAN PABLO",
        "ANDRÉS DE VERA",
        "FRANCISCO PACHECO",
        "18 DE OCTUBRE",
        "SIMÓN BOLÍVAR",
        "PORTOVIEJO",
        "ABDÓN CALDERÓN (SAN FRANCISCO)",
        "ALHAJUELA (BAJO GRANDE)",
        "CRUCITA",
        "PUEBLO NUEVO",
        "RIOCHICO (RÍO CHICO)",
        "SAN PLÁCIDO",
        "CHIRIJOS"
      ];
    }
  }

  Widget hora() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              width: getProportionateScreenWidth(140),
              height: getProportionateScreenWidth(30),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                color: Colors.white10,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  horainicio();
                },
                child: horainicioescogida == null
                    ? Text("Ej. 10:00 AM",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.normal,
                        ))
                    : Text(horainicioescogida,
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.normal,
                        )),
              ),
            ),
            horainicioescogida != null
                ? IconButton(
                    icon: Icon(
                      Icons.remove_circle,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      horainicioescogida = null;
                      setState(() {});
                    })
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(left: 3.0, right: 3.0),
              child: Text("a",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(18),
                    fontWeight: FontWeight.normal,
                  )),
            ),
            Container(
              width: getProportionateScreenWidth(140),
              height: getProportionateScreenWidth(30),
              child: FlatButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                color: Colors.white10,
                padding: EdgeInsets.all(2.0),
                onPressed: () {
                  horaFin();
                },
                child: horafinescogida == null
                    ? Text("Ej. 10:00 AM",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.normal,
                        ))
                    : Text(horafinescogida,
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.normal,
                        )),
              ),
            ),
            horafinescogida != null
                ? IconButton(
                    icon: Icon(
                      Icons.remove_circle,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      horafinescogida = null;
                      setState(() {});
                    })
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  horainicio() {
    DatePicker.showTime12hPicker(
      context,
      showTitleActions: true,
      locale: LocaleType.es,
      // currentTime: ,
      //minTime: fechatimehoy.add(Duration(days: 1)) ,

      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        print('confirm $date');
        String fechartorno = DateFormat('h:mm a').format(date);
        // String horaretorno = DateFormat('hh:mm a').format(date);

        setState(() {
          horainicioescogida = fechartorno;
          horainiciodt = date;
        });
      },
    );
  }

  horaFin() {
    DatePicker.showTime12hPicker(
      context,
      showTitleActions: true,
      locale: LocaleType.es,
      onChanged: (date) {
        print('change $date in time zone ' +
            date.timeZoneOffset.inHours.toString());
      },
      onConfirm: (date) {
        print('confirm $date');
        String fechartorno = DateFormat('h:mm a').format(date);
        print(fechartorno);
        // String horaretorno = DateFormat('hh:mm a').format(date);
        print('horainiciodt $horainiciodt');
        if (horainiciodt == null) {
          showAlertDialogHora(context);
        }

        final differencehoras = date.difference(horainiciodt).inHours;
        print('difference $differencehoras');

        setState(() {
          horafinescogida = fechartorno;
        });
      },
    );
  }

  showAlertDialogHora(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("Entendido"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Recordatorio"),
      content: Text(
          "Recuerda seleccionar la hora de inicio y luego la hora de finalización"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the button
    Widget okButton = FlatButton(
      child: Text("Entendido"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Recordatorio"),
      content: Text("Recuerda seleccionar con 1 hora de adelanto"),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
