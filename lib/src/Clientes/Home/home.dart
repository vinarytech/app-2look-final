import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/registro.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';

class Home extends StatefulWidget {
  final int index;
  final bool finalizado;
  Home({Key key, this.index, this.finalizado}) : super(key: key);

  @override
  _HomeState createState() => new _HomeState();
}

final prefs = new PreferenciasUsuario();

class _HomeState extends State<Home> {
  //int page;
  static int valor;
  int pagina;
  @override
  void initState() {
    super.initState();

    print(prefs.token);
    print('valor index ${widget.index}');

    if (widget.index != null) {
      page = widget.index;
      print('valor NUEVO $valor');
    }
    print('widget finalizado ${widget.finalizado}');
    /*  if(widget.finalizado!=null && widget.finalizado==false){
        setState(() {
          valor=0;
        }); 
      }
      if(widget.finalizado!=null && widget.finalizado==true){
        setState(() {
          valor=1;
        });
      }*/
    /* if(widget.finalizado!=null && widget.finalizado==true){
       
          page=1;
       
      }*/
    print('widget page $page');

    print('el valor $valor');
  }

  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    Reservas(),
    Guardado(),
    Busqueda(),
    Mensajes(),
    (prefs.id == null)?Registro(regreso: false):Perfil(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            height: 50.0,
            backgroundColor: Color(0xfff191B1F),
            color: Color(0xfff191B1F),
            buttonBackgroundColor: Color(0xfffA91D74),
            items: <Widget>[
              TijerasIcon(),
              GuardadoIcon(),
              BuscarIcon(),
              MensajeIcon(),
              PerfilIcon()
            ],
            animationDuration: Duration(milliseconds: 200),
            animationCurve: Curves.bounceInOut,
            index: page,
            onTap: (index) {
              //Handle button tap
              setState(() {
                // if(widget.index!=null){
                page = index;
                print(page);
              });
            },
          ),
          body: Stack(
            children: [
              Container(
                // color: Colors.amber,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("lib/assets/images/splash/fondo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              _children[page]
              //:_children[widget.index],
            ],
          )),
    );
  }
}
