import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/iconobutton.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'busqueda.dart';
import 'mensajes.dart';

class Guardado extends StatefulWidget {
  @override
  _GuardadoState createState() => new _GuardadoState();
}

class _GuardadoState extends State<Guardado> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0, bottom: 10),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              alignment: Alignment.bottomCenter,
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Text("GUARDADOS",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(18),
                                        fontWeight: FontWeight.bold,
                                      ))),
                            ),
                            (prefs.id != null)
                                ? Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 18.0),
                                    child: Container(
                                        margin:
                                            EdgeInsets.symmetric(vertical: 0.0),
                                        child: GuardarCard(bFunction: (value) {
                                          setState(() {
                                            print(value);
                                          });
                                        })),
                                  )
                                : Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            getProportionateScreenWidth(30),
                                        vertical:
                                            getProportionateScreenHeight(20)),
                                    child: Column(
                                      children: [
                                        Text(
                                          "Por favor inicia sesión para poder visualizar los productos o servicios guardados que tienes",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(17),
                                            fontWeight: FontWeight.normal,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    20)),
                                        ButtonGray(
                                          textoBoton: "Iniciar Sesión",
                                          bFuncion: () {
                                            setState(() {
                                              page = 4;
                                            });
                                            Navigator.pushReplacement(
                                                context,
                                                PageTransition(
                                                    child: Home(),
                                                    type: PageTransitionType
                                                        .fade));
                                          },
                                          anchoBoton:
                                              getProportionateScreenHeight(330),
                                          largoBoton:
                                              getProportionateScreenWidth(27),
                                          colorTextoBoton: textColorClaro,
                                          opacityBoton: 1,
                                          sizeTextBoton:
                                              getProportionateScreenWidth(15),
                                          weightBoton: FontWeight.bold,
                                          color1Boton: primaryColor,
                                          color2Boton: primaryColor,
                                        ),
                                      ],
                                    )),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
}
