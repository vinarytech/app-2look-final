import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/textlineButton.dart';

class Reservas extends StatefulWidget {
 // final int pagina;
  Reservas({Key key,}) : super(key: key);

  @override
  _ReservasState createState() => _ReservasState();
}

class _ReservasState extends State<Reservas> {
  int pagi;
  @override
  void initState() { 
    super.initState();
     
    //  print('pagina ${widget.pagina}');

  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child:ListView(
                physics: BouncingScrollPhysics(),
                children:[
                  Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 85.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        //color: Colors.grey,
                       // height: MediaQuery.of(context).size.height * .732,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 18.0),
                              child: Container(
                                alignment: Alignment.bottomCenter,
                                child: Material(
                                    type: MaterialType.transparency,
                                    child: Text("RESERVAS",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(18),
                                          fontWeight: FontWeight.bold,
                                        ))),
                              ),
                            ),
                           
                            TextLineButton(
                            //  pagina: widget.pagina,
                                widgetText1: "En Proceso",
                                widgetText2: "Historial",
                              ),
                             
                            
                          ],
                        ),
                      ),
                    ),
                  ),
                   Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )
                ],
              )
                ]
              ) 
              )),
    );
  }
}
