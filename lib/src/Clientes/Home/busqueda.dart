import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Busqueda/yabusqueda.dart';
import 'package:tl_app/widgets/busqueda/filtrosIcon.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/carouselhorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/iconobutton.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/slidervertical.dart';
import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';

class Busqueda extends StatefulWidget {
  @override
  _BusquedaState createState() => new _BusquedaState();
}

List<Widget> listaRecomendados = [];
TextEditingController controller = new TextEditingController();
String valor;

class _BusquedaState extends State<Busqueda> {
  final searchCtrl = TextEditingController();
  final prefs = new PreferenciasUsuario();

  @override
  void initState() {
    FirebaseFirestore.instance
        .collection("Paquetes")
        .where("id_usuario", isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (value.docs.first.data()["suscripcion"].toString() == 'false' 
      && value.docs.first.data()["paquete1"].toString() == '' 
      && value.docs.first.data()["paquete2"].toString() == ''
      && value.docs.first.data()["paquete3"].toString() == ''
      && value.docs.first.data()["prueba"].toString() == '') {
        FirebaseFirestore.instance
            .collection("Servicios")
            .where("id_usuario", isEqualTo: prefs.id)
            .get()
            .then((value) {
          value.docs.forEach((element) {
            FirebaseFirestore.instance
                .collection("Servicios")
                .doc(element.id)
                .update({"estado": false});
          });
        });
      } else {
        FirebaseFirestore.instance
            .collection("Servicios")
            .where("id_usuario", isEqualTo: prefs.id)
            .get()
            .then((value) {
          value.docs.forEach((element) {
            FirebaseFirestore.instance
                .collection("Servicios")
                .doc(element.id)
                .update({"estado": true});
          });
        });
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: GestureDetector(
        onTap: () {
          controller.text = '';
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Scaffold(
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 45.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            height: 35,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 100.0),
                      child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          height: MediaQuery.of(context).size.height * .732,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                        width: getProportionateScreenWidth(310),
                                        child: TextField(
                                          /*  onChanged: (value){
                                            valor=value;
                                          },*/
                                          onSubmitted: (value) {
                                            valor = value;
                                            if (valor == '') {
                                              valor = null;
                                            }
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    type:
                                                        PageTransitionType.fade,
                                                    child: YaBusqueda(
                                                        valor: valor
                                                            .toLowerCase())));
                                          },
                                          textAlignVertical:
                                              TextAlignVertical.center,
                                          controller: controller,
                                          autocorrect: true,
                                          style:
                                              DecorationsOnInputs.textoInput(),
                                          decoration: InputDecoration(
                                              suffixIcon: Padding(
                                                padding: EdgeInsets.only(
                                                    top:
                                                        0), // add padding to adjust icon
                                                child: Icon(Icons.search,
                                                    color: Colors.white),
                                              ),
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(27)),
                                                borderSide: const BorderSide(
                                                    color: lightColor,
                                                    width: 5.0),
                                              ),
                                              errorStyle: TextStyle(
                                                color: textColorClaro,
                                              ),
                                              focusedErrorBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white,
                                                    width: 2.0),
                                              ),
                                              errorBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white,
                                                    width: 2.0),
                                              ),
                                              labelText: 'Buscar',
                                              labelStyle: TextStyle(
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        11),
                                                color: Colors.white70,
                                                fontWeight: FontWeight.w600,
                                                fontFamily: "Poppins",
                                              ),
                                              contentPadding:
                                                  new EdgeInsets.only(
                                                      left: 14.0),
                                              hintStyle: TextStyle(
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        11),
                                                color: Colors.white38,
                                                fontWeight: FontWeight.normal,
                                                fontFamily: "Poppins",
                                              ),
                                              focusedBorder: InputBorder.none,
                                              border: InputBorder.none,
                                              hintText: 'Buscar'),
                                        )),
                                    SizedBox(
                                      width: 14,
                                    ),
                                    FiltrosIcono(),
                                  ],
                                ),
                              ),
                              Container(
                                child: CarouselHorizontal(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  alignment: Alignment.bottomCenter,
                                  child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("RECOMENDADO",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.bold,
                                          ))),
                                ),
                              ),
                              StreamBuilder<QuerySnapshot>(
                                  stream: FirebaseFirestore.instance
                                      .collection('Usuarios')
                                      .where('recomendado', isEqualTo: true)
                                      .snapshots(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.hasData) {
                                      return ClipRRect(
                                        child: Container(
                                          width: 300,
                                          height:
                                              getProportionateScreenHeight(100),
                                          child: CarouselSlider(
                                            options: CarouselOptions(
                                                aspectRatio: 2.0,
                                                enlargeCenterPage: true,
                                                enableInfiniteScroll: true,
                                                viewportFraction: 0.8,
                                                scrollDirection: Axis.vertical,
                                                autoPlay: true,
                                                autoPlayAnimationDuration:
                                                    Duration(
                                                        milliseconds: 1500)),
                                            items: getExpenseItems(snapshot),
                                          ),
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Center(
                                          child: Text("Ha ocurrido un error"));
                                    } else {
                                      return Center(
                                          child: CircularProgressIndicator());
                                    }
                                  }),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                  ],
                ))),
      ),
    );
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      return Padding(
        padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenHeight(10),
            vertical: getProportionateScreenWidth(10)),
        child: GestureDetector(
          onTap: () {
            String id = doc.id;
            print(id);
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.fade,
                    child: YaBusqueda(recomendado: id)));
          },
          child: Container(
            height: getProportionateScreenHeight(80),
            //margin: EdgeInsets.all(5.0),
            child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                child: Stack(
                  children: <Widget>[
                    Image.network(
                        // 'lib/assets/images/busqueda/carusel.png',
                        doc.data()['fotoPortada'],
                        fit: BoxFit.cover,
                        width: getProportionateScreenWidth(1000.0)),
                    Container(
                      color: Colors.black.withOpacity(0.6),
                    ),
                    Center(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 20.0),
                        child: Text(
                          doc.data()['nombreEmpresa'].toString().toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: getProportionateScreenHeight(20.0),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      );
    }).toList();
  }
}
