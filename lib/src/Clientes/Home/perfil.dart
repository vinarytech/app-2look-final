import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/clases/iconosperfilsvg.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Perfil/editarperfil.dart';
import 'package:tl_app/src/Clientes/Perfil/metododepago.dart';
import 'package:tl_app/src/Clientes/Perfil/notificaciones.dart';
import 'package:tl_app/src/Clientes/Problemas/facturacion.dart';
import 'package:tl_app/src/Clientes/Problemas/solicitarAyuda.dart';
import 'package:tl_app/src/Clientes/Problemas/valoresPendientes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/IntroProf/requisitosProf.dart';
import 'package:tl_app/widgets/Pago/tarjetaspago.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/pagoTarjeta.dart';
import 'package:tl_app/widgets/perfilInfo.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';

import 'package:tl_app/widgets/perfilinicio.dart';

import 'package:tl_app/widgets/size_config.dart';

import 'home.dart';

class Perfil extends StatefulWidget {
  Perfil({Key key}) : super(key: key);

  @override
  _PerfilState createState() => _PerfilState();
}

class _PerfilState extends State<Perfil> {
  PreferenciasUsuario prefs = new PreferenciasUsuario();
  final usuarioS = new UsuarioServices();
  @override
  void initState() {
    usuarioS.obtenerUsuarioId(prefs.id);
    notificacion = false;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(40),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  ListView(
                    physics: BouncingScrollPhysics(),
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: getProportionateScreenHeight(110),
                          ),
                          Container(
                            child: Column(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(200),
                                  child: (prefs.usuario['fotografia_perfil'] ==
                                              null ||
                                          prefs.usuario['fotografia_perfil'] ==
                                              '')
                                      ? Image.asset(
                                          'lib/assets/images/perfil/perfil.png',
                                          height:
                                              getProportionateScreenHeight(110),
                                        )
                                      : Image.network(
                                          prefs.usuario['fotografia_perfil'],
                                          height:
                                              getProportionateScreenHeight(110),
                                        ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(10),
                          ),
                          Text(
                            prefs.usuario['nombre'],
                            style: TextStyle(
                              fontFamily: 'Poppins-Light',
                              fontSize: 16.573440551757812,
                              color: const Color(0xffffffff),
                              height: 1.2000000920677214,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(2),
                          ),
                          Text(
                            prefs.usuario['correo'],
                            style: TextStyle(
                              fontFamily: 'Poppins-Light',
                              fontSize: 16.573440551757812,
                              color: const Color(0xffffffff),
                              height: 1.2000000920677214,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(2),
                          ),
                          Text(
                            prefs.usuario['celular'],
                            style: TextStyle(
                              fontFamily: 'Poppins-Light',
                              fontSize: 16.573440551757812,
                              color: const Color(0xffffffff),
                              height: 1.2000000920677214,
                            ),
                            textAlign: TextAlign.left,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: SingleChildScrollView(
                              physics: BouncingScrollPhysics(),
                              child: Container(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    SizedBox(
                                      height: getProportionateScreenHeight(20),
                                    ),
                                    Container(
                                      child: ButtonGray(
                                        textoBoton: "EDITAR PERFIL",
                                        bFuncion: () async {
                                          var data = await Navigator.push(
                                              context,
                                              PageTransition(
                                                  type: PageTransitionType.fade,
                                                  child: EditarPerfil()));
                                          setState(() {
                                            usuarioS.obtenerUsuarioId(prefs.id);
                                          });
                                        },
                                        anchoBoton:
                                            getProportionateScreenHeight(280),
                                        largoBoton:
                                            getProportionateScreenWidth(30),
                                        colorTextoBoton: textColorClaro,
                                        opacityBoton: 1,
                                        sizeTextBoton:
                                            getProportionateScreenWidth(11),
                                        weightBoton: FontWeight.w400,
                                        color1Boton: primaryColor,
                                        color2Boton: primaryColor,
                                      ),
                                    ),
                                    Container(
                                      child: Column(
                                        children: <Widget>[
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .fade,
                                                        child:
                                                            Notificaciones()));
                                              },
                                              // child: PerfilInfo(
                                              //   texto: "Notificaciones",
                                              //   svg: IconosPerfilSvg().iconoPerfil01(),
                                              // ),
                                              child: Container(
                                                height: 30,
                                                color: Color.fromRGBO(
                                                        255, 255, 255, 0.19)
                                                    .withOpacity(.0),
                                                child: Stack(
                                                  children: <Widget>[
                                                    Transform.translate(
                                                      offset: Offset(0.0, 31.1),
                                                      child: Container(
                                                        width: 253.1,
                                                        height: 1.6,
                                                        color: const Color(
                                                            0x1affffff),
                                                      ),
                                                    ),
                                                    Transform.translate(
                                                      offset: Offset(14.2, 7.6),
                                                      child: SizedBox(
                                                        width: 215.0,
                                                        height: 19.0,
                                                        child: Stack(
                                                          children: <Widget>[
                                                            Text(
                                                              'Notificaciones',
                                                              style: TextStyle(
                                                                fontFamily:
                                                                    'Poppins-Light',
                                                                fontSize: 13,
                                                                color: const Color(
                                                                    0xffffffff),
                                                                height:
                                                                    1.0517760790311372,
                                                              ),
                                                              textAlign:
                                                                  TextAlign
                                                                      .left,
                                                            ),
                                                            (notificacionPerfil)
                                                                ? Transform
                                                                    .translate(
                                                                    offset: Offset(
                                                                        getProportionateScreenHeight(
                                                                            -16),
                                                                        0),
                                                                    child: Container(
                                                                        height: getProportionateScreenHeight(13),
                                                                        width: getProportionateScreenHeight(13),
                                                                        decoration: BoxDecoration(
                                                                          shape:
                                                                              BoxShape.circle,
                                                                          color:
                                                                              Colors.red,
                                                                        )),
                                                                  )
                                                                : SizedBox(),
                                                            Transform.translate(
                                                              offset: Offset(
                                                                  200.2, 0),
                                                              child: SvgPicture
                                                                  .string(
                                                                IconosPerfilSvg()
                                                                    .iconoPerfil01(),
                                                                allowDrawingOutsideViewBox:
                                                                    true,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )),
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  PageTransition(
                                                      type: PageTransitionType
                                                          .fade,
                                                      child: Tarjetas()));
                                            },
                                            child: PerfilInfo(
                                              texto: "Método De Pago",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil02(),
                                            ),
                                          ),
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              if (prefs.usuario[
                                                          'nombreEmpresa'] ==
                                                      null ||
                                                  prefs.usuario[
                                                          'nombreEmpresa'] ==
                                                      '') {
                                                Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .fade,
                                                        child:
                                                            RequisitosProf()));
                                              } else {
                                                setState(() {
                                                  pageProf = 2;
                                                  Navigator.pushReplacement(
                                                      context,
                                                      PageTransition(
                                                          type:
                                                              PageTransitionType
                                                                  .fade,
                                                          child: HomeProf()));
                                                });
                                              }
                                            },
                                            child: PerfilInfo(
                                              texto: "Cambiar A Profesional",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil03(),
                                            ),
                                          ),
                                               SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  PageTransition(
                                                      child:
                                                          ValoresPendientes(),
                                                      type: PageTransitionType
                                                          .fade));
                                            },
                                            child: PerfilInfo(
                                              texto: "Valores Pendientes",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil06(),
                                            ),
                                          ),
                                               SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          // GestureDetector(
                                          //   onTap: () {
                                          //     Navigator.push(
                                          //         context,
                                          //         PageTransition(
                                          //             child:
                                          //                 Facturacion(),
                                          //             type: PageTransitionType
                                          //                 .fade));
                                          //   },
                                          //   child: PerfilInfo(
                                          //     texto: "Facturación",
                                          //     svg: IconosPerfilSvg()
                                          //         .iconoFacturacionPf04(),
                                          //   ),
                                          // ),
                                          // SizedBox(
                                          //   height:
                                          //       getProportionateScreenHeight(
                                          //           15),
                                          // ),
                                          GestureDetector(
                                            onTap: () {
                                            _launchURL();
                                             },
                                            child: PerfilInfo(
                                              texto: "Políticas y Privacidad",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil04(),
                                            ),
                                          ),
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  PageTransition(
                                                      type: PageTransitionType
                                                          .fade,
                                                      child: SolicitarAyuda()));
                                            },
                                            child: PerfilInfo(
                                              texto: "Solicitar Ayuda",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil05(),
                                            ),
                                          ),
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    15),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              setState(() async {
                                                Navigator.pushReplacement(
                                                    context,
                                                    PageTransition(
                                                        type: PageTransitionType
                                                            .fade,
                                                        child: Home()));
                                                prefs.token = await null;
                                                prefs.usuario = await null;
                                                prefs.id = await null;
                                              });
                                            },
                                            child: PerfilInfo(
                                              texto: "Cerrar Sesión",
                                              svg: IconosPerfilSvg()
                                                  .iconoPerfil07(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
  _launchURL() async {
  const url = 'https://2look.app/index.php/politica-privacidad/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
}
