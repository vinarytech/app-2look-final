import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Mensajes/lecturaMensajes.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/mensajeResumen.dart';
import 'package:tl_app/widgets/size_config.dart';

class Mensajes extends StatefulWidget {
  @override
  _MensajesState createState() => _MensajesState();
}

class _MensajesState extends State<Mensajes> {
  final prefs = PreferenciasUsuario();
  List<Mensaje> listaMensaje1 = [];
  List<Mensaje> listaMensaje2 = [];
  List<Mensaje> listaMensaje = [];
  Mensaje mensaje;
  String nombre = '';
  String estrellas = '3';
  final convert = new NumberFormat("#,##0.00", "en_US");
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 110.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.bottomCenter,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text("MENSAJES",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(18),
                                      fontWeight: FontWeight.bold,
                                    ))),
                          ),
                          SizedBox(),
                          (prefs.id != null)?StreamBuilder(
                              stream: FirebaseFirestore.instance
                                  .collection("Mensajes")
                                  .where('idemisor', isEqualTo: prefs.id)
                                  .snapshots(),
                              builder: (context,
                                  AsyncSnapshot<QuerySnapshot> snapshot) {
                                
                                listaMensaje1 = [];
                                if (snapshot.hasData) {
                                        
                                  for (var item in snapshot.data.docs) {
                                    FirebaseFirestore.instance
                                        .collection("Usuarios")
                                        .doc(item.data()['idreceptor'])
                                        .snapshots()
                                        .first
                                        .then((value) {
                                
                                        nombre =
                                            value.data()['nombre'].toString();
                                        estrellas = value
                                            .data()['estrellas']
                                            .toString();
                                    });
                                    listaMensaje1.add(Mensaje(
                                        fecha: item.data()['fecha'],
                                        idemisor:
                                            item.data()['idemisor'].toString(),
                                        mensaje:
                                            item.data()['mensaje'].toString(),
                                        idreceptor: item
                                            .data()['idreceptor']
                                            .toString(),
                                        id: item.id,
                                        calificacion: estrellas,
                                        nombre: nombre,
                                        idsolicitud: item
                                            .data()['idsolicitud']
                                            .toString()));
                                    // }
                                  }
                                  return StreamBuilder(
                                    stream: FirebaseFirestore.instance
                                        .collection("Mensajes")
                                        .where('idreceptor',
                                            isEqualTo: prefs.id)
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot>
                                            snapshot2) {
                       
                                      listaMensaje2 = [];
                                      if (snapshot2.hasData) {
                                          for (var item in snapshot2.data.docs) {
                                          // if (item.data()['estado']) {
                                          FirebaseFirestore.instance
                                              .collection("Usuarios")
                                              .doc(item.data()['idemisor'])
                                              .snapshots()
                                              .first
                                              .then((value) {
                                       
                                              nombre = value
                                                  .data()['nombre']
                                                  .toString();
                                              estrellas = value
                                                  .data()['estrellas']
                                                  .toString();
                                          });
                                          listaMensaje2.add(Mensaje(
                                              fecha: item
                                                  .data()['fecha'],
                                              idemisor: item
                                                  .data()['idemisor']
                                                  .toString(),
                                              mensaje: item
                                                  .data()['mensaje']
                                                  .toString(),
                                              idreceptor: item
                                                  .data()['idreceptor']
                                                  .toString(),
                                              id: item.id,
                                              calificacion:
                                                  estrellas.toString(),
                                              nombre: nombre,
                                              idsolicitud: item
                                                  .data()['idsolicitud']
                                                  .toString()));
                                          // }
                                        }
                                        listaMensaje = [
                                          listaMensaje1,
                                          listaMensaje2
                                        ].expand((x) => x).toList();
                                        listaMensaje.sort((a, b) =>
                                            b.fecha.compareTo(a.fecha));
                                        return Column(
                                            children: List.generate(
                                          listaMensaje.length,
                                          (index) {
                                            return Container(
                                                child: Column(
                                              children: <Widget>[
                                                //margin: EdgeInsets.symmetric(vertical: 20.0),

                                                Container(
                                                  //height: 56.0,
                                                  child: Padding(
                                                    padding: EdgeInsets.all(
                                                        getProportionateScreenHeight(
                                                            10)),
                                                    child: Column(
                                                      children: <Widget>[
                                                        Container(
                                                          height: 56,
                                                          width: 350,
                                                          padding:
                                                              EdgeInsets.all(
                                                                  0.5),
                                                          decoration: BoxDecoration(
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          27)),
                                                              boxShadow: [
                                                                BoxShadow(
                                                                    color: Colors
                                                                        .white30,
                                                                    blurRadius:
                                                                        0)
                                                              ],
                                                              color:
                                                                  lightColor),
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            elevation: 6.0,
                                                            color: Colors
                                                                .transparent,
                                                            shadowColor:
                                                                Colors.grey[50],
                                                            child: InkWell(
                                                              splashColor:
                                                                  Colors
                                                                      .white30,
                                                              onTap: () {
                                                                Navigator.push(
                                                                    context,
                                                                    PageTransition(
                                                                        type: PageTransitionType
                                                                            .fade,
                                                                        child: LecturaMensajes(
                                                                            idMensaje:
                                                                                listaMensaje[index].id,
                                                                            idSolicitud: listaMensaje[index].idsolicitud,
                                                                            estrellas:listaMensaje[index].calificacion,nombre:listaMensaje[index].nombre)));
                                                              },
                                                              borderRadius: BorderRadius
                                                                  .all(Radius
                                                                      .circular(
                                                                          27)),
                                                              child: Center(
                                                                  child: Align(
                                                                alignment: Alignment
                                                                    .bottomRight,
                                                                child: Center(
                                                                  child:
                                                                      Padding(
                                                                    padding: const EdgeInsets
                                                                            .symmetric(
                                                                        horizontal:
                                                                            20),
                                                                    child: Text(
                                                                        listaMensaje[index]
                                                                            .mensaje
                                                                            .toString(),
                                                                        // "Hola Christian, la entrega de tu poducto o servicio sadas askjdnas asjdnkas.. ",
                                                                        overflow:
                                                                            TextOverflow
                                                                                .ellipsis,
                                                                        style: TextStyle(
                                                                            color:
                                                                                textColorClaro,
                                                                            fontSize:
                                                                                getProportionateScreenWidth(12),
                                                                            fontWeight: FontWeight.normal)),
                                                                  ),
                                                                ),
                                                              )),
                                                            ),
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 48.0,
                                                                  top: 5),
                                                          child: Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                height: 30,
                                                                //width: 150,
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      listaMensaje[index]
                                                                              .nombre
                                                                              .toString() +
                                                                          " / " +
                                                                          double.parse(listaMensaje[index].calificacion.toString())
                                                                              .toString(),
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.normal,
                                                                      )),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Container(
                                                                alignment:
                                                                    Alignment
                                                                        .topCenter,
                                                                child: Icon(
                                                                  Icons.star,
                                                                  size: 14,
                                                                  color:
                                                                      primaryColor,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(), SizedBox()
                                              ],
                                            ));
                                          },
                                        ));

                                      } else {
                                        
                                        return CircularProgressIndicator();
                                      }
                                    },
                                  );
                                } else {
                                  return CircularProgressIndicator();
                                }
                              }):Padding(
          padding:  EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(30), vertical: getProportionateScreenHeight(20)),
          child: Column(
            children: [
              Text(
                  "Por favor inicia sesión para poder visualizar los mensajes que tienes",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(17),
                    fontWeight: FontWeight.normal,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                ButtonGray(
                  textoBoton: "Iniciar Sesión",
                  bFuncion: () {
                    setState(() {
                      page = 4;
                    });
                    Navigator.pushReplacement(context, PageTransition(child: Home(), type: PageTransitionType.fade));
                  },
                  anchoBoton: getProportionateScreenHeight(330),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.bold,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
            ],
          )),
                        ],
                      )),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
}

class Mensaje {
  String id;
  Timestamp fecha;
  String idemisor;
  String idreceptor;
  String idsolicitud;
  String mensaje;
  String nombre;
  String calificacion;
  Mensaje(
      {this.id,
      this.fecha,
      this.idemisor,
      this.idreceptor,
      this.idsolicitud,
      this.mensaje,
      this.nombre,
      this.calificacion});
}
