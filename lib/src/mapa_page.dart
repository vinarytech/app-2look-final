import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:address_search_text_field/address_search_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:mapbox_gl/mapbox_gl.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class MapPage extends StatefulWidget {
  @override
  State createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  GoogleMapController _controller;
  final TrabajosProvider trabajosP = new TrabajosProvider();

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  Set<Marker> _markers = {};
  BitmapDescriptor pinLocationIcon;

  @override
  void initState() {
    super.initState();
    setCustomMapPin();
  }

  @override
  Widget build(BuildContext context) {
    // @override
    // void initState() {
    //   super.initState();

    // }

    return Scaffold(
      body: Stack(children: <Widget>[
        GoogleMap(
            markers: _markers,
            onTap: (data) {
              setState(() {
                posicionMarker(data.latitude, data.longitude);
              });
            },
            mapType: MapType.normal,
            myLocationEnabled: true,
            onMapCreated: _onMapCreated,
            initialCameraPosition:
                CameraPosition(target: LatLng(-1.24, -78.63), zoom: 15)),
        SafeArea(
            child: Column(
          children: [
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: BotonAtras()),
            AddressSearchTextField(
              controller: TextEditingController(),

              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  enabledBorder: const OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Color(0xffF6C900), width: 1.0),
                  ),
                  hintText: "Escribe aquí la ubicación...",
                  hintStyle: TextStyle(
                      color: Color(0xff313842),
                      fontStyle: FontStyle.italic,
                      fontSize: 20)),
              style: TextStyle(),
              onDone: (AddressPoint point) {
                setState(() {
                  posicionMarker(point.latitude, point.longitude);
                  _controller.animateCamera(CameraUpdate.newLatLng(
                      LatLng(point.latitude, point.longitude)));
                  Navigator.pop(context);
                });
              },
              // barrierDismissible: bool,
              country: 'Ecuador',
              // city: String,
              hintText: "Escribe aquí la ubicación...",
              noResultsText: "Esperando...",
              exceptions: <String>[],
              // coordForRef: bool,
              onCleaned: () {},
            ),
          ],
        )),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 50),
            child: ButtonGray(
              textoBoton: "Guardar Ubicacion",
              bFuncion: () {
                setState(() {
                  trabajosP.guardarUbicacion(
                      ubicacion.direccion,
                      ubicacion.longitud.toString(),
                      ubicacion.latitud.toString());
                  //  widget.valor = trabajosP.retornarUbicacion();
                  // print(trabajosP.retornarUbicacion().direccion);
                });
                Navigator.pop(context, trabajosP.retornarUbicacion());
              },
              anchoBoton: getProportionateScreenHeight(330),
              largoBoton: getProportionateScreenWidth(47),
              colorTextoBoton: textColorClaro,
              opacityBoton: 1,
              sizeTextBoton: getProportionateScreenWidth(15),
              weightBoton: FontWeight.bold,
              color1Boton: primaryColor,
              color2Boton: primaryColor,
            ),
          ),
        )
      ]),
    );
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))
        .buffer
        .asUint8List();
  }

  void setCustomMapPin() async {
    // pinLocationIcon = await BitmapDescriptor.fromAssetImage(
    //     ImageConfiguration(devicePixelRatio: 10),
    //     'assets/Imagenes/ubicacion.png');
    Position position = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    posicionMarker(position.latitude, position.longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        Coordinates(position.latitude, position.longitude));
    setState(() {
      var first = addresses.first;
      trabajosP.guardarUbicacion(first.subAdminArea,
          position.longitude.toString(), position.latitude.toString());
    });

    // print("impresiond"+
    // ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
  }

  void posicionMarker(latitude, longitude) async {
    var addresses = await Geocoder.local
        .findAddressesFromCoordinates(Coordinates(latitude, longitude));
    var first = addresses.first;
    // print("impresiond"+
    // ' ${first.locality}, ${first.adminArea},${first.subLocality}, ${first.subAdminArea},${first.addressLine}, ${first.featureName},${first.thoroughfare}, ${first.subThoroughfare}');
    setState(() {
      _markers.add(Marker(
        markerId: MarkerId('sada'),
        position: LatLng(latitude, longitude),
        onTap: () {
          print('Tapped');
        },
        icon: pinLocationIcon,
      ));
      _controller
          .animateCamera(CameraUpdate.newLatLng(LatLng(latitude, longitude)));
      trabajosP.guardarUbicacion(
          first.addressLine, longitude.toString(), latitude.toString());
      // print("impresiond"+
      // ' ${first.locality}: ${first.adminArea}:${first.subLocality}: ${first.subAdminArea}:${first.addressLine}: ${first.featureName}:${first.thoroughfare}: ${first.subThoroughfare}');
    });
  }
}

Ubicacion ubicacion = new Ubicacion();

class TrabajosProvider {
  guardarUbicacion(direccion, longitud, latitud) {
    ubicacion.direccion = direccion;
    ubicacion.latitud = latitud;
    ubicacion.longitud = longitud;
  }

  retornarUbicacion() {
    return ubicacion;
  }
}

class Ubicacion {
  String direccion;
  String longitud;
  String latitud;

  Ubicacion({this.direccion, this.longitud, this.latitud});
}
