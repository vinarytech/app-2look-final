import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:intl/intl.dart';

import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/widgets/informacionfinal.dart';




class ReportarProblemaProf extends StatefulWidget {
   final InformacionFinal snap;
  ReportarProblemaProf({Key key, this.snap}) : super(key: key);

  @override
  _ReportarProblemaProfState createState() => _ReportarProblemaProfState();
}

class _ReportarProblemaProfState extends State<ReportarProblemaProf> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  String mensaje;

  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          // Navigator.pushReplacement(
          //     context,
          //     PageTransition(
          //         type: PageTransitionType.fade, child: AlertaCustom()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ButtonGray(
                              textoBoton: "El usuario no llego",
                              bFuncion: () {
                                mensaje="El usuario no llego";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton:
                                  "El usuario no se encontro en el lugar acordado",
                              bFuncion: () {
                                 mensaje="El usuario no se encontro en el lugar acordado";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "Tuve un accidente",
                              bFuncion: () {
                                mensaje="Tuve un accidente";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "No me pagaron lo acordado",
                              bFuncion: () {
                                mensaje="No me pagaron lo acordado";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            ButtonGray(
                              textoBoton: "Otros",
                              bFuncion: () {
                                 mensaje="Otros";
                                reportarProblema(mensaje);
                              },
                              anchoBoton: getProportionateScreenHeight(356),
                              largoBoton: getProportionateScreenWidth(28),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            SizedBox(
                              height: 40,
                            ),
                            Container(
                              child: ButtonGray(
                                textoBoton: "ENVIAR",
                                bFuncion: () {
                                  // Navigator.pushReplacement(
                                  //     context,
                                  //     PageTransition(
                                  //         type: PageTransitionType.fade,
                                  //         child: Home()));
                                },
                                anchoBoton: getProportionateScreenHeight(356),
                                largoBoton: getProportionateScreenWidth(28),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.w400,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
  loaderDialogNormal(BuildContext context) {
    showDialog(

        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
   reportarProblema(String mensaje) {
    var promedio;
    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
     Future(() => loaderDialogNormal(context)).then((v) {


            FirebaseFirestore.instance.collection('Problemas').add({
              'id_usuario_recibe':widget.snap.docservicio.data()["id_usuario"],
              'id_usuario_envia':prefs.id,
              'created_at': date,
              'token_recibe': widget.snap.doc.data()["token"],
              'token_envia': prefs.usuario['token'],
              'problema':mensaje,
              'status':true                                  
             }).then((value) {
            //  sendNotification(notificacion, '2Look',widget.token);
              FirebaseFirestore.instance
            .collection('Solicitudes')
            .doc(widget.snap.documento.id)
            .update({'estado': 'FINALIZADO'});
                                     
             }).then((value) =>  Navigator.pop(context)).then((value) {
               _alerta(context, 'Tu calificación ha sido enviada, gracias por mejorar la comunidad 2Look');                  
             });
                                  
                                    
        });
  }
  _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                               Navigator.of(context).pop();
                                 Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                     Home(index: 0, )), (Route<dynamic> route) => false);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
           
  }

}
