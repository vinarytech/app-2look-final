import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/home/buscarIcon.dart';
import 'package:tl_app/widgets/home/guardadoIcon.dart';
import 'package:tl_app/widgets/home/mensajeIcon.dart';
import 'package:tl_app/widgets/home/perfilIcon.dart';
import 'package:tl_app/widgets/home/tijerasIcon.dart';
import 'package:tl_app/widgets/mensajeCompleto.dart';
import 'package:tl_app/widgets/mensajeResumen.dart';
import 'package:tl_app/widgets/size_config.dart';

class LecturaMensajesProf extends StatefulWidget {
  LecturaMensajesProf({Key key}) : super(key: key);

  @override
  _LecturaMensajesProfState createState() => _LecturaMensajesProfState();
}

class _LecturaMensajesProfState extends State<LecturaMensajesProf> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  _children[page],
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        height: MediaQuery.of(context).size.height * .750,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 20.0),
                                height: 200.0,
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      child: MensajeCompleto(),
                                    ),
                                    Container(
                                      child: MensajeCompleto(),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                                height: 40,
                                width: MediaQuery.of(context).size.width - 40,
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(27)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.white30, blurRadius: 0)
                                  ],
                                  color: lightColor,
                                ),
                                child: Container(
                                  padding: EdgeInsets.only(left: 20),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: TextField(
                                          style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      14),
                                              fontWeight: FontWeight.normal),
                                          decoration: InputDecoration(
                                              hintText: "Envia un mensaje...",
                                              hintStyle: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          14),
                                                  fontWeight:
                                                      FontWeight.normal),
                                              border: InputBorder.none),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {},
                                        child: Container(
                                          height: 40,
                                          width: 60,
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(27)),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: Colors.white30,
                                                  blurRadius: 0)
                                            ],
                                            color: lightColor,
                                          ),
                                          //padding: EdgeInsets.all(12),
                                          child: Image.asset(
                                              "lib/assets/images/mensajes/flechamensaje.png"),
                                        ),
                                      )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }
}
