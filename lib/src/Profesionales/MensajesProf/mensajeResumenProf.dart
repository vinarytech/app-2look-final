import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Mensajes/lecturaMensajes.dart';
import 'package:tl_app/src/Profesionales/MensajesProf/lecturaMensajesProf.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';



class MensajeResumenProf extends StatefulWidget {
  MensajeResumenProf({Key key}) : super(key: key);

  @override
  _MensajeResumenProfState createState() => _MensajeResumenProfState();
}

class _MensajeResumenProfState extends State<MensajeResumenProf> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenHeight(10)),
      child: Column(
        children: <Widget>[
          Container(
            height: 56,
            width: 350,
            padding: EdgeInsets.all(0.5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(27)),
              boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
              color:lightColor,
            ),
            child: Material(
              type: MaterialType.transparency,
              elevation: 6.0,
              color: Colors.transparent,
              shadowColor: Colors.grey[50],
              child: InkWell(
                splashColor: Colors.white30,
                onTap: () {
                  Navigator.push(
                      context,
                      PageTransition(
                          type: PageTransitionType.fade,
                          child: LecturaMensajes()));
                },
                borderRadius: BorderRadius.all(Radius.circular(27)),
                child: Center(
                    child: Align(
                  alignment: Alignment.bottomRight,
                  child: Center(
                    child: Text(
                        "Hola Christian, la entrega de tu poducto o servicio es... ",
                        style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(12),
                            fontWeight: FontWeight.normal)),
                  ),
                )),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 48.0, top: 5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  height: 30,
                  //width: 150,
                  child: Material(
                    type: MaterialType.transparency,
                    child: Text("Christian / 4.6",
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(11),
                          fontWeight: FontWeight.normal,
                        )),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Icon(
                    Icons.star,
                    size: 14,
                    color: primaryColor,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
