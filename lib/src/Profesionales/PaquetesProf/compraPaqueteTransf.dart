import 'dart:io';
import 'dart:ui';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';

import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/Profesionales/PaquetesProf/tipopagoProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/tarjetasMetodo.dart';

class CompraPaqueteTransf extends StatefulWidget {
  final paquete;
  CompraPaqueteTransf({Key key, this.paquete}) : super(key: key);

  @override
  _CompraPaqueteTransfState createState() => _CompraPaqueteTransfState();
}

class _CompraPaqueteTransfState extends State<CompraPaqueteTransf> {
  bool transferencia = false;
  bool tarjeta = false;
  String paquete;
  String precio;
  String texto;
  final searchCtrl = TextEditingController();
  final transferenciaCtlr = TextEditingController();
  final prefs = PreferenciasUsuario();
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  void initState() {
    if (widget.paquete == 'paquete1') {
      precio = '\$ 5.00';
      texto = 'Productos y servicios ilimitados / 7 días';
    } else if (widget.paquete == 'paquete2') {
      precio = '\$ 15.00';
      texto = 'Productos y servicios ilimitados / 30 días';
    } else if (widget.paquete == 'paquete3') {
      precio = '\$ 25.00';
      texto = 'Productos y servicios ilimitados y destacados / 30 días';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 115,
                              height: 115,
                              child: FlatButton(
                                color: Colors.transparent,
                                textColor: Colors.white,
                                padding: EdgeInsets.all(8.0),
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onPressed: () {
                                  // Navigator.pushReplacement(
                                  //     context,
                                  //     PageTransition(
                                  //         type: PageTransitionType.fade,
                                  //         child: CompraPaqueteTransf()));
                                },
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: primaryColor,
                                        width: 1,
                                        style: BorderStyle.solid),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      precio,
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(20),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      texto,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(10),
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, left: 8, right: 8),
                              child: Container(
                                child: Column(
                                  children: [
                                    Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text('MÉTODO DE PAGO',
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          // Row(
                                          //   mainAxisAlignment:
                                          //       MainAxisAlignment.spaceBetween,
                                          //   children: [
                                          //     Material(
                                          //       type: MaterialType.transparency,
                                          //       child: Align(
                                          //           alignment:
                                          //               Alignment.topLeft,
                                          //           child: Padding(
                                          //             padding: EdgeInsets.only(
                                          //               left:
                                          //                   getProportionateScreenWidth(
                                          //                       20),
                                          //             ),
                                          //             child: Text(
                                          //                 'Transferencia o depósito',
                                          //                 style: TextStyle(
                                          //                   color:
                                          //                       textColorClaro,
                                          //                   fontSize:
                                          //                       getProportionateScreenWidth(
                                          //                           14),
                                          //                   fontWeight:
                                          //                       FontWeight
                                          //                           .normal,
                                          //                 )),
                                          //           )),
                                          //     ),
                                          //     Padding(
                                          //       padding: const EdgeInsets.only(
                                          //           right: 20.0),
                                          //       child: Container(
                                          //         height: 20,
                                          //         width: 20,
                                          //         child: CircularCheckBox(
                                          //             value: transferencia,
                                          //             checkColor:
                                          //                 textColorMedio,
                                          //             activeColor: primaryColor,
                                          //             inactiveColor:
                                          //                 textColorMedio,
                                          //             disabledColor:
                                          //                 Colors.grey,
                                          //             onChanged: (val) =>
                                          //                 this.setState(() {
                                          //                   transferencia =
                                          //                       !transferencia;
                                          //                   if (transferencia) {
                                          //                     tarjeta = false;
                                          //                   }
                                          //                 })),
                                          //       ),
                                          //     ),
                                          //   ],
                                          // ),
                                          // SizedBox(
                                          //   height: 10,
                                          // ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Material(
                                                type: MaterialType.transparency,
                                                child: Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Padding(
                                                      padding: EdgeInsets.only(
                                                        left:
                                                            getProportionateScreenWidth(
                                                                20),
                                                      ),
                                                      child: Text('Tarjeta',
                                                          style: TextStyle(
                                                            color:
                                                                textColorClaro,
                                                            fontSize:
                                                                getProportionateScreenWidth(
                                                                    14),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          )),
                                                    )),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 20.0),
                                                child: Container(
                                                  height: 20,
                                                  width: 20,
                                                  child: CircularCheckBox(
                                                      value: tarjeta,
                                                      checkColor:
                                                          textColorMedio,
                                                      activeColor: primaryColor,
                                                      inactiveColor:
                                                          textColorMedio,
                                                      disabledColor:
                                                          Colors.grey,
                                                      onChanged: (val) =>
                                                          this.setState(() {
                                                            tarjeta = !tarjeta;
                                                            if (tarjeta) {
                                                              transferencia =
                                                                  false;
                                                            }
                                                          })),
                                                ),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(10),
                            ),
                            (transferencia == false && tarjeta == false)
                                ? Container()
                                : (transferencia)
                                    ? GestureDetector(
                                        onTap: () {},
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal:
                                                      getProportionateScreenWidth(
                                                          30)),
                                              child: Container(
                                                child: Column(
                                                  children: [
                                                    Text(
                                                        "Datos de la transferencia: ",
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  19),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        )),
                                                    SizedBox(
                                                      height: 10,
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "Nombre del titular: ",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            )),
                                                        Text(
                                                            "Norma Carrera Padilla",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text(
                                                            "Cuenta corriente: ",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            )),
                                                        Text("420005225",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        Text("Banco: ",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                            )),
                                                        Text(
                                                            "Banco Internacional",
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      14),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                padding: EdgeInsets.only(
                                                    top: 16,
                                                    bottom: 16,
                                                    left: 16,
                                                    right: 16),
                                                decoration: BoxDecoration(
                                                    color: lightColor,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    // shape: BoxShape.rectangle,
                                                    boxShadow: [
                                                      BoxShadow(
                                                          color: Colors.black26,
                                                          blurRadius: 10,
                                                          offset: Offset(0, 10))
                                                    ]),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                _showPickOptions(context);
                                              },
                                              child: Container(
                                                child: (_image == null)
                                                    ? Camara()
                                                    : Image.file(_image),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 18.0),
                                              child: Container(
                                                alignment:
                                                    Alignment.bottomCenter,
                                                child: Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Text(
                                                        "Foto de transferencia o deposito",
                                                        style: TextStyle(
                                                          color: textColorClaro,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  11),
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        ))),
                                              ),
                                            ),
                                            InputIconCustomTextField(
                                              icon: Icon(
                                                Icons.credit_card,
                                                color: Colors.white,
                                                size: 20.0,
                                              ),
                                              isPassword: false,
                                              keyboardType: TextInputType
                                                  .numberWithOptions(
                                                      decimal: true),
                                              label: "Número de comprobante",
                                              placeholder: "...",
                                              textController: transferenciaCtlr,
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 20.0),
                                              child: ButtonGray(
                                                textoBoton: "PAGAR",
                                                bFuncion: () {
                                                  _pagar();
                                                  // Navigator.pushReplacement(
                                                  //     context,
                                                  //     PageTransition(
                                                  //         type: PageTransitionType.fade,
                                                  //         child: FinalizarProblema()));
                                                },
                                                anchoBoton:
                                                    getProportionateScreenHeight(
                                                        330),
                                                largoBoton:
                                                    getProportionateScreenWidth(
                                                        27),
                                                colorTextoBoton: textColorClaro,
                                                opacityBoton: 1,
                                                sizeTextBoton:
                                                    getProportionateScreenWidth(
                                                        15),
                                                weightBoton: FontWeight.w400,
                                                color1Boton: primaryColor,
                                                color2Boton: primaryColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    : TarjetasMetodo(
                                        total: precio.split(" ")[1]),
                            SizedBox(
                              height: getProportionateScreenHeight(20),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);
        } else {}
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 16.0),
        // aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 6.0),

        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  void subirTransferencia(id) async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Pago_paquete')
        .doc(id)
        .update({'transferencia_fotografia': url});
  }

  String tipo = '';
  String codigo = '';
  String url;
  File _image;

  void _pagar() {
    // if (transferencia) {
    //   tipo = 'Transferencia';
    //   codigo = transferenciaCtlr.text;
    //   if (codigo == '') {
    //     _alertaProducto(context,
    //         'Por favor ingresa el número del comprobante para continuar');
    //   } else if (_image == null) {
    //     _alertaProducto(context,
    //         'Por favor ingresa la foto del comprobante para continuar');
    //   } else {
    //     metodopagar();
    //   }
    // } else
    if (tarjeta) {
      tipo = 'Tarjeta';
      metodopagartarjeta();
    } else {
      _alertaProducto(context,
          'Por favor selecciona el metodo de pago para poder continuar');
    }
  }

  metodopagar() {
    print(tipo);
    FirebaseFirestore.instance
        .collection('Paquetes')
        .where('id_usuario', isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (widget.paquete == "paquete1") {
        // FirebaseFirestore.instance.collection('Pago_paquete').add({
        //   'fecha': DateTime.now().year.toString() +
        //       '-' +
        //       DateTime.now().month.toString() +
        //       '-' +
        //       DateTime.now().day.toString(),
        //   'token':prefs.usuario['token'],
        //   'fecha_verifica': '',
        //   'forma_pago': tipo,
        //   'id_Admin_paquetes': '8OvFX1mwpKpeki6Bmwjc',
        //   'id_paquetes': value.docs[0].id,
        //   'id_profesional': prefs.id,
        //   'id_transferencia': codigo,
        //   'id_user_action': '',
        //   'pago': 'Pendiente',
        //   'status': true,
        //   'total': 5,
        // }).then((value) {
        //   if (tipo == 'Transferencia') {
        //     subirTransferencia(value.id);
        //   }
        // });
        // FirebaseFirestore.instance
        //     .collection('Paquetes')
        //     .doc(value.docs[0].id)
        //     .update({
        //   'estado': false,
        //   'estadosoli':true,
        // });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 5,
        }).then((value) {
          if (tipo == 'Transferencia') {
            subirTransferencia(value.id);
          }
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
          // 'paquete1': true,
        });
      } else if (widget.paquete == "paquete2") {
        // FirebaseFirestore.instance
        //     .collection('Paquetes')
        //     .doc(value.docs[0].id)
        //     .update({
        //   'paquete2': date.day.toString() +
        //       '/' +
        //       date.month.toString() +
        //       '/' +
        //       date.year.toString()
        // });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 15,
        }).then((value) {
          if (tipo == 'Transferencia') {
            subirTransferencia(value.id);
          }
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
          // 'paquete1': true,
        });
      } else if (widget.paquete == "paquete3") {
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 25,
        }).then((value) {
          if (tipo == 'Transferencia') {
            subirTransferencia(value.id);
          }
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
        });
        // FirebaseFirestore.instance
        //     .collection('Paquetes')
        //     .doc(value.docs[0].id)
        //     .update({
        //   'paquete3': date.day.toString() +
        //       '/' +
        //       date.month.toString() +
        //       '/' +
        //       date.year.toString()
        // });
      }
    }).then((value) => Navigator.pop(context));
    // .then((value) => Navigator.pop(context));
  }

  metodopagartarjeta() {
    FirebaseFirestore.instance
        .collection('Paquetes')
        .where('id_usuario', isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (widget.paquete == "paquete1") {
        // FirebaseFirestore.instance.collection('Pago_paquete').add({
        //   'fecha': DateTime.now().year.toString() +
        //       '-' +
        //       DateTime.now().month.toString() +
        //       '-' +
        //       DateTime.now().day.toString(),
        //   'token':prefs.usuario['token'],
        //   'fecha_verifica': '',
        //   'forma_pago': tipo,
        //   'id_paquetes': value.docs[0].id,
        //   'id_profesional': prefs.id,
        //   'id_transferencia': codigo,
        //   'id_user_action': '',
        //   'pago': 'Pendiente',
        //   'status': true,
        //   'total': 5,
        // });
        // FirebaseFirestore.instance
        //     .collection('Paquetes')
        //     .doc(value.docs[0].id)
        //     .update({
        //   'estado': false,
        //   'estadosoli':true,

        // });

        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete1': DateTime.now().day.toString() +
              '/' +
              DateTime.now().month.toString() +
              '/' +
              DateTime.now().year.toString(),
          'estado': true,
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 5,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
        });
      } else if (widget.paquete == "paquete2") {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete2': DateTime.now().day.toString() +
              '/' +
              DateTime.now().month.toString() +
              '/' +
              DateTime.now().year.toString(),
          'estado': true,
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 15,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
        });
      } else if (widget.paquete == "paquete3") {
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': tipo,
          'id_Admin_paquetes': 'h1ieGiNgm6EfCroysjKH',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': codigo,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 25,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': true,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete3': DateTime.now().day.toString() +
              '/' +
              DateTime.now().month.toString() +
              '/' +
              DateTime.now().year.toString(),
          'estado': true,
        });
      }
    }).then((value) => Navigator.pop(context));
    // .then((value) => Navigator.pop(context));
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
