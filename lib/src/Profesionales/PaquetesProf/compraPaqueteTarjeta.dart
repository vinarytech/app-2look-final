import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/Profesionales/PaquetesProf/tipopagoProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/pago/iconoSeguridad.dart';
import 'package:tl_app/widgets/pago/tarjetaspago.dart';
import 'package:tl_app/widgets/size_config.dart';

class CompraPaqueteTarjeta extends StatefulWidget {
  CompraPaqueteTarjeta({Key key}) : super(key: key);

  @override
  _CompraPaqueteTarjetaState createState() => _CompraPaqueteTarjetaState();
}

class _CompraPaqueteTarjetaState extends State<CompraPaqueteTarjeta> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    final searchCtrl = TextEditingController();
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 115,
                              height: 115,
                              child: FlatButton(
                                color: Colors.transparent,
                                textColor: Colors.white,
                                padding: EdgeInsets.all(8.0),
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                onPressed: () {
                                  // Navigator.pushReplacement(
                                  //     context,
                                  //     PageTransition(
                                  //         type: PageTransitionType.fade,
                                  //         child: CompraPaqueteTransf()));
                                },
                                shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: primaryColor,
                                        width: 1,
                                        style: BorderStyle.solid),
                                    borderRadius: BorderRadius.circular(20)),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(
                                      "\$3.00",
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(20),
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      "Por producto o servicio",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: textColorClaro,
                                        fontSize:
                                            getProportionateScreenWidth(10),
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            TipoPagoProf(),
                            TarjetasPago(),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 12.0, right: 20, left: 20),
                              child: Container(
                                width: getProportionateScreenWidth(370),
                                child: InputIconCustomTextField(
                                  icon: Icon(
                                    Icons.credit_card,
                                    color: Colors.white,
                                    size: 20.0,
                                  ),
                                  isPassword: false,
                                  keyboardType: TextInputType.text,
                                  label: "Número de Tarjeta",
                                  placeholder: "...",
                                  textController: searchCtrl,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12.0, left: 20),
                                  child: Container(
                                    width: getProportionateScreenWidth(150),
                                    child: InputIconCustomTextField(
                                      icon: Icon(
                                        Icons.credit_card,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                      isPassword: false,
                                      keyboardType: TextInputType.text,
                                      label: "Vencimiento",
                                      placeholder: "...",
                                      textController: searchCtrl,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 12.0, right: 20),
                                  child: Container(
                                    width: getProportionateScreenWidth(190),
                                    child: InputIconCustomTextField(
                                      icon: Icon(
                                        Icons.credit_card,
                                        color: Colors.white,
                                        size: 20.0,
                                      ),
                                      isPassword: false,
                                      keyboardType: TextInputType.text,
                                      label: "Codigo de Seguridad",
                                      placeholder: "...",
                                      textController: searchCtrl,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 28.0),
                              child: Row(
                                children: <Widget>[
                                  IconoSeguridad(),
                                  Text(
                                    "  Tu información de pago es guardada de forma segura",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(11),
                                      fontWeight: FontWeight.normal,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: ButtonGray(
                                textoBoton: "PAGAR",
                                bFuncion: () {
                                  // Navigator.pushReplacement(
                                  //     context,
                                  //     PageTransition(
                                  //         type: PageTransitionType.fade,
                                  //         child: FinalizarProblema()));
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.w400,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
}
