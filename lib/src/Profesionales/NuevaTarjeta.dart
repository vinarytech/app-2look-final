import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:paymentez_mobile/add_card/add_card_form.dart';
import 'package:paymentez_mobile/add_card/bloc/bloc.dart';
import 'package:paymentez_mobile/config/bloc.dart';
import 'package:paymentez_mobile/repository/model/user.dart';
import 'package:paymentez_mobile/repository/paymentez_repository.dart';
import 'package:paymentez_mobile/simple_bloc_delegate.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/size_config.dart';

class NuevaTarjetas extends StatefulWidget {
  NuevaTarjetas({Key key}) : super(key: key);

  @override
  _NuevaTarjetasState createState() => _NuevaTarjetasState();
}

class _NuevaTarjetasState extends State<NuevaTarjetas> {
  final prefs = PreferenciasUsuario();
  final apiData = new ServiceApi();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image:
                              AssetImage('lib/assets/images/splash/fondo.png'),
                          fit: BoxFit.cover))),
              Container(
                  child: Stack(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(40),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: getProportionateScreenWidth(450),
                    height: getProportionateScreenHeight(70),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                            "lib/assets/images/busqueda/cabecera.png"),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, top: 10),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Stack(
                        children: [
                          Icon(
                            Icons.circle,
                            size: 50,
                            color: Color(0Xff707070),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 18, top: 13),
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )),
              Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: BlocProvider(
                    create: (context) => ConfigBloc()
                      ..add(SetEnvironment(
                        testMode:
                            'prod', //You can use the environments 'stg' for develop or 'prod' for production environment,
                        isFlutterAppHost: false,
                        //    paymentezClientAppCode: "TPP3-EC-CLIENT",
                        // paymentezClientAppKey: "ZfapAKOk4QFXheRNvndVib9XU3szzg",
                        paymentezClientAppCode: "VAKKER-EC-CLIENT",
                        paymentezClientAppKey: "fyfLqiiJBN4K9wIw246SKOHMMiIOhA",
                      )),
                    child: BlocBuilder<ConfigBloc, ConfigState>(
                        builder: (context, state) {
                      if (state.initiated != true) {
                        //State waiting for connection
                        print('estado');
                      } else {
                        var _paymentezRepository = PaymentezRepository(
                            configState: state,
                            successAction: (card) {
                              // switch (card.status) {
                              //   case 'valid':
                              //     print('Estado valid');
                              //     break;
                              //   case 'review':
                              //     print('Estado review');
                              //     break;
                              //   default:
                              //     print('Estado error');
                              //     break;
                              // }
                              print('successsss' + card.toString());

                              if (card.status == 'valid') {
                                _alertaProducto(
                                    context,
                                    'Tu tarjeta ha sido ingresada correctamente',
                                    false);
                              } else if (card.status == 'rejected') {
                                _alertaProducto(
                                    context,
                                    'Por favor ingresa una tarjeta valida',
                                    false);
                              } else if (card.status == 'BadRequest') {
                                _alertaProducto(
                                    context,
                                    'Por favor ingresa una tarjeta valida',
                                    false);
                              } else if (card.status == 'review') {
                                _alertaProducto(
                                    context,
                                    'Esta tarjeta esta en revisión, pronto recibiras noticias del pago',
                                    false);
                              } else if (card.status == 'pending') {
                                _alertaDatos(
                                    context,
                                    'Tarjeta pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                    card.transactionReference);
                              } else if (card.status.split(' ')[0] +
                                      ' ' +
                                      card.status.split(' ')[1] ==
                                  'Card already') {
                                _alertaProducto(
                                    context,
                                    'Ya ha ingresado esta tarjeta anteriormente',
                                    false);
                              } else {
                                _alertaProducto(
                                    context,
                                    'No es posible ingresar esta tarjeta porfavor, verifica la información e intentalo nuevamente',
                                    false);
                              }
                            },
                            errorAction: (error) {
                              print('error' + error.toString());
                              //Add card error
                            },
                            user: User(
                                id: prefs.id, email: prefs.usuario['correo']));

                        return BlocProvider<AddCardBloc>(
                            create: (context) => AddCardBloc(
                                paymentezRepository: _paymentezRepository),
                            child: AddCardForm(
                                paymentezRepository: _paymentezRepository,
                                title: Container(
                                  alignment: Alignment.center,
                                  child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("Nueva Tarjeta",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(18),
                                            fontWeight: FontWeight.bold,
                                          ))),
                                ), //Set the widget for the title of addcard screen,
                                aboveButton: Text(
                                    ""), //Set the widget for message aboce the submitbutton,
                                summitButton: (value) => _submiting(value)));
                      }
                    })),
              ),
            ],
          ),
        ));
  }

  _submiting(Function value) => ButtonGray(
        textoBoton: "Guardar tarjeta",
        bFuncion: value,
        anchoBoton: getProportionateScreenHeight(330),
        largoBoton: getProportionateScreenWidth(47),
        colorTextoBoton: textColorClaro,
        opacityBoton: 1,
        sizeTextBoton: getProportionateScreenWidth(15),
        weightBoton: FontWeight.bold,
        color1Boton: primaryColor,
        color2Boton: primaryColor,
      );

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alertaDatos(BuildContext context, String text, transaction) {
    final optCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  controller: optCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  apiData
                                      .verifyToken(transaction, optCtlr.text)
                                      .then((value) {
                                    Navigator.pop(context);
                                    var value2 = json.decode(value.body)[0];
                                    print("estado" + value2.toString());
                                    if (value2['status'].toString() == '4') {
                                      _alertaProducto(
                                          context,
                                          'El codigo OTP no es el correcto, por favor vuelvelo a intentar',
                                          false);
                                    } else if (value2['status'].toString() ==
                                        '0') {
                                      _alertaProducto(
                                          context,
                                          'El codigo OTP no es el correcto, por favor vuelvelo a intentar',
                                          false);
                                    } else if (value2['status'].toString() ==
                                        '1') {
                                      _alertaProducto(
                                          context,
                                          'Tarjeta agregada correctamente',
                                          false);
                                    } else {
                                      _alertaProducto(
                                          context,
                                          'Error de codigo otp porfavor vuelvelo a intentar',
                                          true);
                                    }
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaProducto(BuildContext context, String text, valor) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (valor) {
                                  Navigator.pop(context, 'valor');
                                  Navigator.pop(context, 'valor');
                                } else {
                                  Navigator.pop(context, 'valor');
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
