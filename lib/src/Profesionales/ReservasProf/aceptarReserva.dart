import 'dart:convert';
import 'dart:ui';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:http/http.dart' as http;
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/Profesionales/ReservasProf/finalizarServicio.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/pago/tarjetasInfo.dart';
import 'package:tl_app/widgets/pago/total.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:url_launcher/url_launcher.dart';

class AceptarReserva extends StatefulWidget {
  final idSolicitud;
  final idServicio;
  AceptarReserva({Key key, this.idSolicitud, this.idServicio})
      : super(key: key);

  @override
  _AceptarReservaState createState() => _AceptarReservaState();
}

class _AceptarReservaState extends State<AceptarReserva> {
  final convert = new NumberFormat("#,##0.00", "en_US");
  GoogleMapController _controller;
  int cantidad = 0;
  double precio = 0;
  String usuario = '';
  double calificacion = 0;
  String latitud = '';
  String tipo = '';
  String longitud = '';
  String horaFin = '';
  String horaInicio = '';
  String fecha = '';
  String direccion = '';
  String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());

  List<Horarios> listaHorarios = [];
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  // @override
  void initState() {
    super.initState();

    FirebaseFirestore.instance
        .collection('Servicios')
        .doc(widget.idServicio)
        .snapshots()
        .forEach((element) {
      setState(() {
        precio = double.parse(element.data()['precio'].toString());
      });

      FirebaseFirestore.instance
          .collection('Horarios')
          .where('id_solicitud', isEqualTo: widget.idSolicitud)
          .get()
          .then((value) {
        value.docs.forEach((element) {
          setState(() {
            listaHorarios.add(Horarios(
                horaInicio: element.data()['hora_inicio'],
                horaFin: element.data()['hora_fin'],
                estado: false));
          });
        });
      });

      FirebaseFirestore.instance
          .collection('Solicitudes')
          .doc(widget.idSolicitud)
          .snapshots()
          .forEach((element) {
        setState(() {
          this.latitud = element.data()['latitud'].toString();
          this.longitud = element.data()['longitud'].toString();
          this.tipo = element.data()['tipo'].toString();
          this.fecha = element.data()['fecha'].toString();
          this.direccion = element.data()['direccion'].toString();
          FirebaseFirestore.instance
              .collection('Usuarios')
              .doc(element.data()['idsolicitante'])
              .snapshots()
              .forEach((usuario) {
            setState(() {
              this.usuario = usuario.data()['nombre'].toString();
              this.calificacion =
                  double.parse(usuario.data()['estrellas'].toString());
            });
          });
          cantidad = int.parse(element.data()['cantidad'].toString());
        });
      });
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller = controller;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              StreamBuilder<DocumentSnapshot>(
                                  stream: FirebaseFirestore.instance
                                      .collection('Servicios')
                                      .doc(widget.idServicio)
                                      .snapshots(),
                                  builder: (context,
                                      AsyncSnapshot<DocumentSnapshot>
                                          snapshot) {
                                    if (snapshot.hasData) {
                                      return TarjetasInfo(
                                        titulo: snapshot.data
                                                .data()['subcategoria']
                                                .toString()
                                                .toUpperCase() +
                                            " / " +
                                            snapshot.data.data()['categoria'],
                                        descripcion: snapshot.data
                                            .data()['descripcion']
                                            .toString(),
                                        valor: "\$" +
                                            convert
                                                .format(double.parse(snapshot
                                                    .data
                                                    .data()['precio']))
                                                .toString(),
                                      );
                                    } else {
                                      return CircularProgressIndicator();
                                    }
                                  }),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 28.0),
                                child: Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            "TOTAL",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            "\$" +
                                                convert
                                                    .format((precio * cantidad))
                                                    .toString(),
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              fontWeight: FontWeight.normal,
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Center(
                                child: Text('FECHA'),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Text(fecha,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Center(
                                child: Text('HORARIOS'),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Container(
                                child: Column(
                                  children: List.generate(listaHorarios.length,
                                      (index) {
                                    return Padding(
                                      padding: EdgeInsets.symmetric(
                                          vertical:
                                              getProportionateScreenHeight(10)),
                                      child: Container(
                                          child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Material(
                                            type: MaterialType.transparency,
                                            child: Align(
                                                alignment: Alignment.topLeft,
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                    left:
                                                        getProportionateScreenWidth(
                                                            20),
                                                  ),
                                                  child: Text(
                                                      // listaHorarios[index].horaInicio.toString() +" a " + listaHorarios[index].horaFin.toString(),
                                                      listaHorarios[index]
                                                              .horaInicio
                                                              .toString() +
                                                          " A " +
                                                          listaHorarios[index]
                                                              .horaFin
                                                              .toString(),
                                                      style: TextStyle(
                                                        color: textColorClaro,
                                                        fontSize:
                                                            getProportionateScreenWidth(
                                                                14),
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      )),
                                                )),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 35.0),
                                            child: Material(
                                              type: MaterialType.transparency,
                                              child: Center(
                                                  child: GestureDetector(
                                                onTap: () {
                                                  setState(() {
                                                    for (var item
                                                        in listaHorarios) {
                                                      item.estado = false;
                                                    }
                                                    listaHorarios[index]
                                                            .estado =
                                                        !listaHorarios[index]
                                                            .estado;
                                                    horaInicio =
                                                        listaHorarios[index]
                                                            .horaInicio;
                                                    horaFin =
                                                        listaHorarios[index]
                                                            .horaFin;
                                                  });
                                                  // });
                                                },
                                                child: Container(
                                                  height: 20,
                                                  width: 20,
                                                  child: CircularCheckBox(
                                                      value:
                                                          listaHorarios[index]
                                                              .estado,
                                                      checkColor:
                                                          textColorMedio,
                                                      activeColor: primaryColor,
                                                      inactiveColor:
                                                          textColorMedio,
                                                      disabledColor:
                                                          Colors.grey,
                                                      onChanged: (val) =>
                                                          this.setState(() {
                                                            for (var item
                                                                in listaHorarios) {
                                                              item.estado =
                                                                  false;
                                                            }
                                                            horaInicio = '';
                                                            horaFin = '';
                                                            listaHorarios[index]
                                                                    .estado =
                                                                !listaHorarios[
                                                                        index]
                                                                    .estado;
                                                            if (listaHorarios[
                                                                    index]
                                                                .estado) {
                                                              horaInicio =
                                                                  listaHorarios[
                                                                          index]
                                                                      .horaInicio;
                                                              horaFin =
                                                                  listaHorarios[
                                                                          index]
                                                                      .horaFin;
                                                            } else {
                                                              horaInicio = '';
                                                              horaFin = '';
                                                            }
                                                          })),
                                                ),
                                              )),
                                            ),
                                          )
                                        ],
                                      )),
                                    );
                                  }),
                                ),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    child: Material(
                                      type: MaterialType.transparency,
                                      child: Text("$usuario / $calificacion",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(11),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ),
                                  ),
                                  SizedBox(
                                    width: getProportionateScreenHeight(5),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Icon(
                                      Icons.star,
                                      size: getProportionateScreenHeight(17),
                                      color: primaryColor,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              (tipo == 'establecimiento')
                                  ? Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                  "Ubicación del establecimiento",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    )
                                  : Container(
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text(
                                                  "Ubicación del domicilio",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            18),
                                                    fontWeight: FontWeight.bold,
                                                  )),
                                            )),
                                      ),
                                    ),
                              (latitud != "" || longitud != "")
                                  ? Container(
                                      height: 200,
                                      width: double.infinity,
                                      child: GoogleMap(
                                          onTap: (la) {
                                            print('asqui' + la.toString());
                                            _launchMapsUrl(
                                                double.parse(latitud),
                                                double.parse(longitud));
                                          },
                                          markers: Set<Marker>.of([
                                            Marker(
                                                markerId: MarkerId('SomeId'),
                                                position: LatLng(
                                                    double.parse(latitud),
                                                    double.parse(longitud))),
                                            // infoWindow: InfoWindow(
                                            //   //anchor: Offset(0.5,1),
                                            // title: widget.snapshot.doc.data()["nombre"]
                                            // )
                                            // )
                                          ]),
                                          mapType: MapType.normal,
                                          myLocationEnabled: true,
                                          onMapCreated: _onMapCreated,
                                          initialCameraPosition: CameraPosition(
                                              target: LatLng(
                                                  double.parse(latitud),
                                                  double.parse(longitud)),
                                              zoom: 15)),
                                    )
                                  : SizedBox(),
                              SizedBox(
                                height: getProportionateScreenHeight(10),
                              ),
                              Container(
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("DIRECCIÓN",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      )),
                                ),
                              ),
                              Container(
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text(
                                          direccion,
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(14),
                                          ),
                                        ),
                                      )),
                                ),
                              ),
                              SizedBox(
                                height: getProportionateScreenHeight(20),
                              ),
                              Container(
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    ButtonGray(
                                      textoBoton: "RECHAZAR",
                                      bFuncion: () {
                                        _alertaPendiente(
                                            context,
                                            'Esta seguro que desea rechazar la solicitud',
                                            true,
                                            widget.idServicio);
                                      },
                                      anchoBoton:
                                          getProportionateScreenHeight(145),
                                      largoBoton:
                                          getProportionateScreenWidth(28),
                                      colorTextoBoton: textColorMedio,
                                      opacityBoton: 0.10,
                                      sizeTextBoton:
                                          getProportionateScreenWidth(11),
                                      weightBoton: FontWeight.w300,
                                      color1Boton: Color(0xffA01127),
                                      color2Boton: Color(0xffA01127),
                                    ),
                                    SizedBox(
                                      width: getProportionateScreenWidth(20),
                                    ),
                                    ButtonGray(
                                      textoBoton: "ACEPTAR",
                                      bFuncion: () {
                                        _aceptar();
                                        // Navigator.pushReplacement(
                                        //     context,
                                        //     PageTransition(
                                        //         type: PageTransitionType.fade,
                                        //         child: FinalizarServicio()));
                                      },
                                      anchoBoton:
                                          getProportionateScreenHeight(145),
                                      largoBoton:
                                          getProportionateScreenWidth(28),
                                      colorTextoBoton: textColorMedio,
                                      opacityBoton: 0.10,
                                      sizeTextBoton:
                                          getProportionateScreenWidth(11),
                                      weightBoton: FontWeight.w300,
                                      color1Boton: Color(0xffA3BD31),
                                      color2Boton: Color(0xffA3BD31),
                                    ),
                                  ],
                                ),
                              ),
                              Container(),
                              SizedBox(
                                height: getProportionateScreenHeight(40),
                              )
                            ],
                          ),
                        )),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }

  void _launchMapsUrl(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _alertaPendiente(
      BuildContext context, String text, bool cancelado, String id) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (cancelado == true) {
                                  FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(widget.idSolicitud)
                                      .update({'estadosolicitud': false}).then(
                                          (value) {
                                    FirebaseFirestore.instance
                                        .collection('Solicitudes')
                                        .doc(widget.idSolicitud)
                                        .get()
                                        .then((value) {
                                      FirebaseFirestore.instance
                                          .collection('Usuarios')
                                          .doc(value.data()['idsolicitante'])
                                          .get()
                                          .then((usuario) {
                                        FirebaseFirestore.instance
                                            .collection('Notificaciones')
                                            .add({
                                          'idreceptor':
                                              value.data()['idsolicitante'],
                                          'idemisor': prefs.id,
                                          'date': date,
                                          'accion': 'solicitud',
                                          'notificacion':
                                              'Tu solicitud fue cancelada.',
                                        });

                                        sendNotification(
                                            notificacion,
                                            'Two look',
                                            usuario.data()['token']);
                                      });
                                    });
                                  });
                                  Navigator.of(context).pop();

                                  Navigator.of(context).pop();
                                } else {
                                  Navigator.of(context).pop();
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _aceptar() {
    var notificacion =
        "Su solicitud fue aceptada, por favor realizar el pago para continuar";

    if (horaFin == '' || horaInicio == '') {
      _alertaProducto(context, "Por favor selecciona una hora");
    } else {
      String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
      Future(() => loaderDialogNormal(context)).then((v) {
        // print(horaFin);
        // print(horaInicio);
        FirebaseFirestore.instance
            .collection('Solicitudes')
            .doc(widget.idSolicitud)
            .update({
              'horafin': horaFin,
              'horainicio': horaInicio,
              'fechaaceptada': DateTime.now(),
              'estado': 'POR PAGAR'
            })
            .then((value) {
              FirebaseFirestore.instance
                  .collection('Solicitudes')
                  .doc(widget.idSolicitud)
                  .get()
                  .then((value) {
                FirebaseFirestore.instance
                    .collection('Usuarios')
                    .doc(value.data()['idsolicitante'])
                    .get()
                    .then((usuario) {
                  FirebaseFirestore.instance.collection('Notificaciones').add({
                    'idreceptor': value.data()['idsolicitante'],
                    'idemisor': prefs.id,
                    'date': date,
                    'accion': 'solicitud',
                    'notificacion': notificacion,
                  });
                  sendNotification(
                      notificacion, 'Two look', usuario.data()['token']);
                });
              });
            })
            .then((v) => Navigator.pop(context))
            .then((value) => Navigator.pop(context))
            .then((v) {
              _alertaProducto(context,
                  'Listo, tu solicitud fue enviada, esperamos el pago');
            });
      });
    }
  }

  Future<void> sendNotification(subject, title, idToken) async {
    print(idToken);
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {"body": subject, "title": title},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": idToken
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}

class Horarios {
  String horaInicio;
  String horaFin;
  bool estado;

  Horarios({
    this.horaInicio,
    this.horaFin,
    this.estado,
  });
}
