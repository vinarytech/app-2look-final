import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarReserva.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/Profesionales/ProblemasProf/reportarProblemaProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/pago/tarjetasInfo.dart';
import 'package:tl_app/widgets/pago/total.dart';
import 'package:tl_app/widgets/size_config.dart';

class FinalizarServicio extends StatefulWidget {
  FinalizarServicio({Key key}) : super(key: key);

  @override
  _FinalizarServicioState createState() => _FinalizarServicioState();
}

class _FinalizarServicioState extends State<FinalizarServicio> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            TarjetasInfo(
                              titulo: "SERVICIO / PELUQUERÍA",
                              descripcion:
                                  "Corte de cabello para hombre y mujeres",
                              valor: "\$20.00",
                            ),
                            Total(),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  alignment: Alignment.center,
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Text("Christian / 4.6",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ),
                                ),
                                SizedBox(
                                  width: getProportionateScreenHeight(5),
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: Icon(
                                    Icons.star,
                                    size: getProportionateScreenHeight(17),
                                    color: primaryColor,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(20),
                            ),
                            ButtonGray(
                              textoBoton:
                                  "NUMERO:098766655\nCORREO:cnsin@hotmail.com",
                              bFuncion: () {},
                              anchoBoton: getProportionateScreenHeight(363),
                              largoBoton: getProportionateScreenWidth(87),
                              colorTextoBoton: textColorMedio,
                              opacityBoton: 0.10,
                              sizeTextBoton: getProportionateScreenWidth(11),
                              weightBoton: FontWeight.w300,
                              color1Boton: lightColor,
                              color2Boton: lightColor,
                            ),
                            
                            SizedBox(
                              height: getProportionateScreenHeight(20),
                            ),
                            Container(
                              decoration: BoxDecoration(boxShadow: [
                                BoxShadow(
                                    color: Colors.black26,
                                    offset: Offset(0, 2.0),
                                    blurRadius: 6.0)
                              ]),
                              child: Hero(
                                tag: "Hero213",
                                child: Image(
                                  image: AssetImage(
                                      "lib/assets/images/finalizar/mapa.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(20),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 20.0),
                              child: ButtonGray(
                                textoBoton: "FINALIZADO",
                                bFuncion: () {},
                                anchoBoton: getProportionateScreenHeight(350),
                                largoBoton: getProportionateScreenWidth(28),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(11),
                                weightBoton: FontWeight.w400,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.fade,
                                        child: ReportarProblemaProf()));
                              },
                              child: Padding(
                                padding: const EdgeInsets.only(top: 20.0),
                                child: Text(
                                  "REPORTAR PROBLEMA",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(17),
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
}
