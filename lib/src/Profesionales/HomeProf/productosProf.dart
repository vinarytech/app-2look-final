import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/escoger.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/obtenerDataProductos.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/obtenerDataServicios.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirProducto.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirServicio.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'mensajesProf.dart';

class ProductosProf extends StatefulWidget {
  @override
  _ProductosProfState createState() => _ProductosProfState();
}

class _ProductosProfState extends State<ProductosProf> {
  bool escoger;
  var valorRetorno;
  @override
  void initState() {
    escoger = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            (!escoger)
                                ? Container(
                                    width: getProportionateScreenWidth(320),
                                    height: getProportionateScreenHeight(140),
                                    child: FlatButton(
                                      color: Colors.transparent,
                                      textColor: Colors.white,
                                      padding: EdgeInsets.all(8.0),
                                      splashColor: primaryColor,
                                      onPressed: () {
                                        setState(() {
                                          escoger = true;
                                        });
                                        // Navigator.pushReplacement(
                                        //     context,
                                        //     PageTransition(
                                        //         type: PageTransitionType.fade,
                                        //         child: Escoger()));
                                      },
                                      shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                              color: primaryColor,
                                              width: 1,
                                              style: BorderStyle.solid),
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Text(
                                            "NUEVO",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      20),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Container(
                                        width: getProportionateScreenWidth(160),
                                        height:
                                            getProportionateScreenHeight(140),
                                        child: FlatButton(
                                          color: Colors.transparent,
                                          textColor: Colors.white,
                                          padding: EdgeInsets.all(8.0),
                                          splashColor: primaryColor,
                                          onPressed: () {
                                            valorRetorno = Navigator.push(
                                                context,
                                                PageTransition(
                                                    type:
                                                        PageTransitionType.fade,
                                                    child: SubirProducto(
                                                        id: null)));
                                          },
                                          shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                  color: primaryColor,
                                                  width: 1,
                                                  style: BorderStyle.solid),
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(
                                                "PRODUCTO",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          20),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: getProportionateScreenWidth(160),
                                        height:
                                            getProportionateScreenHeight(140),
                                        child: FlatButton(
                                          color: Colors.transparent,
                                          textColor: Colors.white,
                                          padding: EdgeInsets.all(8.0),
                                          splashColor: primaryColor,
                                          onPressed: () {
                                            valorRetorno = Navigator.push(
                                                context,
                                                PageTransition(
                                                    type:
                                                        PageTransitionType.fade,
                                                    child: SubirServicio(
                                                        id: null)));
                                          },
                                          shape: RoundedRectangleBorder(
                                              side: BorderSide(
                                                  color: primaryColor,
                                                  width: 1,
                                                  style: BorderStyle.solid),
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              Text(
                                                "SERVICIO",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          20),
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Container(
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      width: getProportionateScreenWidth(370),
                                      height: getProportionateScreenHeight(1.8),
                                      color: const Color(0x1affffff),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 0.0),
                                  child: Column(
                                    children: <Widget>[
                                      GestureDetector(
                                        child: ObtenerDataProductos(),
                                        // onTap: ()=> _showPopupMenu(),
                                      ),
                                      // ObtenerDataServicios(),
                                    ],
                                  )),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  // Padding(
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: GestureDetector(
                  //     onTap: () {
                  //       setState(() {
                  //         escoger = false;
                  //       });
                  //     },
                  //     child: Stack(
                  //       children: <Widget>[
                  //         Container(
                  //           width: getProportionateScreenWidth(37.8),
                  //           height: getProportionateScreenHeight(40.8),
                  //           decoration: BoxDecoration(
                  //             borderRadius: BorderRadius.all(
                  //                 Radius.elliptical(9999.0, 9999.0)),
                  //             color: const Color(0x1affffff),
                  //           ),
                  //         ),
                  //         Transform.translate(
                  //           offset: Offset(getProportionateScreenHeight(12.5),
                  //               getProportionateScreenHeight(7.8)),
                  //           child: SvgPicture.string(
                  //             _svg_u87zmt,
                  //             allowDrawingOutsideViewBox: true,
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //   ),
                  // ),
                ],
              ))),
    );
  }
  //   void _showPopupMenu(int index) async {
  //   await showMenu(
  //     context: context,
  //     position: RelativeRect.fromLTRB(_tapPosition.dx, _tapPosition.dy,
  //         _tapPosition.dx + 100, _tapPosition.dy + 100),
  //     color: textColorMedio,
  //     items: [
  //       PopupMenuItem(
  //         value: 1,
  //         child: GestureDetector(
  //           onTap: () {
  //             setState(() {
  //               images.removeAt(index);
  //             });

  //             Navigator.pop(context);
  //           },
  //           child: Container(
  //             child: Text(
  //               "Eliminar",
  //               style: TextStyle(
  //                 color: textColorClaro,
  //                 fontSize: getProportionateScreenWidth(17),
  //                 fontWeight: FontWeight.bold,
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //       PopupMenuItem(
  //         value: 2,
  //         child: GestureDetector(
  //           onTap: () {
  //             new Future(() => _showPickOptions(context))
  //                 .then((v) => setState(() {
  //                       images.removeAt(index);
  //                     }))
  //                 .then((v) => Navigator.pop(context));
  //           },
  //           child: Container(
  //             child: Text(
  //               "Cambiar",
  //               style: TextStyle(
  //                 color: textColorClaro,
  //                 fontSize: getProportionateScreenWidth(17),
  //                 fontWeight: FontWeight.bold,
  //               ),
  //             ),
  //           ),
  //         ),
  //       ),
  //     ],
  //     elevation: 5.0,
  //   );
  // }
}

String _svg_u87zmt =
    '<svg viewBox="15.5 9.8 10.2 21.1" ><path transform="translate(-10.57, -14.68)" d="M 34.62599945068359 45.66500091552734 C 34.12400054931641 45.66500091552734 33.6609992980957 45.43899917602539 33.35300064086914 45.04299926757813 L 26.37599945068359 36.08499908447266 C 25.92099952697754 35.50099945068359 25.92099952697754 34.68500137329102 26.37599945068359 34.09999847412109 L 33.35300064086914 25.14299964904785 C 33.6609992980957 24.74699974060059 34.125 24.52000045776367 34.62599945068359 24.52000045776367 C 35.25799942016602 24.52000045776367 35.79999923706055 24.85899925231934 36.07699966430664 25.42600059509277 C 36.35400009155273 25.99399948120117 36.28799819946289 26.62899971008301 35.90000152587891 27.12800025939941 L 29.69599914550781 35.09299850463867 L 35.90000152587891 43.05799865722656 C 36.28799819946289 43.55699920654297 36.35400009155273 44.19300079345703 36.07699966430664 44.7599983215332 C 35.79999923706055 45.32699966430664 35.25799942016602 45.66500091552734 34.62599945068359 45.66500091552734 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
