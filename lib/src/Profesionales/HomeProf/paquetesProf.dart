import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/Profesionales/PaquetesProf/compraPaqueteTarjeta.dart';
import 'package:tl_app/src/Profesionales/PaquetesProf/compraPaqueteTransf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/Profesionales/PaquetesProf/imagenPrincipalPCProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'mensajesProf.dart';

class PaquetesProf extends StatefulWidget {
  final regreso;
  PaquetesProf({Key key, this.regreso}) : super(key: key);
  @override
  _PaquetesProfState createState() => _PaquetesProfState();
}

class _PaquetesProfState extends State<PaquetesProf> {
  final prefs = PreferenciasUsuario();
  DateTime date = DateTime.now();
  // @override
  // void initState() {
  //   super.initState();
  //   // FirebaseFirestore.instance
  //   //     .collection('Paquetes')
  //   //     .where('id_usuario', isEqualTo: prefs.id)
  //   //     .get()
  //   //     .then((value) {});
  //   print('data');
  // }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 45.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          height: 35,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            StreamBuilder(
                                stream: FirebaseFirestore.instance
                                    .collection('Paquetes')
                                    .where('id_usuario', isEqualTo: prefs.id)
                                    .snapshots(),
                                builder: (BuildContext context,
                                    AsyncSnapshot<QuerySnapshot> snapshot) {
                                  if (snapshot.hasData) {
                                    DateFormat format =
                                        DateFormat("dd.MM.yyyy");
                                    if (snapshot.data.docs[0]
                                            .data()['paquete2'] !=
                                        '') {
                                      date = format.parse(snapshot.data.docs[0]
                                          .data()['paquete2']
                                          .toString()
                                          .replaceAll('/', '.'));
                                    } else if (snapshot.data.docs[0]
                                            .data()['paquete3'] !=
                                        '') {
                                      date = format.parse(snapshot.data.docs[0]
                                          .data()['paquete3']
                                          .toString()
                                          .replaceAll('/', '.'));
                                    }

                                    // DateTime fecha1 = DateTime.parse(snapshot
                                    //     .data.docs[0]
                                    //     .data()['paquete2']
                                    //     .toString()
                                    //     .replaceAll('/', '-'));
                                    // print(fecha1);
                                    return Container(
                                      child: Column(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 18.0),
                                            child: Container(
                                              alignment: Alignment.bottomCenter,
                                              child: Material(
                                                  type:
                                                      MaterialType.transparency,
                                                  child: Text(
                                                      "PAQUETE CONTRATADO",
                                                      style: TextStyle(
                                                        color: textColorClaro,
                                                        fontSize:
                                                            getProportionateScreenWidth(
                                                                18),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ))),
                                            ),
                                          ),
                                          (snapshot.data.docs[0].data()['prueba'] == '' &&
                                                  snapshot.data.docs[0]
                                                          .data()['paquete1'] ==
                                                      '' &&
                                                  snapshot.data.docs[0]
                                                          .data()['paquete2']
                                                          .toString() ==
                                                      '' &&
                                                  snapshot.data.docs[0]
                                                          .data()['paquete3']
                                                          .toString() ==
                                                      '')
                                              ? Column(children: [
                                                  Center(
                                                    child:
                                                        ImagenPrincipalPCProf(),
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets
                                                            .symmetric(
                                                        vertical: 18.0),
                                                    child: Container(
                                                      alignment: Alignment
                                                          .bottomCenter,
                                                      child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: (snapshot.data
                                                                          .docs[0]
                                                                          .data()[
                                                                      'estadosoli'] ==
                                                                  false)
                                                              ? Text(
                                                                  "Aún no tienes un paquete contratado",
                                                                  style:
                                                                      TextStyle(
                                                                    color:
                                                                        textColorClaro,
                                                                    fontSize:
                                                                        getProportionateScreenWidth(
                                                                            11),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ))
                                                              : Text(
                                                                  "Tu paquete ha sido solicitado, se te acreditará en unos minutos",
                                                                  style:
                                                                      TextStyle(
                                                                    color:
                                                                        textColorClaro,
                                                                    fontSize:
                                                                        getProportionateScreenWidth(
                                                                            11),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ))),
                                                    ),
                                                  ),
                                                  Container(
                                                    child: Column(
                                                      children: [
                                                        SizedBox(
                                                          height: 40,
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .symmetric(
                                                                  vertical:
                                                                      18.0),
                                                          child: Container(
                                                            alignment: Alignment
                                                                .bottomCenter,
                                                            child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: (snapshot
                                                                            .data
                                                                            .docs[0]
                                                                            .data()['estadosoli'] ==
                                                                        false)
                                                                    ? Text("PAQUETES",
                                                                        style: TextStyle(
                                                                          color:
                                                                              textColorClaro,
                                                                          fontSize:
                                                                              getProportionateScreenWidth(18),
                                                                          fontWeight:
                                                                              FontWeight.bold,
                                                                        ))
                                                                    : SizedBox()),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        (snapshot.data.docs[0]
                                                                        .data()[
                                                                    'estadosoli'] ==
                                                                false)
                                                            ? Container(
                                                                child: Row(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: <
                                                                      Widget>[
                                                                    Container(
                                                                      width:
                                                                          115,
                                                                      height:
                                                                          115,
                                                                      child:
                                                                          FlatButton(
                                                                        color: Colors
                                                                            .transparent,
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(8.0),
                                                                        splashColor:
                                                                            primaryColor,
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.push(
                                                                              context,
                                                                              PageTransition(type: PageTransitionType.fade, child: CompraPaqueteTransf(paquete: 'paquete1')));
                                                                        },
                                                                        shape: RoundedRectangleBorder(
                                                                            side: BorderSide(
                                                                                color: primaryColor,
                                                                                width: 1,
                                                                                style: BorderStyle.solid),
                                                                            borderRadius: BorderRadius.circular(20)),
                                                                        child:
                                                                            Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceEvenly,
                                                                          children: <
                                                                              Widget>[
                                                                            Text(
                                                                              "\$ 5.00",
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(20),
                                                                                fontWeight: FontWeight.bold,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "Un solo producto o servicio / 7 días",
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(10),
                                                                                fontWeight: FontWeight.normal,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width:
                                                                          115,
                                                                      height:
                                                                          115,
                                                                      child:
                                                                          FlatButton(
                                                                        color: Colors
                                                                            .transparent,
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(8.0),
                                                                        splashColor:
                                                                            primaryColor,
                                                                        onPressed:
                                                                            () {
                                                                          // Navigator.push(
                                                                          //     context,
                                                                          //     PageTransition(
                                                                          //         type: PageTransitionType.fade,
                                                                          //         child: CompraPaqueteTarjeta()));
                                                                          Navigator.push(
                                                                              context,
                                                                              PageTransition(type: PageTransitionType.fade, child: CompraPaqueteTransf(paquete: 'paquete2')));
                                                                        },
                                                                        shape: RoundedRectangleBorder(
                                                                            side: BorderSide(
                                                                                color: primaryColor,
                                                                                width: 1,
                                                                                style: BorderStyle.solid),
                                                                            borderRadius: BorderRadius.circular(20)),
                                                                        child:
                                                                            Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceEvenly,
                                                                          children: <
                                                                              Widget>[
                                                                            Text(
                                                                              "\$ 15.00",
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(20),
                                                                                fontWeight: FontWeight.bold,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "Productos y servicios ilimitados / 30 días",
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(10),
                                                                                fontWeight: FontWeight.normal,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Container(
                                                                      width:
                                                                          115,
                                                                      height:
                                                                          115,
                                                                      child:
                                                                          FlatButton(
                                                                        color: Colors
                                                                            .transparent,
                                                                        textColor:
                                                                            Colors.white,
                                                                        padding:
                                                                            EdgeInsets.all(8.0),
                                                                        splashColor:
                                                                            primaryColor,
                                                                        onPressed:
                                                                            () {
                                                                          Navigator.push(
                                                                              context,
                                                                              PageTransition(type: PageTransitionType.fade, child: CompraPaqueteTransf(paquete: 'paquete3')));
                                                                        },
                                                                        shape: RoundedRectangleBorder(
                                                                            side: BorderSide(
                                                                                color: primaryColor,
                                                                                width: 1,
                                                                                style: BorderStyle.solid),
                                                                            borderRadius: BorderRadius.circular(20)),
                                                                        child:
                                                                            Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceEvenly,
                                                                          children: <
                                                                              Widget>[
                                                                            Text(
                                                                              "\$ 25.00",
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(20),
                                                                                fontWeight: FontWeight.bold,
                                                                              ),
                                                                            ),
                                                                            Text(
                                                                              "Productos y servicios ilimitados y destacados / 30 días",
                                                                              textAlign: TextAlign.center,
                                                                              style: TextStyle(
                                                                                color: textColorClaro,
                                                                                fontSize: getProportionateScreenWidth(10),
                                                                                fontWeight: FontWeight.normal,
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                      ],
                                                    ),
                                                  )
                                                ])
                                              : (snapshot.data.docs[0]
                                                          .data()['prueba'] !=
                                                      '')
                                                  ? _widgetPrecioGratuita(
                                                      'Prueba Gratuita',
                                                      "Prueba gratuita por " +
                                                          Jiffy(DateFormat('dd/MM/yyyy').parse(snapshot
                                                                  .data.docs[0]
                                                                  .data()[
                                                                      'prueba']
                                                                  .toString()))
                                                              .subtract(
                                                                days: date.day,
                                                                months:
                                                                    date.month,
                                                                years:
                                                                    date.year,
                                                              )
                                                              .day
                                                              .toString() +
                                                          " días, el " +
                                                          snapshot.data.docs[0]
                                                              .data()['prueba'] +
                                                          " termina tu suscripción gratuita para subir productos o servicios ilimitados")
                                                  : (snapshot.data.docs[0].data()['paquete1'] != '' && snapshot.data.docs[0].data()['estadoPublicar'] == true)
                                                      ? _widgetPrecio(
                                                          '5.00',
                                                          "Tienes " +
                                                              Jiffy(DateFormat('dd/MM/yyyy').parse(snapshot.data.docs[0].data()['paquete1'].toString()))
                                                                  .subtract(
                                                                    days: DateTime
                                                                            .now()
                                                                        .day,
                                                                    months: DateTime
                                                                            .now()
                                                                        .month,
                                                                    years: DateTime
                                                                            .now()
                                                                        .year,
                                                                  )
                                                                  .day
                                                                  .toString() +
                                                              " Días para publicar un producto o servicio")
                                                      : (snapshot.data.docs[0].data()['paquete1'] != '' && snapshot.data.docs[0].data()['estadoPublicar'] == false)
                                                          ? _widgetPrecio(
                                                              '5.00',
                                                              "Tienes " +
                                                                  Jiffy(DateFormat('dd/MM/yyyy').parse(snapshot.data.docs[0].data()['paquete1'].toString()))
                                                                      .subtract(
                                                                        days: DateTime.now()
                                                                            .day,
                                                                        months:
                                                                            DateTime.now().month,
                                                                        years: DateTime.now()
                                                                            .year,
                                                                      )
                                                                      .day
                                                                      .toString() +
                                                                  " Días para que los demas usuarios vizualicen tus productos o servicios")
                                                          : (snapshot.data.docs[0].data()['paquete2'] != '')
                                                              ? _widgetPrecio(
                                                                  '15.00',
                                                                  "Tienes " +
                                                                      Jiffy(DateFormat('dd/MM/yyyy').parse(snapshot.data.docs[0].data()['paquete2'].toString()))
                                                                          .subtract(
                                                                            days:
                                                                                DateTime.now().day,
                                                                            months:
                                                                                DateTime.now().month,
                                                                            years:
                                                                                DateTime.now().year,
                                                                          )
                                                                          .day
                                                                          .toString() +
                                                                      " Días para subir productos o servicios ilimitados")
                                                              : (snapshot.data.docs[0].data()['paquete3'] != '')
                                                                  ? _widgetPrecio(
                                                                      '25.00',
                                                                      "Tienes " +
                                                                          Jiffy(DateFormat('dd/MM/yyyy').parse(snapshot.data.docs[0].data()['paquete3'].toString()))
                                                                              .subtract(
                                                                                days: DateTime.now().day,
                                                                                months: DateTime.now().month,
                                                                                years: DateTime.now().year,
                                                                              )
                                                                              .day
                                                                              .toString() +
                                                                          " Días para subir productos o servicios Destacados ilimitados")
                                                                  : Container(),
                                          SizedBox(
                                            height:
                                                getProportionateScreenHeight(
                                                    50),
                                          ),
                                          (snapshot.data.docs[0]
                                                      .data()['suscripcion']
                                                      .toString() ==
                                                  'true')
                                              ? ButtonGray(
                                                  textoBoton:
                                                      "Cancelar Subscripción",
                                                  bFuncion: () {
                                                    _alertaFinal(context,
                                                        'Recuerda que una vez terminada la fecha de tu ultima subscripción, no podras renovar un plan hasta terminar el que tienes, y los demas usuarios no podran visualizar tus productos o servicios hasta renovarla');
                                                  },
                                                  anchoBoton:
                                                      getProportionateScreenHeight(
                                                          330),
                                                  largoBoton:
                                                      getProportionateScreenWidth(
                                                          40),
                                                  colorTextoBoton:
                                                      textColorClaro,
                                                  opacityBoton: 1,
                                                  sizeTextBoton:
                                                      getProportionateScreenWidth(
                                                          15),
                                                  weightBoton: FontWeight.bold,
                                                  color1Boton: primaryColor,
                                                  color2Boton: primaryColor,
                                                )
                                              : Container()
                                        ],
                                      ),
                                    );
                                  } else {
                                    return Container();
                                  }
                                }),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  (widget.regreso == true)
                      ? Padding(
                          padding: const EdgeInsets.only(left: 10, top: 10),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Stack(
                              children: [
                                Icon(
                                  Icons.circle,
                                  size: 50,
                                  color: Color(0Xff707070),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 18, top: 13),
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      : SizedBox(),
                ],
              ))),
    );
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                FirebaseFirestore.instance
                                    .collection("Paquetes")
                                    .where("id_usuario", isEqualTo: prefs.id)
                                    .get()
                                    .then((value) {
                                  FirebaseFirestore.instance
                                      .collection("Paquetes")
                                      .doc(value.docs.first.id)
                                      .update({"suscripcion": false});
                                }).then((value) => Navigator.pop(context));
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Widget _widgetPrecio(precio, tiempo) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenHeight(30)),
      child: Column(children: [
        Text(
          "\$ $precio",
          style: TextStyle(
              color: textColorClaro,
              fontSize: getProportionateScreenWidth(45),
              fontWeight: FontWeight.bold),
        ),
        Text(tiempo,
            style: TextStyle(
              color: textColorClaro,
              fontSize: getProportionateScreenWidth(15),
              fontWeight: FontWeight.normal,
            ),
            textAlign: TextAlign.center),
      ]),
    );
  }

  Widget _widgetPrecioGratuita(precio, tiempo) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenHeight(30)),
      child: Column(children: [
        Text(
          "$precio",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: textColorClaro,
              fontSize: getProportionateScreenWidth(45),
              fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: getProportionateScreenHeight(15),
        ),
        Text(tiempo,
            style: TextStyle(
              color: textColorClaro,
              fontSize: getProportionateScreenWidth(15),
              fontWeight: FontWeight.normal,
            ),
            textAlign: TextAlign.center),
      ]),
    );
  }
}
