import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';

import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';

import 'package:tl_app/widgets/constants.dart';

class HomeProf extends StatefulWidget {
  HomeProf({Key key}) : super(key: key);

  @override
  _HomeProfState createState() => _HomeProfState();
}

class _HomeProfState extends State<HomeProf> {
  @override
  Widget build(BuildContext context) {
    GlobalKey _bottomNavigationKey = GlobalKey();
    final List<Widget> _children = [
      PaquetesProf(regreso: false),
      ProductosProf(),
      ReservasProf(),
      Mensajes(),
      PerfilProf()
    ];

    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            height: 50.0,
            backgroundColor: Color(0xfff191B1F),
            color: Color(0xfff191B1F),
            buttonBackgroundColor: Color(0xfffA91D74),
            items: <Widget>[
              IconoPaquetes(),
              IconoProductos(),
              IconoReservas(),
              IconoMensajes(),
              IconoPerfil()
            ],
            animationDuration: Duration(milliseconds: 200),
            animationCurve: Curves.bounceInOut,
            index: pageProf,
            onTap: (index) {
              //Handle button tap
              setState(() {
                pageProf = index;
              });
            },
          ),
          body: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("lib/assets/images/splash/fondo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              _children[pageProf],
            ],
          )),
    );
  }
}
