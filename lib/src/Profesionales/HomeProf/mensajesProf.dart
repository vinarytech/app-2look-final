import 'package:flutter/material.dart';
import 'package:tl_app/src/Profesionales/MensajesProf/mensajeResumenProf.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';
import 'package:tl_app/widgets/mensajeResumen.dart';
import 'package:tl_app/widgets/size_config.dart';

class MensajesProf extends StatefulWidget {
  @override
  _MensajesProfState createState() => _MensajesProfState();
}

class _MensajesProfState extends State<MensajesProf> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.bottomCenter,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text("",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(18),
                                      fontWeight: FontWeight.bold,
                                    ))),
                          ),
                          Container(
                              child: Column(
                            children: <Widget>[
                              //margin: EdgeInsets.symmetric(vertical: 20.0),

                              Container(
                                //height: 56.0,
                                child: MensajeResumenProf(),
                              ),
                              Container(
                                //height: 56.0,
                                child: MensajeResumenProf(),
                              ),
                            ],
                          ))
                        ],
                      )),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                ],
              ))),
    );
  }
}
