import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/services/usuariosServices.dart';
import 'package:tl_app/src/Clientes/Intro/crearcuenta.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/size_config.dart';

class RequisitosProf extends StatefulWidget {
  RequisitosProf({Key key}) : super(key: key);

  @override
  _RequisitosProfState createState() => _RequisitosProfState();
}

class _RequisitosProfState extends State<RequisitosProf> {
  final nombreCtrl = TextEditingController();
  final aniosCtrl = TextEditingController();
  final prefs = PreferenciasUsuario();
  final usuarioS = UsuarioServices();
  File _image2;
  File _image;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SafeArea(
        child: Scaffold(
            body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("lib/assets/images/splash/fondo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: BotonAtras()),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 200,
                    ),
                    Container(
                      child: Material(
                        type: MaterialType.transparency,
                        child: Text("LLENA ESTOS CAMPOS",
                            style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            )),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: Material(
                        type: MaterialType.transparency,
                        child: Container(

                            //margin: EdgeInsets.only(bottom: 7.0, top: 7),
                            child: InputCustomTextField(
                          label: "Nombre De Usuario o Empresa",
                          placeholder: "Nombre De Usuario o Empresa",
                          textController: nombreCtrl,
                          isPassword: false,
                          keyboardType: TextInputType.text,
                        )),
                      ),
                    ),
                    Container(
                      child: Material(
                        type: MaterialType.transparency,
                        child: Container(

                            //margin: EdgeInsets.only(bottom: 7.0, top: 7),
                            child: InputCustomTextField(
                          label: "Años de experiencia",
                          placeholder: "Años de experiencia",
                          textController: aniosCtrl,
                          isPassword: false,
                          keyboardType:
                              TextInputType.numberWithOptions(decimal: true),
                        )),
                      ),
                    ),
                    (_image == null)
                        ? GestureDetector(
                            onTap: () {
                              _showPickOptions(context);
                              setState(() {});
                            },
                            child: Column(
                              children: [
                                Camara(),
                                Text('Imagen de Portada'),
                              ],
                            ))
                        : Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenHeight(15)),
                            child: Image.file(_image),
                          ),
                    SizedBox(
                      height: getProportionateScreenHeight(30),
                    ),
                    (_image2 == null)
                        ? Container(
                            child: GestureDetector(
                                onTap: () {
                                  _showPickOptions2(context);
                                },
                                child: Column(
                                  children: [Camara(), Text("Cédula o RUC")],
                                )),
                          )
                        : Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenHeight(15)),
                            child: Image.file(_image2),
                          ),
                    SizedBox(
                      height: 30,
                    ),
                    Container(
                      child: ButtonGray(
                        textoBoton: "SIGUIENTE",
                        bFuncion: () {
                          _alertaConfirmacion(context,
                              'Recuerda una vez seas profesional tendras 30 días de suscripcion gratuita');
                        },
                        anchoBoton: getProportionateScreenHeight(300),
                        largoBoton: getProportionateScreenWidth(27),
                        colorTextoBoton: textColorClaro,
                        opacityBoton: 1,
                        sizeTextBoton: getProportionateScreenWidth(15),
                        weightBoton: FontWeight.w400,
                        color1Boton: primaryColor,
                        color2Boton: primaryColor,
                      ),
                    ),
                    Container(),
                  ],
                ),
              )
            ],
          ),
        )),
      ),
    );
  }

  void _showPickOptions2(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker2(ImageSource.gallery);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker2(ImageSource.camera);
                                      Navigator.pop(context);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker2(ImageSource source) async {
    final picker = ImagePicker();
    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image2 = File(pickedFile.path);
          print(_image);
          _cropImage2(_image2);
        } else {}
      });
    }
  }

  void _cropImage2(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 16.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image2 = cropped;
      });
    } else {
      setState(() {
        print('aqui');
        _image2 = null;
      });
    }
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);
        } else {}
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 16.0, ratioY: 9.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
      });
    } else {
      setState(() {
        _image = null;
      });
    }
  }

  void _botonSiguiente() {
    if (nombreCtrl.text == null || nombreCtrl.text == '') {
      _alertaFinal(
          context, 'Por favor ingresa el nombre de Usuario o de su empres');
    } else if (aniosCtrl.text == null || aniosCtrl.text == '') {
      _alertaFinal(context, 'Por favor ingresa los años de experiencia');
    } else if (_image == null) {
      _alertaFinal(context, 'Ingresa una portada de tu local');
    } else if (_image2 == null) {
      _alertaFinal(context,
          'Ingresa la foto de la cédula o del ruc de tu local para mejorar la comunidad 2look');
    } else {
      new Future(() => loaderDialogNormal(context))
          .then((value) => FirebaseFirestore.instance
                  .collection('Usuarios')
                  .doc(prefs.id)
                  .update({
                'tipo': 'profesional',
                'recomendado': false,
                'nombreEmpresa': nombreCtrl.text,
                'anosExperiencia': aniosCtrl.text
              }))
          .then((value) {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .where("id_usuario", isEqualTo: prefs.id)
            .get()
            .then((value) {
          value.docs.forEach((element) {
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(element.id)
                .update({
              'prueba': Jiffy(DateTime.now()).add(days: 30).day.toString() +
                  '/' +
                  Jiffy(DateTime.now()).add(days: 30).month.toString() +
                  '/' +
                  Jiffy(DateTime.now()).add(days: 30).year.toString(),
            });
          });
        });
        imagen();
        subirfotoportada();
        Navigator.pop(context);
        setState(() {
          usuarioS.obtenerUsuarioId(prefs.id);
          // Navigator.pushReplacement(context,
          //     PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          Navigator.pushAndRemoveUntil(
              context,
              PageTransition(child: HomeProf(), type: PageTransitionType.fade),
              (route) => false);
        });
      });
    }
  }

  void imagen() async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Portadas');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(prefs.id)
        .update({'fotoPortada': url});
  }

  void subirfotoportada() async {
    final StorageReference postImagenRf =
        FirebaseStorage.instance.ref().child('Cedula');
    var timeKey = DateTime.now();
    final StorageUploadTask uploadTask =
        postImagenRf.child(timeKey.toString() + ".jpg").putFile(_image2);
    var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
    var url = imageUrl.toString();
    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(prefs.id)
        .update({'cedula': url});
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _alertaConfirmacion(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                                _botonSiguiente();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
