import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:tl_app/clases/producto.dart';
import 'package:tl_app/services/productoService.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirProducto.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class RadioButtonGeneroProductoProf extends StatefulWidget {
  final int isPressed;
  RadioButtonGeneroProductoProf({
    Key key,
    this.isPressed,
  }) : super(key: key);

  @override
  _RadioButtonGeneroProductoProfState createState() => _RadioButtonGeneroProductoProfState();
}



class _RadioButtonGeneroProductoProfState extends State<RadioButtonGeneroProductoProf> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          customRadio(0),
          SizedBox(
            height: 5,
          ),
          customRadio(1),
        ],
      ),
    );
  }

  void changeIndex(int index) {
    setState(() {
      selectedIndex = index;
      if (index == 1) {
        productoService.cambiarGenero("Femenino");
      } else {
        productoService.cambiarGenero("Masculino");
      }
    });
  }

  Widget customRadio(int index) {
    return GestureDetector(
      onTap: () => changeIndex(index),
      child: Container(
        width: getProportionateScreenWidth(15),
        height: getProportionateScreenWidth(15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.transparent,
            border: Border.all(
              color: Colors.white,
              width: getProportionateScreenWidth(2),
            )),
        child: Container(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(right: 1.0),
            child: selectedIndex == index
                ? Icon(
                    Icons.brightness_1,
                    size: getProportionateScreenWidth(8.0),
                    color: primaryColor,
                  )
                : Icon(
                    Icons.access_alarm,
                    size: getProportionateScreenWidth(7.0),
                    color: Colors.transparent,
                  ),
          ),
        ),
      ),
    );
  }
}
