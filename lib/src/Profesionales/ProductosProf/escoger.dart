import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Home/busqueda.dart';
import 'package:tl_app/src/Clientes/Home/guardado.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Home/perfil.dart';
import 'package:tl_app/src/Clientes/Home/reservas.dart';
import 'package:tl_app/src/Clientes/Intro/importante.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirProducto.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirServicio.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'obtenerDataProductos.dart';

class Escoger extends StatefulWidget {
  @override
  _EscogerState createState() => new _EscogerState();
}

class _EscogerState extends State<Escoger> {
  GlobalKey _bottomNavigationKey = GlobalKey();
  final List<Widget> _children = [
    PaquetesProf(regreso: false),
    ProductosProf(),
    ReservasProf(),
    Mensajes(),
    PerfilProf()
  ];
  @override
  void initState() {
    ObtenerDataProductos();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        setState(() {
          print(index);
              Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar,
          body: SizedBox(
              width: double.infinity,
              child: Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("lib/assets/images/splash/fondo.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 38.0),
                    child: Container(
                      height: getProportionateScreenHeight(45),
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Image.asset(
                          'lib/assets/images/busqueda/2look.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 95.0),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Container(
                                  width: getProportionateScreenWidth(160),
                                  height: getProportionateScreenHeight(140),
                                  child: FlatButton(
                                    color: Colors.transparent,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.all(8.0),
                                    splashColor: primaryColor,
                                    onPressed: () {
                                      Navigator.pushReplacement(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: SubirProducto(id: null,)));
                                    },
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: primaryColor,
                                            width: 1,
                                            style: BorderStyle.solid),
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          "PRODUCTO",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(20),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Container(
                                  width: getProportionateScreenWidth(160),
                                  height: getProportionateScreenHeight(140),
                                  child: FlatButton(
                                    color: Colors.transparent,
                                    textColor: Colors.white,
                                    padding: EdgeInsets.all(8.0),
                                    splashColor: primaryColor,
                                    onPressed: () {
                                      Navigator.pushReplacement(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: SubirServicio(id: null,)));
                                    },
                                    shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: primaryColor,
                                            width: 1,
                                            style: BorderStyle.solid),
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Text(
                                          "SERVICIO",
                                          style: TextStyle(
                                            color: textColorClaro,
                                            fontSize:
                                                getProportionateScreenWidth(20),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Container(
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      width: getProportionateScreenWidth(370),
                                      height: getProportionateScreenHeight(1.8),
                                      color: const Color(0x1affffff),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Container(
                                  margin: EdgeInsets.symmetric(vertical: 0.0),
                                  child: Column(
                                    children: <Widget>[
                                      ObtenerDataProductos(),
                                      // Hero(
                                      //   tag: "Hero",
                                      //   child: CardHorizontal(
                                      //     nameImage:
                                      //         "lib/assets/images/card/I1.png",
                                      //     bFunction: () {
                                      //       // Navigator.pushReplacement(
                                      //       //     context,
                                      //       //     PageTransition(
                                      //       //         type:
                                      //       //             PageTransitionType.fade,
                                      //       //         child: DentroProducto()));
                                      //     },
                                      //   ),
                                      // ),
                                      // Hero(
                                      //   tag: "Hero1",
                                      //   child: CardHorizontal(
                                      //     nameImage:
                                      //         "lib/assets/images/card/I2.png",
                                      //     bFunction: () {
                                      //       // Navigator.pushReplacement(
                                      //       //     context,
                                      //       //     PageTransition(
                                      //       //         type:
                                      //       //             PageTransitionType.fade,
                                      //       //         child: DentroServicio()));
                                      //     },
                                      //   ),
                                      // ),
                                    ],
                                  )),
                            ),
                            Container()
                          ],
                        ),
                      ),
                    ),
                  ),
                  Center(
                      child: Column(
                    children: <Widget>[
                      Container(
                          child: Stack(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(450),
                            height: getProportionateScreenHeight(70),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                    "lib/assets/images/busqueda/cabecera.png"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ],
                      )),
                    ],
                  )),
                  BotonAtras()
                ],
              ))),
    );
  }
}



