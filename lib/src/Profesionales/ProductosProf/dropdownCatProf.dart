import 'package:flutter/material.dart';

import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class DropDownCatProf extends StatefulWidget {
  final String text;
  final List lista;
  final Function function;
  DropDownCatProf({Key key, @required this.text, this.lista, this.function})
      : super(key: key);
  @override
  _DropDownCatProfState createState() => _DropDownCatProfState();
}

class _DropDownCatProfState extends State<DropDownCatProf> {
  String shortSpinnerValue;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(20),
            topLeft: Radius.circular(20),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20)),
        color: fondoBotonClaro
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 20,
        height: 32,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: DropdownButton(
            isExpanded: true,
            elevation: 5,
            dropdownColor: textColorMedio,
            underline: SizedBox(),
            iconEnabledColor: textColorClaro,
            value: shortSpinnerValue,
            items: widget.lista
                .map((value) => DropdownMenuItem(
                      child: Container(
                        alignment: Alignment.centerLeft,
                        height: 40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          
                              color: fondoBotonClaro
                        ),
                        //color: Colors.black12,
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: Text(value,
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(13),
                                fontWeight: FontWeight.normal,
                              )),
                        ),
                      ),
                      value: value,
                    ))
                .toList(),
            onChanged: (value) {
              shortSpinnerValue = value;
              widget.function(value);
            },
            hint: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(widget.text,
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(13),
                    fontWeight: FontWeight.normal,
                  )),
            ),
          ),
        ),
      ),
    );
  }
}
