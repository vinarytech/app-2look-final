import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';


class ObtenerDataServicios extends StatefulWidget {
  ObtenerDataServicios({Key key}) : super(key: key);

  @override
  _ObtenerDataServiciosState createState() => _ObtenerDataServiciosState();
}

class _ObtenerDataServiciosState extends State<ObtenerDataServicios> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection("Servicios").snapshots(),
        builder: (context, snapshot) {
          return !snapshot.hasData
              ? Center(child: CircularProgressIndicator())
              : ListView.builder(
                  physics: BouncingScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: snapshot.data.docs.length,
                  itemBuilder: (context, index) {
                    DocumentSnapshot data = snapshot.data.docs[index];
                    
                    return CardHorizontal(
                        nombre: data.get("Categoria"),
                        nameImage: data.get("Imagenes")[0]["Imagen"]);
                  },
                );
        },
      ),
    );
  }
}