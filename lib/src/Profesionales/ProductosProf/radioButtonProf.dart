import 'package:flutter/material.dart';
import 'package:tl_app/clases/producto.dart';
import 'package:tl_app/services/productoService.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class RadioButtonProf extends StatefulWidget {
  final int isPressed;
  RadioButtonProf({
    Key key,
    this.isPressed,
  }) : super(key: key);

  @override
  _RadioButtonProfState createState() =>
      _RadioButtonProfState();
}



class _RadioButtonProfState extends State<RadioButtonProf> {
  int selectedIndex = 0;
  String establecimiento;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          customRadio(0),
          SizedBox(
            height: 5,
          ),
          customRadio(1),
        ],
      ),
    );
  }

  void changeIndex(int index) {
    setState(() {
      selectedIndex = index;
      if (index == 1) {
        final nuevoProducto = new Producto(ubicacion: "Establecimiento");
        productoService.cargarProducto(nuevoProducto);
        establecimiento = "Establecimiento";
       
      } else {
        final nuevoProducto = new Producto(ubicacion: "A domicilio");
        productoService.cargarProducto(nuevoProducto);
        establecimiento = "A domicilio";
       
      }
    });
  }

  Widget customRadio(int index) {
    return GestureDetector(
      onTap: () => changeIndex(index),
      child: Container(
        width: getProportionateScreenWidth(15),
        height: getProportionateScreenWidth(15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.transparent,
            border: Border.all(
              color: Colors.white,
              width: getProportionateScreenWidth(2),
            )),
        child: Container(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(right: 1.0),
            child: selectedIndex == index
                ? Icon(
                    Icons.brightness_1,
                    size: getProportionateScreenWidth(8.0),
                    color: primaryColor,
                  )
                : Icon(
                    Icons.access_alarm,
                    size: getProportionateScreenWidth(7.0),
                    color: Colors.transparent,
                  ),
          ),
        ),
      ),
    );
  }
}
