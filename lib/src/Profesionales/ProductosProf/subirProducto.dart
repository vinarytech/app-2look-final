import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:tl_app/clases/servicio.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/services/productoService.dart';

import 'package:tl_app/services/servicioService.dart';
import 'package:tl_app/src/Clientes/Home/mensajes.dart';
import 'package:tl_app/src/Clientes/Perfil/editarperfil.dart';

import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/mensajesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/paquetesProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/perfilProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/HomeProf/reservasProf.dart';
import 'package:tl_app/src/mapa_page.dart';

import 'package:tl_app/widgets/Profesionales/HomeProf/iconoMensajes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPaquetes.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoPerfil.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoProductos.dart';
import 'package:tl_app/widgets/Profesionales/HomeProf/iconoReservas.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/tipoDiasProf.dart';

import 'package:tl_app/widgets/Profesionales/ReservasProf/tipoServicioCancelacionServicioProf.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/tipoServicioProf.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';

import 'package:tl_app/widgets/importante/camara.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';

import 'package:tl_app/widgets/size_config.dart';

import 'dropdownCatProf.dart';

class SubirProducto extends StatefulWidget {
  final id;
  SubirProducto({
    Key key,
    @required this.id,
  }) : super(key: key);

  @override
  _SubirProductoState createState() => new _SubirProductoState();
}

String capitalize(String s) => (s != null && s.length > 1)
    ? s[0].toUpperCase() + s.substring(1).toLowerCase()
    : s != null
        ? s.toUpperCase()
        : null;
var bloqueado;
var telefono;

class _SubirProductoState extends State<SubirProducto> {
  final prefs = PreferenciasUsuario();
  List<Widget> _children;
  @override
  void initState() {
    super.initState();
    setState(() {
      FirebaseFirestore.instance
          .collection('Usuarios')
          .doc(prefs.id)
          .get()
          .then((value) {
        bloqueado = value.data()['bloqueado'];
        telefono = value.data()['celular'];
      });

      _children = <Widget>[
        PaquetesProf(regreso: false),
        NuevoProducto(idSer: widget.id),
        ReservasProf(),
        Mensajes(),
        PerfilProf()
      ];
    });
  }

  // GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    var curvedNavigationBar = CurvedNavigationBar(
      height: 50.0,
      backgroundColor: Color(0xfff191B1F),
      color: Color(0xfff191B1F),
      buttonBackgroundColor: Color(0xfffA91D74),
      items: <Widget>[
        IconoPaquetes(),
        IconoProductos(),
        IconoReservas(),
        IconoMensajes(),
        IconoPerfil()
      ],
      animationDuration: Duration(milliseconds: 200),
      animationCurve: Curves.bounceInOut,
      index: pageProf,
      onTap: (index) {
        //Handle button tap
        setState(() {
          Navigator.pushReplacement(context,
              PageTransition(type: PageTransitionType.fade, child: HomeProf()));
          pageProf = index;
        });
      },
    );

    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: curvedNavigationBar, body: _children[pageProf]),
    );
  }
}

class NuevoProducto extends StatefulWidget {
  final String idSer;
  NuevoProducto({Key key, this.idSer}) : super(key: key);

  @override
  NuevoProductoState createState() => NuevoProductoState();
}

class NuevoProductoState extends State<NuevoProducto> {
  final prefs = new PreferenciasUsuario();
  File _image;
  int indexImage;
  String imageUrl;
  String categoria = "Selecciona la categoría";
  String generoM = '';
  String genero = '';
  String generoF = '';
  bool generoMas = false;
  bool generoFem = false;
  bool felxible = false;
  bool sinCancelacion = false;
  bool moderado = false;
  String ubicacionServicioId = "";
  String ciudad = "Selecciona la ciudad";
  String sector = "Selecciona el sector";
  String diasDisponibles = "Selecciona los días";
  String horario;
  String horarioInicio = '9:30 PM';
  String horarioFinal = '9:30 PM';
  String cancelacion;
  List listaSectores = [];
  String paquetebase;
  bool domicilio = false;
  bool establecimiento = false;
  bool imagenesValor = false;
  Ubicacion ubicacion = Ubicacion(direccion: '', latitud: '', longitud: '');
  final precioCtlr = TextEditingController();
  final descripconCtlr = TextEditingController();
  final marcaCtlr = TextEditingController();
  List<File> images = List<File>();
  List urlImages = List();

  List dias = List();
  @override
  final StorageReference firebaseStorageRef =
      FirebaseStorage.instance.ref().child('Servicios');
  ScrollPhysics physics = BouncingScrollPhysics();
  Offset _tapPosition;

  @override
  void initState() {
    super.initState();
    if (widget.idSer != null) {
      imagenesValor = true;
      var servicios = FirebaseFirestore.instance
          .collection('Servicios')
          .doc(widget.idSer)
          .snapshots();
      servicios.forEach((value) {
        setState(() {
          paquetebase = value.data()['paquete'];
          categoria = value.data()['categoria'];
          genero = value.data()['genero'];
          var gen = genero.split('/');
          if (gen.length >= 1) {
            if (gen[0] == 'Masculino ' && gen[1] == ' Femenino') {
              generoMas = true;
              generoFem = true;
            } else {
              if (genero == 'Masculino') {
                generoMas = true;
              } else {
                generoFem = true;
              }
            }
          }

          ciudad = value.data()['ciudad'];
          _cargaLista(ciudad);
          sector = value.data()['sectores'];
          diasDisponibles = value.data()['diasdisponible'];
          horarioInicio = value.data()['horaInicio'];
          horarioFinal = value.data()['horaFinal'];
          precioCtlr.text = value.data()['precio'];
          descripconCtlr.text = value.data()['descripcion'];
          cancelacion = value.data()['cancelacion'];
          marcaCtlr.text = value.data()['marca'];
          if (cancelacion == 'Flexible (Hasta 8 horas)') {
            this.felxible = true;
          } else if (cancelacion == 'Sin Cancelación') {
            sinCancelacion = true;
          } else if (cancelacion == 'Moderado') {
            moderado = true;
          }
        });
      });
      var ubicaciones = FirebaseFirestore.instance
          .collection('Ubicaciones')
          .where('id_servicio', isEqualTo: widget.idSer)
          .snapshots();
      setState(() {
        ubicaciones.forEach((element) {
          ubicacionServicioId = element.docs[0].id;
          if (element.docs[0].data()['domicilio']) {
            domicilio = true;
          } else {
            domicilio = false;
          }
          if (element.docs[0].data()['establecimiento']) {
            establecimiento = true;
            ubicacion = Ubicacion(
                direccion: element.docs[0].data()['establecimiento_dir'],
                longitud: element.docs[0].data()['establecimiento_lng'],
                latitud: element.docs[0].data()['establecimiento_lt']);
          } else {
            establecimiento = false;
          }
        });
      });

      var imagenesServicio = FirebaseFirestore.instance
          .collection('ImagenesServicios')
          .where('id_servicio', isEqualTo: widget.idSer)
          .snapshots();
      imagenesServicio.forEach((element) {
        element.docs.forEach((element) {
          setState(() {
            urlImages.add(element.data()['fotourl']);
          });
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onPanDown: (_) {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: SizedBox(
          width: double.infinity,
          child: Stack(
            children: [
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("lib/assets/images/splash/fondo.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 45.0),
                child: Container(
                  height: getProportionateScreenHeight(45),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Image.asset(
                      'lib/assets/images/busqueda/2look.png',
                      height: 35,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 95.0),
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          height: getProportionateScreenHeight(10),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 18.0),
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            child: Material(
                                type: MaterialType.transparency,
                                child: Text("Fotos del producto",
                                    style: TextStyle(
                                      color: textColorClaro,
                                      fontSize: getProportionateScreenWidth(15),
                                      fontWeight: FontWeight.normal,
                                    ))),
                          ),
                        ),
                        (imagenesValor)
                            ? Container(
                                child: (urlImages.length <= 0)
                                    ? GestureDetector(
                                        onTap: () {
                                          _showPickOptions(context);
                                          setState(() {
                                            imagenesValor = false;
                                          });
                                        },
                                        child: Camara())
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Container(
                                            width: getProportionateScreenWidth(
                                                200),
                                            height:
                                                getProportionateScreenHeight(
                                                    200),
                                            child: Swiper(
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return GestureDetector(
                                                  onTap: () {
                                                    indexImage = index;
                                                    _showPopupMenuEditar(
                                                        indexImage);
                                                  },
                                                  onTapDown: _handleTapDown,
                                                  child: new Image.network(
                                                    urlImages[index],
                                                    fit: BoxFit.fill,
                                                  ),
                                                );
                                              },
                                              itemCount: urlImages.length,
                                              itemWidth: 195.0,
                                              itemHeight: 195.0,
                                              layout: SwiperLayout.TINDER,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              _alertaProducto(context,
                                                  'Por favor primero elimina las fotos anteriores y sube las nuevas');
                                              // if (images.length <= 2) {
                                              //   _showPickOptions(context);
                                              // } else {
                                              //   _showMensajeLimite(context);
                                              // }
                                            },
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  height:
                                                      getProportionateScreenHeight(
                                                          180),
                                                  width:
                                                      getProportionateScreenWidth(
                                                          180),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              textColorMedio)),
                                                  child: Icon(
                                                    Icons.add,
                                                    color: textColorMedio,
                                                    size:
                                                        getProportionateScreenHeight(
                                                            44),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                              )
                            : Container(
                                child: images.length <= 0
                                    ? GestureDetector(
                                        onTap: () {
                                          _showPickOptions(context);
                                        },
                                        child: Camara())
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Container(
                                            width: getProportionateScreenWidth(
                                                200),
                                            height:
                                                getProportionateScreenHeight(
                                                    200),
                                            child: Swiper(
                                              itemBuilder:
                                                  (BuildContext context,
                                                      int index) {
                                                return GestureDetector(
                                                  onTap: () {
                                                    indexImage = index;
                                                    _showPopupMenu(indexImage);
                                                  },
                                                  onTapDown: _handleTapDown,
                                                  child: new Image.file(
                                                    images[index],
                                                    fit: BoxFit.fill,
                                                  ),
                                                );
                                              },
                                              itemCount: images.length,
                                              itemWidth: 195.0,
                                              itemHeight: 195.0,
                                              layout: SwiperLayout.TINDER,
                                            ),
                                          ),
                                          GestureDetector(
                                            onTap: () {
                                              if (images.length <= 2) {
                                                _showPickOptions(context);
                                              } else {
                                                _showMensajeLimite(context);
                                              }
                                            },
                                            child: Column(
                                              children: <Widget>[
                                                Container(
                                                  height:
                                                      getProportionateScreenHeight(
                                                          180),
                                                  width:
                                                      getProportionateScreenWidth(
                                                          180),
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                          color:
                                                              textColorMedio)),
                                                  child: Icon(
                                                    Icons.add,
                                                    color: textColorMedio,
                                                    size:
                                                        getProportionateScreenHeight(
                                                            44),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                              ),

                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("CATEGORIA",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      )),
                                ),
                              ),
                              DropDownCatProf(
                                  text: categoria,
                                  lista: [
                                    'Peluquería',
                                    'Diseño de Miradsa y Maquillaje',
                                    'Manicure y Pedicure',
                                    'Asesoría de Imagen',
                                    'Cosmetología',
                                    'Masaje',
                                    'Bienestar y Cuidado Personal',
                                    'Peluquerias Caninas'
                                  ],
                                  function: (value) {
                                    setState(() {
                                      categoria = value;
                                    });
                                  }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        GeneroSeleccion(
                            titulo: 'GENERO',
                            tipo1: 'Masculino',
                            tipo2: 'Femenino',
                            valor1: generoMas,
                            valor2: generoFem,
                            function1: (val1) {
                              setState(() {
                                if (val1) {
                                  generoM = 'Masculino';
                                  generoMas = true;
                                } else {
                                  generoM = '';
                                  generoMas = false;
                                }
                              });
                            },
                            function2: (val2) {
                              setState(() {
                                if (val2) {
                                  generoF = 'Femenino';
                                  generoFem = true;
                                } else {
                                  generoF = '';
                                  generoFem = false;
                                }
                              });
                            }),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("CIUDAD",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      )),
                                ),
                              ),
                              DropDownCatProf(
                                  text: capitalize(ciudad),
                                  lista: [
                                    'QUITO',
                                    'AMBATO',
                                    'GUAYAQUIL',
                                    'CUENCA',
                                    'IBARRA',
                                    'RIOBAMBA',
                                    'MANTA',
                                    'PORTOVIEJO'
                                  ],
                                  function: (value) {
                                    setState(() {
                                      print(value);
                                      ciudad = value;
                                      _cargaLista(ciudad);
                                      // servicioService.cambiarCiudad(value);
                                    });
                                  }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        Container(
                          child: Column(
                            children: [
                              Container(
                                child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("SECTORES",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      )),
                                ),
                              ),
                              DropDownCatProf(
                                  text: sector,
                                  lista: listaSectores,
                                  function: (value) {
                                    setState(() {
                                      sector = capitalize(value);
                                    });
                                  }),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        // Align(
                        //     alignment: Alignment.topLeft,
                        //     child: Padding(
                        //       padding: EdgeInsets.only(
                        //         left: getProportionateScreenWidth(20),
                        //       ),
                        //       child: Text("DÍAS DISPONIBLES",
                        //           style: TextStyle(
                        //             color: textColorClaro,
                        //             fontSize: getProportionateScreenWidth(18),
                        //             fontWeight: FontWeight.bold,
                        //           )),
                        //     )),
                        // Container(
                        //   child: Column(
                        //     children: [
                        //       DropDownCatProf(
                        //           text: diasDisponibles,
                        //           lista: [
                        //             'Todos los días',
                        //             'Luneas a Viernes',
                        //             'Sabado y Domingo'
                        //           ],
                        //           function: (value) {
                        //             setState(() {
                        //               diasDisponibles = value;
                        //             });
                        //           }),
                        //     ],
                        //   ),
                        // ),

                        Column(
                          children: [
                            Container(
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("MARCA",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: fondoBotonClaro,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  height: getProportionateScreenHeight(30),
                                  width: getProportionateScreenHeight(370),
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: TextField(
                                      // keyboardType: TextInputType.numberWithOptions(decimal: true),
                                      controller: marcaCtlr,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      autocorrect: true,
                                      style: DecorationsOnInputs.textoInput(),
                                      decoration: InputDecoration(
                                        errorStyle: TextStyle(
                                          color: textColorClaro,
                                        ),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        labelStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Poppins",
                                        ),
                                        hintStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white38,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: "Poppins",
                                        ),
                                        focusedBorder: InputBorder.none,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  )),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("PRECIO",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ))),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: fondoBotonClaro,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  height: getProportionateScreenHeight(30),
                                  width: getProportionateScreenHeight(370),
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: TextField(
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true),
                                      controller: precioCtlr,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      autocorrect: true,
                                      style: DecorationsOnInputs.textoInput(),
                                      decoration: InputDecoration(
                                        errorStyle: TextStyle(
                                          color: textColorClaro,
                                        ),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        labelStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Poppins",
                                        ),
                                        hintStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white38,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: "Poppins",
                                        ),
                                        focusedBorder: InputBorder.none,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  )),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              child: Material(
                                  type: MaterialType.transparency,
                                  child: Align(
                                      alignment: Alignment.topLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(
                                          left: getProportionateScreenWidth(20),
                                        ),
                                        child: Text("DESCRIPCIÓN",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ))),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Container(
                                  decoration: BoxDecoration(
                                    color: fondoBotonClaro,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  width: getProportionateScreenHeight(370),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextField(
                                      controller: descripconCtlr,
                                      keyboardType: TextInputType.text,
                                      maxLines: 5,
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      autocorrect: true,
                                      style: DecorationsOnInputs.textoInput(),
                                      decoration: InputDecoration(
                                        errorStyle: TextStyle(
                                          color: textColorClaro,
                                        ),
                                        focusedErrorBorder:
                                            UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        errorBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.white, width: 2.0),
                                        ),
                                        labelStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white70,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Poppins",
                                        ),
                                        contentPadding:
                                            new EdgeInsets.symmetric(
                                                vertical: 0.0,
                                                horizontal: 10.0),
                                        hintStyle: TextStyle(
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          color: Colors.white38,
                                          fontWeight: FontWeight.normal,
                                          fontFamily: "Poppins",
                                        ),
                                        focusedBorder: InputBorder.none,
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  )),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TipoServicioProf(
                            titulo: 'UBICACIÓN',
                            tipo1: 'A domicilio',
                            tipo2: 'En establecimiento',
                            seleccion: establecimiento,
                            domicilio: domicilio,
                            function1: (value1) {
                              setState(() {
                                domicilio = value1;
                              });
                            },
                            function2: (value2) async {
                              if (value2) {
                                ubicacion = await Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.fade,
                                        child: MapPage()));
                                setState(() {
                                  print(ubicacion.direccion);
                                  if (ubicacion == null) {
                                    establecimiento = false;
                                  } else {
                                    establecimiento = value2;
                                  }
                                });
                              } else {
                                setState(() {
                                  establecimiento = false;
                                });
                              }
                            }),
                        (establecimiento)
                            ? Padding(
                                padding: EdgeInsets.only(
                                    left: getProportionateScreenWidth(25)),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                      decoration: BoxDecoration(
                                        border:
                                            Border.all(color: textColorClaro),
                                        borderRadius: BorderRadius.circular(50),
                                        // color: Colors.transparent
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          // snapshot.data[3],style: TextStyle( color: textColorClaro,fontSize: getProportionateScreenWidth(12),
                                          ubicacion.direccion,
                                          style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      12)),
                                        ),
                                      )),
                                ),
                              )
                            : Container(),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        TipoServicioCancelacionProductoProf(
                          titulo: 'CANCELACIÓN',
                          tipo1: 'Flexible (Hasta 8 horas)',
                          tipo2: 'Sin Cancelación',
                          flexible: felxible,
                          cancelancion: sinCancelacion,
                          retorno1: (value1) {
                            setState(() {
                              if (value1) {
                                cancelacion = 'Flexible (Hasta 8 horas)';
                                felxible = true;
                                sinCancelacion = false;
                                moderado = false;
                              } else {
                                felxible = false;
                              }
                            });
                          },
                          retorno2: (value2) {
                            setState(() {
                              if (value2) {
                                cancelacion = 'Sin Cancelación';
                                sinCancelacion = true;
                                felxible = false;
                                moderado = false;
                              } else {
                                sinCancelacion = false;
                              }
                            });
                          },
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        (widget.idSer != null)
                            ? Container(
                                child: ButtonGray(
                                  textoBoton: "CAMBIAR",
                                  bFuncion: () async {
                                    _cambiar();
                                  },
                                  anchoBoton: getProportionateScreenHeight(350),
                                  largoBoton: getProportionateScreenWidth(28),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton:
                                      getProportionateScreenWidth(11),
                                  weightBoton: FontWeight.w400,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              )
                            : Container(
                                child: ButtonGray(
                                  textoBoton: "PUBLICAR",
                                  bFuncion: () {
                                    print("bloqueado " + bloqueado.toString());
                                    if (bloqueado.toString() == "true") {
                                      _alertaProducto(context,
                                          'Tu usuario esta bloqueado por favor dirigete a perfil y solicitar ayuda para saber cual es el problema');
                                    } else if (telefono.toString() == '') {
                                      _alertaEditar(context,
                                          "Por favor ingresa el número de telefono para poder continuar");
                                    } else {
                                      _publicar();
                                    }
                                  },
                                  anchoBoton: getProportionateScreenHeight(350),
                                  largoBoton: getProportionateScreenWidth(28),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton:
                                      getProportionateScreenWidth(11),
                                  weightBoton: FontWeight.w400,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              ),
                        SizedBox(
                          height: getProportionateScreenHeight(20),
                        ),
                        Container()
                      ],
                    ),
                  ),
                ),
              ),
              Center(
                  child: Column(
                children: <Widget>[
                  Container(
                      child: Stack(
                    children: [
                      Container(
                        width: getProportionateScreenWidth(450),
                        height: getProportionateScreenHeight(70),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                                "lib/assets/images/busqueda/cabecera.png"),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    ],
                  )),
                ],
              )),
              BotonAtras()
            ],
          )),
    );
  }

  Future<void> loaderDialogNormalMensaje(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Loading(indicator: BallPulseIndicator(), size: 100.0),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                        color: Colors.transparent,
                        child: Text(
                            "Por favor espera unos minutos hasta cargar las imagenes...",
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                            ))),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void _showPickOptions(BuildContext context) async {
    await showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Seleccione de donde desea obtener las imagenes .",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: (17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Galeria",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.gallery);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  )),
                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: ButtonGray(
                                    textoBoton: "Cámara",
                                    bFuncion: () {
                                      _loadPicker(ImageSource.camera);
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(100),
                                    largoBoton: getProportionateScreenWidth(27),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(15),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _showMensajeLimite(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            "Elimine alguna foto anterior para subir una nueva foto del servicio. Limite(3).",
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(150),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _loadPicker(ImageSource source) async {
    final picker = ImagePicker();

    await Permission.photos.request();
    var permisionStatus = await Permission.photos.status;
    if (permisionStatus.isGranted) {
      var pickedFile = await picker.getImage(source: source);
      setState(() {
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          _cropImage(_image);

          /*_image = File(pickedFile.path);
                                                                                                                     productoService.cambiarImagen(_image);*/
        } else {
          print('No image selected.');
        }
      });
    }

    Navigator.of(context).pop();
  }

  void _cropImage(File pickedFile) async {
    /*_image = File(pickedFile.path);
                                                                                                                     productoService.cambiarImagen(_image);*/

    File cropped = await ImageCropper.cropImage(
        sourcePath: pickedFile.path,
        aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio16x9,
          CropAspectRatioPreset.ratio4x3
        ]);
    if (cropped != null) {
      setState(() {
        _image = cropped;
        if (images.length <= 2) {
          images.add(_image);
        }
        //productoService.cambiarImagen(_image);
      });
    }
  }

  void _alertaEditar(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () async {
                                Navigator.pop(context);
                                var valor = await Navigator.push(
                                    context,
                                    PageTransition(
                                        child: EditarPerfil(),
                                        type: PageTransitionType.fade));
                                setState(() {
                                  FirebaseFirestore.instance
                                      .collection('Usuarios')
                                      .doc(prefs.id)
                                      .get()
                                      .then((value) {
                                    bloqueado = value.data()['bloqueado'];
                                    telefono = value.data()['celular'];
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _handleTapDown(TapDownDetails details) {
    final RenderBox referenceBox = context.findRenderObject();
    setState(() {
      _tapPosition = referenceBox.globalToLocal(details.globalPosition);
    });
  }

  void _showPopupMenuEditar(int index) async {
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_tapPosition.dx, _tapPosition.dy,
          _tapPosition.dx + 100, _tapPosition.dy + 100),
      color: textColorMedio,
      items: [
        PopupMenuItem(
          value: 1,
          child: GestureDetector(
            onTap: () {
              setState(() {
                urlImages = [];
              });
              Navigator.pop(context);
            },
            child: Container(
              child: Text(
                "Eliminar Fotos",
                style: TextStyle(
                  color: textColorClaro,
                  fontSize: getProportionateScreenWidth(17),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
      elevation: 5.0,
    );
  }

  void _showPopupMenu(int index) async {
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(_tapPosition.dx, _tapPosition.dy,
          _tapPosition.dx + 100, _tapPosition.dy + 100),
      color: textColorMedio,
      items: [
        PopupMenuItem(
          value: 1,
          child: GestureDetector(
            onTap: () {
              setState(() {
                images.removeAt(index);
              });

              Navigator.pop(context);
            },
            child: Container(
              child: Text(
                "Eliminar",
                style: TextStyle(
                  color: textColorClaro,
                  fontSize: getProportionateScreenWidth(17),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
      elevation: 5.0,
    );
  }

  void _alertaProducto(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> _uploadImages() async {
    int index = 0;

    images.forEach((f) async {
      index = index + 1;
      final StorageReference _ref =
          firebaseStorageRef.child(('Servicios' + index.toString()));
      _ref.putFile(f);
    });
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _publicar() {
    new Future(() => loaderDialogNormalMensaje(context)).then((v) {
      FirebaseFirestore.instance
          .collection('Paquetes')
          .where('id_usuario', isEqualTo: prefs.id)
          .get()
          .then((value) {
        print(value);
        if (value.docs[0].data()['paquete1'] != '') {
          // _publicacion(false);
          // FirebaseFirestore.instance
          //     .collection('Paquetes')
          //     .doc(value.docs[0].id)
          //     .update({
          //   'paquete1': false,
          //   'estadosoli':false,
          // });

          var fecha =
              value.docs[0].data()['paquete1'].toString().replaceAll('/', '.');
          DateFormat format = DateFormat("dd.MM.yyyy");
          if (format.parse(fecha).isAfter(DateTime.now())) {
            _publicacion(false, 'paquete1');
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(value.docs[0].id)
                .update({'estadoPublicar': false});
          } else {
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(value.docs[0].id)
                .update({'paquete1': ''}).then((value) {
              Navigator.pop(context);

              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade,
                      child: PaquetesProf(regreso: true)));
              _alertaProducto(context,
                  'Por favor contrata un paquete para poder publicar tu producto o servicio');
            });
          }
        } else if (value.docs[0].data()['paquete2'] != "") {
          var fecha =
              value.docs[0].data()['paquete2'].toString().replaceAll('/', '.');
          DateFormat format = DateFormat("dd.MM.yyyy");
          if (format.parse(fecha).isAfter(DateTime.now())) {
            _publicacion(false, 'paquete2');
          } else {
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(value.docs[0].id)
                .update({'paquete2': ''}).then((value) {
              Navigator.pop(context);

              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade,
                      child: PaquetesProf(regreso: true)));
              _alertaProducto(context,
                  'Por favor contrata un paquete para poder publicar tu producto o servicio');
            });
          }
        } else if (value.docs[0].data()['paquete3'] != "") {
          var fecha =
              value.docs[0].data()['paquete3'].toString().replaceAll('/', '.');
          DateFormat format = DateFormat("dd.MM.yyyy");
          if (format.parse(fecha).isAfter(DateTime.now())) {
            _publicacion(true, 'paquete3');
          } else {
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(value.docs[0].id)
                .update({'paquete3': ''}).then((value) {
              Navigator.pop(context);
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade,
                      child: PaquetesProf(regreso: true)));
              _alertaProducto(context,
                  'Por favor contrata un paquete para poder publicar tu producto o servicio');
            });
          }
        } else if (value.docs[0].data()['prueba'] != "") {
          var fecha =
              value.docs[0].data()['prueba'].toString().replaceAll('/', '.');
          DateFormat format = DateFormat("dd.MM.yyyy");
          if (format.parse(fecha).isAfter(DateTime.now())) {
            _publicacion(false, 'paquete2');
          } else {
            FirebaseFirestore.instance
                .collection('Paquetes')
                .doc(value.docs[0].id)
                .update({'prueba': ''}).then((value) {
              Navigator.pop(context);
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade,
                      child: PaquetesProf(regreso: true)));
              _alertaProducto(context,
                  'Por favor contrata un paquete para poder publicar tu producto o servicio');
            });
          }
        } else {
          Navigator.pop(context);
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.fade,
                  child: PaquetesProf(regreso: true)));
          _alertaProducto(context,
              'Por favor contrata un paquete para poder publicar tu producto o servicio');
        }
      });
    });
  }

  _publicacion(destacado, paquete) {
    if (images.length == 0) {
      Navigator.pop(context);

      _alertaProducto(context, 'Agregue al menos una imagen');
    } else if (images.length > 1 && paquete == 'paquete1') {
      Navigator.pop(context);
      _alertaProducto(
          context, 'Tu paquete de \$ 5.00 no permite agregar mas de una foto ');
    } else if (categoria == null || categoria == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'No ha seleccionado una categoria');
    } else if ((generoM == null || generoM == '') &&
        (generoF == null || generoF == '')) {
      Navigator.pop(context);

      _alertaProducto(context, 'Seleccione al menos un genero');
    } else if (ciudad == null || ciudad == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'No ha seleccionado una ciudad');
    } else if (sector == null || sector == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'No ha seleccionado un sector');
    } else if (marcaCtlr.text == null || marcaCtlr.text == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'No ha ingresado una marca del producto');
    } else if (domicilio == false && establecimiento == false) {
      Navigator.pop(context);

      _alertaProducto(context, 'Seleccione la ubicación');
    } else if (cancelacion == null || cancelacion == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'Ingrese el tipo de cancelación');
    } else if (precioCtlr.text == null || precioCtlr.text == '') {
      Navigator.pop(context);

      _alertaProducto(context, 'Ingrese una descripcion');
    } else if (descripconCtlr.text == null || descripconCtlr.text == '') {
      Navigator.pop(context);
      _alertaProducto(context, 'Ingrese una descripcion');
    } else {
      if (generoM != '' && generoF == '') {
        genero = generoM;
      } else if (generoM == '' && generoF != '') {
        genero = generoF;
      } else if (generoM != '' && generoF != '') {
        genero = '$generoM / $generoF';
      }

      new Future(() => loaderDialogNormal(context))
          .then((v) {
            FirebaseFirestore.instance.collection('Servicios').add({
              'categoria': categoria.toUpperCase(),
              'genero': genero,
              'ciudad': ciudad,
              'sectores': sector,
              'marca': marcaCtlr.text,
              'subcategoria': 'producto',
              // 'horaInicio': horarioInicio,
              // 'horaFinal': horarioFinal,
              'ubicacion': ubicacion.direccion,
              'cancelacion': cancelacion,
              'precio': precioCtlr.text.replaceAll(',', '.'),
              'descripcion': descripconCtlr.text,
              'id_usuario': prefs.id,
              'paquete': paquete,
              'destacado': destacado,
              'estado': true
            }).then((v) async {
              subirImagenes(images, v.id);
              FirebaseFirestore.instance.collection('Ubicaciones').add({
                'id_servicio': v.id,
                'id_usuario': prefs.id,
                'domicilio': domicilio,
                'domicilio_dir': '',
                'domicilio_lng': '',
                'establecimiento': establecimiento,
                'establecimiento_dir': ubicacion.direccion,
                'establecimiento_lng': ubicacion.longitud,
                'establecimiento_lt': ubicacion.latitud,
              });
            });
          })
          .then((v) => Navigator.pop(context, 'si'))
          .then((value) => Navigator.pop(context, 'si'))
          .then((value) => Navigator.pop(context, 'si'))
          .then((v) {
            _alertaProducto(context, 'Producto Publicado Exitosamente, muy pronto lo veras en tus nuevos productos');
          });
    }
  }

  void subirImagenes(listaimagenes, id) async {
    for (var item in listaimagenes) {
      final StorageReference postImagenRf =
          FirebaseStorage.instance.ref().child('Productos');
      var timeKey = DateTime.now();
      final StorageUploadTask uploadTask =
          postImagenRf.child(timeKey.toString() + ".jpg").putFile(item);
      var imageUrl = await (await uploadTask.onComplete).ref.getDownloadURL();
      var url = imageUrl.toString();
      FirebaseFirestore.instance.collection('ImagenesServicios').add({
        'id_servicio': id,
        'fotourl': url,
      });
    }
  }

  void _cambiar() {
    if (generoMas == true && generoFem == false) {
      genero = 'Masculino';
    } else if (generoMas == false && generoFem == true) {
      genero = 'Femenino';
    } else if (generoMas == true && generoFem == true) {
      genero = 'Masculino / Femenino';
    }
    if (images.length == 0 && urlImages.length == 0) {
      _alertaProducto(context, 'Agregue al menos una imagen');
    }
    if (images.length > 1 &&
        urlImages.length == 0 &&
        paquetebase == "paquete1") {
      _alertaProducto(
          context, 'Tu paquete de \$ 5.00 no permite agregar mas de una foto ');
    } else if (categoria == null || categoria == '') {
      _alertaProducto(context, 'No ha seleccionado una categoria');
    } else if (genero == '' || genero == null) {
      _alertaProducto(context, 'Seleccione al menos un genero');
    } else if (ciudad == null || ciudad == '') {
      _alertaProducto(context, 'No ha seleccionado una ciudad');
    } else if (sector == null || sector == '') {
      _alertaProducto(context, 'No ha seleccionado un sector');
    } else
    // if (diasDisponibles == null || diasDisponibles == '') {
    //   _alertaProducto(context, 'No ha seleccionado los dias disponibles');
    // } else
    if (domicilio == false && establecimiento == false) {
      _alertaProducto(context, 'Seleccione la ubicación');
    } else if (cancelacion == null || cancelacion == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else if (precioCtlr.text == null || precioCtlr.text == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else if (descripconCtlr.text == null || descripconCtlr.text == '') {
      _alertaProducto(context, 'Seleccione el tipo de cancelación');
    } else {
      if (sinCancelacion) {
        cancelacion = 'Sin Cancelación';
      }
      if (felxible) {
        cancelacion = 'Flexible (Hasta 8 horas)';
      }
      if (moderado) {
        cancelacion = 'Moderado';
      }

      new Future(() => loaderDialogNormal(context))
          .then((v) {
            FirebaseFirestore.instance
                .collection('Servicios')
                .doc(widget.idSer)
                .update({
              'categoria': categoria.toUpperCase(),
              'genero': genero,
              'ciudad': ciudad,
              'sectores': sector,
              'marca': marcaCtlr.text,
              'subcategoria': 'producto',
              // 'horaInicio': horarioInicio,
              // 'horaFinal': horarioFinal,
              'ubicacion': ubicacion.direccion,
              'cancelacion': cancelacion,
              'precio': precioCtlr.text.replaceAll(',', '.'),
              'descripcion': descripconCtlr.text,
              'id_usuario': prefs.id,
              'estado': true
            }).then((v) {
              FirebaseFirestore.instance
                  .collection('Ubicaciones')
                  .doc(ubicacionServicioId)
                  .update({
                'domicilio': domicilio,
                'domicilio_dir': '',
                'domicilio_lng': '',
                'establecimiento': establecimiento,
                'establecimiento_dir': ubicacion.direccion,
                'establecimiento_lng': ubicacion.longitud,
                'establecimiento_lt': ubicacion.latitud,
              });
            }).then((value) {
              if (images.length > 0) {
                var inicial = FirebaseFirestore.instance
                    .collection('ImagenesServicios')
                    .where('id_servicio', isEqualTo: widget.idSer)
                    .snapshots();
                inicial.forEach((element) {
                  element.docs.forEach((v) {
                    print(v.data()['fotourl']);
                    FirebaseFirestore.instance
                        .collection('ImagenesServicios')
                        .doc(v.id)
                        .delete();
                    // // //METODO PARA ELIMINAR LAS FOTOS
                    FirebaseStorage.instance
                        .getReferenceFromUrl(v.data()['fotourl'])
                        .then((value) {
                      print("value " + value.path);
                      value.delete();
                    });
                  });
                  setState(() {});
                });
              }
            }).then((value) {
              if (images.length > 0) {
                subirImagenes(images, widget.idSer);
              }
            });
          })
          .then((v) => Navigator.pop(context, 'si'))
          .then((value) => Navigator.pop(context, 'si'))
          .then((v) {
            _alertaProducto(context, 'Producto Publicado Exitosamente');
          });
    }
  }

  void _cargaLista(capital) {
    if (capital == 'QUITO') {
      listaSectores = [
        "BELISARIO QUEVEDO",
        "CARCELÉN",
        "CENTRO HISTÓRICO",
        "COCHAPAMBA",
        "COMITÉ DEL PUEBLO",
        "COTOCOLLAO",
        "CHILIBULO",
        "CHILLOGALLO",
        "CHIMBACALLE",
        "EL CONDADO",
        "GUAMANÍ",
        "IÑAQUITO",
        "ITCHIMBÍA",
        "JIPIJAPA",
        "KENNEDY",
        "LA ARGELIA",
        "LA CONCEPCIÓN",
        "LA ECUATORIANA",
        "LA FERROVIARIA",
        "LA LIBERTAD",
        "LA MAGDALENA",
        "LA MENA",
        "MARISCAL SUCRE",
        "PONCEANO",
        "PUENGASÍ",
        "QUITUMBE",
        "RUMIPAMBA",
        "SAN BARTOLO",
        "SAN ISIDRO DEL INCA",
        "SAN JUAN",
        "SOLANDA",
        "TURUBAMBA",
        "QUITO DISTRITO METROPOLITANO",
        "ALANGASÍ",
        "AMAGUAÑA",
        "ATAHUALPA",
        "CALACALÍ",
        "CALDERÓN",
        "CONOCOTO",
        "CUMBAYÁ",
        "CHAVEZPAMBA",
        "CHECA",
        "EL QUINCHE",
        "GUALEA",
        "GUANGOPOLO",
        "GUAYLLABAMBA",
        "LA MERCED",
        "LLANO CHICO",
        "LLOA",
        "MINDO",
        "NANEGAL",
        "NANEGALITO",
        "NAYÓN",
        "NONO",
        "PACTO",
        "PEDRO VICENTE MALDONADO",
        "PERUCHO",
        "PIFO",
        "PÍNTAG",
        "POMASQUI",
        "PUÉLLARO",
        "PUEMBO",
        "SAN ANTONIO",
        "SAN JOSÉ DE MINAS",
        "SAN MIGUEL DE LOS BANCOS",
        "TABABELA",
        "TUMBACO",
        "YARUQUÍ",
        "ZAMBIZA",
        "PUERTO QUITO"
      ];
    } else if (capital == 'AMBATO') {
      listaSectores = [
        "ATOCHA – FICOA",
        "CELIANO MONGE",
        "HUACHI CHICO",
        "HUACHI LORETO",
        "LA MERCED",
        "LA PENÍNSULA",
        "MATRIZ",
        "PISHILATA",
        "SAN FRANCISCO",
        "AMBATILLO",
        "ATAHUALPA (CHISALATA)",
        "AUGUSTO N. MARTÍNEZ (MUNDUGLEO)",
        "CONSTANTINO FERNÁNDEZ (CAB. EN CULLITAHUA)",
        "HUACHI GRANDE",
        "IZAMBA"
            "JUAN BENIGNO VELA",
        "MONTALVO",
        "NUEVA AMBATO",
        "PASA",
        "PICAIGUA",
        "PILAGÜÍN (PILAHÜÍN)",
        "QUISAPINCHA (QUIZAPINCHA)",
        "SAN BARTOLOMÉ DE PINLLOG",
        "SAN FERNANDO (PASA SAN FERNANDO)",
        "SANTA ROSA",
        "TOTORAS",
        "CUNCHIBAMBA",
        "UNAMUNCHO"
      ];
    } else if (capital == 'GUAYAQUIL') {
      listaSectores = [
        "AYACUCHO",
        "BOLÍVAR (SAGRARIO)",
        "CARBO (CONCEPCIÓN)",
        "FEBRES CORDERO",
        "GARCÍA MORENO",
        "LETAMENDI",
        "NUEVE DE OCTUBRE",
        "OLMEDO (SAN ALEJO)",
        "ROCA",
        "ROCAFUERTE",
        "SUCRE",
        "TARQUI",
        "URDANETA",
        "XIMENA",
        "PASCUALES",
        "GUAYAQUIL",
        "CHONGÓN",
        "JUAN GÓMEZ RENDÓN (PROGRESO)",
        "MORRO",
        "PASCUALES",
        "PLAYAS (GRAL. VILLAMIL)",
        "POSORJA",
        "PUNÁ",
        "TENGUEL"
      ];
    } else if (capital == 'CUENCA') {
      listaSectores = [
        "BELLAVISTA",
        "CAÑARIBAMBA",
        "EL BATÁN",
        "EL SAGRARIO",
        "EL VECINO",
        "GIL RAMÍREZ DÁVALOS",
        "HUAYNACÁPAC",
        "MACHÁNGARA",
        "MONAY",
        "SAN BLAS",
        "SAN SEBASTIÁN",
        "SUCRE",
        "TOTORACOCHA",
        "YANUNCAY",
        "HERMANO MIGUEL",
        "CUENCA",
        "BAÑOS",
        "CUMBE",
        "CHAUCHA",
        "CHECA (JIDCAY)",
        "CHIQUINTAD",
        "LLACAO",
        "MOLLETURO",
        "NULTI",
        "OCTAVIO CORDERO PALACIOS (SANTA ROSA)",
        "PACCHA",
        "QUINGEO",
        "RICAURTE",
        "SAN JOAQUÍN",
        "SANTA ANA",
        "SAYAUSÍ",
        "SIDCAY",
        "SININCAY",
        "TARQUI",
        "TURI",
        "VALLE",
        "VICTORIA DEL PORTETE (IRQUIS)"
      ];
    } else if (capital == 'IBARRA') {
      listaSectores = [
        "CARANQUI",
        "GUAYAQUIL DE ALPACHACA",
        "SAGRARIO",
        "SAN FRANCISCO",
        "LA DOLOROSA DEL PRIORATO",
        "SAN MIGUEL DE IBARRA",
        "AMBUQUÍ",
        "ANGOCHAGUA",
        "CAROLINA",
        "LA ESPERANZA",
        "LITA",
        "SALINAS",
        "SAN ANTONIO"
      ];
    } else if (capital == 'RIOBAMBA') {
      listaSectores = [
        "LIZARZABURU",
        "MALDONADO",
        "VELASCO",
        "VELOZ",
        "YARUQUÍES",
        "RIOBAMBA",
        "CACHA (CAB. EN MACHÁNGARA)",
        "CALPI",
        "CUBIJÍES",
        "FLORES",
        "LICÁN",
        "LICTO",
        "PUNGALÁ",
        "PUNÍN",
        "QUIMIAG",
        "SAN JUAN",
        "SAN LUIS"
      ];
    } else if (capital == 'MANTA') {
      listaSectores = [
        "LOS ESTEROS",
        "MANTA",
        "SAN MATEO",
        "TARQUI",
        "ELOY ALFARO",
        "MANTA",
        "SAN LORENZO",
        "SANTA MARIANITA (BOCA DE PACOCHE)"
      ];
    } else if (capital == 'PORTOVIEJO') {
      listaSectores = [
        "PORTOVIEJO",
        "12 DE MARZO",
        "COLÓN",
        "PICOAZÁ",
        "SAN PABLO",
        "ANDRÉS DE VERA",
        "FRANCISCO PACHECO",
        "18 DE OCTUBRE",
        "SIMÓN BOLÍVAR",
        "PORTOVIEJO",
        "ABDÓN CALDERÓN (SAN FRANCISCO)",
        "ALHAJUELA (BAJO GRANDE)",
        "CRUCITA",
        "PUEBLO NUEVO",
        "RIOCHICO (RÍO CHICO)",
        "SAN PLÁCIDO",
        "CHIRIJOS"
      ];
    }
  }
}
