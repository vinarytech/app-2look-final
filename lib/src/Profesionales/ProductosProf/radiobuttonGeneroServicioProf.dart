import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:tl_app/services/servicioService.dart';

import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class RadioButtonGeneroServicioProf extends StatefulWidget {
  final int isPressed;
  RadioButtonGeneroServicioProf({
    Key key,
    this.isPressed,
  }) : super(key: key);

  @override
  _RadioButtonGeneroServicioProfState createState() =>
      _RadioButtonGeneroServicioProfState();
}

class _RadioButtonGeneroServicioProfState
    extends State<RadioButtonGeneroServicioProf> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          customRadio1(0),
          SizedBox(
            height: 5,
          ),
          customRadio1(1),
        ],
      ),
    );
  }

  void changeIndex(int index) {
    setState(() {
      // selectedIndex = index;
      // if (index == 1) {
      //   servicioService.cambiarGenero("Femenino");
      // } else {
      //   servicioService.cambiarGenero("Masculino");
      // }
    });
  }

  Widget customRadio1(int index) {
    return GestureDetector(
      onTap: () => changeIndex(index),
      child: Container(
        width: getProportionateScreenWidth(15),
        height: getProportionateScreenWidth(15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.transparent,
            border: Border.all(
              color: Colors.white,
              width: getProportionateScreenWidth(2),
            )),
        child: Container(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(right: 1.0),
            child: selectedIndex == index
                ? Icon(
                    Icons.brightness_1,
                    size: getProportionateScreenWidth(8.0),
                    color: primaryColor,
                  )
                : Icon(
                    Icons.access_alarm,
                    size: getProportionateScreenWidth(7.0),
                    color: Colors.transparent,
                  ),
          ),
        ),
      ),
    );
  }

  Widget customRadio2(int index) {
    return GestureDetector(
      onTap: () => changeIndex(index),
      child: Container(
        width: getProportionateScreenWidth(15),
        height: getProportionateScreenWidth(15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.transparent,
            border: Border.all(
              color: Colors.white,
              width: getProportionateScreenWidth(2),
            )),
        child: Container(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.only(right: 1.0),
            child: selectedIndex == index
                ? Icon(
                    Icons.brightness_1,
                    size: getProportionateScreenWidth(8.0),
                    color: primaryColor,
                  )
                : Icon(
                    Icons.access_alarm,
                    size: getProportionateScreenWidth(7.0),
                    color: Colors.transparent,
                  ),
          ),
        ),
      ),
    );
  }
}
