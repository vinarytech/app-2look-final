/*ListView(
                      children: [
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("QUE ESTAS BUSCANDO",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("Servicio",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("Producto",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                        Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("GENERO",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("Masculino",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("Femenino",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("CATEGORIA",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                DropDownCustomed(
                                    text: "Selecciona la categoria")
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("RANGO DE PRECIO",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                RangeSliderCustom()
                              ],
                            )),
                        SizedBox(
                          height: 10,
                        ),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("CANCELACION",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                DropDownCustomed(text: "Selecciona el tipo")
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("CIUDAD",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                DropDownCustomed(text: "Selecciona la ciudad")
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("SECTOR",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                DropDownCustomed(text: "Selecciona el sector")
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Align(
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                            left:
                                                getProportionateScreenWidth(20),
                                          ),
                                          child: Text("HORARIOS",
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        18),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        )),
                                  ),
                                ),
                                Row(
                                  children: [
                                    DateTimePickerCustom(),
                                    Text("a",
                                        style: TextStyle(
                                          color: textColorClaro,
                                          fontSize:
                                              getProportionateScreenWidth(18),
                                          fontWeight: FontWeight.normal,
                                        )),
                                    DateTimePickerCustom()
                                  ],
                                )
                              ],
                            )),
                        Expanded(
                            flex: 1,
                            child: Column(
                              children: [
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("A domicilio",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        type: MaterialType.transparency,
                                        child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Padding(
                                              padding: EdgeInsets.only(
                                                left:
                                                    getProportionateScreenWidth(
                                                        20),
                                              ),
                                              child: Text("En establecimiento",
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            14),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  )),
                                            )),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 28.0),
                                        child: RadioButton(),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )),
                        Expanded(
                          flex: 1,
                          child: ButtonGray(
                            textoBoton: "APLICAR",
                            bFuncion: () {
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.fade,
                                      child: Home()));
                            },
                            anchoBoton: getProportionateScreenHeight(350),
                            largoBoton: getProportionateScreenWidth(27),
                            colorTextoBoton: textColorClaro,
                            opacityBoton: 1,
                            sizeTextBoton: getProportionateScreenWidth(11),
                            weightBoton: FontWeight.w400,
                            color1Boton: darkColor,
                            color2Boton: darkColor,
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Container(),
                        )
                      ],
                    ),*/