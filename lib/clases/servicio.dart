class Servicio {
  String ubicacion;
  String imagen;
  List imagenes;
  bool masculino;
  bool femenino;
  bool lunes;
  bool martes;
  bool miercoles;
  bool jueves;
  bool viernes;
  bool sabado;
  bool domingo;
  bool aDomicilio;
  bool enEstablecimiento;
  String horaInicio;
  String horaFin;
  String categoria;
  String ciudad;
  String marca;
  String sector;
  bool sinCancelacion;
  bool flexible;

  Servicio(
      {this.horaInicio,
      this.aDomicilio,
      this.enEstablecimiento,
      this.horaFin,
      this.lunes,
      this.martes,
      this.miercoles,
      this.jueves,
      this.viernes,
      this.sabado,
      this.domingo,
      this.sinCancelacion,
      this.flexible,
      this.sector,
      this.imagenes,
      this.marca,
      this.ubicacion,
      this.imagen,
      this.masculino,
      this.categoria,
      this.femenino,
      this.ciudad});
}
