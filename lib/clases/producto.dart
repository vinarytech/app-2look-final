class Producto {
  String ubicacion;
  String imagen;
  List imagenes;
  String genero;
  String categoria;
  String ciudad;
  String marca;
  String sector;
  String cancelacion;
  bool masculino;
  bool femenino;
  bool sinCancelacion;
  bool flexible;
  bool aDomicilio;
  bool enEstablecimiento;

  Producto(
      {this.cancelacion,
      this.aDomicilio,
      this.enEstablecimiento,
      this.sector,
      this.imagenes,
      this.marca,
      this.ubicacion,
      this.sinCancelacion,
      this.flexible,
      this.imagen,
      this.genero,
      this.masculino,
      this.femenino,
      this.categoria,
      this.ciudad});
}
