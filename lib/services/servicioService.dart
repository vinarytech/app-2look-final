import 'dart:async';

import 'package:tl_app/clases/servicio.dart';

class _ServicioService {
  Servicio _servicio;

  StreamController<Servicio> _usuarioStreamController =
      new StreamController<Servicio>();

  Servicio get servicio => this._servicio;

  bool get existeServicio => (this._servicio != null) ? true : false;

  Stream<Servicio> get servicioStream => _usuarioStreamController.stream;

  void cargarServicio(Servicio servicio) {
    this._servicio = servicio;
  }

  void cambiarCategoria(String categoria) {
    this._servicio.categoria = categoria;
  }

  void cambiarCiudad(String ciudad) {
    this._servicio.ciudad = ciudad;
  }

  void cambiarSector(String sector) {
    this._servicio.sector = sector;
  }

  void cambiarMasculino(bool masculino) {
    this._servicio.masculino = masculino;
  }

  void cambiarFemenino(bool femenino) {
    this._servicio.femenino = femenino;
  }

  void cambiarUbicacion(String ubicacion) {
    this._servicio.ubicacion = ubicacion;
  }

  void cambiarImagen(String imagen) {
    this._servicio.imagen = imagen;
  }

  void cambiarMarca(String marca) {
    this._servicio.marca = marca;
  }

  void agregarImagenes(List imagenes) {
    this._servicio.imagenes = imagenes;
  }

  void cambiarMiercoles(bool miercoles) {
    this._servicio.miercoles = miercoles;
  }

  void cambiarMartes(bool martes) {
    this._servicio.martes = martes;
  }

  void cambiarLunes(bool lunes) {
    this._servicio.lunes = lunes;
  }

  void cambiarJueves(bool jueves) {
    this._servicio.jueves = jueves;
  }

  void cambiarViernes(bool viernes) {
    this._servicio.viernes = viernes;
  }

  void cambiarSabado(bool sabado) {
    this._servicio.sabado = sabado;
  }

  void cambiarDomingo(bool domingo) {
    this._servicio.domingo = domingo;
  }

  void cambiarHoraInicio(String horainicio) {
    this._servicio.horaInicio = horainicio;
  }

  void cambiarHoraFin(String horafin) {
    this._servicio.horaFin = horafin;
  }

  void cambiarADomicilio(bool domicilio) {
    this._servicio.aDomicilio = domicilio;
  }

  void cambiarEnEstablecimiento(bool establecimiento) {
    this._servicio.enEstablecimiento = establecimiento;
  }

  void cambiarFlexible(bool flexible) {
    this._servicio.flexible = flexible;
  }

  void cambiarSinCancelacion(bool cancelacion) {
    this._servicio.sinCancelacion = cancelacion;
  }
}

final servicioService = new _ServicioService();
