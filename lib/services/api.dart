import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/card.dart';

class ServiceApi {
  final ruta = "https://2look.app/api/public/api/";
  Future sedEmail(informacion, nombre, email, telefono) async {
    final resp = await http.post(
      ruta + "emails/app_help",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'nombre': nombre,
        'email': email,
        'telefono': telefono,
        'mensaje': informacion,
      }),
    );
    print("valorcito " + resp.body.toString());
    return resp;
  }

  final prefs = PreferenciasUsuario();

  Future<List<CardModelo>> getCards() async {
    final resp = await http.get(ruta + "paymentez/showByUser/${prefs.id}",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final decodeData = json.decode(resp.body);
    final cards = json.decode(decodeData[0])['cards'];
    final List<CardModelo> tarjetas = new List();

    if (cards == null) return [];
    for (var item in cards) {
      print(item);
      if (item['status'] != 'pending') {
        tarjetas.add(CardModelo.fromJson(item));
      }
    }
    print('asdas');
    return tarjetas;
  }

  Future deleteCards(String token) async {
    final resp = await http.post(
      ruta + "paymentez/delete",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'paymentez': jsonEncode({
          'card': {'token': token.toString()},
          'user': {'id': prefs.id.toString()}
        })
      }),
    );
    return resp;
  }

  Future debitTokenPaquetes(
      String tokenTarjeta, double totalPagar, String descripcion) async {
    final resp = await http.post(
      ruta + "paymentez/debitToken",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'paymentez': jsonEncode({
          'card': {'token': tokenTarjeta.toString()},
          'user': {'id': this.prefs.id, 'email': this.prefs.usuario['correo']},
          'order': {
            'amount': totalPagar,
            'description': descripcion,
            'dev_reference': '0',
            'vat': 0,
            'tax_percentage': 0
          },
        })
      }),
    );
    return resp;
  }



    Future debitTokenPaquetesEmail(
      String tokenTarjeta, double totalPagar, String descripcion, email) async {
    final resp = await http.post(
      ruta + "paymentez/debitToken",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'paymentez': jsonEncode({
          'card': {'token': tokenTarjeta.toString()},
          'user': {'id': this.prefs.id, 'email': email},
          'order': {
            'amount': totalPagar,
            'description': descripcion,
            'dev_reference': '0',
            'vat': 0,
            'tax_percentage': 0
          },
        })
      }),
    );
    return resp;
  }


  Future verifyToken(String transaction, String codigoOpt) async {

    final resp = await http.post(
      ruta + "paymentez/verify",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'paymentez': jsonEncode({
          'transaction': {'id': transaction.toString()},
          'user': {'id': this.prefs.id.toString()},
          'type': "BY_OTP",
          'value': codigoOpt,
        })
      }),
    );
    return resp;
  }


    Future verifyTokenPay(String transaction, String codigoOpt, email) async {

    final resp = await http.post(
      ruta + "paymentez/verify",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode({
        'paymentez': jsonEncode({
          'transaction': {'id': transaction.toString()},
          'user': {'id': this.prefs.id, 'email': email},
          'type': "BY_OTP",
          'value': codigoOpt,
          'more_info':true
        })
      }),
    );
    return resp;
  }
}




CardModelo cardModelFromJson(String str) => CardModelo.fromJson(json.decode(str));

String cardModelToJson(CardModelo data) => json.encode(data.toJson());

class CardModelo {
  CardModelo({
    this.bin,
    this.status,
    this.token,
    this.holderName,
    this.expiryYear,
    this.expiryMonth,
    this.transactionReference,
    this.type,
    this.number,
  });

  String bin;
  String status;
  String token;
  String holderName;
  String expiryYear;
  String expiryMonth;
  String transactionReference;
  String type;
  String number;

  factory CardModelo.fromJson(Map<String, dynamic> json) => CardModelo(
        bin: json["bin"],
        status: json["status"],
        token: json["token"],
        holderName: json["holder_name"],
        expiryYear: json["expiry_year"],
        expiryMonth: json["expiry_month"],
        transactionReference: json["transaction_reference"],
        type: json["type"],
        number: json["number"],
      );

  Map<String, dynamic> toJson() => {
        "bin": bin,
        "status": status,
        "token": token,
        "holder_name": holderName,
        "expiry_year": expiryYear,
        "expiry_month": expiryMonth,
        "transaction_reference": transactionReference,
        "type": type,
        "number": number,
      };
}
