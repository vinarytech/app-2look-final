import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:flutter/material.dart';

class UsuarioServices {
  final String firebaseToken = 'AIzaSyD6wekMhoTm1kdpWjIe1MTRlZ4aTBs2T9s';
  final pref = new PreferenciasUsuario();
  var data;

  Future<Map<String, dynamic>> login(String email, String password) async {
    final authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final resp = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=$firebaseToken',
        body: json.encode(authData));

    Map<String, dynamic> decodeResp = json.decode(resp.body);

    if (decodeResp.containsKey('idToken')) {
      pref.token = decodeResp['idToken'];
      // pref.usuario = jsonEncode({'email': email, 'password': password});
      return {'ok': true, 'token': decodeResp['idToken']};
    } else {
      return {'ok': false, 'mensaje': decodeResp['error']['message']};
    }
  }

  Future<Map<String, dynamic>> nuevoUsuario(
      String email, String password, String usuario) async {
    final authData = {
      'email': email,
      'password': password,
      'returnSecureToken': true
    };

    final resp = await http.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=$firebaseToken',
        body: json.encode(authData));
    Map<String, dynamic> decodeResp = json.decode(resp.body);

    if (decodeResp.containsKey('idToken')) {
      // pref.usuario = jsonEncode({'email': email, 'password': password, 'usuario': usuario});
      pref.token = decodeResp['idToken'];
      print(decodeResp['idToken']);
      return {'ok': true, 'token': decodeResp['idToken']};
    } else {
      return {'ok': false, 'mensaje': decodeResp['error']['message']};
    }
  }

  final prefs = PreferenciasUsuario();

  Future<Map<String, dynamic>> obtenerUsuario(
      String email, String password) async {
    var data = await FirebaseFirestore.instance
        .collection("Usuarios")
        .where('correo', isEqualTo: email)
        .get();

    if (data == null) return null;
    data.docs.forEach((element) {
      if (element.data()['contrasenia'].toString() == password) {
        prefs.id = element.id.toString();
        prefs.usuario = json.encode(element.data());
        print(prefs.id);
        print(prefs.usuario);
        return element.data();
      } else {
        return false;
      }
    });
  }

  Future<Map<String, dynamic>> obtenerUsuarioId(id) async {
    var data =
        await FirebaseFirestore.instance.collection("Usuarios").doc(id).get();
    if (data == null) return null;
    prefs.usuario = json.encode(data.data());
    return data.data();
  }
}
