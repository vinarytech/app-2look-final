import 'package:tl_app/clases/scroll.dart';

class _ScrollService {
  Scroll _scroll;

  Scroll get scroll => this._scroll;

  bool get existeScroll => (this._scroll != null) ? true : false;

  void cargarScroll(Scroll scroll) {
    this._scroll = scroll;
  }
}

final scrollService = new _ScrollService();
