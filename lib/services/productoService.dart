import 'dart:async';

import 'package:tl_app/clases/producto.dart';

class _ProductoService {
  Producto _producto;

  StreamController<Producto> _usuarioStreamController =
      new StreamController<Producto>();

  Producto get producto => this._producto;

  bool get existeProducto => (this._producto != null) ? true : false;

  Stream<Producto> get productoStream => _usuarioStreamController.stream;

  void cargarProducto(Producto producto) {
    this._producto = producto;
  }

  void cambiarCategoria(String categoria) {
    this._producto.categoria = categoria;
  }

  void cambiarCiudad(String ciudad) {
    this._producto.ciudad = ciudad;
  }

  void cambiarSector(String sector) {
    this._producto.sector = sector;
  }

  void cambiarCancelacion(String cancelacion) {
    this._producto.cancelacion = cancelacion;
  }

  void cambiarMasculino(bool masculino) {
    this._producto.masculino = masculino;
  }

  void cambiarFemenino(bool femenino) {
    this._producto.femenino = femenino;
  }

  void cambiarGenero(String genero) {
    this._producto.genero = genero;
  }

  void cambiarUbicacion(String ubicacion) {
    this._producto.ubicacion = ubicacion;
  }

  void cambiarImagen(String imagen) {
    this._producto.imagen = imagen;
  }

  void cambiarMarca(String marca) {
    this._producto.marca = marca;
  }

  void agregarImagenes(List imagenes) {
    this._producto.imagenes = imagenes;
  }

  void cambiarFlexible(bool flexible) {
    this._producto.flexible = flexible;
  }

  void cambiarSinCancelacion(bool cancelacion) {
    this._producto.sinCancelacion = cancelacion;
  }

  void cambiarADomicilio(bool domicilio) {
    this._producto.aDomicilio = domicilio;
  }

  void cambiarEnEstablecimiento(bool establecimiento) {
    this._producto.enEstablecimiento = establecimiento;
  }
}

final productoService = new _ProductoService();
