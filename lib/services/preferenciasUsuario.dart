import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

get usuario {
    // return _prefs.getString('usuario') ?? null;
         dynamic valor =_prefs.getString('usuario') ?? null;
     dynamic arreglo = json.decode(valor);
     return arreglo;
}

set usuario(String value){
    _prefs.setString('usuario', value);
}

get id {
    return _prefs.getString('id') ?? null;
}

set id(String value){
    _prefs.setString('id', value);
}

  // GET y SET del token
  get token {
    return _prefs.getString('token') ?? null;
  }

  set token( String value ) {
    _prefs.setString('token', value);
  }
  

  get inicio {
    return _prefs.getBool('inicio') ?? null;
  }

  set inicio( bool value ) {
    _prefs.setBool('inicio', value);
  }



  // GET y SET de la última página
  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'login';
  }

  set ultimaPagina( String value ) {
    _prefs.setString('ultimaPagina', value);
  }

}