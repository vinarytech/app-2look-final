import 'dart:ui';
import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProductoFinal.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicioFinal.dart';
import 'package:tl_app/src/Clientes/Perfil/metododepago.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/informacionfinal.dart';
import 'package:http/http.dart' as http;

import 'package:tl_app/services/preferenciasUsuario.dart';

import 'constants.dart';

class CardDividida extends StatefulWidget {
  final Function bFunction;
  final QueryDocumentSnapshot document;
  CardDividida({Key key, this.bFunction, this.document}) : super(key: key);

  @override
  _CardDivididaState createState() => _CardDivididaState();
}

class _CardDivididaState extends State<CardDividida> {
  List listanuneva = [];
  final prefs = PreferenciasUsuario();
  String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
  String tokenPush;
  String idUsurioRecibe;

  Future _listaimagenes(idservicio) async {
    List lista = [];
    await FirebaseFirestore.instance
        .collection("ImagenesServicios")
        .where('id_servicio', isEqualTo: idservicio)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        var list = result.data().values.toList();
        list.remove(idservicio);
        String finalStr = list.reduce((value, element) {
          return value + element;
        });
        lista.add(finalStr);
      });
    });
    return lista;
  }

  Future<InformacionFinal> obtener(
      String id, String idservicio, QueryDocumentSnapshot documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    var dataservicio = await FirebaseFirestore.instance
        .collection('Servicios')
        .doc(idservicio)
        .get();

    listanuneva = await _listaimagenes(idservicio);

    var informacionusuario;
    informacionusuario = InformacionFinal(
        doc: data1,
        docservicio: dataservicio,
        fotos: listanuneva,
        documento: documento);

    return informacionusuario;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenHeight(16)),
      child: FutureBuilder(
          future: obtener(widget.document.data()["idencargado"],
              widget.document.data()["idservicio"], widget.document),
          builder:
              (BuildContext context, AsyncSnapshot<InformacionFinal> snapshot) {
            //  print('snapshot pasar $snapshotpasar');
            if (snapshot.hasData) {
              idUsurioRecibe = widget.document.data()["idencargado"];
              tokenPush = snapshot.data.doc.data()["token"];
              return Container(
                child: Material(
                    color: Colors.transparent,
                    shadowColor: Colors.white70,
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (snapshot.data.documento.data()['estado'] ==
                                    'PENDIENTE') {
                                  print('hola');
                                  _alertapendiente(
                                      context,
                                      'El profesional debe confirmar la fecha y hora para que puedas proceder al pago',
                                      );
                                }
                                if (snapshot.data.documento.data()['estado'] ==
                                    'POR PAGAR') {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.fade,
                                          child: MetodoDePago(
                                            servicio:
                                                '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                                            precio:
                                                '${snapshot.data.docservicio.data()["precio"].toString()}'
                                                r'$',
                                            cantidad: snapshot.data.documento
                                                .data()["cantidad"]
                                                .toString(),
                                            total:
                                                '${snapshot.data.documento.data()["total"].toString()}'
                                                r' $',
                                            iddoc: snapshot.data.documento.id,
                                            idEncargado: snapshot.data.documento
                                                .data()["idencargado"],
                                            token: snapshot.data.doc
                                                .data()["token"],
                                          )));
                                }
                                if (snapshot.data.documento.data()['estado'] ==
                                    'EN TRANSCURSO') {
                                  print('Hola');
                                  if (snapshot.data.docservicio
                                          .data()["subcategoria"] ==
                                      'producto') {
                                    print(snapshot.data.docservicio
                                        .data()["subcategoria"]);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: DentroProductoFinal(
                                              snapshot: snapshot.data,
                                              tipo:'usuario'
                                            )));
                                  } else {
                                    print(snapshot.data.docservicio
                                        .data()["subcategoria"]);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: DentroServicioFinal(
                                              snapshot: snapshot.data,
                                              tipo:'usuario'
                                            )));
                                  }
                                }
                              },
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 3,
                                    child: Stack(
                                      children: [
                                        Stack(
                                          children: <Widget>[
                                            Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      150),
                                              width:
                                                  getProportionateScreenWidth(
                                                      150),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                              child: Container(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 10,
                                                                right: 10),
                                                        child: Text(
                                                            '${snapshot.data.doc.data()["nombre"]}',
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      11),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width:
                                                          getProportionateScreenHeight(
                                                              5),
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: [
                                                        Text(
                                                            '${snapshot.data.doc.data()["estrellas"]}',
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      11),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                        Icon(
                                                          Icons.star,
                                                          size:
                                                              getProportionateScreenHeight(
                                                                  17),
                                                          color: primaryColor,
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left:
                                                      getProportionateScreenHeight(
                                                          12.0)),
                                              child: Container(
                                                height:
                                                    getProportionateScreenHeight(
                                                        84),
                                                width:
                                                    getProportionateScreenWidth(
                                                        150),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: getProportionateScreenHeight(5),
                                  ),
                                  Expanded(
                                      flex: 5,
                                      child: Stack(
                                        children: [
                                          Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      150),
                                              width:
                                                  getProportionateScreenWidth(
                                                      300),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 10, right: 10),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: <Widget>[
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      "FECHA ACEPTADA:",
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      "HORA INICIO:",
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      "HORA FIN:",
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        Column(
                                                          children: [
                                                            Container(
                                                              width:
                                                                  getProportionateScreenWidth(
                                                                      110),
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    // "SERVICIO/PELUQERIA",
                                                                    '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Container(
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                // "SERVICIO/PELUQERIA",
                                                                'Cantidad'
                                                                    .toUpperCase(),
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                        Container(
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                "TOTAL:",
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      snapshot
                                                                          .data
                                                                          .documento
                                                                          .data()[
                                                                              'fecha']
                                                                          .toString(),
                                                                      // DateFormat('yyyy-MM-dd').format(snapshot.data
                                                                      //         .documento
                                                                      //         .data()[
                                                                      //     'fechaaceptada'].toDate()),
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      snapshot
                                                                          .data
                                                                          .documento
                                                                          .data()['horainicio'],
                                                                      style: TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        snapshot.data.documento
                                                                            .data()[
                                                                        'estado'] ==
                                                                    'POR PAGAR' ||
                                                                snapshot.data
                                                                        .documento
                                                                        .data()['estado'] ==
                                                                    'EN TRANSCURSO'
                                                            ? Container(
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      snapshot
                                                                          .data
                                                                          .documento
                                                                          .data()['horafin'],
                                                                      style: TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      )),
                                                                ),
                                                              )
                                                            : SizedBox(),
                                                        Container(
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                '${snapshot.data.docservicio.data()["precio"].toString()}'
                                                                r'$',
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                        Container(
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                snapshot.data
                                                                    .documento
                                                                    .data()[
                                                                        "cantidad"]
                                                                    .toString(),
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                        Container(
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                '${snapshot.data.documento.data()["total"].toString()}'
                                                                r' $',
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ))
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 4.0, left: 20, right: 20),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Text(
                                        snapshot.data.documento
                                            .data()["estado"],
                                        style: TextStyle(
                                          color: primaryColor,
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ),
                                ),
                                GestureDetector(
                                    onTap: () async {
                                      if (snapshot.data.documento
                                              .data()['estado'] ==
                                          'PENDIENTE') {
                                        _alerta(
                                            context,
                                            'Esta seguro que desea cancelar',
                                            true,
                                            snapshot.data.documento.id,
                                            0,
                                            '',
                                            'PENDIENTE');
                                      } else if (snapshot.data.documento
                                              .data()['estado'] ==
                                          'POR PAGAR') {
                                        _alerta(
                                            context,
                                            'Esta seguro que desea cancelar',
                                            true,
                                            snapshot.data.documento.id,
                                            0,
                                            '',
                                            'POR PAGAR');
                                      } else {
                                        print(
                                            '${snapshot.data.docservicio.data()['cancelacion']}');
                                        if (snapshot.data.docservicio
                                                .data()['cancelacion'] ==
                                            'Sin Cancelación') {
                                          _alerta(
                                              context,
                                              'Si cancelas ahora tendras que pagar una multa de ' +
                                                  snapshot.data.documento
                                                      .data()['total'],
                                              true,
                                              snapshot.data.documento.id,
                                              snapshot.data.documento
                                                  .data()['total'],
                                              tokenPush,
                                              '');
                                          print('sin cancelacion');
                                        } else {
                                          if (Jiffy(DateTime.now())
                                              .subtract(hours: 8)
                                              .isAfter(snapshot.data.documento
                                                  .data()['fechaaceptada']
                                                  .toDate())) {
                                            _alerta(
                                                context,
                                                'Si cancelas ahora tendras que pagar una multa de ' +
                                                    snapshot.data.documento
                                                        .data()['total'],
                                                true,
                                                snapshot.data.documento.id,
                                                snapshot.data.documento
                                                    .data()['total'],
                                                tokenPush,
                                                '');
                                          } else {
                                            _alertaFlexible(
                                                context,
                                                '¿Estas seguro de querer cancelar la reserva?' +
                                                    snapshot.data.documento
                                                        .data()['total'],
                                                snapshot.data.documento.id);
                                          }
                                        }
                                      }

                                      /* await  FirebaseFirestore.instance
                                    .collection('Solicitudes')
                                    .doc(widget.iddoc)
                                    .update({'estado': 'EN TRANSCURSO'});*/
                                    },
                                    child: Container(
                                      alignment: Alignment.bottomRight,
                                      child: Material(
                                        type: MaterialType.transparency,
                                        child: Text("CANCELAR SOLICITUD",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              fontWeight: FontWeight.normal,
                                            )),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }

  void _alerta(BuildContext context, String text, bool cancelado, String id,
      precio, token, String estado) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                // if (cancelado == true &&
                                //     estado != 'PENDIENTE') {
                                //   FirebaseFirestore.instance
                                //       .collection('Solicitudes')
                                //       .doc(id)
                                //       .update({
                                //     'estadosolicitud': false,
                                //     'estado': 'FINALIZADO',
                                //     'cancelado': 'SI'
                                //   }).then((value) {
                                //     FirebaseFirestore.instance
                                //         .collection('Pago_multas')
                                //         .add({
                                //       'fecha': DateTime.now().year.toString() +
                                //           "-" +
                                //           DateTime.now().month.toString() +
                                //           "-" +
                                //           DateTime.now().day.toString(),
                                //       'forma_pago': "",
                                //       'id_paymentez': '',
                                //       'id_transferencia': '',
                                //       'fotografia_transferencia': '',
                                //       'motivo': 'Cancelación de Solicitud',
                                //       'negado': '',
                                //       'id_usuario': prefs.id,
                                //       'id_solicitud': id,
                                //       'id_user_action': "",
                                //       'status': true,
                                //       'fecha_cancela': '',
                                //       'total': precio,
                                //     }).then((value) {
                                //       FirebaseFirestore.instance
                                //           .collection('Notificaciones')
                                //           .add({
                                //         'idreceptor': idUsurioRecibe,
                                //         'idemisor': prefs.id,
                                //         'date': date,
                                //         'accion': 'solicitud',
                                //         'notificacion':
                                //             'Tu solicitud fue cancelada.',
                                //       });
                                //     });
                                //   });
                                //   sendNotification(
                                //       'Tu solicitud fue cancelada.',
                                //       'Tow Look',
                                //       tokenPush);

                                //   Navigator.of(context).pop();
                                // } else

                                if (estado == 'POR PAGAR' ||
                                    estado == 'PENDIENTE') {
                                  FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(id)
                                      .update({
                                    'estadosolicitud': false,
                                    'estado': 'FINALIZADO',
                                    'cancelado': 'SI'
                                  }).then((value) {
                                    FirebaseFirestore.instance
                                        .collection('Pago_multas')
                                        .add({
                                      'fecha': DateTime.now().year.toString() +
                                          "-" +
                                          DateTime.now().month.toString() +
                                          "-" +
                                          DateTime.now().day.toString(),
                                      'token': prefs.usuario['token'],
                                      'forma_pago': "",
                                      'id_paymentez': '',
                                      'id_transferencia': '',
                                      'fotografia_transferencia': '',
                                      'motivo': 'Cancelación de Solicitud',
                                      'negado': '',
                                      'pago': '',
                                      'id_usuario': prefs.id,
                                      'id_solicitud': id,
                                      'id_user_action': "",
                                      'status': true,
                                      'fecha_cancela': '',
                                      'total': precio,
                                    }).then((value) {
                                      FirebaseFirestore.instance
                                          .collection('Notificaciones')
                                          .add({
                                        'idreceptor': idUsurioRecibe,
                                        'idemisor': prefs.id,
                                        'date': date,
                                        'accion': 'solicitud',
                                        'notificacion':
                                            'Tu solicitud fue cancelada.',
                                      });
                                    });
                                  });
                                  sendNotification(
                                      'Tu solicitud fue cancelada.',
                                      'Tow Look',
                                      tokenPush);
                                  Navigator.of(context).pop();
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
  void _alertapendiente(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                           Navigator.of(context).pop();

                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }


  void _alertaFlexible(BuildContext context, String text, String id) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () async {
                                FirebaseFirestore.instance
                                    .collection('Solicitudes')
                                    .doc(id)
                                    .update({
                                  'estadosolicitud': false,
                                  'estado': 'FINALIZADO',
                                  'cancelado': 'SI'
                                });
                                Navigator.of(context).pop();
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> sendNotification(subject, title, idToken) async {
    print(idToken);
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {"body": subject, "title": title},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": idToken
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }
}


