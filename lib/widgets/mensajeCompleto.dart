import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class MensajeCompleto extends StatefulWidget {
  final mensaje;
  final nombre;
  final estrellas;
  MensajeCompleto({Key key, this.mensaje, this.nombre, this.estrellas})
      : super(key: key);

  @override
  _MensajeCompletoState createState() => _MensajeCompletoState();
}

class _MensajeCompletoState extends State<MensajeCompleto> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: getProportionateScreenHeight(10)),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5.0, top: 5),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.topLeft,
                    height: 20,
                    //width: 150,
                    child: Material(
                      type: MaterialType.transparency,
                      child: Text(
                          widget.nombre.toString() +
                              " / " +
                              widget.estrellas.toString(),
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(11),
                            fontWeight: FontWeight.bold,
                          )),
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    alignment: Alignment.topCenter,
                    child: Icon(
                      Icons.star,
                      size: 14,
                      color: primaryColor,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: primaryColor.withOpacity(0.5),
                ),
                width: 300,
                child: Material(
                  type: MaterialType.transparency,
                  elevation: 6.0,
                  color: Colors.transparent,
                  shadowColor: Colors.grey[50],
                  child: InkWell(
                    splashColor: Colors.white30,
                    onTap: () {},
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.mensaje.toString(),
                          style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(12),
                              fontWeight: FontWeight.normal)),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}

class MensajeReseptor extends StatefulWidget {
  final mensaje;
  MensajeReseptor({Key key, this.mensaje}) : super(key: key);

  @override
  _MensajeReseptorState createState() => _MensajeReseptorState();
}

class _MensajeReseptorState extends State<MensajeReseptor> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Container(
      child: Padding(
       padding: EdgeInsets.symmetric(horizontal: getProportionateScreenHeight(10)),
        child: Column(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(left: 5.0, top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      alignment: Alignment.topRight,
                      height: 20,
                      child: Material(
                        type: MaterialType.transparency,
                        child: Text(
             "Yo",
              style: TextStyle(

                color: textColorClaro,
                fontSize: getProportionateScreenWidth(11),
                fontWeight: FontWeight.bold,
              )),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                 
                  ],
                ),
              ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: lightColor.withOpacity(0.5),
                ),
                width: 300,
                child: Material(
                  type: MaterialType.transparency,
                  elevation: 6.0,
                  color: Colors.transparent,
                  shadowColor: Colors.grey[50],
                  child: InkWell(
                    splashColor: Colors.white30,
                    onTap: () {},
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(widget.mensaje.toString(),
                          style: TextStyle(
                              color: textColorClaro,
                              fontSize: getProportionateScreenWidth(12),
                              fontWeight: FontWeight.normal)),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
