import 'package:flutter/material.dart';

class BusquedaButton extends StatefulWidget {
  final String textoBoton;
  final Function bFuncion;
  final double anchoBoton;
  final double largoBoton;
  final Color colorTextoBoton;
  final double opacityBoton;
  final double sizeTextBoton;
  final FontWeight weightBoton;
  final Color color1Boton;
  final Color color2Boton;
  final Icon icon;

  BusquedaButton(
      {this.textoBoton,
      this.bFuncion,
      this.anchoBoton,
      this.largoBoton,
      this.colorTextoBoton,
      this.opacityBoton,
      this.sizeTextBoton,
      this.weightBoton,
      this.color1Boton,
      this.color2Boton,
      this.icon});

  @override
  _BusquedaButtonState createState() => new _BusquedaButtonState();
}

class _BusquedaButtonState extends State<BusquedaButton> {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          height: widget.largoBoton,
          width: widget.anchoBoton,
          padding: EdgeInsets.all(0.5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(27)),
            boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
            
                color:widget.color1Boton
          ),
          child: Material(
            type: MaterialType.transparency,
            elevation: 6.0,
            color: Colors.transparent,
            shadowColor: Colors.grey[50],
            child: InkWell(
                splashColor: Colors.white30,
                borderRadius: BorderRadius.all(Radius.circular(10)),
                onTap: () {
                  widget.bFuncion();
                },
                child: Center(
                    child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(
                      width: 10,
                    ),
                    widget.icon,
                    SizedBox(
                      width: 10,
                    ),
                    Text(widget.textoBoton,
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          color: widget.colorTextoBoton,
                          fontSize: widget.sizeTextBoton,
                          fontWeight: widget.weightBoton,
                          fontStyle: FontStyle.normal,
                        )),
                  ],
                ))),
          ),
        ),
      ],
    );
  }
}

