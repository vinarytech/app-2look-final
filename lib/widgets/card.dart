import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class Card extends StatefulWidget {
  final String textoBoton;
  final Function bFuncion;
  final double anchoBoton;
  final double largoBoton;
  final Color colorTextoBoton;
  final double opacityBoton;
  final double sizeTextBoton;
  final FontWeight weightBoton;
  final Color color1Boton;
  final Color color2Boton;
  

  Card(
      {this.textoBoton,
      this.bFuncion,
      this.anchoBoton,
      this.largoBoton,
      this.colorTextoBoton,
      this.opacityBoton,
      this.sizeTextBoton,
      this.weightBoton,
      this.color1Boton,
      this.color2Boton});

  @override
  _CardState createState() => new _CardState();
}

class _CardState extends State<Card> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: ListView(
        scrollDirection: Axis.vertical,
        children: [
          Padding(
              padding: EdgeInsets.all(getProportionateScreenHeight(16)),
              child: Container(
                child: Material(
                    color: Colors.transparent,
                    elevation: 5,
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20.0)),
                    shadowColor: Colors.white70,
                    child: Stack(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(
                            height: 150,
                            width: 300,
                            decoration: BoxDecoration(
                              color: lightColor,
                              borderRadius: BorderRadius.circular(20),
                            ),
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 2,
                              child: Stack(
                                children: [
                                  Container(
                                    height: 150,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: AssetImage(
                                              "lib/assets/images/card/I1.png"),
                                          fit: BoxFit.fill,
                                        ),
                                        borderRadius:
                                            BorderRadius.circular(20.0)),
                                  ),
                                  Positioned(
                                    top: 115,
                                    left: 5,
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      height: 30,
                                      width: 30,
                                      child: IconButton(
                                        icon: new Image.asset(
                                            'lib/assets/images/card/esquina.png'),
                                        iconSize: 30,
                                        color: Colors.white,
                                        onPressed: () => Navigator.pop(context),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                                flex: 3,
                                child: Stack(
                                  children: [
                                    Container(
                                      height: 150,
                                      decoration: BoxDecoration(
                                        color: lightColor,
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(20.0),
                                            bottomRight: Radius.circular(20.0)),
                                      ),
                                    ),
                                    Positioned(
                                        top: 10,
                                        left: 15,
                                        child: Column(
                                          children: [
                                            Container(
                                              alignment: Alignment.topLeft,
                                              width: 150,
                                              child: Material(
                                                type: MaterialType.transparency,
                                                child: Text(
                                                    "SERVICIO / PELUQUERÍA",
                                                    style: TextStyle(
                                                      color: textColorClaro,
                                                      fontSize:
                                                          getProportionateScreenWidth(
                                                              14),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.topLeft,
                                              width: 150,
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    height: 30,
                                                    //width: 150,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: Text(
                                                          "Christian / 4.6",
                                                          style: TextStyle(
                                                            color:
                                                                textColorClaro,
                                                            fontSize:
                                                                getProportionateScreenWidth(
                                                                    11),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          )),
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 5,
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topCenter,
                                                    child: Icon(
                                                      Icons.star,
                                                      size: 14,
                                                      color: primaryColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.topLeft,
                                              height: 60,
                                              width: 150,
                                              child: Material(
                                                type: MaterialType.transparency,
                                                child: Text(
                                                    "Corte de cabello para hombre y mujer",
                                                    style: TextStyle(
                                                      color: textColorClaro,
                                                      fontSize:
                                                          getProportionateScreenWidth(
                                                              11),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    )),
                                              ),
                                            ),
                                            Container(
                                              alignment: Alignment.topLeft,
                                              width: 150,
                                              child: Material(
                                                type: MaterialType.transparency,
                                                child: Text("\$10.00",
                                                    style: TextStyle(
                                                      color: textColorClaro,
                                                      fontSize:
                                                          getProportionateScreenWidth(
                                                              18),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ))
                                  ],
                                )),
                          ],
                        ),
                      ],
                    )),
              )),
        ],
      ),
    );
  }
}
