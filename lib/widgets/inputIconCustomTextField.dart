import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/yabusqueda.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';
import 'decorationsOnInputs.dart';

class InputIconCustomTextField extends StatefulWidget {
  final String placeholder;
  final TextEditingController textController;
  final TextInputType keyboardType;
  final bool isPassword;
  final String label;
  final Icon icon;
  final Widget page;
  InputIconCustomTextField(
      {Key key,
      this.placeholder,
      this.textController,
      this.keyboardType,
      this.isPassword,
      this.label,
      this.icon,
      this.page})
      : super(key: key);

  @override
  _InputIconCustomTextFieldState createState() =>
      _InputIconCustomTextFieldState();
}

class _InputIconCustomTextFieldState extends State<InputIconCustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        width: getProportionateScreenWidth(197),
        height: getProportionateScreenHeight(38),
        child: TextField(
         
          textAlignVertical: TextAlignVertical.center,
          obscureText: widget.isPassword,
          controller: widget.textController,
          autocorrect: true,
          keyboardType: widget.keyboardType,
          style: DecorationsOnInputs.textoInput(),
          decoration: InputDecoration(
              /*prefixIcon: Padding(
                padding: EdgeInsets.only(top: 0), // add padding to adjust icon
                child: widget.icon,
              ),*/
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(27)),
                borderSide: const BorderSide(color: lightColor, width: 5.0),
              ),
              errorStyle: TextStyle(
                color: textColorClaro,
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              labelText: widget.label,
              labelStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white70,
                fontWeight: FontWeight.w600,
                fontFamily: "Poppins",
              ),
              contentPadding: new EdgeInsets.only(left: 14.0),
              hintStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white38,
                fontWeight: FontWeight.normal,
                fontFamily: "Poppins",
              ),
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              hintText: widget.placeholder),
        ));
  }
}









class InputIconCustomTextFieldDisable extends StatefulWidget {
  final String placeholder;
  final TextEditingController textController;
  final TextInputType keyboardType;
  final bool isPassword;
  final String label;
  final Icon icon;
  final Widget page;
  InputIconCustomTextFieldDisable(
      {Key key,
      this.placeholder,
      this.textController,
      this.keyboardType,
      this.isPassword,
      this.label,
      this.icon,
      this.page})
      : super(key: key);

  @override
  _InputIconCustomTextFieldDisableState createState() =>
      _InputIconCustomTextFieldDisableState();
}

class _InputIconCustomTextFieldDisableState extends State<InputIconCustomTextFieldDisable> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        width: getProportionateScreenWidth(197),
        height: getProportionateScreenHeight(38),
        child: TextField(
          // enabled: false,
          onSubmitted: (value) => Navigator.pushReplacement(
              context,
              PageTransition(
                  type: PageTransitionType.fade, child: widget.page)),
          textAlignVertical: TextAlignVertical.center,
          obscureText: widget.isPassword,
          controller: widget.textController,
          autocorrect: true,
          keyboardType: widget.keyboardType,
          style: DecorationsOnInputs.textoInput(),
          decoration: InputDecoration(
          enabled: false,
            
              /*prefixIcon: Padding(
                padding: EdgeInsets.only(top: 0), // add padding to adjust icon
                child: widget.icon,
              ),*/
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(27)),
                borderSide: const BorderSide(color: lightColor, width: 5.0),
              ),
              errorStyle: TextStyle(
                color: textColorClaro,
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              labelText: widget.label,
              labelStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white70,
                fontWeight: FontWeight.w600,
                fontFamily: "Poppins",
              ),
              contentPadding: new EdgeInsets.only(left: 14.0),
              hintStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white38,
                fontWeight: FontWeight.normal,
                fontFamily: "Poppins",
              ),
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              hintText: widget.placeholder),
        ));
  }
}