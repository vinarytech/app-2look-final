import 'package:flutter/material.dart';

import 'package:tl_app/widgets/decorationsOnInputsSearch.dart';

class Plantilla extends StatefulWidget {
  @override
  _PlantillaState createState() => new _PlantillaState();
}

class _PlantillaState extends State<Plantilla> {
  FocusNode _focus = new FocusNode();
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("lib/assets/images/splash/fondo.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Align(
            alignment: Alignment.topCenter,
            child: Image.asset(
              'lib/assets/images/busqueda/cabecera.png',
            ),
          ),
        ),
        Center(
            child: Column(
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Container(
              height: 45,
              child: Align(
                alignment: Alignment.topCenter,
                child: Image.asset(
                  'lib/assets/drawable-xxxhdpi/busqueda/2look.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 7.0, top: 7),
                  child: searchBarField(_focus),
                ),
                /*BusquedaButton(
                  icon: Icon(Icons.search, size: 10, color: Colors.white),
                  textoBoton: "",
                  bFuncion: () {

                  },
                  anchoBoton: 342,
                  largoBoton: 27,
                  colorTextoBoton: Color(0xffffffff),
                  opacityBoton: 0.10,
                  sizeTextBoton: 11,
                  weightBoton: FontWeight.w300,
                  color1Boton: Color.fromRGBO(57, 59, 63, 1),
                  color2Boton: Color.fromRGBO(57, 59, 63, 1),
                ),*/
                SizedBox(
                  width: 20,
                ),
                Center(
                  child: Align(
                    alignment: Alignment.center,
                    child: Image.asset(
                      'lib/assets/drawable-xxxhdpi/busqueda/filtros.png',
                    ),
                  ),
                ),
              ],
            )
          ],
        )),
      ],
    ));
  }
}

Widget searchBarField(_focus) {
  return Container(
      width: 342,
      height: 40,
      child: TextFormField(
        style: DecorationsOnInputsSearch.textoInput(),
        keyboardType: TextInputType.text,
        decoration: DecorationsOnInputsSearch.decorationInput(
          "Busqueda",
          "",
        ),
        onSaved: (String value) {
          print(value);
        },
        focusNode: _focus,
      ));
}
