import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';

import 'constants.dart';

class CardDividida2 extends StatefulWidget {
  final Function bFunction;
  final QueryDocumentSnapshot document;
  CardDividida2({Key key, this.bFunction, this.document}) : super(key: key);

  @override
  _CardDividida2State createState() => _CardDividida2State();
}


class _CardDividida2State extends State<CardDividida2> {
  Informacion snapshot;
  List listanuneva = [];
  Future _listaimagenes(idservicio) async {
    List lista = [];
    await FirebaseFirestore.instance
        .collection("ImagenesServicios")
        .where('id_servicio', isEqualTo: idservicio)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        var list = result.data().values.toList();
        list.remove(idservicio);
        String finalStr = list.reduce((value, element) {
          return value + element;
        });
        lista.add(finalStr);
      });
    });
    return lista;
  }

  Future<InformacionFinal> obtener(
      String id, String idservicio, QueryDocumentSnapshot documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    var dataservicio = await FirebaseFirestore.instance
        .collection('Servicios')
        .doc(idservicio)
        .get();

    listanuneva = await _listaimagenes(idservicio);

    var informacionusuario;
    informacionusuario = InformacionFinal(
        doc: data1,
        docservicio: dataservicio,
        fotos: listanuneva,
        documento: documento);

    return informacionusuario;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenHeight(16)),
      child: FutureBuilder(
          future: obtener(widget.document.data()["idencargado"],
              widget.document.data()["idservicio"], widget.document),
          builder:
              (BuildContext context, AsyncSnapshot<InformacionFinal> snapshot) {
            //  print('snapshot pasar $snapshotpasar');
            if (snapshot.hasData) {
              return Container(
                child: Material(
                    color: Colors.transparent,
                    shadowColor: Colors.white70,
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            GestureDetector(
                              onTap: () {
                                /* if(snapshot.data.documento.data()['estado']=='PENDIENTE'){
                            print('hola');
                            _alerta(context, 'El profesional debe confirmar la fecha y hora para que puedas proceder al pago',false,'');
                            }
                            if(snapshot.data.documento.data()['estado']=='POR PAGAR'){
                             Navigator.pushReplacement(
                            context,
                            PageTransition(
                            type: PageTransitionType.fade, child: MetodoDePago(
                              servicio: '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                              precio: '${snapshot.data.docservicio.data()["precio"].toString()}' r'$',
                              cantidad: snapshot.data.documento.data()["cantidad"].toString(),
                              total: '${snapshot.data.documento.data()["total"].toString()}' r' $' ,
                              iddoc: snapshot.data.documento.id,
                              idEncargado:snapshot.data.documento.data()["idencargado"] ,
                            )));
                            }
                            if(snapshot.data.documento.data()['estado']=='EN TRANSCURSO'){
                             print('Hola');
                              if(snapshot.data.docservicio.data()["subcategoria"]=='producto'){
                                print(snapshot.data.docservicio.data()["subcategoria"]);
                                Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: DentroProductoFinal(snapshot: snapshot.data,)));
                              }else{
                                print(snapshot.data.docservicio.data()["subcategoria"]);
                                Navigator.pushReplacement(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: DentroServicioFinal(snapshot: snapshot.data,)));
                              }
                            }*/
                              },
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 3,
                                    child: Stack(
                                      children: [
                                        Stack(
                                          children: <Widget>[
                                            Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      150),
                                              width:
                                                  getProportionateScreenWidth(
                                                      150),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left:
                                                      getProportionateScreenHeight(
                                                          12.0)),
                                              child: Container(
                                                height:
                                                    getProportionateScreenHeight(
                                                        84),
                                                width:
                                                    getProportionateScreenWidth(
                                                        150),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Material(
                                                        type: MaterialType
                                                            .transparency,
                                                        child: Text(
                                                            '${snapshot.data.doc.data()["nombre"]}'
                                                            ' / ${snapshot.data.doc.data()["estrellas"]}',
                                                            style: TextStyle(
                                                              color:
                                                                  textColorClaro,
                                                              fontSize:
                                                                  getProportionateScreenWidth(
                                                                      11),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            )),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width:
                                                          getProportionateScreenHeight(
                                                              5),
                                                    ),
                                                    Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Icon(
                                                        Icons.star,
                                                        size:
                                                            getProportionateScreenHeight(
                                                                17),
                                                        color: primaryColor,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: getProportionateScreenHeight(5),
                                  ),
                                  Expanded(
                                      flex: 5,
                                      child: Stack(
                                        children: [
                                          Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      150),
                                              width:
                                                  getProportionateScreenWidth(
                                                      300),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: <Widget>[
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "FECHA ACEPTADA:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "HORA INICIO:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "HORA FIN:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      Container(
                                                        width:
                                                            getProportionateScreenWidth(
                                                                140),
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              // "SERVICIO/PELUQERIA",
                                                              '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              // "SERVICIO/PELUQERIA",
                                                              'Cantidad'
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text("TOTAL:",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'fecha'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'horainicio'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                      .data()[
                                                                  'estado'] ==
                                                              'FINALIZADO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'horafin'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              '${snapshot.data.docservicio.data()["precio"].toString()}'
                                                              r'$',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              snapshot.data
                                                                  .documento
                                                                  .data()[
                                                                      "cantidad"]
                                                                  .toString(),
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              '${snapshot.data.documento.data()["total"].toString()}'
                                                              r' $',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ))
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 4.0, left: 20, right: 20),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: Text(
                                      
                                        snapshot.data.documento
                                            .data()["estado"],
                                        style: TextStyle(
                                          color: primaryColor,
                                          fontSize:
                                              getProportionateScreenWidth(11),
                                          fontWeight: FontWeight.normal,
                                        )),
                                  ),
                                ),
                                
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }

  void _alerta(BuildContext context, String text, bool cancelado, String id) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () async {
                                if (cancelado == true) {
                                  await FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(id)
                                      .delete();
                                  Navigator.of(context).pop();
                                } else {
                                  Navigator.of(context).pop();
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}

class InformacionFinal {
  List fotos;
  DocumentSnapshot doc;
  DocumentSnapshot docservicio;
  QueryDocumentSnapshot documento;
  InformacionFinal({
    this.fotos,
    this.doc,
    this.docservicio,
    this.documento,
  });
}
