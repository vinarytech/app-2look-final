import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:http/http.dart' as http;
import 'package:tl_app/widgets/informacionfinal.dart';

import 'package:tl_app/src/Clientes/Busqueda/dentroProductoFinal.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicioFinal.dart';
import 'package:tl_app/src/Clientes/Perfil/metododepago.dart';
import 'package:tl_app/src/Profesionales/ReservasProf/aceptarReserva.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class CardDivididaProf extends StatefulWidget {
  final Function bFunction;
  final QueryDocumentSnapshot document;
  final idSolicitud;
  CardDivididaProf({Key key, this.bFunction, this.document, this.idSolicitud})
      : super(key: key);

  @override
  _CardDivididaProfState createState() => _CardDivididaProfState();
}

class _CardDivididaProfState extends State<CardDivididaProf> {
  final convert = new NumberFormat("#,##0.00", "en_US");
  final prefs = PreferenciasUsuario();
  String tokenPush;
  String usuariorecibe;
  List listanuneva = [];
  String valortotal;
  String idUsurioRecibe;
  String idSolicitud;
  String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
  Future _listaimagenes(idservicio) async {
    List lista = [];
    await FirebaseFirestore.instance
        .collection("ImagenesServicios")
        .where('id_servicio', isEqualTo: idservicio)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        var list = result.data().values.toList();
        list.remove(idservicio);
        String finalStr = list.reduce((value, element) {
          return value + element;
        });
        lista.add(finalStr);
      });
    });
    return lista;
  }

  Future<InformacionFinal> obtener(
      String id, String idservicio, QueryDocumentSnapshot documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    var dataservicio = await FirebaseFirestore.instance
        .collection('Servicios')
        .doc(idservicio)
        .get();

    listanuneva = await _listaimagenes(idservicio);

    var informacionusuario;
    informacionusuario = InformacionFinal(
        doc: data1,
        docservicio: dataservicio,
        fotos: listanuneva,
        documento: documento);

    return informacionusuario;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenHeight(16)),
      child: FutureBuilder(
          future: obtener(widget.document.data()["idsolicitante"],
              widget.document.data()["idservicio"], widget.document),
          builder:
              (BuildContext context, AsyncSnapshot<InformacionFinal> snapshot) {
            //  print('snapshot pasar $snapshotpasar');
            if (snapshot.hasData) {
              idSolicitud = snapshot.data.documento.id;
              idUsurioRecibe = widget.document.data()["idsolicitante"];
              return Container(
                child: Material(
                    color: Colors.transparent,
                    shadowColor: Colors.white70,
                    child: Column(
                      children: [
                        Stack(
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (snapshot.data.documento.data()['estado'] ==
                                    'PENDIENTE') {
                                  Navigator.push(
                                      context,
                                      PageTransition(
                                          type: PageTransitionType.fade,
                                          child: AceptarReserva(
                                            idServicio: widget.document
                                                .data()["idservicio"],
                                            idSolicitud: widget.idSolicitud,
                                          )));
                                  // print('hola');
                                  // _alerta(context, 'El profesional debe confirmar la fecha y hora para que puedas proceder al pago',false,'');
                                }
                                if (snapshot.data.documento.data()['estado'] ==
                                    'POR PAGAR') {
                                  //  Navigator.pushReplacement(
                                  // context,
                                  // PageTransition(
                                  // type: PageTransitionType.fade, child: MetodoDePago(
                                  //   servicio: '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                                  //   precio: '${snapshot.data.docservicio.data()["precio"].toString()}' r'$',
                                  //   cantidad: snapshot.data.documento.data()["cantidad"].toString(),
                                  //   total: '${snapshot.data.documento.data()["total"].toString()}' r' $' ,
                                  //   iddoc: snapshot.data.documento.id,
                                  //   idEncargado:snapshot.data.documento.data()["idencargado"] ,
                                  // )));
                                }
                                if (snapshot.data.documento.data()['estado'] ==
                                    'EN TRANSCURSO') {
                                  if (snapshot.data.docservicio
                                          .data()["subcategoria"] ==
                                      'producto') {
                                    print(snapshot.data.docservicio
                                        .data()["subcategoria"]);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: DentroProductoFinal(
                                                snapshot: snapshot.data,
                                                tipo: 'profesional',
                                                )));
                                  } else {
                                    print(snapshot.data.docservicio
                                        .data()["subcategoria"]);
                                    Navigator.push(
                                        context,
                                        PageTransition(
                                            type: PageTransitionType.fade,
                                            child: DentroServicioFinal(
                                                snapshot: snapshot.data,
                                                tipo:'profesional'
                                                )));
                                  }
                                }
                              },
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 3,
                                    child: Stack(
                                      children: [
                                        Stack(
                                          children: <Widget>[
                                            Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      115),
                                              width:
                                                  getProportionateScreenWidth(
                                                      150),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomLeft: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  left:
                                                      getProportionateScreenHeight(
                                                          12.0)),
                                              child: Container(
                                                height:
                                                    getProportionateScreenHeight(
                                                        84),
                                                width:
                                                    getProportionateScreenWidth(
                                                        150),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Material(
                                                        type: MaterialType
                                                            .transparency,
                                                        child: Container(
                                                          width:
                                                              getProportionateScreenWidth(
                                                                  50),
                                                          child: Text(
                                                              '${snapshot.data.doc.data()["nombre"]}'
                                                              ' / ${snapshot.data.doc.data()["estrellas"]}',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      width:
                                                          getProportionateScreenHeight(
                                                              5),
                                                    ),
                                                    Container(
                                                      alignment:
                                                          Alignment.center,
                                                      child: Icon(
                                                        Icons.star,
                                                        size:
                                                            getProportionateScreenHeight(
                                                                17),
                                                        color: primaryColor,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    width: getProportionateScreenHeight(5),
                                  ),
                                  Expanded(
                                      flex: 5,
                                      child: Stack(
                                        children: [
                                          Container(
                                              height:
                                                  getProportionateScreenHeight(
                                                      115),
                                              width:
                                                  getProportionateScreenWidth(
                                                      300),
                                              decoration: BoxDecoration(
                                                color: lightColor,
                                                borderRadius: BorderRadius.only(
                                                    topRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50)),
                                                    bottomRight: Radius.circular(
                                                        getProportionateScreenHeight(
                                                            50))),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: <Widget>[
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "FECHA ACEPTADA:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "HORA INICIO:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "HORA FIN:",
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      Container(
                                                        width:
                                                            getProportionateScreenWidth(
                                                                140),
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              // "SERVICIO/PELUQERIA",
                                                              '${snapshot.data.docservicio.data()["subcategoria"].toString().toUpperCase()}/${snapshot.data.docservicio.data()["categoria"].toString().toUpperCase()}',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              // "SERVICIO/PELUQERIA",
                                                              'Cantidad'
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text("TOTAL:",
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'fecha'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'horainicio'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      snapshot.data.documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'POR PAGAR' ||
                                                              snapshot.data
                                                                          .documento
                                                                          .data()[
                                                                      'estado'] ==
                                                                  'EN TRANSCURSO'
                                                          ? Container(
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    snapshot.data
                                                                            .documento
                                                                            .data()[
                                                                        'horafin'],
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            )
                                                          : SizedBox(),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              '${snapshot.data.docservicio.data()["precio"].toString()}'
                                                              r'$',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              snapshot.data
                                                                  .documento
                                                                  .data()[
                                                                      "cantidad"]
                                                                  .toString(),
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                      Container(
                                                        child: Material(
                                                          type: MaterialType
                                                              .transparency,
                                                          child: Text(
                                                              '${snapshot.data.documento.data()["total"].toString()}'
                                                              r' $',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        11),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ))
                                        ],
                                      )),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 4.0, left: 20, right: 20),
                          child: Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Material(
                                    type: MaterialType.transparency,
                                    child: (snapshot.data.documento
                                                .data()['estado'] ==
                                            'PENDIENTE')
                                        ? Text('ACEPTAR/RECHAZAR',
                                            style: TextStyle(
                                              color: primaryColor,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              fontWeight: FontWeight.normal,
                                            ))
                                        : (snapshot.data.documento
                                                    .data()['estado'] ==
                                                'POR PAGAR')
                                            ? Text('ESPERANDO PAGO',
                                                style: TextStyle(
                                                  color: primaryColor,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          11),
                                                  fontWeight: FontWeight.normal,
                                                ))
                                            : Text(
                                                snapshot.data.documento
                                                    .data()['estado'],
                                                style: TextStyle(
                                                  color: primaryColor,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          11),
                                                  fontWeight: FontWeight.normal,
                                                )),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    if (snapshot.data.documento
                                            .data()['estado'] ==
                                        'POR PAGAR') {
                                      FirebaseFirestore.instance
                                          .collection('Usuarios')
                                          .doc(snapshot.data.documento
                                              .data()['idsolicitante'])
                                          .snapshots()
                                          .forEach((element) {
                                        tokenPush = element.data()['token'];
                                        usuariorecibe = element.id;

                                        
                                        
                                      });
                                      FirebaseFirestore.instance
                                          .collection('Servicios')
                                          .doc(snapshot.data.documento
                                              .data()['idservicio'])
                                          .snapshots()
                                          .forEach((element) {
                                        _alerta(
                                            context,
                                            'Esta seguro que desea cancelar la solicitud, recuerda una vez la canceles recibiras la multa de ' +
                                                convert.format(double.parse(
                                                    element
                                                        .data()['precio']
                                                        .toString())),
                                            true,
                                            snapshot.data.documento.id,
                                            tokenPush,
                                            element
                                                .data()['precio']
                                                .toString());
                                 
                    
                                      });
                                    } else if (snapshot.data.documento
                                            .data()['estado'] ==
                                        'PENDIENTE') {
                                      _alertaPendiente(
                                          context,
                                          'Esta seguro que desea rechazar la solicitud',
                                          true,
                                          snapshot.data.documento.id);
                                    } else if (snapshot.data.documento
                                            .data()['estado'] ==
                                        'EN TRANSCURSO') {
                                      FirebaseFirestore.instance
                                          .collection('Servicios')
                                          .doc(snapshot.data.documento
                                              .data()['idservicio'])
                                          .snapshots()
                                          .forEach((element) {
                                        _alerta(
                                            context,
                                            'Esta seguro que desea cancelar la solicitud, recuerda una vez la canceles recibiras la multa de ' +
                                                convert.format(double.parse(
                                                    element
                                                        .data()['precio']
                                                        .toString())),
                                            true,
                                            snapshot.data.documento.id,
                                            tokenPush,
                                            element
                                                .data()['precio']
                                                .toString());
                                                
                                      });
                                    }
                                  },
                                  child: (snapshot.data.documento
                                                  .data()['estado'] ==
                                              'EN PROCESO' ||
                                          snapshot.data.documento
                                                  .data()['estado'] ==
                                              'PENDIENTE' ||
                                          snapshot.data.documento
                                                  .data()['estado'] ==
                                              'POR PAGAR' ||
                                          snapshot.data.documento
                                                  .data()['estado'] ==
                                              'EN TRANSCURSO')
                                      ? Container(
                                          alignment: Alignment.bottomRight,
                                          child: Material(
                                            type: MaterialType.transparency,
                                            child: Text("CANCELAR SOLICITUD",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          11),
                                                  fontWeight: FontWeight.normal,
                                                )),
                                          ),
                                        )
                                      : SizedBox(),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    )),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    );
  }

  Future<void> sendNotification(subject, title, idToken) async {
    print(idToken);
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {"body": subject, "title": title},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": idToken
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }

  void _alertaPendiente(
      BuildContext context, String text, bool cancelado, String id) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (cancelado == true) {
                                  FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(id)
                                      .update({'estadosolicitud': false}).then(
                                          (value) {
                                    FirebaseFirestore.instance
                                        .collection('Notificaciones')
                                        .add({
                                      'idreceptor': idUsurioRecibe,
                                      'idemisor': prefs.id,
                                      'date': date,
                                      'accion': 'solicitud',
                                      'notificacion':
                                          'Tu solicitud fue cancelada.',
                                    });
                                    sendNotification(
                                        'Tu solicitud fue cancelada.',
                                        'Tow Look',
                                        tokenPush);
                                  });
                                  Navigator.of(context).pop();
                                } else {
                                  Navigator.of(context).pop();
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alerta(BuildContext context, String text, bool cancelado, String id,
      token, precio) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (cancelado == true) {
                                  FirebaseFirestore.instance
                                      .collection('Solicitudes')
                                      .doc(id)
                                      .update({'estadosolicitud': false}).then(
                                          (value) {
                                    FirebaseFirestore.instance
                                        .collection('Pago_multas')
                                        .add({
                                      'fecha': DateTime.now().year.toString() +
                                          "-" +
                                          DateTime.now().month.toString() +
                                          "-" +
                                          DateTime.now().day.toString(),

                                      'token':prefs.usuario['token'],
                                      'forma_pago': "",
                                      'id_paymentez': '',
                                      'id_transferencia': '',
                                      'fotografia_transferencia': '',
                                      'motivo': 'Cancelación de Solicitud',
                                      'negado': '',
                                      'pago': '',
                                      'id_usuario': prefs.id,
                                      'id_solicitud': idSolicitud,
                                      'id_user_action': "",
                                      'status': true,
                                      'fecha_cancela': '',
                                      'total': precio,
                                    }).then((value) {
                                      FirebaseFirestore.instance
                                          .collection('Notificaciones')
                                          .add({
                                        'idreceptor': idUsurioRecibe,
                                        'idemisor': prefs.id,
                                        'date': date,
                                        'accion': 'solicitud',
                                        'notificacion':
                                            'Tu solicitud fue cancelada.',
                                      });
                                    });
                                  });
                                  sendNotification(
                                      'Tu solicitud fue cancelada.',
                                      'Tow Look',
                                      tokenPush);

                                  Navigator.of(context).pop();
                                } else {
                                  Navigator.of(context).pop();
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}


