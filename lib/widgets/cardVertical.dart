import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class CardVertical extends StatefulWidget {
  final QueryDocumentSnapshot  documentSnapshot;
  CardVertical({Key key, this.documentSnapshot}) : super(key: key);

  @override
  _CardVerticalState createState() => _CardVerticalState();
}

class _CardVerticalState extends State<CardVertical> {

  List listanuneva=[];
  Future _listaimagenes(idservicio) async {
   List lista=[]; 
  await FirebaseFirestore.instance.collection("ImagenesServicios").where('id_servicio', isEqualTo:idservicio ).get().then((querySnapshot) {
   querySnapshot.docs.forEach((result) {
      print(result.data());
      var list=result.data().values.toList();
      list.remove(idservicio);
      String finalStr = list.reduce((value, element) {
        return value + element;
      });
      print(finalStr);
     lista.add(finalStr);
    });
    print('$idservicio $lista');

  });
  return lista;
  
}


  Future<Informacion> obtener(String id, String idservicio, QueryDocumentSnapshot documento) async {
     
      var data1 = await FirebaseFirestore.instance
              .collection('Usuarios')
              .doc(id)
              .get();
     
   listanuneva= await _listaimagenes(idservicio);


    print('listanueva $listanuneva');
    var informacionusuario;
    informacionusuario=Informacion(doc: data1,fotos:listanuneva, documento: documento );

      

     return informacionusuario;
    }

  @override
  Widget build(BuildContext context) {
    return  FutureBuilder(
      future:obtener(widget.documentSnapshot.data()["id_usuario"],widget.documentSnapshot.id,widget.documentSnapshot) ,
        builder: (BuildContext context, AsyncSnapshot<Informacion>snapshot) {
          
        //  print('snapshot pasar $snapshotpasar');
          if(snapshot.hasData){
        
          
            print('im primir ${snapshot.data.fotos.length}');
             return 
    
    GestureDetector(
          onTap: (){
            print('hola erick');
            print(widget.documentSnapshot.data()["subcategoria"]);
            print('snapshot ${snapshot.data}');
             if(snapshot.data.documento.data()["subcategoria"]=='producto'){
                    // print(snapshot.data.fotos);
                     Navigator.pushReplacement(
                      context,
                      PageTransition(
                      type:PageTransitionType.fade,
                      child: DentroProducto(snapshot:snapshot.data, mostrarmas: false,)));
                 }else{
                   Navigator.pushReplacement(
                      context,
                      PageTransition(
                      type:PageTransitionType.fade,
                      child: DentroServicio(snapshot:snapshot.data, mostrarmas: false,)));
                    //  print(snapshot.data.fotos);
                 }
          },
          child: Padding(
          padding: EdgeInsets.all(getProportionateScreenHeight(16)),
          child: Container(
            width: 150,
            height: 200,
            child: Material(
                color: Colors.transparent,
                child: Stack(
                  children: [
                    Container(
                      child: Container(
                        height: 200,
                        width: 150,
                        decoration: BoxDecoration(
                          color: lightColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                    ),
                    Column(
                      children: <Widget>[
                        Expanded(
                          flex: 3,
                          child: Stack(
                            children: [
                              Container(
                                height: 150,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: NetworkImage(
                                          snapshot.data.fotos[0]),
                                      fit: BoxFit.fill,
                                    ),
                                    borderRadius: BorderRadius.circular(20.0)),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                            flex: 1,
                            child: Stack(
                              children: [
                                Container(
                                  height: getProportionateScreenHeight(150),
                                  decoration: BoxDecoration(
                                    color: lightColor,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 8.0, top: 4),
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.topLeft,
                                        width: 150,
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Text('${snapshot.data.documento.data()["subcategoria"].toString().toUpperCase()} / ${snapshot.data.documento.data()["categoria"].toString().toUpperCase()} ',
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        10),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        width: 150,
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Text('${snapshot.data.documento.data()["precio"]}' r'$',
                                              style: TextStyle(
                                                color: textColorClaro,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        12),
                                                fontWeight: FontWeight.bold,
                                              )),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )),
                      ],
                    ),
                  ],
                )),
          )),
    );
     }
       return SizedBox();
        }
    );
  }
}

