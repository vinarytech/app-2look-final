import 'package:flutter/material.dart';

import '../src/Clientes/Home/filtros.dart';

class IconoButton extends StatefulWidget {
  @override
  _IconoButtonState createState() => new _IconoButtonState();
}

class _IconoButtonState extends State<IconoButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 20,
        height: 20,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('lib/assets/images/busqueda/filtros.png'))),
        child: FlatButton(
          onPressed: () {
            Navigator.of(context).pushReplacement(
              PageRouteBuilder(
                  pageBuilder: (context, animation, secondaryAnimation) {
                return Filtros();
              }),
            );
          },
          child: null,
        ),
      ),
    );
  }
}
