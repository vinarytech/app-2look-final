import 'package:flutter/material.dart';
import 'package:tl_app/src/Clientes/Reservas/enProceso.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';

import 'package:tl_app/src/Clientes/Reservas/historial.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/pest1Prof.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/pest2Prof.dart';

import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class TextLineButtonProf extends StatefulWidget {
  final String widgetText1;
  final String widgetText2;

  TextLineButtonProf({Key key, this.widgetText1, this.widgetText2})
      : super(key: key);

  @override
  _TextLineButtonProfState createState() => _TextLineButtonProfState();
}

class _TextLineButtonProfState extends State<TextLineButtonProf> {
  var colorBoton1 = primaryColor;
  final prefs = new PreferenciasUsuario();
  var colorBoton2 = textColorMedio;
  String paquete1 = "";
  String paquete2 = "";
  String paquete3 = "";
  String prueba = "";
  bool viewVisible1 = true;

  bool viewVisible2 = false;

  void showWidget1() {
    setState(() {
      viewVisible1 = true;
      viewVisible2 = false;
    });
  }

  void showWidget2() {
    setState(() {
      viewVisible2 = true;
      viewVisible1 = false;
    });
  }

  @override
  void initState() {
    FirebaseFirestore.instance
        .collection("Paquetes")
        .where("id_usuario", isEqualTo: prefs.id)
        .get()
        .then((value) {
      print(value.docs.first.data()['paquete1']);
      paquete1 = value.docs.first.data()["paquete1"];
      paquete2 = value.docs.first.data()["paquete2"];
      paquete3 = value.docs.first.data()["paquete3"];
      prueba = value.docs.first.data()["prueba"];
      setState(() {});
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      alignment: FractionalOffset.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (colorBoton1 == textColorMedio) {
                      colorBoton1 = primaryColor;
                      colorBoton2 = textColorMedio;
                    }
                    showWidget1();
                  });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    children: <Widget>[
                      Text(widget.widgetText1,
                          style: TextStyle(
                            color: colorBoton1,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.normal,
                          )),
                      Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width / 2.25,
                          color: colorBoton1),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (colorBoton2 == textColorMedio) {
                      colorBoton2 = primaryColor;
                      colorBoton1 = textColorMedio;
                    }
                    showWidget2();
                  });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    children: <Widget>[
                      Text(widget.widgetText2,
                          style: TextStyle(
                            color: colorBoton2,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.normal,
                          )),
                      Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width / 2.25,
                          color: colorBoton2),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Stack(
            children: <Widget>[
              Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: viewVisible1,
                  child: EnProcesoProf()),
              Visibility(
                maintainSize: true,
                maintainAnimation: true,
                maintainState: true,
                visible: viewVisible2,
                child: (paquete1 == "" &&
                        paquete2 == "" &&
                        paquete3 == "" &&
                        prueba == "")
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Text(
                              "No tienes paquetes contratados por lo cual no podras ver el historial de tus reservas finalizadas",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          ButtonGray(
                            textoBoton: "IR A PAQUETES",
                            bFuncion: () {
                              setState(() {
                                pageProf = 0;
                              });
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      child: HomeProf(),
                                      type: PageTransitionType.fade));
                            },
                            anchoBoton: getProportionateScreenHeight(330),
                            largoBoton: getProportionateScreenWidth(27),
                            colorTextoBoton: textColorClaro,
                            opacityBoton: 1,
                            sizeTextBoton: getProportionateScreenWidth(15),
                            weightBoton: FontWeight.bold,
                            color1Boton: primaryColor,
                            color2Boton: primaryColor,
                          ),
                        ],
                      )
                    : (paquete1 != "" &&
                            paquete2 == "" &&
                            paquete3 == "" &&
                            prueba == "")
                        ? Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "Tu paquete  actual de \$5.00 no te permite ver el historial de tus reservas finalizadas",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(17),
                                    fontWeight: FontWeight.normal,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              ButtonGray(
                                textoBoton: "IR A PAQUETES",
                                bFuncion: () {
                                  setState(() {
                                    pageProf = 0;
                                  });
                                  Navigator.pushReplacement(
                                      context,
                                      PageTransition(
                                          child: HomeProf(),
                                          type: PageTransitionType.fade));
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ],
                          )
                        : Pest2Prof(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
