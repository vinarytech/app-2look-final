import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tl_app/widgets/constants.dart';

class CheckBoxWidget extends StatefulWidget {
  final Function onpresscheck;
  CheckBoxWidget({Key key, @required this.onpresscheck}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool _value1 = false;

  //we omitted the brackets '{}' and are using fat arrow '=>' instead, this is dart syntax
  void _value1Changed(bool value) {
    setState(() => _value1 = value);
    widget.onpresscheck(_value1);
  } 
    _launchURL() async {
  const url = 'https://2look.app/index.php/politica-privacidad/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Color(0xff707070),
          ),
          child: Container(
            width: 20,
            height: 20,
            child: new Checkbox(
              value: _value1,
              onChanged: _value1Changed,
              activeColor: Colors.blue,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        GestureDetector(
          onTap: (){
            _launchURL();
          },
                  child: new Text(
            'Ver términos y condiciones',
            style: TextStyle(color: primaryColor),
          ),
        ),
      ],
    );
  }
}
