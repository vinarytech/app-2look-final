import 'package:flutter/material.dart';

import '../constants.dart';
import '../size_config.dart';

class Total extends StatefulWidget {
  Total({Key key}) : super(key: key);

  @override
  _TotalState createState() => _TotalState();
}

class _TotalState extends State<Total> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 28.0),
      child: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "TOTAL",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(11),
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  "\$23.50",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(11),
                    fontWeight: FontWeight.normal,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
