import 'package:flutter/material.dart';

import '../constants.dart';
import '../size_config.dart';

class TarjetasInfo extends StatefulWidget {
  final String titulo, descripcion, valor;
  TarjetasInfo({Key key, this.titulo, this.descripcion, this.valor})
      : super(key: key);

  @override
  _TarjetasInfoState createState() => _TarjetasInfoState();
}

class _TarjetasInfoState extends State<TarjetasInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 28.0),
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.titulo,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    Text(
                      widget.descripcion,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.normal,
                      ),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      widget.valor,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(11),
                        fontWeight: FontWeight.normal,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Container(
            child: Stack(
              children: <Widget>[
                Container(
                  width: getProportionateScreenWidth(370),
                  height: getProportionateScreenHeight(1.8),
                  color: const Color(0x1affffff),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: getProportionateScreenHeight(10),
        ),
      ],
    );
  }
}
