import 'package:flutter/material.dart';

import '../constants.dart';
import '../radiobutton.dart';
import '../size_config.dart';

class TipoPago extends StatefulWidget {
  TipoPago({Key key}) : super(key: key);

  @override
  _TipoPagoState createState() => _TipoPagoState();
}

class _TipoPagoState extends State<TipoPago> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 38.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text("METODO DE PAGO",
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Material(
                    type: MaterialType.transparency,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20),
                          ),
                          child: Text("Efectivo",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: FontWeight.normal,
                              )),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 35.0),
                    child: RadioButton(),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Material(
                    type: MaterialType.transparency,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20),
                          ),
                          child: Text("Tarjeta",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: FontWeight.normal,
                              )),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 35.0),
                    child: RadioButton(),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
