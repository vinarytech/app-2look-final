import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class DropDownCustomed extends StatefulWidget {
  final String text;
  DropDownCustomed({Key key, @required this.text}) : super(key: key);
  @override
  _DropDownCustomedState createState() => _DropDownCustomedState();
}

class _DropDownCustomedState extends State<DropDownCustomed> {
  GlobalKey actionKey;
  bool isDropdownOpened = false;
  double height, width, xPosition, yPosition;
  OverlayEntry floatingDropdown;
  double radio1 = 25;
  double radio2 = 25;
  double radio3 = 25;
  double radio4 = 25;
  @override
  void initState() {
    super.initState();
    actionKey = LabeledGlobalKey(widget.text);
  }

  void findDropdownData() {
    RenderBox renderBox = actionKey.currentContext.findRenderObject();
    height = renderBox.size.height;
    width = renderBox.size.width;
    Offset offset = renderBox.localToGlobal(Offset.zero);
    xPosition = offset.dx;
    yPosition = offset.dy;
  }

  OverlayEntry createFloatingDropdown() {
    return OverlayEntry(builder: (context) {
      return Positioned(
          left: xPosition,
          width: width,
          top: yPosition + height,
          height: 4 * height + 40,
          child: DropDown(
            itemHeight: height,
          ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 32,
      child: Padding(
        padding: const EdgeInsets.only(left: 12.0, right: 12.0),
        child: GestureDetector(
          key: actionKey,
          onTap: () {
            setState(() {
              if (isDropdownOpened) {
                floatingDropdown.remove();
                radio1 = 25;
                radio2 = 25;
                radio3 = 25;
                radio4 = 25;
              } else {
                findDropdownData();
                radio1 = 25;
                radio2 = 25;
                radio3 = 0;
                radio4 = 0;
                floatingDropdown = createFloatingDropdown();
                Overlay.of(context).insert(floatingDropdown);
              }

              isDropdownOpened = !isDropdownOpened;
            });
          },
          child: Container(
              //height: 80,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(radio1),
                    topLeft: Radius.circular(radio2),
                    bottomLeft: Radius.circular(radio3),
                    bottomRight: Radius.circular(radio4)),
                boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
                color: decolarado
              ),
              padding: const EdgeInsets.symmetric(
                horizontal: 12,
              ),
              child: Row(
                children: <Widget>[
                  Text(widget.text,
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(13),
                        fontWeight: FontWeight.normal,
                      )),
                  Spacer(),
                  Icon(
                    Icons.arrow_drop_down,
                    color: textColorClaro,
                  )
                ],
              )),
        ),
      ),
    );
  }
}

class DropDown extends StatelessWidget {
  final double itemHeight;
  const DropDown({Key key, this.itemHeight}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 4 * itemHeight,
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(25)),
          child: Column(
            children: <Widget>[
              DropDownItem(
                text: "Peluqueria",
              ),
              DropDownItem(
                text: "Spa",
              ),
              DropDownItem(
                text: "Spa",
              ),
              DropDownItem.last(
                text: "Spa",
              ),
            ],
          ),
        )
      ],
    );
  }
}

class DropDownItem extends StatelessWidget {
  final String text;
  final bool isSelected;
  final bool isFirstItem;
  final bool isLastItem;

  const DropDownItem(
      {Key key,
      this.text,
      this.isSelected,
      this.isFirstItem = false,
      this.isLastItem = false})
      : super(key: key);

  factory DropDownItem.first({String text, bool isSelected}) {
    return DropDownItem(
      text: text,
      isSelected: isSelected,
      isFirstItem: true,
    );
  }
  factory DropDownItem.last({String text, bool isSelected}) {
    return DropDownItem(
      text: text,
      isSelected: isSelected,
      isLastItem: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        //SizedBox(height: 12, child: Container(color: decolarado)),
        Container(
            height: 32,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.vertical(
                top: isFirstItem ? Radius.circular(20) : Radius.zero,
                bottom: isLastItem ? Radius.circular(20) : Radius.zero,
              ),
              boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
              color: decolarado
            ),
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              children: <Widget>[
                Material(
                    type: MaterialType.transparency,
                    child: Text(text.toUpperCase(),
                        style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.normal,
                        ))),
              ],
            )),
      ],
    );
  }
}
