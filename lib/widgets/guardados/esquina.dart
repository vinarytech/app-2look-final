import 'dart:ui';

import 'package:adobe_xd/pinned.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/constants.dart';

import '../size_config.dart';

class Esquina extends StatefulWidget {
  final idServicio;
  bool valor;
  Esquina({Key key, this.valor, this.idServicio}) : super(key: key);

  @override
  _EsquinaState createState() => _EsquinaState();
}

class _EsquinaState extends State<Esquina> {
  List listaFavoritos = [];

  final prefs = PreferenciasUsuario();
  String id;
  String idServicio;
  String idUsuario;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          setState(() {
            if (prefs.id == null) {
              _alertaFinal(context,
                  "Por favor inicia sesión para poder guardar este producto o servicio a tus favoritos");
            } else {
              widget.valor = !widget.valor;
            }
          });

          if (widget.valor) {
            FirebaseFirestore.instance
                .collection('Favoritos')
                .add({'idUsuario': prefs.id, 'idServicio': widget.idServicio});
          } else {
            FirebaseFirestore.instance
                .collection('Favoritos')
                .where('idUsuario', isEqualTo: prefs.id)
                .get()
                .then((value) => value.docs.forEach((element) {
                      if (element.data()['idServicio'] == widget.idServicio) {
                        FirebaseFirestore.instance
                            .collection('Favoritos')
                            .doc(element.id)
                            .delete();
                      }
                    }));
          }
        },
        child: Stack(
          children: <Widget>[
            Container(
                child: (!widget.valor)
                    ? Icon(Icons.bookmark_border_outlined, color: Colors.white)
                    : Icon(Icons.bookmark_sharp, color: Colors.white),
                width: getProportionateScreenWidth(40.8),
                height: getProportionateScreenHeight(40.8),
                decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                  color: Color(0Xff707070),
                )),
          ],
        ),
      ),
    );
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}

const String _svg_u87zmt =
    '<svg viewBox="376.7 25.4 13.7 19.3" ><path transform="translate(0.23, 0.22)" d="M 376.4559936523438 35.48899841308594 C 376.4559936523438 32.92200088500977 376.4909973144531 30.35400009155273 376.4419860839844 27.78700065612793 C 376.4140014648438 26.3129997253418 377.2699890136719 25.21199989318848 378.9739990234375 25.17499923706055 C 381.9010009765625 25.11199951171875 384.8309936523438 25.13299942016602 387.760009765625 25.16699981689453 C 389.2080078125 25.18400001525879 390.1119995117188 26.29199981689453 390.1759948730469 28.01799964904785 C 390.2000122070313 28.66799926757813 390.1820068359375 29.31999969482422 390.1820068359375 29.96999931335449 C 390.1820068359375 34.36399841308594 390.1820068359375 38.75699996948242 390.1809997558594 43.1510009765625 C 390.1809997558594 43.33100128173828 390.1929931640625 43.51499938964844 390.1619873046875 43.69100189208984 C 390.0440063476563 44.36800003051758 389.5889892578125 44.5880012512207 388.9859924316406 44.25899887084961 C 387.2590026855469 43.31800079345703 385.531005859375 42.37900161743164 383.8139953613281 41.41899871826172 C 383.4949951171875 41.24100112915039 383.2479858398438 41.22900009155273 382.9230041503906 41.40800094604492 C 381.1849975585938 42.36600112915039 379.4360046386719 43.30300140380859 377.68798828125 44.24399948120117 C 376.9320068359375 44.6510009765625 376.4590148925781 44.37400054931641 376.4580078125 43.51599884033203 C 376.4530029296875 40.84099960327148 376.4559936523438 38.16500091552734 376.4559936523438 35.48899841308594 Z M 388.7139892578125 42.31000137329102 C 388.7269897460938 42.05099868774414 388.7409973144531 41.90800094604492 388.7409973144531 41.76599884033203 C 388.7420043945313 37.2239990234375 388.7439880371094 32.68299865722656 388.7409973144531 28.14200019836426 C 388.739990234375 27.07500076293945 388.385986328125 26.73299980163574 387.2940063476563 26.73200035095215 C 385.5029907226563 26.72900009155273 383.7120056152344 26.73100090026855 381.9209899902344 26.73100090026855 C 381.0159912109375 26.73100090026855 380.1109924316406 26.72299957275391 379.2070007324219 26.73399925231934 C 378.1210021972656 26.74699974060059 377.8240051269531 27.04999923706055 377.8240051269531 28.11199951171875 C 377.8240051269531 32.61700057983398 377.8240051269531 37.12200164794922 377.8240051269531 41.62699890136719 L 377.8240051269531 42.32600021362305 C 378.0780029296875 42.20800018310547 378.2420043945313 42.14099884033203 378.39599609375 42.05799865722656 C 379.8760070800781 41.26499938964844 381.3599853515625 40.47800064086914 382.8299865722656 39.66799926757813 C 383.2040100097656 39.46200180053711 383.5159912109375 39.45299911499023 383.8900146484375 39.66799926757813 C 384.9089965820313 40.25099945068359 385.9450073242188 40.80199813842773 386.9750061035156 41.3650016784668 C 387.5249938964844 41.66600036621094 388.0769958496094 41.9640007019043 388.7139892578125 42.31000137329102 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';

class Favoritos {
  String idServicio;
  String idUsuario;
  String estado;
  String id;
  Favoritos({this.idServicio, this.idUsuario, this.id, this.estado});
}
