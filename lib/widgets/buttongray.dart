import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:flutter_svg/avd.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'constants.dart';

class ButtonGray extends StatefulWidget {
  final String textoBoton;
  final Function bFuncion;
  final double anchoBoton;
  final double largoBoton;
  final Color colorTextoBoton;
  final double opacityBoton;
  final double sizeTextBoton;
  final FontWeight weightBoton;
  final Color color1Boton;
  final Color color2Boton;

  ButtonGray(
      {this.textoBoton,
      this.bFuncion,
      this.anchoBoton,
      this.largoBoton,
      this.colorTextoBoton,
      this.opacityBoton,
      this.sizeTextBoton,
      this.weightBoton,
      this.color1Boton,
      this.color2Boton});

  @override
  _ButtonGrayState createState() => new _ButtonGrayState();
}

class _ButtonGrayState extends State<ButtonGray> {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          height: widget.largoBoton,
          width: widget.anchoBoton,
          padding: EdgeInsets.all(0.5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(27)),
              boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
              color: widget.color1Boton),
          child: Material(
            type: MaterialType.transparency,
            elevation: 6.0,
            color: Colors.transparent,
            shadowColor: Colors.grey[50],
            child: InkWell(
              splashColor: Colors.white30,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              onTap: () {
                widget.bFuncion();
              },
              child: Center(
                  child: Align(
                alignment: Alignment.bottomRight,
                child: Center(
                  child: Text(widget.textoBoton,
                      style: TextStyle(
                          color: textColorClaro,
                          fontSize: getProportionateScreenWidth(14),
                          fontWeight: FontWeight.normal)),
                ),
              )),
            ),
          ),
        )
      ],
    );
  }
}

class ButtonGrayRedes extends StatefulWidget {
  final String textoBoton;
  final Function bFuncion;
  final double anchoBoton;
  final double largoBoton;
  final Color colorTextoBoton;
  final String imagen;
  final double opacityBoton;
  final double sizeTextBoton;
  final FontWeight weightBoton;
  final Color color1Boton;
  final Color color2Boton;

  ButtonGrayRedes(
      {this.textoBoton,
      this.bFuncion,
      this.anchoBoton,
      this.largoBoton,
      this.colorTextoBoton,
      this.opacityBoton,
      this.sizeTextBoton,
      this.imagen,
      this.weightBoton,
      this.color1Boton,
      this.color2Boton});

  @override
  _ButtonGrayRedesState createState() => new _ButtonGrayRedesState();
}

class _ButtonGrayRedesState extends State<ButtonGrayRedes> {
  @override
  Widget build(BuildContext context) {
    return new Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          height: widget.largoBoton,
          width: widget.anchoBoton,
          padding: EdgeInsets.all(0.5),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(27)),
              boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
              color: widget.color1Boton),
          child: Material(
            type: MaterialType.transparency,
            elevation: 6.0,
            color: Colors.transparent,
            shadowColor: Colors.grey[50],
            child: InkWell(
              splashColor: Colors.white30,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              onTap: () {
                widget.bFuncion();
              },
              child: Center(
                  child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenHeight(45),
                    vertical: getProportionateScreenHeight(10)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(widget.imagen),
                    Text(widget.textoBoton,
                        style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(14),
                            fontWeight: FontWeight.normal))
                  ],
                ),
              )),
            ),
          ),
        )
      ],
    );
  }
}
