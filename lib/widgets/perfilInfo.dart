import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PerfilInfo extends StatelessWidget {
  final String texto;
  final String svg;

  PerfilInfo({Key key, this.texto, this.svg}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      color: Color.fromRGBO(255, 255, 255, 0.19).withOpacity(.0),
      child: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(0.0, 31.1),
            child: Container(
              width: 253.1,
              height: 1.6,
              color: const Color(0x1affffff),
            ),
          ),
          Transform.translate(
            offset: Offset(14.2, 7.6),
            child: SizedBox(
              width: 215.0,
              height: 19.0,
              child: Stack(
                children: <Widget>[
                  Text(
                    texto,
                    style: TextStyle(
                      fontFamily: 'Poppins-Light',
                      fontSize: 13,
                      color: const Color(0xffffffff),
                      height: 1.0517760790311372,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  Transform.translate(
                    offset: Offset(200.2, 0),
                    child: SvgPicture.string(
                      svg,
                      allowDrawingOutsideViewBox: true,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
