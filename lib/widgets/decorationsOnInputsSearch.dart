import 'dart:ui';

import 'package:flutter/material.dart';

class DecorationsOnInputsSearch {
  static TextStyle textoInput() {
    return TextStyle(
      fontSize: 11,
      color: Colors.white,
      fontWeight: FontWeight.normal,
      fontFamily: "AgencyFB",
    );
  }

  static InputDecoration decorationInput(String labelText, String hinText) {
    return InputDecoration(
      enabledBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(27)),
        borderSide:
            const BorderSide(color: Color.fromRGBO(57, 59, 63, 1), width: 5.0),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2.0),
      ),
      errorStyle: TextStyle(
        color: Colors.white,
      ),
      prefixIcon: Padding(
        padding: EdgeInsets.only(top: 0), // add padding to adjust icon
        child: Icon(
          Icons.search,
          color: Colors.white,
        ),
      ),
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2.0),
      ),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2.0),
      ),
      labelText: labelText,
      labelStyle: TextStyle(
        fontSize: 11,
        color: Colors.white70,
        fontWeight: FontWeight.w600,
        fontFamily: "AgencyFB",
      ),
      contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
      hintText: hinText,
      hintStyle: TextStyle(
        fontSize: 11,
        color: Colors.white38,
        fontWeight: FontWeight.normal,
        fontFamily: "AgencyFB",
      ),
    );
  }
}
