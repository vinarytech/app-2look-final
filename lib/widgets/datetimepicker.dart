import 'package:flutter/material.dart';

import 'package:tl_app/services/servicioService.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class DateTimePickerCustom extends StatefulWidget {
  final Color colorFondo;
  final String periodo;
  final String texto;
  final Function retornoInicio;
  final Function retornoFin;
  DateTimePickerCustom({this.colorFondo, this.periodo,  this.retornoInicio, this.retornoFin, this.texto});

  @override
  _DateTimePickerCustomState createState() => _DateTimePickerCustomState();
}

class _DateTimePickerCustomState extends State<DateTimePickerCustom> {
  TimeOfDay time;
  @override
  void initState() {
    time = TimeOfDay.now();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 12.0, left: 12.0),
      child: Container(
        height: 30,
        width: getProportionateScreenWidth(173),
        decoration: BoxDecoration(
          color: widget.colorFondo,
          borderRadius: BorderRadius.circular(20),
        ),
        child: FlatButton(
            onPressed: () {
              _pickTime();
            },
            child: Text(
              widget.texto,
              style: TextStyle(
                color: textColorClaro,
                fontSize: getProportionateScreenWidth(14),
                fontWeight: FontWeight.normal,
              ),
            )),
      ),
    );
  }

  void _pickTime() async {
    TimeOfDay t = await showTimePicker(
      context: context,
      initialTime: time,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );

    if (t != null) {
      setState(() {
        time = t;
        print(time.format(context).toString());
        if (widget.periodo == "Inicio") {
        widget.retornoInicio(time.format(context).toString());
          // servicioService.cambiarHoraInicio(time.format(context).toString());
        } else {
        widget.retornoFin(time.format(context).toString());
          // servicioService.cambiarHoraFin(time.format(context).toString());
        }
      });
    }
  }
}
