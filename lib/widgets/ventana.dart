import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'buttongray.dart';
import 'constants.dart';

class Ventana extends StatefulWidget {
  Ventana({Key key}) : super(key: key);

  @override
  _VentanaState createState() => _VentanaState();
}

class _VentanaState extends State<Ventana> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(getProportionateScreenHeight(14)),
        child: Container(
            alignment: Alignment.center,
            child: Container(
              height: 350,
              width: 300,
              child: Material(
                  color: Colors.transparent,
                  elevation: 5,
                  borderRadius:
                      BorderRadius.circular(getProportionateScreenHeight(20.0)),
                  shadowColor: Colors.white70,
                  child: Stack(
                    children: [
                      Container(
                        alignment: Alignment.center,
                        child: Container(
                          height: 350,
                          width: 300,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 25.0, right: 25.0),
                            child: Container(
                              alignment: Alignment.topCenter,
                              child: Material(
                                type: MaterialType.transparency,
                                child: Text(
                                    "Listo, tu solicitud esta siendo atendida, te notificaremos cuando el profesional apruebe tu pedido",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: textColorOscuro,
                                      fontSize: getProportionateScreenWidth(18),
                                      fontWeight: FontWeight.normal,
                                    )),
                              ),
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(
                                  left: 25.0, right: 25.0, top: 25),
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                      height: 33,
                                      width: 300,
                                      child: Material(
                                        color: Colors.transparent,
                                        elevation: 5,
                                        borderRadius: BorderRadius.circular(
                                            getProportionateScreenHeight(20.0)),
                                        shadowColor: Colors.white70,
                                      )),
                                  Container(
                                    child: ButtonGray(
                                      textoBoton: "SOLICITAR",
                                      bFuncion: () {},
                                      anchoBoton:
                                          getProportionateScreenHeight(350),
                                      largoBoton:
                                          getProportionateScreenWidth(28),
                                      colorTextoBoton: textColorClaro,
                                      opacityBoton: 1,
                                      sizeTextBoton:
                                          getProportionateScreenWidth(11),
                                      weightBoton: FontWeight.w400,
                                      color1Boton: primaryColor,
                                      color2Boton: primaryColor,
                                    ),
                                  ),
                                ],
                              ))
                        ],
                      )
                    ],
                  )),
            )));
  }
}
