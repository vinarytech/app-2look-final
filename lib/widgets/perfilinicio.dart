import 'package:flutter/material.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/size_config.dart';

class PerfilInicio extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  final VoidCallback fotoPerfil;
  PerfilInicio({
    Key key,
    this.fotoPerfil,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: getProportionateScreenWidth(269),
      height: getProportionateScreenHeight(86),
      child: Stack(
        children: <Widget>[
          Transform.translate(
            offset: Offset(100.8, 3.0),
            child: Text(
              prefs.usuario['nombre'],
              style: TextStyle(
                fontFamily: 'Poppins-Light',
                fontSize: 16.573440551757812,
                color: const Color(0xffffffff),
                height: 1.2000000920677214,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(100.8, 27.9),
            child: Text(
              prefs.usuario['correo'],
              style: TextStyle(
                fontFamily: 'Poppins-Light',
                fontSize: 16.573440551757812,
                color: const Color(0xffffffff),
                height: 1.2000000920677214,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Transform.translate(
            offset: Offset(100.8, 55.6),
            child: Text(
              prefs.usuario['celular'],
              style: TextStyle(
                fontFamily: 'Poppins-Light',
                fontSize: 16.573440551757812,
                color: const Color(0xffffffff),
                height: 1.2000000920677214,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          GestureDetector(
            onTap: () => fotoPerfil?.call(),
            child: SizedBox(
              width: 86.0,
              height: 86.0,
              child: Stack(
                children: <Widget>[
                  SizedBox(
                    width: 86.0,
                    height: 86.0,
                    child: Stack(
                      children: <Widget>[
                        Transform.translate(
                          offset: Offset(-3.9, -12.4),
                          child:
                              // Adobe XD layer: 'z2dBLz' (group)
                              SizedBox(
                            width: 91.0,
                            height: 113.0,
                            child: Stack(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(200),
                                  child: (prefs.usuario['fotografia_perfil'] ==
                                              null ||
                                          prefs.usuario['fotografia_perfil'] ==
                                              '')
                                      ? Image.asset(
                                          'lib/assets/images/perfil/perfil.png')
                                      : Image.network(
                                          prefs.usuario['fotografia_perfil']),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: 85.9,
                          height: 85.9,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(19.53),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
