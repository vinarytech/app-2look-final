import 'dart:collection';
import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Profesionales/HomeProf/homeProf.dart';
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Alertas/alertaCalificacion.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/reportarProblemaP.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/src/Clientes/Problemas/reportarProblema.dart';
import 'package:tl_app/src/mapa_page.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';
import 'package:tl_app/widgets/guardados/esquina.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/metodoCobro.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:loading/loading.dart';
import 'package:url_launcher/url_launcher.dart';

import 'decorationsOnInputs.dart';

class Billetera extends StatefulWidget {
  Billetera({
    Key key,
  }) : super(key: key);

  @override
  _BilleteraState createState() => _BilleteraState();
}

class _BilleteraState extends State<Billetera> {

  final convert = new NumberFormat("#,##0.00", "en_US");
  bool pagoPendiente;
  double suma = 0;
  dynamic infoPagoPedniente;
  List listaCard = [];
  GoogleMapController _controller;
  String paquete1 = "";
  String paquete2 = "";
  String paquete3 = "";
  String prueba = "";


  @override
  void initState() {
    FirebaseFirestore.instance
        .collection("Paquetes")
        .where("id_usuario", isEqualTo: prefs.id)
        .get()
        .then((value) {
      print(value.docs.first.data()['paquete1']);
      paquete1 = value.docs.first.data()["paquete1"];
      paquete2 = value.docs.first.data()["paquete2"];
      paquete3 = value.docs.first.data()["paquete3"];
      prueba = value.docs.first.data()["prueba"];
      setState(() {});
    });

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: 95.0,
                            left: getProportionateScreenWidth(20),
                            right: getProportionateScreenWidth(20)),
                        child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Container(
                              child: Column(
                                children: [
                                  Container(
                                    alignment: Alignment.center,
                                    child: Material(
                                        type: MaterialType.transparency,
                                        child: Text("TU DINERO",
                                            style: TextStyle(
                                              color: textColorClaro,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      18),
                                              fontWeight: FontWeight.bold,
                                            ))),
                                  ),
                                  StreamBuilder(
                                    stream: FirebaseFirestore.instance
                                        .collection('Billetera')
                                        .where('id_usuario',
                                            isEqualTo: prefs.id)
                                        .snapshots(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.hasData) {
                                        suma = 0;
                                        for (var item in snapshot.data.docs) {
                                          if (item.data()['estado'] == true) {
                                            if (item.data()['tipo'] ==
                                                'Acreditación') {
                                              suma += double.parse(
                                                  item.data()['monto']);
                                            } else {
                                              suma -= double.parse(
                                                  item.data()['monto']);
                                            }
                                          }
                                        }
                                        return Container(
                                          alignment: Alignment.center,
                                          child: Material(
                                              type: MaterialType.transparency,
                                              child: Text(
                                                  '\$ ' +
                                                      convert.format(
                                                          double.parse(
                                                              suma.toString())),
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            38),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ))),
                                        );
                                      } else {
                                        return Container(
                                          alignment: Alignment.center,
                                          child: Material(
                                              type: MaterialType.transparency,
                                              child: Text('\$ ' + '0.00',
                                                  style: TextStyle(
                                                    color: textColorClaro,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            38),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ))),
                                        );
                                      }
                                    },
                                  ),
                                   (suma < 0)?Padding(
                                    padding: const EdgeInsets.only(top: 20.0),
                                    child: ButtonGray(
                                      textoBoton: "COBRAR",
                                      bFuncion: () {
                                        Future(() =>
                                                loaderDialogNormal(context))
                                            .then((v) {
                                          FirebaseFirestore.instance
                                              .collection('Billetera')
                                              .where('id_usuario',
                                                  isEqualTo: prefs.id)
                                              .get()
                                              .then((value) {
                                            for (var item in value.docs) {
                                              if (item.data()['estado'] ==
                                                  false) {
                                                setState(() {
                                                  pagoPendiente = false;
                                                infoPagoPedniente = item;
                                                });
                                                break;
                                              } else {
                                                setState(() {
                                                pagoPendiente = true;
                                                });
                                              }
                                            }
                                             if (pagoPendiente) {
                                            FirebaseFirestore.instance
                                                .collection('MetodoCobro')
                                                .where('id_usuario',
                                                    isEqualTo: prefs.id)
                                                .get()
                                                .then((value) {
                                              Navigator.pop(context);

                                              if (value.docs.length > 0) {
                                                _alerta(
                                                    context,
                                                    'Ingresa el valor de retiro, una vez realizado el retiro tranferiremos el dinero a la cuenta ' +
                                                        value.docs[0].data()[
                                                            'numeroCuenta'] +
                                                        ' del ' +
                                                        value.docs[0]
                                                            .data()['banco']);
                                              } else {
                                                Navigator.push(
                                                    context,
                                                    PageTransition(
                                                        child: MetodoCobro(),
                                                        type: PageTransitionType
                                                            .fade));
                                              }
                                            });
                                          } else {
                                            Navigator.pop(context);
                                            _alertaFinal(
                                                context,
                                                'Tienes una solicitud de retiro pendiente de \$' +
                                                    infoPagoPedniente
                                                        .data()['monto'] +
                                                    ' dolares');
                                          }
                                          });
                                         
                                        });
                                      },
                                      anchoBoton:
                                          getProportionateScreenHeight(350),
                                      largoBoton:
                                          getProportionateScreenWidth(28),
                                      colorTextoBoton: textColorClaro,
                                      opacityBoton: 1,
                                      sizeTextBoton:
                                          getProportionateScreenWidth(11),
                                      weightBoton: FontWeight.w400,
                                      color1Boton: primaryColor,
                                      color2Boton: primaryColor,
                                    ),
                                  ):SizedBox(),
                                  
                                  
                                  (paquete1 == "" &&
                        paquete2 == "" &&
                        paquete3 == "" &&
                        prueba == "")
                    ? Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Text(
                              "No tienes paquetes contratados por lo cual no podras ver el historial de tus reservas finalizadas",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          ButtonGray(
                            textoBoton: "IR A PAQUETES",
                            bFuncion: () {
                              setState(() {
                                pageProf = 0;
                              });
                              Navigator.pushReplacement(
                                  context,
                                  PageTransition(
                                      child: HomeProf(),
                                      type: PageTransitionType.fade));
                            },
                            anchoBoton: getProportionateScreenHeight(330),
                            largoBoton: getProportionateScreenWidth(27),
                            colorTextoBoton: textColorClaro,
                            opacityBoton: 1,
                            sizeTextBoton: getProportionateScreenWidth(15),
                            weightBoton: FontWeight.bold,
                            color1Boton: primaryColor,
                            color2Boton: primaryColor,
                          ),
                        ],
                      )
                    : (paquete1 != "" &&
                            paquete2 == "" &&
                            paquete3 == "" &&
                            prueba == "")
                        ? Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "Tu paquete  actual de \$5.00 no te permite ver el historial de tus reservas finalizadas",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(17),
                                    fontWeight: FontWeight.normal,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              ButtonGray(
                                textoBoton: "IR A PAQUETES",
                                bFuncion: () {
                                  setState(() {
                                    pageProf = 0;
                                  });
                                  Navigator.pushReplacement(
                                      context,
                                      PageTransition(
                                          child: HomeProf(),
                                          type: PageTransitionType.fade));
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ],
                          )
                        :
                                  StreamBuilder(
                                      stream: FirebaseFirestore.instance
                                          .collection('Billetera')
                                          .where('id_usuario',
                                              isEqualTo: prefs.id)
                                          .snapshots(),
                                      builder: (context,
                                          AsyncSnapshot<QuerySnapshot>
                                              snapshot) {
                                        if (snapshot.hasData) {
                                          listaCard = [];
                                          for (var item in snapshot.data.docs) {
                                            print(item.data()['monto']);
                                            if (item.data()['estado'] == true) {
                                              listaCard.add(item);
                                            }
                                          }
                                          return ListView.builder(
                                              physics: BouncingScrollPhysics(),
                                              shrinkWrap: true,
                                              itemBuilder: (BuildContext ctxt,
                                                  int index) {
                                                return Padding(
                                                  padding: EdgeInsets.symmetric(
                                                      vertical:
                                                          getProportionateScreenHeight(
                                                              10)),
                                                  child: Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                        flex: 3,
                                                        child: Stack(
                                                          children: [
                                                            Stack(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  height:
                                                                      getProportionateScreenHeight(
                                                                          80),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color:
                                                                        lightColor,
                                                                    borderRadius: BorderRadius.only(
                                                                        topLeft:
                                                                            Radius.circular(getProportionateScreenHeight(
                                                                                50)),
                                                                        bottomLeft:
                                                                            Radius.circular(getProportionateScreenHeight(50))),
                                                                  ),
                                                                  child:
                                                                      Container(
                                                                    child:
                                                                        Center(
                                                                      child: Text(
                                                                          listaCard[index].data()[
                                                                              'tipo'],
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                textColorClaro,
                                                                            fontSize:
                                                                                getProportionateScreenWidth(11),
                                                                            fontWeight:
                                                                                FontWeight.normal,
                                                                          )),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width:
                                                            getProportionateScreenHeight(
                                                                5),
                                                      ),
                                                      Expanded(
                                                          flex: 5,
                                                          child: Stack(
                                                            children: [
                                                              Container(
                                                                  height:
                                                                      getProportionateScreenHeight(
                                                                          80),
                                                                  width:
                                                                      getProportionateScreenWidth(
                                                                          300),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color:
                                                                        lightColor,
                                                                    borderRadius: BorderRadius.only(
                                                                        topRight:
                                                                            Radius.circular(getProportionateScreenHeight(
                                                                                50)),
                                                                        bottomRight:
                                                                            Radius.circular(getProportionateScreenHeight(50))),
                                                                  ),
                                                                  child:
                                                                      Padding(
                                                                    padding: const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10,
                                                                        right:
                                                                            10),
                                                                    child: Row(
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .spaceBetween,
                                                                      children: <
                                                                          Widget>[
                                                                        Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.start,
                                                                          children: <
                                                                              Widget>[
                                                                            Container(
                                                                              width: getProportionateScreenWidth(130),
                                                                              child: Text(listaCard[index].data()['descripcion'] + " el día " + DateFormat.yMMMMd('es').format(listaCard[index].data()['fecha'].toDate()).toString(),
                                                                                  style: TextStyle(
                                                                                    color: textColorClaro,
                                                                                    fontSize: getProportionateScreenWidth(11),
                                                                                    fontWeight: FontWeight.bold,
                                                                                  ),
                                                                                  textAlign: TextAlign.justify),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                        Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.center,
                                                                          children: <
                                                                              Widget>[
                                                                            Text('\$ ' + convert.format(double.parse(listaCard[index].data()['monto'])),
                                                                                style: TextStyle(
                                                                                  color: textColorClaro,
                                                                                  fontSize: getProportionateScreenWidth(11),
                                                                                  fontWeight: FontWeight.bold,
                                                                                )),
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ))
                                                            ],
                                                          )),
                                                    ],
                                                  ),
                                                );
                                              },
                                              itemCount: listaCard.length);
                                        } else {
                                          return CircularProgressIndicator();
                                        }
                                      }),
                                ],
                              ),
                            ))),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: BotonAtras())
                  ],
                ))));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alerta(BuildContext context, String text) {
    final cantidadCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                                  controller: cantidadCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (suma >= double.parse(cantidadCtlr.text)) {
                                  Future(() => loaderDialogNormal(context))
                                      .then((v) {
                                    FirebaseFirestore.instance
                                        .collection('Billetera')
                                        .add({
                                      'id_usuario': prefs.id,
                                      'monto': cantidadCtlr.text,
                                      'descripcion': 'Retiro de mi billetera',
                                      'fecha': DateTime.now(),
                                      'tipo': 'Retiro',
                                      'estado': false
                                    }).then((value) {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      _alertaFinal(context,
                                          'Te informaremos cuando el dinero de tu retiro sea transferido a tu cuenta');
                                    });
                                  });
                                } else {
                                  _alertaFinal(context,
                                      'El valor ingresado es mayor al total.');
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
