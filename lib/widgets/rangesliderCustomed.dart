import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class RangeSliderCustom extends StatefulWidget {
  final Function function;
  RangeSliderCustom({Key key, this.function}) : super(key: key);

  @override
  _RangeSliderCustomState createState() => _RangeSliderCustomState();
}

class _RangeSliderCustomState extends State<RangeSliderCustom> {
  static double _lowerValue = 1;
  static double _upperValue = 100;

  /*¡RangeValues values = RangeValues(lowervalue, uppervalue);
  RangeLabels labels = RangeLabels("10", "500");
*/
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 32,
        child: Material(
            type: MaterialType.transparency,
            child: FlutterSlider(
              rangeSlider: true,
              minimumDistance: 2,
              trackBar: FlutterSliderTrackBar(
                inactiveTrackBar: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.white, blurRadius: 0)],
                  color: decolarado
                ),
                activeTrackBar: BoxDecoration(
                  boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
                  color: decolarado
                ),
              ),
              selectByTap: true,
              handlerAnimation: FlutterSliderHandlerAnimation(
                  curve: Curves.elasticOut,
                  reverseCurve: Curves.bounceIn,
                  duration: Duration(milliseconds: 500),
                  scale: 1.5),
              handler: FlutterSliderHandler(
                decoration: BoxDecoration(),
                child: Material(
                  type: MaterialType.transparency,
                  child: Container(
                      padding: EdgeInsets.all(5),
                      child: Icon(
                        Icons.brightness_1,
                        size: 15,
                        color: textColorClaro,
                      )),
                ),
              ),
              rightHandler: FlutterSliderHandler(
                  decoration: BoxDecoration(),
                  child: Icon(
                    Icons.brightness_1,
                    size: 15,
                    color: textColorClaro,
                  )),
              tooltip: FlutterSliderTooltip(
                  custom: (value) {
                    return Text(
                      "\$" + value.toInt().toString(),
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(13),
                        fontWeight: FontWeight.normal,
                      ),
                    );
                  },
                  alwaysShowTooltip: true,
                  positionOffset: FlutterSliderTooltipPositionOffset(top: 22),
                  leftPrefix: Icon(
                    Icons.attach_money,
                    size: 11,
                    color: Colors.white,
                  ),
                  rightPrefix: Icon(
                    Icons.attach_money,
                    size: 11,
                    color: Colors.white,
                  ),
                  textStyle: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(11),
                    fontWeight: FontWeight.normal,
                  ),
                  boxStyle: FlutterSliderTooltipBox(
                      decoration: BoxDecoration(
                          color: Colors.redAccent.withOpacity(0.0)))),
              values: [_lowerValue, _upperValue],
              max: 100,
              min: 1,
              step: FlutterSliderStep(step: 1),
              onDragging: (handlerIndex, lowerValue, upperValue) {
                _lowerValue = lowerValue;
                _upperValue = upperValue;
                setState(() {});
                widget.function(_lowerValue,_upperValue);
              },
            )));
  }
}
