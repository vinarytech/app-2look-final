import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class Notificacion extends StatefulWidget {
  final QueryDocumentSnapshot documentSnapshot;
  Notificacion({Key key, this.documentSnapshot}) : super(key: key);

  @override
  _NotificacionState createState() => _NotificacionState();
}
class _NotificacionState extends State<Notificacion> {
  DateTime date;
  String fecha;
  @override
  void initState() { 
    super.initState();
  
  }

  @override
  Widget build(BuildContext context) {
     date= new DateFormat("yyyy-MM-dd hh:mm").parse(widget.documentSnapshot.data()['date']);
   fecha = DateFormat("dd-MM-yyyy hh:mm a").format(date);
    return Column(
      children: <Widget>[
        Container(
          height: 56,
          width: 350,
          padding: EdgeInsets.all(0.5),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(27)),
            boxShadow: [BoxShadow(color: Colors.white30, blurRadius: 0)],
            color: lightColor
          ),
          child: Material(
            type: MaterialType.transparency,
            elevation: 6.0,
            color: Colors.transparent,
            shadowColor: Colors.grey[50],
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Align(
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Text(
                    fecha,
                    style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(12),
                        fontWeight: FontWeight.normal)),
                    Text(
                    widget.documentSnapshot.data()['notificacion'],
                    style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(12),
                        fontWeight: FontWeight.normal))
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
