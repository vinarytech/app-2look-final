import 'package:adobe_xd/page_link.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';


class BotonAtras extends StatefulWidget {
  BotonAtras({Key key}) : super(key: key);

  @override
  _BotonAtrasState createState() => _BotonAtrasState();
}

class _BotonAtrasState extends State<BotonAtras> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, top: 15),
      child: GestureDetector(
        onTap: () {
       
          Navigator.pop(context);
        },
        child: Stack(
          children: <Widget>[
            Container(
              width: getProportionateScreenWidth(40),
              height: getProportionateScreenHeight(37),
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: Color(0Xff707070),
              ),
            ),
            Transform.translate(
              offset: Offset(getProportionateScreenHeight(12.5),
                  getProportionateScreenHeight(7.8)),
              child: SvgPicture.string(
                _svg_u87zmt,
                allowDrawingOutsideViewBox: true,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
class BotonAtrasF extends StatefulWidget {
  BotonAtrasF({Key key}) : super(key: key);

  @override
  _BotonAtrasFState createState() => _BotonAtrasFState();
}

class _BotonAtrasFState extends State<BotonAtrasF> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                      Home()), (Route<dynamic> route) => false);
        },
        child: Stack(
          children: <Widget>[
            Container(
              width: getProportionateScreenWidth(40.8),
              height: getProportionateScreenHeight(40.8),
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0x1affffff),
              ),
            ),
            Transform.translate(
              offset: Offset(getProportionateScreenHeight(12.5),
                  getProportionateScreenHeight(7.8)),
              child: SvgPicture.string(
                _svg_u87zmt,
                allowDrawingOutsideViewBox: true,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const String _svg_u87zmt =
    '<svg viewBox="15.5 9.8 10.2 21.1" ><path transform="translate(-10.57, -14.68)" d="M 34.62599945068359 45.66500091552734 C 34.12400054931641 45.66500091552734 33.6609992980957 45.43899917602539 33.35300064086914 45.04299926757813 L 26.37599945068359 36.08499908447266 C 25.92099952697754 35.50099945068359 25.92099952697754 34.68500137329102 26.37599945068359 34.09999847412109 L 33.35300064086914 25.14299964904785 C 33.6609992980957 24.74699974060059 34.125 24.52000045776367 34.62599945068359 24.52000045776367 C 35.25799942016602 24.52000045776367 35.79999923706055 24.85899925231934 36.07699966430664 25.42600059509277 C 36.35400009155273 25.99399948120117 36.28799819946289 26.62899971008301 35.90000152587891 27.12800025939941 L 29.69599914550781 35.09299850463867 L 35.90000152587891 43.05799865722656 C 36.28799819946289 43.55699920654297 36.35400009155273 44.19300079345703 36.07699966430664 44.7599983215332 C 35.79999923706055 45.32699966430664 35.25799942016602 45.66500091552734 34.62599945068359 45.66500091552734 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
