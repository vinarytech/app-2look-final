import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Busqueda/yabusqueda.dart';
import 'package:tl_app/src/Clientes/Finalizar/finalizarProblema.dart';
import 'package:tl_app/src/Profesionales/HomeProf/productosProf.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/obtenerDataProductos.dart';
import 'package:tl_app/widgets/size_config.dart';

class CarouselHorizontal extends StatefulWidget {
  @override
  _CarouselHorizontalState createState() => new _CarouselHorizontalState();
}

class _CarouselHorizontalState extends State<CarouselHorizontal> {
  List list = [
    'Peluquería'.toUpperCase(),
    'Diseño de Mirada y Maquillaje'.toUpperCase(),
    'Manicure y Pedicure'.toUpperCase(),
    'Asesoría de Imagen'.toUpperCase(),
    'Cosmetología'.toUpperCase(),
    'Masaje'.toUpperCase(),
    'Bienestar y Cuidado Personal'.toUpperCase(),
    'Peluquerias Caninas'.toUpperCase()
  ];

  List<String> imgList = [
    'lib/assets/images/carousel/1.png',
    'lib/assets/images/carousel/2.png',
    'lib/assets/images/carousel/3.png',
    'lib/assets/images/carousel/4.png',
    'lib/assets/images/carousel/5.png',
    'lib/assets/images/carousel/6.png',
    'lib/assets/images/carousel/8.png',
    'lib/assets/images/carousel/7.png'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Swiper(
          itemCount: 8,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
                print(list[index]);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            YaBusqueda(nombre: list[index])));
              },
              child: new Stack(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 30),
                    child: Image.asset(
                      imgList[index],
                      fit: BoxFit.cover,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: Center(
                      child: Container(
                        child: Text("${list[index]}",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Poppins',
                              fontSize: getProportionateScreenHeight(18),
                              fontWeight: FontWeight.bold
                            ),
                            textAlign: TextAlign.center),
                      ),
                    ),
                  )
                ],
              ),
            );
          },
          viewportFraction: 0.8,
          itemHeight: getProportionateScreenHeight(260),
          itemWidth: getProportionateScreenWidth(240),
          layout: SwiperLayout.CUSTOM,
          customLayoutOption:
              CustomLayoutOption(startIndex: -1, stateCount: 5).addTranslate([
            Offset(-470, -30),
            Offset(-270, -30),
            Offset(0, 0),
            Offset(270, -30),
            Offset(470, -30),
          ])),
    );
  }
}
