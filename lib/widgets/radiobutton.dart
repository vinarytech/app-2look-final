import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class RadioButton extends StatefulWidget {
  final bool isChecked;
  RadioButton({Key key, this.isChecked}) : super(key: key);

  @override
  _RadioButtonState createState() => _RadioButtonState();
}

class _RadioButtonState extends State<RadioButton> {
  bool _value = false;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Material(
      type: MaterialType.transparency,
      child: Center(
          child: GestureDetector(
        onTap: () {
          setState(() {
            _value = !_value;
            
          });
        },
        child: Container(
          width: getProportionateScreenWidth(15),
          height: getProportionateScreenWidth(15),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.transparent,
              border: Border.all(
                color: Colors.white,
                width: getProportionateScreenWidth(2),
              )),
          child: Container(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(right: 1.0),
              child: _value
                  ? Icon(
                      Icons.brightness_1,
                      size: getProportionateScreenWidth(8.0),
                      color: primaryColor,
                    )
                  : Icon(
                      Icons.access_alarm,
                      size: getProportionateScreenWidth(7.0),
                      color: Colors.transparent,
                    ),
            ),
          ),
        ),
      )),
    );
  }
}
