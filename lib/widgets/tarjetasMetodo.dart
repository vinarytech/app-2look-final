import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Profesionales/NuevaTarjeta.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'buttongray.dart';
import 'constants.dart';

class TarjetasMetodo extends StatefulWidget {
  final total;
  TarjetasMetodo({Key key, this.total}) : super(key: key);

  @override
  TarjetasMetodoState createState() => TarjetasMetodoState();
}

class TarjetasMetodoState extends State<TarjetasMetodo> {
  String paquete;
  @override
  void initState() {
    super.initState();
    if (widget.total == '5.00') {
      paquete = 'paquete1';
    } else if (widget.total == '15.00') {
      paquete = 'paquete2';
    } else if (widget.total == '25.00') {
      paquete = 'paquete3';
    }
  }

  ServiceApi apiData = new ServiceApi();
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FutureBuilder(
      future: apiData.getCards(),
      // initialData: InitialData,
      builder:
          (BuildContext context, AsyncSnapshot<List<CardModelo>> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length > 0) {
            return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(30)),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      elevation: 0.1,
                      color: Colors.white,
                      // shadowColor: Colors.white70,
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        getProportionateScreenHeight(10)),
                                    child: Image.asset(
                                      (snapshot.data[index].type == "vi")
                                          ? 'lib/assets/images/tarjetas/visa.png'
                                          : (snapshot.data[index].type == "mc")
                                              ? 'lib/assets/images/tarjetas/master.png'
                                              : (snapshot.data[index].type ==
                                                      "ax")
                                                  ? 'lib/assets/images/tarjetas/american.png'
                                                  : (snapshot.data[index]
                                                              .type ==
                                                          "di")
                                                      ? 'lib/assets/images/tarjetas/dinners.png'
                                                      : (snapshot.data[index]
                                                                  .type ==
                                                              "dc")
                                                          ? 'lib/assets/images/tarjetas/discober.png'
                                                          : (snapshot
                                                                      .data[
                                                                          index]
                                                                      .type ==
                                                                  "ms")
                                                              ? 'lib/assets/images/tarjetas/mestro.png'
                                                              : '',
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                ),
                                flex: 1,
                              ),
                              Expanded(
                                  child: Column(
                                    children: [
                                      (snapshot.data[index].type == "vi")
                                          ? Text('Visa',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: darkColor,
                                                fontSize:
                                                    getProportionateScreenHeight(
                                                        15),
                                                fontWeight: FontWeight.normal,
                                              ))
                                          : (snapshot.data[index].type == "mc")
                                              ? Text('Mastercard',
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    color: darkColor,
                                                    fontSize:
                                                        getProportionateScreenHeight(
                                                            15),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ))
                                              : (snapshot.data[index].type ==
                                                      "ax")
                                                  ? Text('American Express',
                                                      textAlign:
                                                          TextAlign.start,
                                                      style: TextStyle(
                                                        color: darkColor,
                                                        fontSize:
                                                            getProportionateScreenHeight(
                                                                15),
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ))
                                                  : (snapshot.data[index]
                                                              .type ==
                                                          "di")
                                                      ? Text('Dinners',
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                            color: darkColor,
                                                            fontSize:
                                                                getProportionateScreenHeight(
                                                                    15),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ))
                                                      : (snapshot.data[index]
                                                                  .type ==
                                                              "dc")
                                                          ? Text('Discover',
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                              style: TextStyle(
                                                                color:
                                                                    darkColor,
                                                                fontSize:
                                                                    getProportionateScreenHeight(
                                                                        15),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              ))
                                                          : (snapshot
                                                                      .data[
                                                                          index]
                                                                      .type ==
                                                                  "ms")
                                                              ? Text('Maestro',
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                  style:
                                                                      TextStyle(
                                                                    color:
                                                                        darkColor,
                                                                    fontSize:
                                                                        getProportionateScreenHeight(
                                                                            15),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ))
                                                              : SizedBox(),
                                      Text(snapshot.data[index].number,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: darkColor,
                                            fontSize:
                                                getProportionateScreenHeight(
                                                    15),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ],
                                  ),
                                  flex: 3),
                              Expanded(
                                  child: GestureDetector(
                                onTap: () {
                                  _alerta(
                                      context,
                                      '¿Estás seguro de realizar el pago con esta tarjeta?, recuerda que una vez realizado el pago tu plan sera acreditado. Cuando termine se renovara tu suscripción automaticamente',
                                      snapshot.data[index],
                                      widget.total);
                                },
                                child: Icon(Icons.chevron_right),
                              ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: snapshot.data.length);
          } else {
            return Column(
              children: [
                Text(
                  'No tienes tarjetas por el momento, Agrega una para continuar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(17),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                ButtonGray(
                  textoBoton: "Agregar Tarjeta",
                  bFuncion: () async {
                    var valor = await Navigator.push(
                        context,
                        PageTransition(
                            child: NuevaTarjetas(),
                            // child: NuevoTarjetita(),
                            type: PageTransitionType.fade));
                    setState(() {
                      print(valor);
                    });
                  },
                  anchoBoton: getProportionateScreenHeight(330),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.bold,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
              ],
            );
          }
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }

  void _alertaDatos(BuildContext context, String text, transaction) {
    final optCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  controller: optCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  apiData
                                      .verifyToken(transaction, optCtlr.text)
                                      .then((value) {
                                    Navigator.pop(context);
                                    var value2 = json.decode(value.body)[0];
                                    print("estado" + value2.toString());
                                    if (value2['status'].toString() == '4') {
                                      _alertaProducto(
                                          context,
                                          'El codigo OTP no es el correcto, por favor vuelvelo a intentar',
                                          false);
                                    } else if (value2['status'].toString() ==
                                        '0') {
                                      _alertaProducto(
                                          context,
                                          'El codigo OTP no es el correcto, por favor vuelvelo a intentar',
                                          false);
                                    } else if (value2['status'].toString() ==
                                        '1') {
                                      _alertaProducto(
                                          context,
                                          'Tarjeta agregada correctamente',
                                          false);
                                    } else {
                                      _alertaProducto(
                                          context,
                                          'Error de codigo otp porfavor vuelvelo a intentar',
                                          true);
                                    }
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaProducto(BuildContext context, String text, valor) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (valor) {
                                  Navigator.pop(context, 'valor');
                                  Navigator.pop(context, 'valor');
                                } else {
                                  Navigator.pop(context, 'valor');
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  _alerta(
      BuildContext context, String text, CardModelo snapshot, String total) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: ButtonGray(
                                  textoBoton: "Entendido",
                                  bFuncion: () {
                                    if (prefs.usuario['tipoLogin'] ==
                                        'icloud') {
                                      if (prefs.usuario['correo']
                                              .toString()
                                              .split('@')[1]
                                              .toString()[0] ==
                                          'p') {
                                        _alertaDatosCorreo(
                                            context,
                                            'Por favor ingresa un email al que te llegara la confirmación del pago.',
                                            snapshot.token,
                                            double.parse(total));
                                      } else {
                                        Future(() =>
                                                loaderDialogNormal(context))
                                            .then((v) {
                                          apiData
                                              .debitTokenPaquetes(
                                                  snapshot.token,
                                                  double.parse(total),
                                                  'Pago a 2Look por la compra de paquete numero de ' +
                                                      total +
                                                      'dolares')
                                              .then((value) {
                                            var decodeData =
                                                jsonDecode(value.body);
                                            if (decodeData[0]['transaction']
                                                    ['status'] ==
                                                'success') {
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              FirebaseFirestore.instance
                                                  .collection('Paymentez')
                                                  .add(decodeData[0])
                                                  .then((value) =>
                                                      metodopagartarjeta(
                                                          value.id,
                                                          snapshot.token,
                                                          double.parse(total)));
                                            } else if (decodeData[0]
                                                    ['transaction']['status'] ==
                                                'pending') {
                                              print(decodeData[0].toString());
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              _alertaDatosOpt(
                                                  context,
                                                  'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                                  decodeData[0]['card']
                                                      ['transaction_reference'],
                                                  decodeData[0]['card']
                                                      ['token'],
                                                  total,
                                                  prefs.usuario['correo'].toString());
                                            } else if (decodeData[0]
                                                    ['transaction']['status'] ==
                                                'failure') {
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              _alertaFinal(
                                                  context,
                                                  decodeData[0]['transaction']
                                                      ['status']);
                                            }
                                          }).then((value) {
                                            // Navigator.pop(context);
                                            // Navigator.pop(context);
                                          });
                                        });
                                      }
                                    } else {
                                      Future(() => loaderDialogNormal(context))
                                          .then((v) {
                                        apiData
                                            .debitTokenPaquetes(
                                                snapshot.token,
                                                double.parse(total),
                                                'Pago a 2Look por la compra de paquete numero de ' +
                                                    total +
                                                    'dolares')
                                            .then((value) {
                                          var decodeData =
                                              jsonDecode(value.body);
                                          if (decodeData[0]['transaction']
                                                  ['status'] ==
                                              'success') {
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                            FirebaseFirestore.instance
                                                .collection('Paymentez')
                                                .add(decodeData[0])
                                                .then((value) =>
                                                    metodopagartarjeta(
                                                        value.id,
                                                        snapshot.token,
                                                        double.parse(total)));
                                          } else if (decodeData[0]
                                                  ['transaction']['status'] ==
                                              'pending') {
                                            print(decodeData[0].toString());
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                            _alertaDatosOpt(
                                                context,
                                                'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                                decodeData[0]['card']
                                                    ['transaction_reference'],
                                                decodeData[0]['card']['token'],
                                                total,
                                                prefs.usuario['correo'].toString());
                                          } else if (decodeData[0]
                                                  ['transaction']['status'] ==
                                              'failure') {
                                            Navigator.pop(context);
                                            Navigator.pop(context);
                                            _alertaFinal(
                                                context,
                                                decodeData[0]['transaction']
                                                    ['status']);
                                          }
                                        }).then((value) {
                                          // Navigator.pop(context);
                                          // Navigator.pop(context);
                                        });
                                      });
                                    }

                                    // print(snapshot.token);
                                  },
                                  anchoBoton: getProportionateScreenHeight(120),
                                  largoBoton: getProportionateScreenWidth(27),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton:
                                      getProportionateScreenWidth(15),
                                  weightBoton: FontWeight.bold,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              ),
                              Align(
                                alignment: Alignment.center,
                                child: ButtonGray(
                                  textoBoton: "Cancelar",
                                  bFuncion: () {
                                    Navigator.pop(context);
                                  },
                                  anchoBoton: getProportionateScreenHeight(120),
                                  largoBoton: getProportionateScreenWidth(27),
                                  colorTextoBoton: textColorClaro,
                                  opacityBoton: 1,
                                  sizeTextBoton:
                                      getProportionateScreenWidth(15),
                                  weightBoton: FontWeight.bold,
                                  color1Boton: primaryColor,
                                  color2Boton: primaryColor,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  metodopagartarjeta(idPaimentez, token, total) {
    FirebaseFirestore.instance
        .collection('Paquetes')
        .where('id_usuario', isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (paquete == "paquete1") {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete1': Jiffy(DateTime.now()).add(days: 7).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 7).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 7).year.toString(),
          'estado': true,
          'suscripcion': true,
          'estadoPublicar': true,
          'token': token,
          'total': total
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_Paymentez': idPaimentez,
          'id_transferencia': '',
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 5,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': false,
        });
      } else if (paquete == "paquete2") {
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete2': Jiffy(DateTime.now()).add(days: 30).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).year.toString(),
          'estado': true,
          'suscripcion': true,
          'estadoPublicar': true,
          'token': token,
          'total': total
        });
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'gkI23Pc2NjtyugNADu5H',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_Paymentez': idPaimentez,
          'id_transferencia': '',
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 15,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'estado': false,
          'estadosoli': false,
        });
      } else if (paquete == "paquete3") {
        FirebaseFirestore.instance.collection('Pago_paquete').add({
          'fecha': DateTime.now().year.toString() +
              '-' +
              DateTime.now().month.toString() +
              '-' +
              DateTime.now().day.toString(),
          'token': prefs.usuario['token'],
          'fecha_verifica': '',
          'forma_pago': 'Tarjeta',
          'id_Admin_paquetes': 'h1ieGiNgm6EfCroysjKH',
          'id_paquetes': value.docs[0].id,
          'id_profesional': prefs.id,
          'id_transferencia': '',
          'id_Paymentez': idPaimentez,
          'id_user_action': '',
          'pago': 'Pendiente',
          'status': true,
          'total': 25,
        });
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update(
                {'estado': false, 'estadosoli': false, 'suscripcion': true});
        FirebaseFirestore.instance
            .collection('Paquetes')
            .doc(value.docs[0].id)
            .update({
          'paquete3': Jiffy(DateTime.now()).add(days: 30).day.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).month.toString() +
              '/' +
              Jiffy(DateTime.now()).add(days: 30).year.toString(),
          'estado': true,
          'estadoPublicar': true,
          'estadosoli': false,
          'token': token,
          'total': total
        });
      }

      FirebaseFirestore.instance
          .collection("Servicios")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
        value.docs.forEach((element) {
          FirebaseFirestore.instance
              .collection("Servicios")
              .doc(element.id)
              .update({"estado": true});
        });
      });
    }).then((value) => Navigator.pop(context));
    // .then((value) => Navigator.pop(context));
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alertaDatosCorreo(BuildContext context, String text, token, total) {
    final emailCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  controller: emailCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  apiData
                                      .debitTokenPaquetesEmail(
                                          token,
                                          double.parse(total),
                                          'Pago a 2Look por la compra de paquete numero de ' +
                                              total +
                                              'dolares',
                                          emailCtlr.text)
                                      .then((value) {
                                    var decodeData = jsonDecode(value.body);
                                    if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'success') {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      FirebaseFirestore.instance
                                          .collection('Paymentez')
                                          .add(decodeData[0])
                                          .then((value) => metodopagartarjeta(
                                              value.id,
                                              token,
                                              double.parse(total)));
                                    } else if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'pending') {
                                      print(decodeData[0].toString());
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      _alertaDatosOpt(
                                          context,
                                          'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                          decodeData[0]['card']
                                              ['transaction_reference'],
                                          decodeData[0]['card']['token'],
                                          total,
                                          emailCtlr.text);
                                    } else if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'failure') {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      _alertaFinal(
                                          context,
                                          decodeData[0]['transaction']
                                              ['status']);
                                    }
                                  }).then((value) {
                                    Navigator.pop(context);
                                    // Navigator.pop(context);
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaDatosOpt(
      BuildContext context, String text, transaction, token, total, email) {
    final optCtlr = TextEditingController();
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => WillPopScope(
              onWillPop: () async => false,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                height: getProportionateScreenHeight(30),
                                width: getProportionateScreenHeight(370),
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: TextField(
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    controller: optCtlr,
                                    textAlignVertical: TextAlignVertical.center,
                                    autocorrect: true,
                                    style: DecorationsOnInputs.textoInput(),
                                    decoration: InputDecoration(
                                      hintText: "INGRESE EL CODIGO OTP",
                                      errorStyle: TextStyle(
                                        color: textColorClaro,
                                      ),
                                      focusedErrorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      labelStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white70,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "Poppins",
                                      ),
                                      hintStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white38,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: "Poppins",
                                      ),
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                    ),
                                  ),
                                )),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {
                                  Future(() => loaderDialogNormal(context))
                                      .then((v) {
                                    apiData
                                        .verifyTokenPay(
                                            transaction, optCtlr.text,email)
                                        .then((value) {
                                      Navigator.pop(context);
                                      var value2 = json.decode(value.body)[0];
                                      print(value2);
                                      print('asdddddddddddddddddddddddddddddddddddddddddddddaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
                                      print(value2['transaction']['status']);
                                    
                                    
                                      // if (value2['transaction']['status'] ==
                                      //     'success') {
                                      //   Navigator.pop(context);
                                      //   Navigator.pop(context);
                                      //   FirebaseFirestore.instance
                                      //       .collection('Paymentez')
                                      //       .add(value2)
                                      //       .then((value) => metodopagartarjeta(
                                      //           value.id,
                                      //           token,
                                      //           double.parse(total)));
                                      // } else if (value2['transaction']
                                      //         ['status'] ==
                                      //     'pending') {
                                      //   _alertaDatosOpt(context, text,
                                      //       transaction, token, total);
                                      // } else if (value2['transaction']
                                      //         ['status'] ==
                                      //     'failure') {
                                      //   Navigator.pop(context);
                                      //   Navigator.pop(context);
                                      //   _alertaFinal(context,
                                      //       value2['transaction']['status']);
                                      // }
                                    }).catchError((onError) {
                                      Navigator.pop(context);
                                      _alertaFinal(context, onError);
                                    });
                                  });
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }
}
