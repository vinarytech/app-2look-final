import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/services/servicioService.dart';

import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/size_config.dart';

class MetodoPagoRadio extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;
  final Function retorno1;
  final Function retorno2;

  MetodoPagoRadio(
      {Key key,
      this.titulo,
      this.tipo1,
      this.tipo2,
      this.retorno1,
      this.retorno2})
      : super(key: key);

  @override
  _MetodoPagoRadioState createState() => _MetodoPagoRadioState();
}

class _MetodoPagoRadioState extends State<MetodoPagoRadio> {
  bool selected1 = false;
  bool selected2 = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Material(
                  //       type: MaterialType.transparency,
                  //       child: Align(
                  //           alignment: Alignment.topLeft,
                  //           child: Padding(
                  //             padding: EdgeInsets.only(
                  //               left: getProportionateScreenWidth(20),
                  //             ),
                  //             child: Text(widget.tipo1,
                  //                 style: TextStyle(
                  //                   color: textColorClaro,
                  //                   fontSize: getProportionateScreenWidth(14),
                  //                   fontWeight: FontWeight.normal,
                  //                 )),
                  //           )),
                  //     ),
                  //     Padding(
                  //       padding: const EdgeInsets.only(right: 20.0),
                  //       child: Container(
                  //         height: 20,
                  //         width: 20,
                  //         child: CircularCheckBox(
                  //             value: this.selected1,
                  //             checkColor: textColorMedio,
                  //             activeColor: primaryColor,
                  //             inactiveColor: textColorMedio,
                  //             disabledColor: Colors.grey,
                  //             onChanged: (val) => this.setState(() {
                  //                   this.selected1 = !this.selected1;
                  //                   if (this.selected1) {
                  //                     selected2 = false;
                  //                     widget.retorno1(this.selected1);
                  //                   } else {
                  //                     widget.retorno1(this.selected1);
                  //                   }
                  //                 })),
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(
                  //   height: 10,
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected2 = !this.selected2;
                                    if (this.selected2) {
                                      selected1 = false;
                                      widget.retorno2(this.selected2);
                                    } else {
                                      widget.retorno2(this.selected2);
                                    }
                                  })),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

// class MetodoRadioCobro extends StatefulWidget {
//   final String titulo;
//   final String tipo1;
//   final String tipo2, cuenta;
//   final Function retorno1;
//   final Function retorno2;

//   MetodoRadioCobro(
//       {Key key,
//       this.titulo,
//       this.tipo1,
//       this.tipo2,
//       this.retorno1,
//       this.retorno2,
//       this.cuenta})
//       : super(key: key);

//   @override
//   _MetodoRadioCobroState createState() => _MetodoRadioCobroState();
// }

// class _MetodoRadioCobroState extends State<MetodoRadioCobro> {
//   bool selected1 = false;
//   bool selected2 = false;
//   @override
//   void initState() {
//     if (widget.cuenta == 'Corriente') {
//       selected1 = true;
//       selected2 = false;
//     } else if (widget.cuenta == 'Ahorros') {
//       selected1 = false;
//       selected2 = true;
//     } else if (widget.cuenta == '') {
//       selected1 = false;
//       selected2 = false;
//     }
//     super.initState();

//   }

//   @override
//   Widget build(BuildContext context) {
//     return
//   }
// }
