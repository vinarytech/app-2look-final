import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

class DecorationsOnInputs {
  static TextStyle textoInput() {
    return TextStyle(
      fontSize: getProportionateScreenWidth(11),
      color: Colors.white,
      fontWeight: FontWeight.normal,
      fontFamily: "Poppins",
    );
  }

  static InputDecoration decorationInput(String labelText, String hinText) {
    return InputDecoration(
      enabledBorder: const OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(27)),
        borderSide: const BorderSide(color: lightColor, width: 5.0),
      ),
      focusedBorder: InputBorder.none,
      errorStyle: TextStyle(
        color: textColorClaro,
      ),
      
      focusedErrorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2.0),
      ),
      errorBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.white, width: 2.0),
      ),
      labelText: labelText,
      labelStyle: TextStyle(
        fontSize: getProportionateScreenWidth(11),
        color: Colors.white70,
        fontWeight: FontWeight.w600,
        fontFamily: "Poppins",
      ),
      contentPadding: new EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
      hintText: hinText,
      hintStyle: TextStyle(
        fontSize: getProportionateScreenWidth(11),
        color: Colors.white38,
        fontWeight: FontWeight.normal,
        fontFamily: "Poppins",
      ),
    );
  }
}
