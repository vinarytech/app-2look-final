import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Profesionales/NuevaTarjeta.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:http/http.dart' as http;

import 'buttongray.dart';
import 'constants.dart';

class TarjetasMetodoPorPagar extends StatefulWidget {
  final total;
  final snapshot;
  final token;
  final idEncargado;
  TarjetasMetodoPorPagar(
      {Key key, this.total, this.snapshot, this.idEncargado, this.token})
      : super(key: key);

  @override
  TarjetasMetodoPorPagarState createState() => TarjetasMetodoPorPagarState();
}

class TarjetasMetodoPorPagarState extends State<TarjetasMetodoPorPagar> {
  String paquete;
  @override
  ServiceApi apiData = new ServiceApi();
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FutureBuilder(
      future: apiData.getCards(),
      // initialData: InitialData,
      builder:
          (BuildContext context, AsyncSnapshot<List<CardModelo>> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.length > 0) {
            return ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(30)),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      elevation: 0.1,
                      color: Colors.white,
                      // shadowColor: Colors.white70,
                      child: Stack(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        getProportionateScreenHeight(10)),
                                    child: Image.asset(
                                      (snapshot.data[index].type == "vi")
                                          ? 'lib/assets/images/tarjetas/visa.png'
                                          : (snapshot.data[index].type == "mc")
                                              ? 'lib/assets/images/tarjetas/master.png'
                                              : (snapshot.data[index].type ==
                                                      "ax")
                                                  ? 'lib/assets/images/tarjetas/american.png'
                                                  : (snapshot.data[index]
                                                              .type ==
                                                          "di")
                                                      ? 'lib/assets/images/tarjetas/dinners.png'
                                                      : (snapshot.data[index]
                                                                  .type ==
                                                              "dc")
                                                          ? 'lib/assets/images/tarjetas/discober.png'
                                                          : (snapshot
                                                                      .data[
                                                                          index]
                                                                      .type ==
                                                                  "ms")
                                                              ? 'lib/assets/images/tarjetas/mestro.png'
                                                              : '',
                                      fit: BoxFit.fitWidth,
                                    ),
                                  ),
                                ),
                                flex: 1,
                              ),
                              Expanded(
                                  child: Column(
                                    children: [
                                      (snapshot.data[index].type == "vi")
                                          ? Text('Visa',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                color: darkColor,
                                                fontSize:
                                                    getProportionateScreenHeight(
                                                        15),
                                                fontWeight: FontWeight.normal,
                                              ))
                                          : (snapshot.data[index].type == "mc")
                                              ? Text('Mastercard',
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    color: darkColor,
                                                    fontSize:
                                                        getProportionateScreenHeight(
                                                            15),
                                                    fontWeight:
                                                        FontWeight.normal,
                                                  ))
                                              : (snapshot.data[index].type ==
                                                      "ax")
                                                  ? Text('American Express',
                                                      textAlign:
                                                          TextAlign.start,
                                                      style: TextStyle(
                                                        color: darkColor,
                                                        fontSize:
                                                            getProportionateScreenHeight(
                                                                15),
                                                        fontWeight:
                                                            FontWeight.normal,
                                                      ))
                                                  : (snapshot.data[index]
                                                              .type ==
                                                          "di")
                                                      ? Text('Dinners',
                                                          textAlign:
                                                              TextAlign.start,
                                                          style: TextStyle(
                                                            color: darkColor,
                                                            fontSize:
                                                                getProportionateScreenHeight(
                                                                    15),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          ))
                                                      : (snapshot.data[index]
                                                                  .type ==
                                                              "dc")
                                                          ? Text('Discover',
                                                              textAlign:
                                                                  TextAlign
                                                                      .start,
                                                              style: TextStyle(
                                                                color:
                                                                    darkColor,
                                                                fontSize:
                                                                    getProportionateScreenHeight(
                                                                        15),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              ))
                                                          : (snapshot
                                                                      .data[
                                                                          index]
                                                                      .type ==
                                                                  "ms")
                                                              ? Text('Maestro',
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                  style:
                                                                      TextStyle(
                                                                    color:
                                                                        darkColor,
                                                                    fontSize:
                                                                        getProportionateScreenHeight(
                                                                            15),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal,
                                                                  ))
                                                              : SizedBox(),
                                      Text(snapshot.data[index].number,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            color: darkColor,
                                            fontSize:
                                                getProportionateScreenHeight(
                                                    15),
                                            fontWeight: FontWeight.normal,
                                          )),
                                    ],
                                  ),
                                  flex: 3),
                              Expanded(
                                  child: GestureDetector(
                                onTap: () {
                                  print(widget.total);
                                  _alerta(
                                      context,
                                      '¿Estás seguro de realizar el pago con esta tarjeta?',
                                      snapshot.data[index],
                                      widget.total);
                                },
                                child: Icon(Icons.chevron_right),
                              ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
                itemCount: snapshot.data.length);
          } else {
            return Column(
              children: [
                Text(
                  'No tienes tarjetas por el momento, Agrega una para continuar',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(17),
                    fontWeight: FontWeight.normal,
                  ),
                ),
                ButtonGray(
                  textoBoton: "Agregar Tarjeta",
                  bFuncion: () async {
                    var valor = await Navigator.push(
                        context,
                        PageTransition(
                            child: NuevaTarjetas(),
                            // child: NuevoTarjetita(),
                            type: PageTransitionType.fade));
                    setState(() {
                      print(valor);
                    });
                  },
                  anchoBoton: getProportionateScreenHeight(330),
                  largoBoton: getProportionateScreenWidth(27),
                  colorTextoBoton: textColorClaro,
                  opacityBoton: 1,
                  sizeTextBoton: getProportionateScreenWidth(15),
                  weightBoton: FontWeight.bold,
                  color1Boton: primaryColor,
                  color2Boton: primaryColor,
                ),
              ],
            );
          }
        } else {
          return CircularProgressIndicator();
        }
      },
    );
  }

  _alerta(BuildContext context, String text, snapshot, String total) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                if (prefs.usuario['tipoLogin'] == 'icloud') {
                                  if (prefs.usuario['correo']
                                          .toString()
                                          .split('@')[1]
                                          .toString()[0] ==
                                      'p') {
                                    _alertaDatosCorreo(
                                        context,
                                        'Por favor ingresa un email al que te llegara la confirmación del pago.',
                                        snapshot.token,
                                        double.parse(total));
                                  } else {
                                    Future(() => loaderDialogNormal(context))
                                        .then((v) {
                                      apiData
                                          .debitTokenPaquetes(
                                              snapshot.token,
                                              double.parse(total),
                                              'Pago de la solicitud con el id ' +
                                                  widget.snapshot)
                                          .then((value) {
                                        var decodeData = jsonDecode(value.body);
                                        print(decodeData[0]);
                                        if (decodeData[0]['transaction']
                                                ['status'] ==
                                            'success') {
                                          FirebaseFirestore.instance
                                              .collection('Paymentez')
                                              .add(decodeData[0])
                                              .then(
                                                  (value) => metodoPagoTarjeta(
                                                        value.id,
                                                        widget.snapshot,
                                                      ));
                                        } else if (decodeData[0]['transaction']
                                                ['status'] ==
                                            'pending') {
                                          print(decodeData[0].toString());
                                          Navigator.pop(context);
                                          Navigator.pop(context);
                                          _alertaDatosOpt(
                                              context,
                                              'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                              decodeData[0]['card']
                                                  ['transaction_reference'],
                                              decodeData[0]['card']['token'],
                                              total,
                                              prefs.usuario['correo']);
                                        } else if (decodeData[0]['transaction']
                                                ['status'] ==
                                            'failure') {
                                          _alertaFinal(
                                              context,
                                              decodeData[0]['transaction']
                                                  ['status']);
                                        }
                                      }).then((value) {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                      });
                                    });
                                  }
                                } else {
                                  Future(() => loaderDialogNormal(context))
                                      .then((v) {
                                    apiData
                                        .debitTokenPaquetes(
                                            snapshot.token,
                                            double.parse(total),
                                            'Pago de la solicitud con el id ' +
                                                widget.snapshot)
                                        .then((value) {
                                      var decodeData = jsonDecode(value.body);
                                      print(decodeData[0]);
                                      if (decodeData[0]['transaction']
                                              ['status'] ==
                                          'success') {
                                        FirebaseFirestore.instance
                                            .collection('Paymentez')
                                            .add(decodeData[0])
                                            .then((value) => metodoPagoTarjeta(
                                                  value.id,
                                                  widget.snapshot,
                                                ));
                                      } else if (decodeData[0]['transaction']
                                              ['status'] ==
                                          'pending') {
                                        print(decodeData[0].toString());
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        _alertaDatosOpt(
                                            context,
                                            'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                            decodeData[0]['card']
                                                ['transaction_reference'],
                                            decodeData[0]['card']['token'],
                                            total,prefs.usuario['correo']);
                                      } else if (decodeData[0]['transaction']
                                              ['status'] ==
                                          'failure') {
                                        _alertaFinal(
                                            context,
                                            decodeData[0]['transaction']
                                                ['status']);
                                      }
                                    }).then((value) {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    });
                                  });
                                }
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaDatosCorreo(BuildContext context, String text, token, total) {
    final emailCtlr = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          Container(
                              decoration: BoxDecoration(
                                color: fondoBotonClaro,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: getProportionateScreenHeight(30),
                              width: getProportionateScreenHeight(370),
                              child: Padding(
                                padding: EdgeInsets.only(left: 10),
                                child: TextField(
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true),
                                  controller: emailCtlr,
                                  textAlignVertical: TextAlignVertical.center,
                                  autocorrect: true,
                                  style: DecorationsOnInputs.textoInput(),
                                  decoration: InputDecoration(
                                    hintText: "CANTIDAD A RETIRAR",
                                    errorStyle: TextStyle(
                                      color: textColorClaro,
                                    ),
                                    focusedErrorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    errorBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.white, width: 2.0),
                                    ),
                                    labelStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white70,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Poppins",
                                    ),
                                    hintStyle: TextStyle(
                                      fontSize: getProportionateScreenWidth(11),
                                      color: Colors.white38,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: "Poppins",
                                    ),
                                    focusedBorder: InputBorder.none,
                                    border: InputBorder.none,
                                  ),
                                ),
                              )),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  apiData
                                      .debitTokenPaquetesEmail(
                                          token,
                                          double.parse(total),
                                          'Pago de la solicitud con el id ' +
                                              widget.snapshot,
                                          emailCtlr)
                                      .then((value) {
                                    var decodeData = jsonDecode(value.body);
                                    print(decodeData[0]);
                                    if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'success') {
                                      FirebaseFirestore.instance
                                          .collection('Paymentez')
                                          .add(decodeData[0])
                                          .then((value) => metodoPagoTarjeta(
                                                value.id,
                                                widget.snapshot,
                                              ));
                                    } else if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'pending') {
                                      print(decodeData[0].toString());
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                      _alertaDatosOpt(
                                          context,
                                          'Pago pendiente de revision, porfavor ingresa el codigo OTP enviado a tu correo',
                                          decodeData[0]['card']
                                              ['transaction_reference'],
                                          decodeData[0]['card']['token'],
                                          total,
                                          emailCtlr.text);
                                    } else if (decodeData[0]['transaction']
                                            ['status'] ==
                                        'failure') {
                                      _alertaFinal(
                                          context,
                                          decodeData[0]['transaction']
                                              ['status']);
                                    }
                                  }).then((value) {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alertaDatosOpt(
      BuildContext context, String text, transaction, token, total, email) {
    final optCtlr = TextEditingController();
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) => WillPopScope(
              onWillPop: () async => false,
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(
                          getProportionateScreenHeight(20))),
                  elevation: 0,
                  backgroundColor: Colors.transparent,
                  child: Stack(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: 40, bottom: 16, left: 16, right: 16),
                        margin: EdgeInsets.only(top: 16),
                        decoration: BoxDecoration(
                            color: textColorClaro,
                            borderRadius: BorderRadius.circular(20),
                            shape: BoxShape.rectangle,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  blurRadius: 10,
                                  offset: Offset(0, 10))
                            ]),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              text,
                              style: TextStyle(
                                color: textColorOscuro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  color: fondoBotonClaro,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                height: getProportionateScreenHeight(30),
                                width: getProportionateScreenHeight(370),
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10),
                                  child: TextField(
                                    keyboardType:
                                        TextInputType.numberWithOptions(
                                            decimal: true),
                                    controller: optCtlr,
                                    textAlignVertical: TextAlignVertical.center,
                                    autocorrect: true,
                                    style: DecorationsOnInputs.textoInput(),
                                    decoration: InputDecoration(
                                      hintText: "INGRESE EL CODIGO OTP",
                                      errorStyle: TextStyle(
                                        color: textColorClaro,
                                      ),
                                      focusedErrorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      errorBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.white, width: 2.0),
                                      ),
                                      labelStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white70,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "Poppins",
                                      ),
                                      hintStyle: TextStyle(
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: Colors.white38,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: "Poppins",
                                      ),
                                      focusedBorder: InputBorder.none,
                                      border: InputBorder.none,
                                    ),
                                  ),
                                )),
                            SizedBox(
                              height: 24,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {
                                  Future(() => loaderDialogNormal(context))
                                      .then((v) {
                                    apiData
                                        .verifyTokenPay(
                                            transaction,
                                            optCtlr.text,
                                            email
                                            )
                                        .then((value) {
                                      Navigator.pop(context);
                                      var value2 = json.decode(value.body)[0];
                                      print("estado" + value2.toString());
                                      if (value2['transaction']['status'] ==
                                          'success') {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        FirebaseFirestore.instance
                                            .collection('Paymentez')
                                            .add(value2)
                                            .then((value) => metodoPagoTarjeta(
                                                  value.id,
                                                  widget.snapshot,
                                                ));
                                      } else if (value2['transaction']
                                              ['status'] ==
                                          'pending') {
                                        _alertaDatosOpt(context, text,
                                            transaction, token, total,email);
                                      } else if (value2['transaction']
                                              ['status'] ==
                                          'failure') {
                                        Navigator.pop(context);
                                        Navigator.pop(context);
                                        _alertaFinal(context,
                                            value2['transaction']['status']);
                                      }
                                    }).catchError((onError) {
                                      Navigator.pop(context);
                                      _alertaFinal(context, onError);
                                    });
                                  });
                                },
                                anchoBoton: getProportionateScreenHeight(330),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }

  metodoPagoTarjeta(idPaimentez, idSolicitud) {
    FirebaseFirestore.instance
        .collection('Solicitudes')
        .doc(idSolicitud)
        .update({
      'estado': 'EN TRANSCURSO',
      'metodoPago': 'Tarjeta',
      'estadoMetodoPago': true,
      'id_Paymentez': idPaimentez,
    });

    String date = DateFormat("yyyy-MM-dd hh:mm").format(DateTime.now());
    String notificacion =
        'Se ha realizado el pago del servicio, ahora se encuentra en transcurso';

    FirebaseFirestore.instance.collection('Notificaciones').add({
      'idemisor': prefs.id,
      'idreceptor': widget.idEncargado,
      'date': date,
      'notificacion': notificacion,
      // 'tokensolicitante': prefs.token,
      //  'remitente':'usuario',
      'accion': 'chat'
    }).then((value) {
      sendNotification(notificacion, '2Look', widget.token);
      sendMessage(
        prefs.id,
        widget.idEncargado,
      );
    }).then((value) => Navigator.pop(context));
  }

  sendMessage(idEmisor, idReceptor) {
    FirebaseFirestore.instance
        .collection('Usuarios')
        .doc(widget.idEncargado)
        .get()
        .then((value) {
      var mensaje = 'Hola ' +
          value.data()['nombre'].toString() +
          ", la entrega de mi producto o servicio esta solicitada lista en la hora acordada te llamare cuando me encuentre cerca." +
          value.data()['celular'].toString();
      FirebaseFirestore.instance.collection('Mensajes').add({
        'fecha': DateTime.now(),
        'idemisor': idEmisor,
        'idreceptor': idReceptor,
        'idsolicitud': widget.snapshot,
        'mensaje': mensaje,
        'estado': true,
      }).then((value) =>
          FirebaseFirestore.instance.collection('MensajesDetalle').add({
            'fecha': DateTime.now(),
            'idemisor': idEmisor,
            'idsolicitud': widget.snapshot,
            'idMensaje': value.id,
            'mensaje': mensaje,
            'estado': true,
          }));
    });
  }

  Future<void> sendNotification(subject, title, token) async {
    final postUrl = 'https://fcm.googleapis.com/fcm/send';
    final data = {
      "notification": {"body": subject, "title": title},
      "priority": "high",
      "data": {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "title": title,
        "body": subject,
      },
      "to": token
    };

    final headers = {
      'content-type': 'application/json',
      'Authorization':
          'key=AAAAgycTLFo:APA91bEv3hNGzZCF27hQWg1J2CeaiP-OT86-pJPZOrHdLeGRSrYw6gfsqqudyqEO5VfPAoq87Vy1A-nEhiK630s7y1tHrlLMzIsemH1uUBrdR3uDTg5X40yyaNiLYBSSbuxM0h5YovdU'
    };

    final response = await http.post(postUrl,
        body: json.encode(data),
        encoding: Encoding.getByName('utf-8'),
        headers: headers);

    if (response.statusCode == 200) {
      // on success do
      print("true");
    } else {
      // on failure do
      print("false");
    }
  }

  _alertaFinal(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
}
