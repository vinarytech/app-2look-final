import 'dart:convert';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/api.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Profesionales/NuevaTarjeta.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'constants.dart';

class Tarjetas extends StatefulWidget {
  Tarjetas({Key key}) : super(key: key);

  @override
  _TarjetasState createState() => _TarjetasState();
}

class _TarjetasState extends State<Tarjetas> {
  final apiData = ServiceApi();
  final List<Widget> cardWidget = List();
  @override
  ServiceApi api = new ServiceApi();
  final prefs = new PreferenciasUsuario();
  var suscripcion = 'false';
  @override
  void initState() { 
    super.initState();
        FirebaseFirestore.instance
          .collection("Paquetes")
          .where("id_usuario", isEqualTo: prefs.id)
          .get()
          .then((value) {
            suscripcion = value.docs.first.data()['suscripcion'].toString();
        setState(() {});
      });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
            body: Stack(
          children: [
            Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('lib/assets/images/splash/fondo.png'),
                        fit: BoxFit.cover))),
            Center(
                child: Column(
              children: <Widget>[
                Container(
                    child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(40),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: getProportionateScreenWidth(450),
                      height: getProportionateScreenHeight(70),
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage(
                              "lib/assets/images/busqueda/cabecera.png"),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10, top: 10),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Stack(
                          children: [
                            Icon(
                              Icons.circle,
                              size: 50,
                              color: Color(0Xff707070),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 18, top: 13),
                              child: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                )),

                SizedBox(
                  height: getProportionateScreenHeight(30),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(30)),
                    child: FutureBuilder(
                      future: apiData.getCards(),
                      // initialData: InitialData,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<CardModelo>> snapshot) {
                        if (snapshot.hasData) {
                          if (snapshot.data.length > 0) {
                            return ListView.builder(
                                itemBuilder: (context, index) {
                                  return Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    elevation: 0.1,
                                    color: Colors.white,
                                    // shadowColor: Colors.white70,
                                    child: Stack(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                child: Padding(
                                                  padding: EdgeInsets.all(
                                                      getProportionateScreenHeight(
                                                          10)),
                                                  child: Image.asset(
                                                    (snapshot.data[index]
                                                                .type ==
                                                            "vi")
                                                        ? 'lib/assets/images/tarjetas/visa.png'
                                                        : (snapshot.data[index]
                                                                    .type ==
                                                                "mc")
                                                            ? 'lib/assets/images/tarjetas/master.png'
                                                            : (snapshot
                                                                        .data[
                                                                            index]
                                                                        .type ==
                                                                    "ax")
                                                                ? 'lib/assets/images/tarjetas/american.png'
                                                                : (snapshot
                                                                            .data[
                                                                                index]
                                                                            .type ==
                                                                        "di")
                                                                    ? 'lib/assets/images/tarjetas/dinners.png'
                                                                    : (snapshot.data[index].type ==
                                                                            "dc")
                                                                        ? 'lib/assets/images/tarjetas/discober.png'
                                                                        : (snapshot.data[index].type ==
                                                                                "ms")
                                                                            ? 'lib/assets/images/tarjetas/mestro.png'
                                                                            : '',
                                                    fit: BoxFit.fitWidth,
                                                  ),
                                                ),
                                              ),
                                              flex: 1,
                                            ),
                                            Expanded(
                                                child: Column(
                                                  children: [
                                                    (snapshot.data[index]
                                                                .type ==
                                                            "vi")
                                                        ? Text('Visa',
                                                            textAlign: TextAlign
                                                                .start,
                                                            style: TextStyle(
                                                              fontSize:
                                                                  getProportionateScreenHeight(
                                                                      15),
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal,
                                                            ))
                                                        : (snapshot.data[index]
                                                                    .type ==
                                                                "mc")
                                                            ? Text('Mastercard',
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                                style:
                                                                    TextStyle(
                                                                  fontSize:
                                                                      getProportionateScreenHeight(
                                                                          15),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                ))
                                                            : (snapshot
                                                                        .data[
                                                                            index]
                                                                        .type ==
                                                                    "ax")
                                                                ? Text(
                                                                    'American Express',
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                    style:
                                                                        TextStyle(
                                                                      fontSize:
                                                                          getProportionateScreenHeight(
                                                                              15),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .normal,
                                                                    ))
                                                                : (snapshot
                                                                            .data[
                                                                                index]
                                                                            .type ==
                                                                        "di")
                                                                    ? Text(
                                                                        'Dinners',
                                                                        textAlign:
                                                                            TextAlign
                                                                                .start,
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              getProportionateScreenHeight(15),
                                                                          fontWeight:
                                                                              FontWeight.normal,
                                                                        ))
                                                                    : (snapshot.data[index].type ==
                                                                            "dc")
                                                                        ? Text(
                                                                            'Discover',
                                                                            textAlign: TextAlign
                                                                                .start,
                                                                            style:
                                                                                TextStyle(
                                                                              fontSize: getProportionateScreenHeight(15),
                                                                              fontWeight: FontWeight.normal,
                                                                            ))
                                                                        : (snapshot.data[index].type ==
                                                                                "ms")
                                                                            ? Text('Maestro',
                                                                                textAlign: TextAlign.start,
                                                                                style: TextStyle(
                                                                                  fontSize: getProportionateScreenHeight(15),
                                                                                  fontWeight: FontWeight.normal,
                                                                                ))
                                                                            : SizedBox(),
                                                    Text(
                                                        snapshot
                                                            .data[index].number,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          fontSize:
                                                              getProportionateScreenHeight(
                                                                  15),
                                                          fontWeight:
                                                              FontWeight.normal,
                                                        )),
                                                  ],
                                                ),
                                                flex: 3)
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(
                                              getProportionateScreenHeight(5)),
                                          child: Align(
                                              alignment: Alignment.topRight,
                                              child:(suscripcion == 'false')? GestureDetector(
                                                  onTap: () {
                                                    _alertaProducto(
                                                        context,
                                                        '¿Estás seguro de querer eliminar esta tarjeta?',
                                                        snapshot
                                                            .data[index].token);
                                                  },
                                                  child:Icon(Icons.close,
                                                      color: Colors.red)):SizedBox()),
                                        )
                                      ],
                                    ),
                                  );
                                },
                                itemCount: snapshot.data.length);
                          } else {
                            return Text(
                              'No tienes tarjetas por el momento, Agrega una para continuar',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(17),
                                fontWeight: FontWeight.normal,
                              ),
                            );
                          }
                        } else {
                          return SizedBox(
                              child: CircularProgressIndicator(
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.blue),
                                  strokeWidth: 5.0));
                        }
                      },
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(getProportionateScreenHeight(30)),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: FloatingActionButton(
                      backgroundColor: primaryColor,
                      onPressed: () async {
                        var valor = await Navigator.push(
                            context,
                            PageTransition(
                                child: NuevaTarjetas(),
                                // child: NuevoTarjetita(),
                                type: PageTransitionType.fade));
                        setState(() {
                          print(valor);
                        });
                      },
                      child: Text('+',
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenHeight(40),
                            fontWeight: FontWeight.normal,
                          )),
                    ),
                  ),
                ),
                // ListView(
                //   physics: BouncingScrollPhysics(),
                //   children: [
                //     Column(
                //       children: [
                //         Placeholder(),
                //         Placeholder(),
                //         Placeholder(),
                //         Placeholder(),
                //       ],
                //     ),
                //     SizedBox()
                //   ],
                // )
              ],
            )),
          ],
        )

            /*Center(
            child: Text('Running on: $_platformVersion\n'),
          ),*/

            ),
      ),
    );
  }

  void _alertaProducto(BuildContext context, String text, token) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Future(() => loaderDialogNormal(context))
                                    .then((v) {
                                  apiData.deleteCards(token).then((value) {
                                    var value2 =
                                        json.decode(value.body)['action'];
                                    print(value2.toString());
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                    Navigator.pop(context);

                                    if (value2.toString() ==
                                        '{message: card deleted}') {
                                      _alerta(context,
                                          'Tarjeta eliminada correctamente');
                                    } else {
                                      _alerta(context, 'Algo salio mal');
                                    }
                                  });
                                });
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
