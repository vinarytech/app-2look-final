import 'package:flutter/material.dart';
import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';
import 'decorationsOnInputs.dart';

class InputCustomTextField extends StatefulWidget {
  final String placeholder;
  final TextEditingController textController;
  final TextInputType keyboardType;
  final bool isPassword;
  final String label;

  InputCustomTextField({
    Key key,
    this.placeholder,
    this.textController,
    this.keyboardType,
    this.isPassword,
    this.label,
  }) : super(key: key);

  @override
  _InputCustomTextFieldState createState() => _InputCustomTextFieldState();
}

class _InputCustomTextFieldState extends State<InputCustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        width: getProportionateScreenWidth(197),
        height: getProportionateScreenHeight(38),
        child: TextField(
          textAlignVertical: TextAlignVertical.center,
          obscureText: widget.isPassword,
          controller: widget.textController,
          autocorrect: true,
          keyboardType: widget.keyboardType,
          style: DecorationsOnInputs.textoInput(),
          decoration: InputDecoration(
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(27)),
                borderSide: const BorderSide(color: lightColor, width: 5.0),
              ),
              errorStyle: TextStyle(
                color: textColorClaro,
              ),
              focusedErrorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              errorBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 2.0),
              ),
              labelText: widget.label,
              labelStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white70,
                fontWeight: FontWeight.w600,
                fontFamily: "Poppins",
              ),
              contentPadding:
                  new EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
              hintStyle: TextStyle(
                fontSize: getProportionateScreenWidth(11),
                color: Colors.white38,
                fontWeight: FontWeight.normal,
                fontFamily: "Poppins",
              ),
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              hintText: widget.placeholder),
        ));
  }
}
