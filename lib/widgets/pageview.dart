import 'dart:math';

import 'package:flutter/material.dart';

class PageViewWidget extends StatefulWidget {
  @override
  _PageViewWidgetState createState() => new _PageViewWidgetState();
}

class _PageViewWidgetState extends State<PageViewWidget> {
  List _list = [
    'lib/assets/images/busqueda/2look.png',
    'lib/assets/images/busqueda/2look.png',
    'lib/assets/images/busqueda/2look.png',
    'lib/assets/images/busqueda/2look.png',
    'lib/assets/images/busqueda/2look.png'
  ];

  PageController pageController;
  double viewportFraction = 0.8;
  double pageOffset;

  @override
  void initState() {
    super.initState();
    pageController =
        PageController(initialPage: 0, viewportFraction: viewportFraction)
          ..addListener(() {
            setState(() {
              pageOffset = pageController.page;
            });
          });
  }

  @override
  Widget build(BuildContext context) {
    return new PageView.builder(
      controller: pageController,
      itemBuilder: (context, index) {
        double angle = 1 - (pageOffset - index).abs();

        double scale = max(viewportFraction,
            (1 - (pageOffset - index).abs() + viewportFraction));

        if (angle > 0.5) {
          angle = 1 - angle;
        }

        return Container(
            padding: EdgeInsets.only(
                right: 10, bottom: 100 - scale * 25, left: 20, top: 10),
            child: Transform(
              transform: Matrix4.identity()
                ..setEntry(
                  3,
                  2,
                  0.001,
                )
                ..rotateY(angle),
              alignment: Alignment.center,
              child: Material(
                  elevation: 2,
                  child: Image.asset(
                    _list[index],
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  )),
            ));
      },
      itemCount: _list.length,
    );
  }
}
