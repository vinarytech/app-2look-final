import 'package:flutter/material.dart';
import 'package:tl_app/src/Clientes/Reservas/enProceso.dart';
import 'package:tl_app/src/Clientes/Reservas/historial.dart';

import 'package:tl_app/widgets/size_config.dart';

import 'constants.dart';

class TextLineButton extends StatefulWidget {
  final String widgetText1;
  final String widgetText2;
  final int pagina;

  TextLineButton({Key key, this.widgetText1, this.widgetText2, this.pagina})
      : super(key: key);

  @override
  _TextLineButtonState createState() => _TextLineButtonState();
}

class _TextLineButtonState extends State<TextLineButton> {
  var colorBoton1 = primaryColor;
  var  colorBoton2 = textColorMedio;

  bool viewVisible1 = true;

  bool viewVisible2 = false;
  int pag;

  void showWidget1() {
    setState(() {
      viewVisible1 = true;
      viewVisible2 = false;
    });
  }

  void showWidget2() {
    setState(() {
      viewVisible2 = true;
      viewVisible1 = false;
    });
  }
  @override
  void initState() {
   /* pag=widget.pagina;
    if(widget.pagina==1){
      colorBoton1 = primaryColor;
    colorBoton2 = textColorMedio;
    }
    
    if(widget.pagina==0){
    colorBoton1 = textColorMedio;
    colorBoton2 = primaryColor;
    }
    
    if(widget.pagina!=0 && widget.pagina!=1){
    colorBoton1 = primaryColor;
    colorBoton2 = textColorMedio;
    }*/
    
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      alignment: FractionalOffset.center,
      child: Column(
      //  shrinkWrap: true,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (colorBoton1 == textColorMedio) {
                      colorBoton1 = primaryColor;
                      colorBoton2 = textColorMedio;
                    pag=null;
                    }
                    showWidget1();
                  });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    children: <Widget>[
                      Text(widget.widgetText1,
                          style: TextStyle(
                            color: colorBoton1,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.normal,
                          )),
                      Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width / 2.25,
                          color: colorBoton1),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    if (colorBoton2 == textColorMedio) {
                      colorBoton2 = primaryColor;
                      colorBoton1 = textColorMedio;
                      pag=null;
                    }
                    showWidget2();
                  });
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Column(
                    children: <Widget>[
                      Text(widget.widgetText2,
                          style: TextStyle(
                            color: colorBoton2,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.normal,
                          )),
                      Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width / 2.25,
                          color: colorBoton2),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Stack(
            children: <Widget>[
             Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible:  viewVisible1,
                  child: EnProceso()
                    
                
                  
                
                  //
                      
                      ),
              Visibility(
                  maintainSize: true,
                  maintainAnimation: true,
                  maintainState: true,
                  visible: viewVisible2,
                  child:  Historial()),
            ],
          ),
        ],
      ),
    );
  }
}
