import 'dart:convert';

import 'package:circular_check_box/circular_check_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart' as http;
import 'dart:ui';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Alertas/alerta.dart';
import 'package:tl_app/src/Clientes/Alertas/alertaCalificacion.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/reportarProblemaP.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Clientes/Pago/pago.dart';
import 'package:tl_app/src/Clientes/Problemas/reportarProblema.dart';
import 'package:tl_app/src/mapa_page.dart';
import 'package:tl_app/widgets/botonatras.dart';
import 'package:tl_app/widgets/buttongray.dart';
import 'package:tl_app/widgets/cardHorizontal.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/datetimepicker.dart';
import 'package:tl_app/widgets/decorationsOnInputs.dart';
import 'package:tl_app/widgets/dropdownCustomed.dart';
import 'package:tl_app/widgets/finalizar/tipoubicacion.dart';
import 'package:tl_app/widgets/guardados/esquina.dart';
import 'package:tl_app/widgets/inputCustomTextField.dart';
import 'package:tl_app/widgets/inputIconCustomTextField.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';
import 'package:tl_app/widgets/radiometodopago.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:loading/loading.dart';
import 'package:url_launcher/url_launcher.dart';

class MetodoCobro extends StatefulWidget {
  MetodoCobro({
    Key key,
  }) : super(key: key);

  @override
  _MetodoCobroState createState() => _MetodoCobroState();
}

class _MetodoCobroState extends State<MetodoCobro> {
  final convert = new NumberFormat("#,##0.00", "en_US");
  final bancoCtlr = TextEditingController();
  final numeroCtlr = TextEditingController();
  final nombreCtlr = TextEditingController();
  String cuenta = '';
  String idMetodoCobro;
  bool selected1 = false;
  bool selected2 = false;
  bool edicion;

  GoogleMapController _controller;
  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection('MetodoCobro')
        .where('id_usuario', isEqualTo: prefs.id)
        .get()
        .then((value) {
      if (value.docs.length > 0) {
        edicion = true;
        idMetodoCobro = value.docs[0].id;
        bancoCtlr.text = value.docs[0].data()['banco'];
        numeroCtlr.text = value.docs[0].data()['numeroCuenta'];
        nombreCtlr.text = value.docs[0].data()['nombreTitular'];
        cuenta = value.docs[0].data()['cuenta'];
        if (value.docs[0].data()['cuenta'] == 'Corriente') {
          selected1 = true;
          selected2 = false;
          setState(() {
            
          });
        } else if (value.docs[0].data()['cuenta'] == 'Ahorros') {
          selected1 = false;
          selected2 = true;
             setState(() {
            
          });
        }
      } else {
        edicion = false;
        print('nada para mostrar');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
        child: Scaffold(
            body: SizedBox(
                width: double.infinity,
                child: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage("lib/assets/images/splash/fondo.png"),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Center(
                        child: Column(
                      children: <Widget>[
                        Container(
                            child: Stack(
                          children: [
                            Container(
                              width: getProportionateScreenWidth(450),
                              height: getProportionateScreenHeight(70),
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage(
                                      "lib/assets/images/busqueda/cabecera.png"),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        )),
                      ],
                    )),
                    Padding(
                      padding: const EdgeInsets.only(top: 38.0),
                      child: Container(
                        height: getProportionateScreenHeight(45),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Image.asset(
                            'lib/assets/images/busqueda/2look.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                        padding: EdgeInsets.only(
                            top: 95.0,
                            left: getProportionateScreenWidth(20),
                            right: getProportionateScreenWidth(20)),
                        child: SingleChildScrollView(
                            physics: BouncingScrollPhysics(),
                            child: Container(
                              child: Column(children: [
                                Container(
                                  child: Material(
                                      type: MaterialType.transparency,
                                      child: Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                              left: getProportionateScreenWidth(
                                                  20),
                                            ),
                                            child: Text("BANCO",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          18),
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          ))),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        color: fondoBotonClaro,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      height: getProportionateScreenHeight(30),
                                      width: getProportionateScreenHeight(370),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10),
                                        child: TextField(
                                          // keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          controller: bancoCtlr,
                                          textAlignVertical:
                                              TextAlignVertical.center,
                                          autocorrect: true,
                                          style:
                                              DecorationsOnInputs.textoInput(),
                                          decoration: InputDecoration(
                                            errorStyle: TextStyle(
                                              color: textColorClaro,
                                            ),
                                            focusedErrorBorder:
                                                UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white70,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Poppins",
                                            ),
                                            hintStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white38,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: "Poppins",
                                            ),
                                            focusedBorder: InputBorder.none,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 8.0, left: 8, right: 8),
                                  child: Container(
                                    child: Column(
                                      children: [
                                        Container(
                                          child: Material(
                                            type: MaterialType.transparency,
                                            child: Align(
                                                alignment: Alignment.topLeft,
                                                child: Padding(
                                                  padding: EdgeInsets.only(
                                                    left:
                                                        getProportionateScreenWidth(
                                                            20),
                                                  ),
                                                  child: Text('CUENTA',
                                                      style: TextStyle(
                                                        color: textColorClaro,
                                                        fontSize:
                                                            getProportionateScreenWidth(
                                                                18),
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      )),
                                                )),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Align(
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left:
                                                                getProportionateScreenWidth(
                                                                    20),
                                                          ),
                                                          child: Text(
                                                              'Corriente',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        )),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 20.0),
                                                    child: Container(
                                                      height: 20,
                                                      width: 20,
                                                      child: CircularCheckBox(
                                                          value: this.selected1,
                                                          checkColor:
                                                              textColorMedio,
                                                          activeColor:
                                                              primaryColor,
                                                          inactiveColor:
                                                              textColorMedio,
                                                          disabledColor:
                                                              Colors.grey,
                                                          onChanged: (val) =>
                                                              this.setState(() {
                                                                this.selected1 =
                                                                    !this
                                                                        .selected1;
                                                                if (this
                                                                    .selected1) {
                                                                  selected2 =
                                                                      false;
                                                                  cuenta =
                                                                      'Corriente';
                                                                } else {
                                                                  cuenta = '';
                                                                }
                                                              })),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Material(
                                                    type: MaterialType
                                                        .transparency,
                                                    child: Align(
                                                        alignment:
                                                            Alignment.topLeft,
                                                        child: Padding(
                                                          padding:
                                                              EdgeInsets.only(
                                                            left:
                                                                getProportionateScreenWidth(
                                                                    20),
                                                          ),
                                                          child: Text('Ahorros',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        14),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .normal,
                                                              )),
                                                        )),
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            right: 20.0),
                                                    child: Container(
                                                      height: 20,
                                                      width: 20,
                                                      child: CircularCheckBox(
                                                          value: this.selected2,
                                                          checkColor:
                                                              textColorMedio,
                                                          activeColor:
                                                              primaryColor,
                                                          inactiveColor:
                                                              textColorMedio,
                                                          disabledColor:
                                                              Colors.grey,
                                                          onChanged: (val) =>
                                                              this.setState(() {
                                                                this.selected2 =
                                                                    !this
                                                                        .selected2;
                                                                if (this
                                                                    .selected2) {
                                                                  selected1 =
                                                                      false;
                                                                  cuenta =
                                                                      'Ahorros';
                                                                } else {
                                                                  cuenta = '';
                                                                }
                                                              })),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  child: Material(
                                      type: MaterialType.transparency,
                                      child: Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                              left: getProportionateScreenWidth(
                                                  20),
                                            ),
                                            child: Text("NOMBRE DEL TITULAR",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          18),
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          ))),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        color: fondoBotonClaro,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      height: getProportionateScreenHeight(30),
                                      width: getProportionateScreenHeight(370),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10),
                                        child: TextField(
                                          // keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          controller: nombreCtlr,
                                          textAlignVertical:
                                              TextAlignVertical.center,
                                          autocorrect: true,
                                          style:
                                              DecorationsOnInputs.textoInput(),
                                          decoration: InputDecoration(
                                            errorStyle: TextStyle(
                                              color: textColorClaro,
                                            ),
                                            focusedErrorBorder:
                                                UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white70,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Poppins",
                                            ),
                                            hintStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white38,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: "Poppins",
                                            ),
                                            focusedBorder: InputBorder.none,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  child: Material(
                                      type: MaterialType.transparency,
                                      child: Align(
                                          alignment: Alignment.topLeft,
                                          child: Padding(
                                            padding: EdgeInsets.only(
                                              left: getProportionateScreenWidth(
                                                  20),
                                            ),
                                            child: Text("NÚMERO DE CUENTA",
                                                style: TextStyle(
                                                  color: textColorClaro,
                                                  fontSize:
                                                      getProportionateScreenWidth(
                                                          18),
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          ))),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 10, right: 10),
                                  child: Container(
                                      decoration: BoxDecoration(
                                        color: fondoBotonClaro,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      height: getProportionateScreenHeight(30),
                                      width: getProportionateScreenHeight(370),
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 10),
                                        child: TextField(
                                          keyboardType: TextInputType.numberWithOptions(decimal: true),
                                          controller: numeroCtlr,
                                          textAlignVertical:
                                              TextAlignVertical.center,
                                          autocorrect: true,
                                          style:
                                              DecorationsOnInputs.textoInput(),
                                          decoration: InputDecoration(
                                            errorStyle: TextStyle(
                                              color: textColorClaro,
                                            ),
                                            focusedErrorBorder:
                                                UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            errorBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.white,
                                                  width: 2.0),
                                            ),
                                            labelStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white70,
                                              fontWeight: FontWeight.w600,
                                              fontFamily: "Poppins",
                                            ),
                                            hintStyle: TextStyle(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: Colors.white38,
                                              fontWeight: FontWeight.normal,
                                              fontFamily: "Poppins",
                                            ),
                                            focusedBorder: InputBorder.none,
                                            border: InputBorder.none,
                                          ),
                                        ),
                                      )),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 20.0),
                                  child: ButtonGray(
                                    textoBoton: "GUARDAR",
                                    bFuncion: () {
                                      if (bancoCtlr.text == null ||
                                          bancoCtlr.text == '') {
                                        _alerta(context,
                                            'Por favor ingresa un banco al que se hara el pago');
                                      } else if (cuenta == '') {
                                        _alerta(context,
                                            'Por favor selecciona el tipo de cuenta');
                                      } else if (nombreCtlr.text == null ||
                                          nombreCtlr.text == '') {
                                        _alerta(context,
                                            'Porfavor ingresa el nombre del titular de la cuenta');
                                      } else if (numeroCtlr.text == null ||
                                          numeroCtlr.text == '') {
                                        _alerta(context,
                                            'Porfavor ingresa el número de cuenta al que se realizara el pago');
                                      } else {
                                        if (edicion) {
                                          Future(() =>
                                                  loaderDialogNormal(context))
                                              .then((v) {
                                            FirebaseFirestore.instance
                                                .collection('MetodoCobro')
                                                .doc(idMetodoCobro)
                                                .update({
                                              'banco': bancoCtlr.text,
                                              'cuenta': cuenta,
                                              'nombreTitular': nombreCtlr.text,
                                              'numeroCuenta': numeroCtlr.text
                                            }).then((value) {
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              _alerta(context,
                                                  'Tu metodo de cobro se ha actualizado correctamente');
                                            });
                                          });
                                        } else {
                                          Future(() =>
                                                  loaderDialogNormal(context))
                                              .then((v) {
                                            FirebaseFirestore.instance
                                                .collection('MetodoCobro')
                                                .add({
                                              'id_usuario': prefs.id,
                                              'banco': bancoCtlr.text,
                                              'cuenta': cuenta,
                                              'nombreTitular': nombreCtlr.text,
                                              'numeroCuenta': numeroCtlr.text
                                            }).then((value) {
                                              Navigator.pop(context);
                                              Navigator.pop(context);
                                              _alerta(context,
                                                  'Tu metodo de cobro se ha actualizado correctamente');
                                            });
                                          });
                                        }
                                      }
                                    },
                                    anchoBoton:
                                        getProportionateScreenHeight(350),
                                    largoBoton: getProportionateScreenWidth(28),
                                    colorTextoBoton: textColorClaro,
                                    opacityBoton: 1,
                                    sizeTextBoton:
                                        getProportionateScreenWidth(11),
                                    weightBoton: FontWeight.w400,
                                    color1Boton: primaryColor,
                                    color2Boton: primaryColor,
                                  ),
                                ),
                              ]),
                            ))),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10, vertical: 10),
                        child: BotonAtras())
                  ],
                ))));
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }

  void _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(330),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }
}
