import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/widgets/cardDividida.dart';
import 'package:tl_app/widgets/cardDivididaProf.dart';
import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/size_config.dart';

// class Pest2Prof extends StatefulWidget {
//   Pest2Prof({Key key}) : super(key: key);

//   @override
//   _Pest2ProfState createState() => _Pest2ProfState();
// }

// class _Pest2ProfState extends State<Pest2Prof> {
//    final prefs = PreferenciasUsuario();
//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     return Container(
//       child:StreamBuilder<QuerySnapshot>(
//             stream: FirebaseFirestore.instance
//                 .collection("Solicitudes")
//                 .where("idsolicitante", isEqualTo: prefs.id)
//                 .snapshots(),
//             builder:
//                 (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
//               if (snapshot.hasData) {
//                 return Column(children: getExpenseItems(snapshot));
//               } else if (snapshot.hasError) {
//                 return Center(child: Text("Ha ocurrido un error"));
//               } else {
//                 return Center(child: CircularProgressIndicator());
//               }
//             }));
//   }

//   getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
//     return snapshot.data.docs.map((doc) {
//       return doc.data()['estado']!='FINALIZADO' && doc.data()['estadosolicitud']==true ? CardDividida(document: doc,):SizedBox();
//     }).toList();
//   }
// }

class Pest2Prof extends StatefulWidget {
  Pest2Prof({Key key}) : super(key: key);

  @override
  _Pest2ProfState createState() => _Pest2ProfState();
}

class _Pest2ProfState extends State<Pest2Prof> {
  final convert = new NumberFormat("#,##0.00", "en_US");
  double suma = 0;
  final prefs = PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        child: Column(
      children: <Widget>[
        SizedBox(
          height: 20,
        ),
        Container(
          alignment: Alignment.center,
          child: Material(
              type: MaterialType.transparency,
              child: Text("INGRESOS",
                  style: TextStyle(
                    color: textColorClaro,
                    fontSize: getProportionateScreenWidth(18),
                    fontWeight: FontWeight.bold,
                  ))),
        ),
        StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('Solicitudes')
              .where('idencargado', isEqualTo: prefs.id)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            double suma = 0;
            for (var item in snapshot.data.docs) {
              if (item.data()['estado'] == 'FINALIZADO') {
                suma += double.parse(item.data()['total']);
              }
            }
            return Container(
              alignment: Alignment.center,
              child: Material(
                  type: MaterialType.transparency,
                  child: Text(convert.format(double.parse(suma.toString())),
                      style: TextStyle(
                        color: textColorClaro,
                        fontSize: getProportionateScreenWidth(38),
                        fontWeight: FontWeight.normal,
                      ))),
            );
          },
        ),
        SizedBox(
          height: 40,
        ),
        Container(
            child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection("Solicitudes")
                    .where("idencargado", isEqualTo: prefs.id)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasData) {
                    return Column(children: getExpenseItems(snapshot));
                  } else if (snapshot.hasError) {
                    return Center(child: Text("Ha ocurrido un error"));
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                }))
      ],
    ));
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    // return ListView.builder(itemBuilder: (context, index){
    // return CardDivididaProf(document: doc,);
    // },itemCount: snapshot.data.docs.length);
    return snapshot.data.docs.map((doc) {
      // return Text(doc.id);
      return doc.data()['estado'] == 'FINALIZADO'
          ? CardDivididaProf(document: doc, idSolicitud: doc.id)
          : SizedBox();
    }).toList();
  }
}
