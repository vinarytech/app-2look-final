import 'package:flutter/material.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/radiobuttonGeneroProductoProf.dart';

import 'package:tl_app/widgets/constants.dart';
import 'package:tl_app/widgets/radiobutton.dart';
import 'package:tl_app/widgets/size_config.dart';

class TipoGeneroProductoProf extends StatefulWidget {
  TipoGeneroProductoProf({Key key}) : super(key: key);

  @override
  _TipoGeneroProductoProfState createState() => _TipoGeneroProductoProfState();
}

class _TipoGeneroProductoProfState extends State<TipoGeneroProductoProf> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text("GENERO",
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text("Masculino",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text("Femenino",
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 18.0),
                    child: Column(
                      children: [
                        Container(
                          height: 40,
                          width: 40,
                          child: RadioButtonGeneroProductoProf(),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
