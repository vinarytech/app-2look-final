import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';

import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/size_config.dart';

class TipoServicioProf extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;
  final seleccion;
  final domicilio;

  final Function function1;
  final Function function2;

  TipoServicioProf({
    Key key,
    this.titulo,
    this.tipo1,
    this.tipo2,
    this.function1,
    this.function2,
    this.seleccion,
    this.domicilio,
  }) : super(key: key);

  @override
  _TipoServicioProfState createState() => _TipoServicioProfState();
}

class _TipoServicioProfState extends State<TipoServicioProf> {
  bool selected1 = false;
  bool selected2 = false;

  @override
  Widget build(BuildContext context) {
    this.selected2 = widget.seleccion;
    this.selected1 = widget.domicilio;
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo1,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: Checkbox(
                              value: this.selected1,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              // inactiveColor: textColorMedio,
                              // disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected1 = !this.selected1;
                                    widget.function1(val);
                                  })),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: Checkbox(
                              value: this.selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              // inactiveColor: textColorMedio,
                              // disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected2 = !this.selected2;
                                    this.selected2 = !widget.seleccion;
                                    widget.function2(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GeneroSeleccion extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;
  final bool valor1;
  final bool valor2;

  final Function function1;
  final Function function2;

  GeneroSeleccion({
    Key key,
    this.titulo,
    this.tipo1,
    this.tipo2,
    this.function1,
    this.function2,
    this.valor1,
    this.valor2,
  }) : super(key: key);

  @override
  _GeneroSeleccionState createState() => _GeneroSeleccionState();
}

class _GeneroSeleccionState extends State<GeneroSeleccion> {


  @override
  Widget build(BuildContext context) {
    bool selected1 = widget.valor1;
    bool selected2 = widget.valor2;
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo1,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: Checkbox(
                              value: selected1,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              // inactiveColor: textColorMedio,
                              // disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    selected1 = !selected1;
                                    widget.function1(val);
                                  })),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: Checkbox(
                              value: selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              // inactiveColor: textColorMedio,
                              // disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    selected2 = !selected2;
                                    widget.function2(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class GeneroServicioProf extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;

  final Function function1;
  final Function function2;

  GeneroServicioProf({
    Key key,
    this.titulo,
    this.tipo1,
    this.tipo2,
    this.function1,
    this.function2,
  }) : super(key: key);

  @override
  _GeneroServicioProfState createState() => _GeneroServicioProfState();
}

class _GeneroServicioProfState extends State<GeneroServicioProf> {
  bool selected1 = false;
  bool selected2 = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo1,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected1,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected1 = !this.selected1;
                                    widget.function1(val);
                                  })),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected2 = !this.selected2;

                                    widget.function2(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
