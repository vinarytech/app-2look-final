import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tl_app/src/Profesionales/ReservasProf/aceptarReserva.dart';
import 'package:tl_app/widgets/cardDividida.dart';

import 'package:tl_app/widgets/size_config.dart';

class Pest1Prof extends StatefulWidget {
  Pest1Prof({Key key}) : super(key: key);

  @override
  _Pest1ProfState createState() => _Pest1ProfState();
}

class _Pest1ProfState extends State<Pest1Prof> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(child: CardDividida(
      bFunction: () {
        Navigator.pushReplacement(
            context,
            PageTransition(
                type: PageTransitionType.fade, child: AceptarReserva()));
      },
    ));
  }
}
