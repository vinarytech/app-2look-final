import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';

import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/size_config.dart';

class TipoDiasProf extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;
  final String tipo3;
  final String tipo4;
  final String tipo5;
  final String tipo6;
  final String tipo7;

  final Function function1;
  final Function function2;
  final Function function3;
  final Function function4;
  final Function function5;
  final Function function6;
  final Function function7;

  TipoDiasProf(
      {Key key,
      this.titulo,
      this.tipo1,
      this.tipo2,
      this.function1,
      this.function2,
      this.tipo3,
      this.tipo4,
      this.tipo5,
      this.tipo6,
      this.tipo7,
      this.function3,
      this.function4,
      this.function5,
      this.function6,
      this.function7})
      : super(key: key);

  @override
  _TipoDiasProfState createState() => _TipoDiasProfState();
}

class _TipoDiasProfState extends State<TipoDiasProf> {
  bool selected1 = false;
  bool selected2 = false;
  bool selected3 = false;
  bool selected4 = false;
  bool selected5 = false;
  bool selected6 = false;
  bool selected7 = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo1,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected1,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected1 = !this.selected1;
                                    widget.function1(val);
                                  })),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected2 = !this.selected2;
                                    widget.function2(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo3,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected3,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected3 = !this.selected3;
                                    widget.function3(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo4,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected4,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected4 = !this.selected4;
                                    widget.function4(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo5,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected5,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected5 = !this.selected5;
                                    widget.function5(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo6,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected6,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected6 = !this.selected6;
                                    widget.function6(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo7,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected7,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected7 = !this.selected7;
                                    widget.function7(val);
                                  })),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
