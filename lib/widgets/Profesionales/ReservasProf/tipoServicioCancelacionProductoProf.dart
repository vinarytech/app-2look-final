import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:tl_app/services/productoService.dart';
import 'package:tl_app/services/servicioService.dart';

import 'package:tl_app/widgets/constants.dart';

import 'package:tl_app/widgets/size_config.dart';

class TipoServicioCancelacionProductoProf extends StatefulWidget {
  final String titulo;
  final String tipo1;
  final String tipo2;

  TipoServicioCancelacionProductoProf({Key key, this.titulo, this.tipo1, this.tipo2})
      : super(key: key);

  @override
  _TipoServicioCancelacionProductoProfState createState() =>
      _TipoServicioCancelacionProductoProfState();
}

class _TipoServicioCancelacionProductoProfState
    extends State<TipoServicioCancelacionProductoProf> {
  bool selected1 = false;
  bool selected2 = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text(widget.titulo,
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo1,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected1,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected1 = !this.selected1;
                                    this.selected2 = !this.selected1;
                                   
                                      productoService
                                          .cambiarFlexible(selected1);
                                     productoService
                                          .cambiarSinCancelacion(selected2);
                                    
                                  })),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Material(
                        type: MaterialType.transparency,
                        child: Align(
                            alignment: Alignment.topLeft,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: getProportionateScreenWidth(20),
                              ),
                              child: Text(widget.tipo2,
                                  style: TextStyle(
                                    color: textColorClaro,
                                    fontSize: getProportionateScreenWidth(14),
                                    fontWeight: FontWeight.normal,
                                  )),
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 20.0),
                        child: Container(
                          height: 20,
                          width: 20,
                          child: CircularCheckBox(
                              value: this.selected2,
                              checkColor: textColorMedio,
                              activeColor: primaryColor,
                              inactiveColor: textColorMedio,
                              disabledColor: Colors.grey,
                              onChanged: (val) => this.setState(() {
                                    this.selected2 = !this.selected2;
                                    this.selected1 = !this.selected2;

                                    
                                      servicioService
                                          .cambiarFlexible(selected1);
                                      servicioService
                                          .cambiarSinCancelacion(selected2);
                                    
                                  })),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
