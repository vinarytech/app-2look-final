import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class IconoMensajes extends StatefulWidget {
  IconoMensajes({Key key}) : super(key: key);

  @override
  _IconoMensajesState createState() => _IconoMensajesState();
}

class _IconoMensajesState extends State<IconoMensajes> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 22.0,
      height: 22.0,
      child: Stack(
        children: <Widget>[
          SvgPicture.string(
            '<svg viewBox="282.5 701.0 22.3 22.3" ><path transform="translate(0.2, 0.22)" d="M 282.75 723.0469970703125 L 284.4869995117188 718.4559936523438 C 283.0830078125 716.541015625 282.343994140625 714.2789916992188 282.343994140625 711.8839721679688 C 282.343994140625 705.739990234375 287.3420104980469 700.7410278320313 293.4859924316406 700.7410278320313 C 299.6300048828125 700.7410278320313 304.6279907226563 705.739990234375 304.6279907226563 711.8839721679688 C 304.6279907226563 718.0280151367188 299.6300048828125 723.0269775390625 293.4859924316406 723.0269775390625 C 291.7550048828125 723.0269775390625 290.0360107421875 722.6199951171875 288.4960021972656 721.8489990234375 L 282.75 723.0469970703125 Z M 288.7260131835938 720.208984375 L 288.9760131835938 720.3419799804688 C 290.3529968261719 721.0780029296875 291.9129943847656 721.4669799804688 293.4859924316406 721.4669799804688 C 298.7699890136719 721.4669799804688 303.0690002441406 717.1680297851563 303.0690002441406 711.8839721679688 C 303.0690002441406 706.5999755859375 298.7699890136719 702.301025390625 293.4859924316406 702.301025390625 C 288.2019958496094 702.301025390625 283.9030151367188 706.5999755859375 283.9030151367188 711.8839721679688 C 283.9030151367188 714.072998046875 284.6210021972656 716.1329956054688 285.9800109863281 717.8419799804688 L 286.2569885253906 718.1890258789063 L 285.2149963378906 720.9400024414063 L 288.7260131835938 720.208984375 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>',
            allowDrawingOutsideViewBox: true,
          ),
          Transform.translate(
            offset: Offset(13.7, 9.6),
            child: Container(
              width: 3.1,
              height: 3.1,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xffffffff),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(9.6, 9.6),
            child: Container(
              width: 3.1,
              height: 3.1,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xffffffff),
              ),
            ),
          ),
          Transform.translate(
            offset: Offset(5.5, 9.6),
            child: Container(
              width: 3.1,
              height: 3.1,
              decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
                color: const Color(0xffffffff),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
