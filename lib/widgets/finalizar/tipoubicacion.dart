import 'package:flutter/material.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/tipoServicioCancelacionProductoProf.dart';
import 'package:tl_app/widgets/Profesionales/ReservasProf/tipoServicioCancelacionServicioProf.dart';
import 'package:tl_app/widgets/radiobuttonjuntos.dart';

import '../constants.dart';
import '../radiobutton.dart';
import '../size_config.dart';

class TipoUbicacion extends StatefulWidget {
  final List lista;
  final Function onpress;
  TipoUbicacion({Key key, this.lista,this.onpress}) : super(key: key);

  @override
  _TipoUbicacionState createState() => _TipoUbicacionState();
}

class _TipoUbicacionState extends State<TipoUbicacion> {
  @override
  void initState() { 
    super.initState();
    print(widget.lista[0]);
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 38.0, left: 8, right: 8),
      child: Container(
        child: Column(
          children: [
            Container(
              child: Material(
                type: MaterialType.transparency,
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                      ),
                      child: Text("UBICACIÓN",
                          style: TextStyle(
                            color: textColorClaro,
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          )),
                    )),
              ),
            ),
            widget.lista[0]=="domicilio"?Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Material(
                    type: MaterialType.transparency,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20),
                          ),
                          child:
                           Text("A domicilio",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: FontWeight.normal,
                              ))
                              ,
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 35.0),
                    child: RadioButton(),
                  )
                ],
              ),
            ): SizedBox(),
            widget.lista[0]=="establecimiento"?Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Material(
                    type: MaterialType.transparency,
                    child: Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(20),
                          ),
                          child: Text("En establecimiento",
                              style: TextStyle(
                                color: textColorClaro,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: FontWeight.normal,
                              )),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 35.0),
                    child: RadioButton(),
                  )
                ],
              ),
            ):SizedBox(),
            widget.lista[0]=="union"?Container(
             
               child:   Padding(
                    padding: const EdgeInsets.only(right: 5.0),
                    child: TipoUbicacionRadio(
                        
                        tipo1: 'En establecimiento',
                        tipo2: 'Domicilio',
                        retorno1: (value1) {
                         setState(() {
                           
                         });
                            widget.onpress("establecimiento");
                         
                        },
                        retorno2: (value2) {
                          setState(() {
                            
                          });
                         widget.onpress("domicilio");
                        },
                      ),
                  
                  
              
            )
            ):SizedBox(),
          ],
        ),
      ),
    );
  }
}
