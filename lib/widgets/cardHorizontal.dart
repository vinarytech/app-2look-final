import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:page_transition/page_transition.dart';
import 'package:social_share/social_share.dart';
import 'package:tl_app/models/modelservicio.dart';
import 'package:tl_app/services/preferenciasUsuario.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroProducto.dart';
import 'package:tl_app/src/Clientes/Busqueda/dentroServicio.dart';
import 'package:tl_app/src/Clientes/Home/home.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirProducto.dart';
import 'package:tl_app/src/Profesionales/ProductosProf/subirServicio.dart';
import 'package:tl_app/widgets/guardados/esquina.dart';
import 'package:tl_app/widgets/size_config.dart';
import 'package:screenshot/screenshot.dart';

import 'buttongray.dart';
import 'constants.dart';

class CardHorizontal extends StatefulWidget {
  final String recomendado;
  final String nombre;
  final prefs = PreferenciasUsuario();
  final String valor;
  final Function bFunction;
  final String nameImage;
  final List<ServiciosF> listafiltrada;
  CardHorizontal(
      {Key key,
      this.bFunction,
      this.nameImage,
      this.nombre,
      this.valor,
      this.listafiltrada,
      this.recomendado})
      : super(key: key);

  @override
  _CardHorizontalState createState() => _CardHorizontalState();
}

class _CardHorizontalState extends State<CardHorizontal> {
  ScreenshotController screenshotController = ScreenshotController();
  bool valor = false;

  // final convert = new NumberFormat("#,##0.00", "en_US");
  Future<Informacion> futInfo;
  List listaFavoritos = [];

  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection('Favoritos')
        .where('idUsuario', isEqualTo: prefs.id)
        .get()
        .then((value) => value.docs.forEach((element) {
              setState(() {
                print(element.id);
                setState(() {
                  listaFavoritos.add(element);
                });
              });
              // element.data()[idUsuario]
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: FutureBuilder<QuerySnapshot>(
            future: (widget.valor == null &&
                    widget.listafiltrada == null &&
                    widget.recomendado == null)
                ? FirebaseFirestore.instance
                    .collection("Servicios")
                    .orderBy('destacado', descending: true)
                    .where("categoria", isEqualTo: widget.nombre)
                    .get()
                : FirebaseFirestore.instance
                    .collection("Servicios")
                    .orderBy('destacado', descending: true)
                    .get(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                //print('snapshotsito ${snapshot.data}');
                return (widget.listafiltrada == null)
                    ? Column(children: getExpenseItems(snapshot))
                    : Column(children: getExpenseItems2());
              } else if (snapshot.hasError) {
                return Center(child: Text("Ha ocurrido un error"));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  getExpenseItems2() {
    if (widget.listafiltrada.length == 0) {
      List<Widget> lista = [
        SizedBox(
          height: 40,
        ),
        Image.asset(
          'lib/assets/images/icons/x.png',
          height: 80,
        ),
        Center(
            child: Text(
                'No hemos podido encontrar lo que necesitas, intenta más tarde'))
      ];
      return lista;
    } else {
      return widget.listafiltrada.map((doc) {
        return doc.estado == true ? cardfiltrado(doc, false) : SizedBox();
      }).toList();
    }
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    print('bum ${snapshot.data.size}');

    if (snapshot.data.size == 0) {
      List<Widget> lista = [
        SizedBox(
          height: 40,
        ),
        Image.asset(
          'lib/assets/images/icons/x.png',
          height: 80,
        ),
        Center(
            child: Text(
                'No hemos podido encontrar lo que necesitas, intenta más tarde'))
      ];
      return lista;
    } else {
      return snapshot.data.docs.map((doc) {
        if (widget.valor == null && widget.recomendado == null) {
          return doc.data()['estado'] == true ? _card(doc, false) : SizedBox();
        } else {
          if (widget.valor != null) {
            print('valor');
            print('hola perros');
            return (doc.data()['estado'] == true) &&
                    (doc
                            .data()['subcategoria']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['categoria']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['ciudad']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['descripcion']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['genero']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['sector']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['precio']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['marca']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor) ||
                        doc
                            .data()['diasdisponible']
                            .toString()
                            .toLowerCase()
                            .contains(widget.valor))
                ? _card(doc, false)
                : SizedBox();
          }
          if (widget.recomendado != null) {
            print('valor');
            print(widget.recomendado);
            return (doc.data()['estado'] == true) &&
                    (doc
                        .data()['id_usuario']
                        .toString()
                        .contains(widget.recomendado))
                ? _card(doc, false)
                : SizedBox();
          }
        }
      }).toList();
    }
  }

  List listanuneva = [];
  List listanunevaubicacion = [];
  Future _listaimagenes(idservicio) async {
    List lista = [];
    await FirebaseFirestore.instance
        .collection("ImagenesServicios")
        .where('id_servicio', isEqualTo: idservicio)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        var list = result.data().values.toList();

        print('lista  $list');
        list.remove(idservicio);
        String finalStr = list.reduce((value, element) {
          return value + element;
        });
        //  print('el valor lista $lista');
        //lista.add(result.data()['fotourl']);
        //lista = result.data().values.toList();
        lista.add(finalStr);
        //        print('el valor lista $lista');
      });
      /* var _results=querySnapshot.docs[0].data();
     lista = _results.values.toList();
    print('_list ${lista[1]}');
    print('query ${querySnapshot.docs[0].data()}');*/
    });
    return lista;
  }

  Future<Informacion> obtener(
      String id, String idservicio, QueryDocumentSnapshot documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    //var dataestrellas=await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    listanuneva = await _listaimagenes(idservicio);
    print('AQUI LISTA NUEVA $listanuneva');

    var informacionusuario;
    informacionusuario =
        Informacion(doc: data1, fotos: listanuneva, documento: documento);

    return informacionusuario;
  }

  Future<InformacionF> obtenerfiltrado(
      String id, String idservicio, ServiciosF documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    //var dataestrellas=await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    listanuneva = await _listaimagenes(idservicio);
    print('AQUI LISTA NUEVA $listanuneva');

    var informacionusuario;
    informacionusuario =
        InformacionF(doc: data1, fotos: listanuneva, documento: documento);
    print('informacionusuario $informacionusuario');
    return informacionusuario;
  }

  Widget cardfiltrado(ServiciosF serviciosF, favorito) {
    // retorno=docs.id;
    // print(serviciosF.id);
    return FutureBuilder(
        future:
            obtenerfiltrado(serviciosF.idusuario, serviciosF.id, serviciosF),
        builder: (BuildContext context, AsyncSnapshot<InformacionF> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.fotos.length == 0) {
              setState(() {});
            } else {
              return GestureDetector(
                onTap: () {
                  if (snapshot.data.documento.subcategoria == 'producto') {
                    // print(snapshot.data.fotos);
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade,
                            child: DentroProducto(snapshotf: snapshot.data)));
                    /* */
                  } else {
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade,
                            child: DentroServicio(snapshotf: snapshot.data)));
                    //  print(snapshot.data.fotos);
                  }
                },
                child: Padding(
                    padding: EdgeInsets.all(getProportionateScreenHeight(16)),
                    child: Container(
                      child: Material(
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Container(
                                child: Container(
                                  height: getProportionateScreenHeight(150),
                                  width: getProportionateScreenWidth(350),
                                  decoration: BoxDecoration(
                                    color: lightColor,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: Stack(
                                      children: [
                                        Container(
                                          height:
                                              getProportionateScreenHeight(150),
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    snapshot.data.fotos[0]),
                                                fit: BoxFit.fill,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                        ),
                                        Positioned(
                                            top: getProportionateScreenHeight(
                                                95),
                                            left: getProportionateScreenHeight(
                                                -3),
                                            child: Esquina(
                                              idServicio:
                                                  snapshot.data.documento.id,
                                              valor: favorito,
                                            ))
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: Stack(
                                        children: [
                                          Container(
                                            height:
                                                getProportionateScreenHeight(
                                                    150),
                                          ),
                                          Positioned(
                                              top: 10,
                                              left: 15,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    width: 150,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: (widget.valor ==
                                                                  null &&
                                                              widget.listafiltrada ==
                                                                  null &&
                                                              widget.recomendado ==
                                                                  null)
                                                          ? Text(
                                                              "${snapshot.data.documento.subcategoria.toUpperCase()} / " +
                                                                  widget.nombre,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        10),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ))
                                                          : Text(
                                                              "${snapshot.data.documento.categoria.toUpperCase()} / "
                                                              '${snapshot.data.documento.subcategoria.toUpperCase()}',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        10),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    width: 150,
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: 30,
                                                          //width: 150,
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                "${snapshot.data.doc.data()["nombre"]} / ${snapshot.data.doc.data()["estrellas"]}",
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                )),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        Container(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          child: Icon(
                                                            Icons.star,
                                                            size: 14,
                                                            color: primaryColor,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    height:
                                                        getProportionateScreenHeight(
                                                            25),
                                                    width: 150,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: Text(
                                                          snapshot
                                                              .data
                                                              .documento
                                                              .descripcion,
                                                          style: TextStyle(
                                                            color:
                                                                textColorClaro,
                                                            fontSize:
                                                                getProportionateScreenWidth(
                                                                    11),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          )),
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    width: 150,
                                                    child: Material(
                                                        type: MaterialType
                                                            .transparency,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(
                                                                '${snapshot.data.documento.precio}'
                                                                r'$',
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          18),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              child: Row(
                                                                children: [
                                                                  IconButton(
                                                                    icon:
                                                                        FaIcon(
                                                                      FontAwesomeIcons
                                                                          .share,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 20,
                                                                    ),

                                                                    onPressed:
                                                                        () {
                                                                      SocialShare.shareOptions('Te comparto este ' +
                                                                              snapshot.data.documento.subcategoria +
                                                                              ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                              snapshot.data.documento.categoria +
                                                                              ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año https://2look.app/')
                                                                          .then((data) {});
                                                                    },

                                                                    // onPressed:
                                                                    //     () {
                                                                    //   final RenderBox
                                                                    //       box =
                                                                    //       context.findRenderObject();
                                                                    //   Share.share(
                                                                    //       'https://2look.app/ Te comparto este ' +
                                                                    //           snapshot.data.documento.subcategoria +
                                                                    //           ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                    //           snapshot.data.documento.categoria +
                                                                    //           ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año',
                                                                    //       subject: 'TTULO',
                                                                    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
                                                                    // }
                                                                  ),

                                                                  // FlutterShareMe().shareToFacebook(
                                                                  //     url:
                                                                  //         'https://2look.app/',
                                                                  //     msg: 'Te comparto este ' +
                                                                  //         snapshot.data.documento.subcategoria +
                                                                  //         ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                  //         snapshot.data.documento.categoria +
                                                                  //         ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año');
                                                                  // },

                                                                  // IconButton(
                                                                  //   icon: FaIcon(
                                                                  //       FontAwesomeIcons
                                                                  //           .whatsapp,
                                                                  //       color: Colors
                                                                  //           .white),
                                                                  //   onPressed:
                                                                  //       () async {},
                                                                  //   // onPressed:
                                                                  //   //     () {
                                                                  //   //   FlutterShareMe().shareToWhatsApp(
                                                                  //   //       msg: 'Te comparto este ' +
                                                                  //   //           snapshot.data.documento.subcategoria +
                                                                  //   //           ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                  //   //           snapshot.data.documento.categoria +
                                                                  //   //           ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año' +
                                                                  //   //           'https://2look.app/');
                                                                  //   // },
                                                                  // ),
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        )),
                                                  ),
                                                ],
                                              ))
                                        ],
                                      )),
                                ],
                              ),
                            ],
                          )),
                    )),
              );
            }
          }
          return SizedBox();
        });
  }

  Widget _card(QueryDocumentSnapshot docs, favorito) {
    // retorno=docs.id;
    return FutureBuilder(
        future: obtener(docs.data()["id_usuario"], docs.id, docs),
        builder: (BuildContext context, AsyncSnapshot<Informacion> snapshot) {
//usuario docs
//documento servicio
//lista de fotos
          //print('snapshot pasar ${snapshot.data.fotos[0]}');

          if (snapshot.hasData) {
            if (listaFavoritos.length > 0) {
              for (var item in listaFavoritos) {
                if (snapshot.data.documento.id == item.data()['idServicio']) {
                  favorito = true;
                  break;
                } else {
                  favorito = false;
                }
              }
            }
            if (snapshot.data.fotos.length == 0) {
              // setState(() {});
            } else {
              return GestureDetector(
                onTap: () {
                  if (snapshot.data.documento.data()["subcategoria"] ==
                      'producto') {
                    // print(snapshot.data.fotos);
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade,
                            child: DentroProducto(snapshot: snapshot.data)));
                  } else {
                    Navigator.pushReplacement(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade,
                            child: DentroServicio(snapshot: snapshot.data)));
                    //  print(snapshot.data.fotos);
                  }
                },
                child: Padding(
                    padding: EdgeInsets.all(getProportionateScreenHeight(16)),
                    child: Container(
                      child: Material(
                          color: Colors.transparent,
                          child: Stack(
                            children: [
                              Container(
                                child: Container(
                                  height: getProportionateScreenHeight(150),
                                  width: getProportionateScreenWidth(350),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width:2,
                                      color: (snapshot.data.documento.data()["destacado"] == true)?primaryColor:Colors.transparent
                                    ),
                                    color: lightColor,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 2,
                                    child: Stack(
                                      children: [
                                        Container(
                                          height:
                                              getProportionateScreenHeight(150),
                                          decoration: BoxDecoration(
                                              image: DecorationImage(
                                                image: NetworkImage(
                                                    snapshot.data.fotos[0]),
                                                fit: BoxFit.fill,
                                              ),
                                              borderRadius:
                                                  BorderRadius.circular(20.0)),
                                        ),
                                        Positioned(
                                            top: getProportionateScreenHeight(
                                                95),
                                            left: getProportionateScreenHeight(
                                                -3),
                                            child: Esquina(
                                              idServicio:
                                                  snapshot.data.documento.id,
                                              valor: favorito,
                                            ))
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                      flex: 3,
                                      child: Stack(
                                        children: [
                                          Container(
                                            height:
                                                getProportionateScreenHeight(
                                                    150),
                                          ),
                                          Positioned(
                                              top: 10,
                                              left: 15,
                                              child: Column(
                                                children: [
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    width: 150,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: widget.nombre !=
                                                              null
                                                          ? Text(
                                                              "${snapshot.data.documento.data()["subcategoria"].toString().toUpperCase()} / " +
                                                                  widget.nombre,
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        10),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              ))
                                                          : Text(
                                                              "${snapshot.data.documento.data()["subcategoria"].toString().toUpperCase()} /" +
                                                                  '${snapshot.data.documento.data()["categoria"].toString().toUpperCase()}',
                                                              style: TextStyle(
                                                                color:
                                                                    textColorClaro,
                                                                fontSize:
                                                                    getProportionateScreenWidth(
                                                                        10),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                              )),
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    width: 150,
                                                    child: Row(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: 30,
                                                          //width: 150,
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                "${snapshot.data.doc.data()["nombre"]} / ${snapshot.data.doc.data()["estrellas"]}",
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                )),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 5,
                                                        ),
                                                        Container(
                                                          alignment: Alignment
                                                              .topCenter,
                                                          child: Icon(
                                                            Icons.star,
                                                            size: 14,
                                                            color: primaryColor,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    height:
                                                        getProportionateScreenHeight(
                                                            25),
                                                    width: 150,
                                                    child: Material(
                                                      type: MaterialType
                                                          .transparency,
                                                      child: Text(
                                                          snapshot.data.documento
                                                                  .data()[
                                                              "descripcion"],
                                                          style: TextStyle(
                                                            color:
                                                                textColorClaro,
                                                            fontSize:
                                                                getProportionateScreenWidth(
                                                                    11),
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                          )),
                                                    ),
                                                  ),
                                                  Container(
                                                    width: 150,
                                                    child: Material(
                                                        type: MaterialType
                                                            .transparency,
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: [
                                                            Text(
                                                                '${snapshot.data.documento.data()["precio"]}'
                                                                r'$',
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          18),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .center,
                                                              child: Row(
                                                                children: [
                                                                  IconButton(
                                                                    icon:
                                                                        FaIcon(
                                                                      FontAwesomeIcons
                                                                          .share,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 20,
                                                                    ),
                                                                    onPressed:
                                                                        () {
                                                                      SocialShare.shareOptions('Te comparto este ' +
                                                                              snapshot.data.documento.data()['subcategoria'] +
                                                                              ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                              snapshot.data.documento.data()['categoria'] +
                                                                              ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año https://2look.app/')
                                                                          .then((data) {});
                                                                    },
                                                                    // onPressed:
                                                                    //     () {
                                                                    //   final RenderBox
                                                                    //       box =
                                                                    //       context.findRenderObject();
                                                                    //   Share.share(
                                                                    //       'https://2look.app/ Te comparto este ' +
                                                                    //           snapshot.data.documento.data()['subcategoria'] +
                                                                    //           ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                    //           snapshot.data.documento.data()['categoria'] +
                                                                    //           ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año',
                                                                    //       subject: 'TTULO',
                                                                    //       sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
                                                                    // }
                                                                  ),
                                                                  // IconButton(
                                                                  //   icon: FaIcon(
                                                                  //       FontAwesomeIcons
                                                                  //           .whatsapp,
                                                                  //       color: Colors
                                                                  //           .white),
                                                                  //   onPressed:
                                                                  //       () async {},
                                                                  //   // onPressed:
                                                                  //   //     () {
                                                                  //   //   FlutterShareMe().shareToWhatsApp(
                                                                  //   //       msg: 'Te comparto este ' +
                                                                  //   //           snapshot.data.documento.data()['subcategoria'] +
                                                                  //   //           ' se encuentra en la Aplicacion 2Look en la categoria de ' +
                                                                  //   //           snapshot.data.documento.data()['categoria'] +
                                                                  //   //           ' Únete a la comunidad mas grande de belleza y mantente hermos@ los 365 dias del año');
                                                                  //   // },
                                                                  // ),
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        )),
                                                  ),
                                                ],
                                              ))
                                        ],
                                      )),
                                ],
                              ),
                            ],
                          )),
                    )),
              );
            }
          }
          return SizedBox();
        });
  }
}

class CardHorizontalMio extends StatefulWidget {
  Function retorno;
  CardHorizontalMio({Key key, this.retorno}) : super(key: key);

  @override
  _CardHorizontalMioState createState() => _CardHorizontalMioState();
}

class _CardHorizontalMioState extends State<CardHorizontalMio> {
  final prefs = PreferenciasUsuario();
  List<CardMio> listacard = [];
  Offset _tapPosition;
  List<ImagenServicio> listImagenes = [];
  List<Servicios> listaServicios = [];
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection("Servicios")
            .where("id_usuario", isEqualTo: prefs.id)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> servicio) {
          listaServicios = [];
          if (servicio.hasData) {
            for (var item in servicio.data.docs) {
              listaServicios.add(Servicios(
                  id: item.id,
                  cancelacion: item.data()['cancelacion'].toString(),
                  categoria: item.data()['categoria'].toString(),
                  ciudad: item.data()['ciudad'].toString(),
                  descripcion: item.data()['descripcion'].toString(),
                  destacado: item.data()['destacado'].toString(),
                  paquete: item.data()['paquete'].toString(),
                  estado: item.data()['estado'].toString(),
                  genero: item.data()['genero'].toString(),
                  id_usuario: item.data()['id_usuario'].toString(),
                  marca: item.data()['marca'].toString(),
                  precio: item.data()['precio'].toString(),
                  sectores: item.data()['sectores'].toString(),
                  subcategoria: item.data()['subcategoria'].toString(),
                  ubicacion: item.data()['ubicacion'].toString()));
            }
            return StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection('ImagenesServicios')
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> imagenesS) {
                  listImagenes = [];
                  if (imagenesS.hasData) {
                    for (var item in imagenesS.data.docs) {
                      listImagenes.add(ImagenServicio(
                        id: item.id,
                        fotourl: item.data()['fotourl'],
                        id_servicio: item.data()['id_servicio'],
                      ));
                    }

                    listacard = [];

                    for (var servicio in listaServicios) {
                      List imagenes = [];
                      var id = servicio.id;
                      var subcategoria = servicio.subcategoria;
                      var categoria = servicio.categoria;
                      var usuario = prefs.usuario['nombre'];
                      var calificacion = prefs.usuario['estrellas'].toString();
                      var descripcion = servicio.descripcion;
                      var precio = servicio.precio;
                      var paquete = servicio.paquete;
                      for (var imagen in listImagenes) {
                        if (imagen.id_servicio == servicio.id) {
                          print('si');
                          imagenes.add(imagen.fotourl);
                        }
                      }
                      listacard.add(CardMio(
                        id: id,
                        subcategoria: subcategoria,
                        categoria: categoria,
                        usuario: usuario,
                        calificacion: calificacion,
                        descripcion: descripcion,
                        precio: precio,
                        paquete: paquete,
                        imagenes: imagenes,
                      ));
                    }
                    if (listacard.length > 0) {
                      return Column(
                          children: List.generate(
                        listacard.length,
                        (index) => GestureDetector(
                          onTap: () {
                            _showPopupMenu(listacard[index].id,
                                listacard[index].subcategoria);
                          },
                          onTapDown: _handleTapDown,
                          child: Padding(
                              padding: EdgeInsets.only(
                                  top: getProportionateScreenHeight(16)),
                              child: Container(
                                child: Material(
                                    color: Colors.transparent,
                                    child: Stack(
                                      children: [
                                        Container(
                                          child: Container(
                                            height:
                                                getProportionateScreenHeight(
                                                    150),
                                            width: getProportionateScreenWidth(
                                                350),
                                            decoration: BoxDecoration(
                                              color: lightColor,
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                          ),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 2,
                                              child: Stack(
                                                children: [
                                                  Container(
                                                    height:
                                                        getProportionateScreenHeight(
                                                            150),
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                          image: NetworkImage(
                                                              listacard[index]
                                                                  .imagenes[0]
                                                                  .toString()),
                                                          // image: NetworkImage(
                                                          //   "https://firebasestorage.googleapis.com/v0/b/twolook-42739.appspot.com/o/Productos%2F2020-12-19%2002%3A20%3A17.651625.jpg?alt=media&token=9f8ee0f4-aab9-4e0e-8803-811435a0e9e8"),
                                                          fit: BoxFit.fill,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(
                                                                    20.0)),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                                flex: 3,
                                                child: Stack(
                                                  children: [
                                                    Container(
                                                      height:
                                                          getProportionateScreenHeight(
                                                              150),
                                                    ),
                                                    Positioned(
                                                        top: 10,
                                                        left: 15,
                                                        child: Column(
                                                          children: [
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              width: 150,
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    listacard[index]
                                                                            .subcategoria
                                                                            .toString() +
                                                                        " / " +
                                                                        listacard[index]
                                                                            .categoria
                                                                            .toString(),
                                                                    // 'asdasd',
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              10),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            ),
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              width: 150,
                                                              child: Row(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    alignment:
                                                                        Alignment
                                                                            .topLeft,
                                                                    height: 30,
                                                                    //width: 150,
                                                                    child:
                                                                        Material(
                                                                      type: MaterialType
                                                                          .transparency,
                                                                      child: Text(
                                                                          listacard[index].usuario.toString() +
                                                                              " / " +
                                                                              listacard[index].calificacion.toString(),
                                                                          style: TextStyle(
                                                                            color:
                                                                                textColorClaro,
                                                                            fontSize:
                                                                                getProportionateScreenWidth(11),
                                                                            fontWeight:
                                                                                FontWeight.normal,
                                                                          )),
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    width: 5,
                                                                  ),
                                                                  Container(
                                                                    alignment:
                                                                        Alignment
                                                                            .topCenter,
                                                                    child: Icon(
                                                                      Icons
                                                                          .star,
                                                                      size: 14,
                                                                      color:
                                                                          primaryColor,
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              height: 40,
                                                              width: 150,
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    listacard[
                                                                            index]
                                                                        .descripcion,
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              11),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .normal,
                                                                    )),
                                                              ),
                                                            ),
                                                            Container(
                                                              alignment:
                                                                  Alignment
                                                                      .topLeft,
                                                              width: 150,
                                                              child: Material(
                                                                type: MaterialType
                                                                    .transparency,
                                                                child: Text(
                                                                    "\$" +
                                                                        listacard[index]
                                                                            .precio
                                                                            .toString(),
                                                                    style:
                                                                        TextStyle(
                                                                      color:
                                                                          textColorClaro,
                                                                      fontSize:
                                                                          getProportionateScreenWidth(
                                                                              18),
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    )),
                                                              ),
                                                            ),
                                                            (listacard[index]
                                                                        .paquete
                                                                        .toString() ==
                                                                    "paquete1")
                                                                ? Text(
                                                                    'Paquete \$ 5.00',
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right)
                                                                : (listacard[index]
                                                                            .paquete
                                                                            .toString() ==
                                                                        "paquete2")
                                                                    ? Text(
                                                                        'Paquete \$ 15.00',
                                                                        textAlign:
                                                                            TextAlign
                                                                                .right)
                                                                    : (listacard[index].paquete.toString() ==
                                                                            "paquete3")
                                                                        ? Text(
                                                                            'Paquete \$ 25.00',
                                                                            textAlign:
                                                                                TextAlign.right)
                                                                        : SizedBox()
                                                          ],
                                                        ))
                                                  ],
                                                )),
                                          ],
                                        ),
                                      ],
                                    )),
                              )),
                        ),
                      ));
                    } else {
                      return Text('No tienes productos por el momento');
                    }
                  } else {
                    return CircularProgressIndicator();
                  }
                });
          } else {
            return CircularProgressIndicator();
          }
        });
  }

  void _handleTapDown(TapDownDetails details) {
    final RenderBox referenceBox = context.findRenderObject();
    setState(() {
      _tapPosition = referenceBox.globalToLocal(details.globalPosition);
    });
  }

  void _showPopupMenu(idServicio, tipo) async {
    var variable;
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(
          _tapPosition.dx,
          _tapPosition.dy + getProportionateScreenHeight(330),
          _tapPosition.dx + 100,
          _tapPosition.dy + 100),
      color: textColorMedio,
      items: [
        PopupMenuItem(
          value: 1,
          child: GestureDetector(
            onTap: () {
              print('Eliminar');
              _alertaProducto(
                  context,
                  '¿Estás seguro que deseas eliminar este producto o servicio?',
                  idServicio);
            },
            child: Container(
              child: Text(
                "Eliminar",
                style: TextStyle(
                  color: textColorClaro,
                  fontSize: getProportionateScreenWidth(17),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
        PopupMenuItem(
          value: 2,
          child: GestureDetector(
            onTap: () async {
              print('Editar');
              Navigator.pop(context);
              if (tipo == 'producto') {
                variable = await Navigator.push(
                    context,
                    PageTransition(
                        child: SubirProducto(id: idServicio),
                        type: PageTransitionType.fade));
              } else {
                variable = await Navigator.push(
                    context,
                    PageTransition(
                        child: SubirServicio(id: idServicio),
                        type: PageTransitionType.fade));
              }
              setState(() {
                widget.retorno(variable);
              });
              // images.removeAt(index);
            },
            child: Container(
              child: Text(
                "Cambiar",
                style: TextStyle(
                  color: textColorClaro,
                  fontSize: getProportionateScreenWidth(17),
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      ],
      elevation: 5.0,
    );
  }

  void _alertaProducto(BuildContext context, String text, idServicio) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ButtonGray(
                                textoBoton: "Cancelar",
                                bFuncion: () {
                                  Navigator.of(context).pop();
                                },
                                anchoBoton: getProportionateScreenHeight(100),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                              ButtonGray(
                                textoBoton: "Entendido",
                                bFuncion: () {
                                  _eliminar(idServicio);
                                },
                                anchoBoton: getProportionateScreenHeight(100),
                                largoBoton: getProportionateScreenWidth(27),
                                colorTextoBoton: textColorClaro,
                                opacityBoton: 1,
                                sizeTextBoton: getProportionateScreenWidth(15),
                                weightBoton: FontWeight.bold,
                                color1Boton: primaryColor,
                                color2Boton: primaryColor,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _alerta(BuildContext context, String text) {
    showDialog(
        context: context,
        builder: (context) => BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 4, sigmaY: 4),
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                        getProportionateScreenHeight(20))),
                elevation: 0,
                backgroundColor: Colors.transparent,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: 40, bottom: 16, left: 16, right: 16),
                      margin: EdgeInsets.only(top: 16),
                      decoration: BoxDecoration(
                          color: textColorClaro,
                          borderRadius: BorderRadius.circular(20),
                          shape: BoxShape.rectangle,
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black26,
                                blurRadius: 10,
                                offset: Offset(0, 10))
                          ]),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            text,
                            style: TextStyle(
                              color: textColorOscuro,
                              fontSize: getProportionateScreenWidth(17),
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: ButtonGray(
                              textoBoton: "Entendido",
                              bFuncion: () {
                                Navigator.pop(context);
                              },
                              anchoBoton: getProportionateScreenHeight(100),
                              largoBoton: getProportionateScreenWidth(27),
                              colorTextoBoton: textColorClaro,
                              opacityBoton: 1,
                              sizeTextBoton: getProportionateScreenWidth(15),
                              weightBoton: FontWeight.bold,
                              color1Boton: primaryColor,
                              color2Boton: primaryColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ));
  }

  void _eliminar(idServicio) {
    Navigator.pop(context);
    new Future(() => loaderDialogNormal(context)).then((v) {
      FirebaseFirestore.instance
          .collection('Servicios')
          .doc(idServicio)
          .delete()
          .then((value) {
        var inicial = FirebaseFirestore.instance
            .collection('ImagenesServicios')
            .where('id_servicio', isEqualTo: idServicio)
            .snapshots();
        inicial.forEach((element) {
          element.docs.forEach((v) {
            setState(() {
              print(v.data()['fotourl']);
              FirebaseFirestore.instance
                  .collection('ImagenesServicios')
                  .doc(v.id)
                  .delete();
              // // //METODO PARA ELIMINAR LAS FOTOS
              FirebaseStorage.instance
                  .getReferenceFromUrl(v.data()['fotourl'])
                  .then((value) {
                print("value " + value.path);
                value.delete();
              });
            });
          });
        });
        FirebaseFirestore.instance
            .collection('Ubicaciones')
            .where('id_servicio', isEqualTo: idServicio)
            .get()
            .then((value) => FirebaseFirestore.instance
                .collection('Ubicaciones')
                .doc(value.docs[0].id)
                .delete());
        FirebaseFirestore.instance
            .collection('Solicitudes')
            .where('idservicio', isEqualTo: idServicio)
            .get()
            .then((value) => value.docs.forEach((element) {
                  FirebaseFirestore.instance
                      .collection('Solicitudes')
                      .doc(element.id)
                      .delete();
                }));
      }).then((value) {
        Navigator.pop(context);
        _alerta(context, 'Registro eliminado correctamente');
      });
    });
  }

  Future<void> loaderDialogNormal(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        child: WillPopScope(
          onWillPop: () async => false,
          child: Center(
            child: Loading(indicator: BallPulseIndicator(), size: 100.0),
          ),
        ));
  }
}

class GuardarCard extends StatefulWidget {
  final prefs = PreferenciasUsuario();
  final Function bFunction;
  final String nameImage;
  GuardarCard({Key key, this.bFunction, this.nameImage}) : super(key: key);

  @override
  _GuardarCardState createState() => _GuardarCardState();
}

class _GuardarCardState extends State<GuardarCard> {
  bool valor = false;

  Future<Informacion> futInfo;
  List listaFavoritos = [];
  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection('Favoritos')
        .where('idUsuario', isEqualTo: prefs.id)
        .get()
        .then((value) => value.docs.forEach((element) {
              setState(() {
                print(element.id);
                setState(() {
                  listaFavoritos.add(element);
                });
              });
              // element.data()[idUsuario]
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: StreamBuilder<QuerySnapshot>(
            stream:
                FirebaseFirestore.instance.collection("Servicios").snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.hasData) {
                return Column(children: getExpenseItems(snapshot));
              } else if (snapshot.hasError) {
                return Center(child: Text("Ha ocurrido un error"));
              } else {
                return Center(child: CircularProgressIndicator());
              }
            }));
  }

  getExpenseItems(AsyncSnapshot<QuerySnapshot> snapshot) {
    return snapshot.data.docs.map((doc) {
      print("aqui" + doc.id);
      //futInfo = await  obtener(doc.data()["id_usuario"], doc.id, doc);
      return doc.data()['estado'] == true ? _card(doc, false) : SizedBox();
    }).toList();
  }

  List listanuneva = [];
  List listanunevaubicacion = [];
  Future _listaimagenes(idservicio) async {
    List lista = [];
    await FirebaseFirestore.instance
        .collection("ImagenesServicios")
        .where('id_servicio', isEqualTo: idservicio)
        .get()
        .then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        var list = result.data().values.toList();

        print('lista  $list');
        list.remove(idservicio);
        String finalStr = list.reduce((value, element) {
          return value + element;
        });
        //  print('el valor lista $lista');
        //lista.add(result.data()['fotourl']);
        //lista = result.data().values.toList();
        lista.add(finalStr);
        //        print('el valor lista $lista');
      });
      /* var _results=querySnapshot.docs[0].data();
     lista = _results.values.toList();
    print('_list ${lista[1]}');
    print('query ${querySnapshot.docs[0].data()}');*/
    });
    return lista;
  }

  Future<Informacion> obtener(
      String id, String idservicio, QueryDocumentSnapshot documento) async {
    print(idservicio);
    var data1 =
        await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    //var dataestrellas=await FirebaseFirestore.instance.collection('Usuarios').doc(id).get();
    listanuneva = await _listaimagenes(idservicio);
    //print('AQUI LISTA NUEVA $listanuneva');

    var informacionusuario;
    informacionusuario =
        Informacion(doc: data1, fotos: listanuneva, documento: documento);

    return informacionusuario;
  }

  Widget _card(QueryDocumentSnapshot docs, favorito) {
    // retorno=docs.id;
    return FutureBuilder(
        future: obtener(docs.data()["id_usuario"], docs.id, docs),
        builder: (BuildContext context, AsyncSnapshot<Informacion> snapshot) {
          if (snapshot.hasData) {
            if (listaFavoritos.length > 0) {
              for (var item in listaFavoritos) {
                if (snapshot.data.documento.id == item.data()['idServicio']) {
                  favorito = true;
                  if (snapshot.data.fotos.length == 0) {
                    setState(() {});
                  } else {
                    return GestureDetector(
                      onTap: () {
                        if (snapshot.data.documento.data()["subcategoria"] ==
                            'producto') {
                          // print(snapshot.data.fotos);
                          Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  type: PageTransitionType.fade,
                                  child:
                                      DentroProducto(snapshot: snapshot.data)));
                        } else {
                          Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  type: PageTransitionType.fade,
                                  child:
                                      DentroServicio(snapshot: snapshot.data)));
                          //  print(snapshot.data.fotos);
                        }
                      },
                      child: Padding(
                          padding:
                              EdgeInsets.all(getProportionateScreenHeight(16)),
                          child: Container(
                            child: Material(
                                color: Colors.transparent,
                                child: Stack(
                                  children: [
                                    Container(
                                      child: Container(
                                        height:
                                            getProportionateScreenHeight(150),
                                        width: getProportionateScreenWidth(350),
                                        decoration: BoxDecoration(
                                          color: lightColor,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Stack(
                                            children: [
                                              Container(
                                                height:
                                                    getProportionateScreenHeight(
                                                        150),
                                                decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: NetworkImage(
                                                          snapshot
                                                              .data.fotos[0]),
                                                      fit: BoxFit.fill,
                                                    ),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20.0)),
                                              ),
                                              Positioned(
                                                  top:
                                                      getProportionateScreenHeight(
                                                          95),
                                                  left:
                                                      getProportionateScreenHeight(
                                                          -3),
                                                  child: Esquina(
                                                    idServicio: snapshot
                                                        .data.documento.id,
                                                    valor: favorito,
                                                  ))
                                            ],
                                          ),
                                        ),
                                        Expanded(
                                            flex: 3,
                                            child: Stack(
                                              children: [
                                                Container(
                                                  height:
                                                      getProportionateScreenHeight(
                                                          150),
                                                ),
                                                Positioned(
                                                    top: 10,
                                                    left: 15,
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          width: 150,
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                "${snapshot.data.documento.data()["subcategoria"].toString().toUpperCase()} / " +
                                                                    "${snapshot.data.documento.data()["categoria"].toString().toUpperCase()}",
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          10),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          width: 150,
                                                          child: Row(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Container(
                                                                alignment:
                                                                    Alignment
                                                                        .topLeft,
                                                                height: 30,
                                                                //width: 150,
                                                                child: Material(
                                                                  type: MaterialType
                                                                      .transparency,
                                                                  child: Text(
                                                                      "${snapshot.data.doc.data()["nombre"]} / ${snapshot.data.doc.data()["estrellas"]}",
                                                                      style:
                                                                          TextStyle(
                                                                        color:
                                                                            textColorClaro,
                                                                        fontSize:
                                                                            getProportionateScreenWidth(11),
                                                                        fontWeight:
                                                                            FontWeight.normal,
                                                                      )),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 5,
                                                              ),
                                                              Container(
                                                                alignment:
                                                                    Alignment
                                                                        .topCenter,
                                                                child: Icon(
                                                                  Icons.star,
                                                                  size: 14,
                                                                  color:
                                                                      primaryColor,
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          height: 40,
                                                          width: 150,
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                snapshot.data
                                                                        .documento
                                                                        .data()[
                                                                    "descripcion"],
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          11),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                )),
                                                          ),
                                                        ),
                                                        Container(
                                                          alignment:
                                                              Alignment.topLeft,
                                                          width: 150,
                                                          child: Material(
                                                            type: MaterialType
                                                                .transparency,
                                                            child: Text(
                                                                '${snapshot.data.documento.data()["precio"]}'
                                                                r'$',
                                                                style:
                                                                    TextStyle(
                                                                  color:
                                                                      textColorClaro,
                                                                  fontSize:
                                                                      getProportionateScreenWidth(
                                                                          18),
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                )),
                                                          ),
                                                        ),
                                                      ],
                                                    ))
                                              ],
                                            )),
                                      ],
                                    ),
                                  ],
                                )),
                          )),
                    );
                  }
                  break;
                } else {
                  favorito = false;
                }
              }
            }
          }
          return SizedBox();
        });
  }
}

class Informacion {
  List fotos;
  DocumentSnapshot doc;
  QueryDocumentSnapshot documento;
  Informacion({this.fotos, this.doc, this.documento});
}

class CardMio {
  String id;
  String subcategoria;
  String categoria;
  String usuario;
  String calificacion;
  String descripcion;
  String paquete;
  String precio;
  List imagenes;
  CardMio(
      {this.id,
      this.subcategoria,
      this.categoria,
      this.usuario,
      this.paquete,
      this.calificacion,
      this.descripcion,
      this.precio,
      this.imagenes});
}

class InformacionF {
  List fotos;
  DocumentSnapshot doc;
  ServiciosF documento;
  InformacionF({
    this.fotos,
    this.doc,
    this.documento,
  });
}

class Servicios {
  String id;
  String cancelacion;
  String categoria;
  String ciudad;
  String descripcion;
  String destacado;
  String paquete;
  String estado;
  String genero;
  String id_usuario;
  String marca;
  String precio;
  String sectores;
  String subcategoria;
  String ubicacion;
  Servicios({
    this.id,
    this.cancelacion,
    this.categoria,
    this.ciudad,
    this.descripcion,
    this.destacado,
    this.estado,
    this.genero,
    this.paquete,
    this.id_usuario,
    this.marca,
    this.precio,
    this.sectores,
    this.subcategoria,
    this.ubicacion,
  });
}

class ImagenServicio {
  String id;
  String fotourl;
  String id_servicio;
  ImagenServicio({
    this.id,
    this.fotourl,
    this.id_servicio,
  });
}
