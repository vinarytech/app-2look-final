import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FlechaAbajo extends StatefulWidget {
  FlechaAbajo({Key key}) : super(key: key);

  @override
  _FlechaAbajoState createState() => _FlechaAbajoState();
}

class _FlechaAbajoState extends State<FlechaAbajo> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 63.0,
      height: 17.0,
      child: Stack(
        children: <Widget>[
          SvgPicture.string(
            '<svg viewBox="176.0 641.0 62.6 17.0" ><path transform="translate(0.6, -3.57)" d="M 206.7059936523438 655.3049926757813 L 179.5229949951172 644.780029296875 C 177.5359954833984 644.010986328125 175.39599609375 645.4769897460938 175.39599609375 647.6079711914063 L 175.39599609375 647.6079711914063 C 175.39599609375 648.8599853515625 176.1660003662109 649.9829711914063 177.3339996337891 650.4349975585938 L 205.6109924316406 661.3839721679688 C 206.3159942626953 661.656005859375 207.0959930419922 661.656005859375 207.8009948730469 661.3839721679688 L 236.0780029296875 650.4349975585938 C 237.2460021972656 649.9829711914063 238.0160064697266 648.8599853515625 238.0160064697266 647.6079711914063 L 238.0160064697266 647.6079711914063 C 238.0160064697266 645.4769897460938 235.8760070800781 644.010986328125 233.8890075683594 644.780029296875 L 206.7059936523438 655.3049926757813 Z" fill="#ffffff" fill-opacity="0.1" stroke="none" stroke-width="1" stroke-opacity="0.1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>',
            allowDrawingOutsideViewBox: true,
          ),
        ],
      ),
    );
  }
}
